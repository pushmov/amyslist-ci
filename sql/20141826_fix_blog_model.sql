ALTER TABLE `blog` CHANGE `ref_id` `post_num_id` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;
ALTER TABLE `blog` CHANGE `location` `location_id` INT( 20 ) NOT NULL ;
ALTER TABLE `blog` ADD `category_id` INT( 20 ) NOT NULL AFTER `location_id` ;
ALTER TABLE `blog` ADD `address` TINYTEXT NULL DEFAULT NULL AFTER `description` ;
ALTER TABLE `blog` ADD `email` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `user_id` ;
ALTER TABLE `blog` ADD `url_name` VARCHAR( 255 ) NULL DEFAULT NULL ;

ALTER TABLE `blog`
  DROP `latitude`,
  DROP `longitude`;
	
CREATE TABLE IF NOT EXISTS `blog_images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_blog_id` int(20) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `image_description` text,
  PRIMARY KEY (`id`)
);

ALTER TABLE `blog` CHANGE `blog_id` `id` BIGINT( 11 ) NOT NULL AUTO_INCREMENT ;
ALTER TABLE `blog` ADD `status` TINYINT( 1 ) NOT NULL DEFAULT '0';