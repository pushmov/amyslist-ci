DROP TABLE user_settings;

CREATE TABLE IF NOT EXISTS `user_social` (
  `user_id` int(20) NOT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `pinterest` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `gplus` varchar(255) DEFAULT NULL,
  `tumblr` varchar(255) DEFAULT NULL,
  `vimeo` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `skype` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
);
ALTER TABLE `user` ADD `notify_ads_posted` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `notify_like` ;