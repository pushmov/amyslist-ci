					$("select").selectpicker({style: 'btn', menuStyle: 'dropdown-inverse'});
					
					if(window.innerWidth < 600)
					{
						$('h3').click(function(){
							
							$('.ct-list-toggle').hide('fast');
							$('i.fa-minus').addClass('fa-plus').removeClass('fa-minus');
							
							var currList = $(this).next();
							
							if(currList.is(':visible')){
								currList.hide('fast');
							}
							else
							{
								$(this).find('i.fa').removeClass('fa-plus').addClass('fa-minus');
								currList.show('fast');
							}
						});
					}