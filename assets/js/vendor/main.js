$(function () {
	
	/* search page */
	$('#list-view').click(function(e){
		e.preventDefault();
		$('.item').removeClass("pic-view grid-view map-view").addClass("list-view");
		$('#list-view').addClass("active");
		$('#pic-view, #grid-view').removeClass("active");
	});
	$('#pic-view').click(function(e){
		e.preventDefault();
		$('.item').removeClass("list-view grid-view map-view").addClass("pic-view");
		$('#pic-view').addClass("active");
		$('#list-view, #grid-view').removeClass("active");
	});
	$('#grid-view').click(function(e){
		e.preventDefault();
		$('.item').removeClass("pic-view list-view map-view").addClass("grid-view");
		$('#grid-view').addClass("active");
		$('#pic-view, #list-view').removeClass("active");
	});

	$('#map-view').click(function(e){
		e.preventDefault();
		$('.item').removeClass("pic-view list-view grid-view").addClass("map-view");
		$('#map-view').addClass("active");
	});

	/* ad-post slider */
	if( $("#ad-post-slider").length){
		$('#ad-post-slider').carouFredSel({
			circular: false,
			infinite: false,
			auto: false,
			prev: '#ad-post-slider-prev',
			next: '#ad-post-slider-next',
			pagination: "#ad-post-slider-pager"
		});
	}

	/* ads-posted checkbox */
	$(".select-all, .check-buttons .check").click(function () {

    	if( $(".check-buttons .check .checkbox").hasClass("checked") ) {
    		console.log("if");
    		$(".checkBoxClass").attr("checked", "");
    		$(".check-box-all").attr("checked", "");
    		$(".checkbox").removeClass("checked");
    	} else {
    		console.log("else");
    		$(".checkBoxClass").attr("checked", "checked");
    		$(".check-box-all").attr("checked", "checked");
    		$(".checkbox").addClass("checked");
    	}
	});

});