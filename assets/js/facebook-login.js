
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    if (response.status === 'connected') {
      testAPI();
    } else if (response.status === 'not_authorized') {
      	FB.login(function(response) {
				 if (response.authResponse) {
					 testAPI();
				 } 
			 }, {scope: 'email'});
			 
    } else {
      
				FB.login(function(response) {
				 if (response.authResponse) {
					 testAPI();
				 } 
			 }, {scope: 'email'});
    }
  }

  function checkLoginState() {
		
		$('.fb-loader').html('<img src="'+$('base').attr('id')+'assets/img/720.gif" >');
		
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  
	
	function processSuccess(data){
		console.log(data);
		window.location.href=data.redirect
	}

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
		var pic = "";
    FB.api('/me/picture', {
			"redirect": false,
      "height": "200",
      "type": "normal",
      "width": "200"
    },
		function(response) {
		
			var postdata;
			
			pic = response.data.url;
			
			FB.api('/me/', {fields : "first_name, email, last_name, gender"}, function(response) {
			
				console.log(response);
				postdata = {
					picture : pic, 
					email : response.email,
					firstname : response.first_name, 
					lastname : response.last_name,
					sex : response.gender
				}
				
				$('.fb-loader').html('<img src="'+$('base').attr('alt') +'assets/img/sign/fb-connect.png">');
				
				$.ajax({
					type: "POST",
					url: $('base').attr('alt') + "register/facebook/",
					data: postdata,
					success: processSuccess,
					dataType: 'json'
				});
				
			});
    });
		
  }
