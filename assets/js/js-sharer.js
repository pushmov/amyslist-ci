Share = {
facebook: function(purl, ptitle, pimg, text) {
url = 'http://www.facebook.com/sharer.php?s=100';
url += '&p[title]=' + encodeURIComponent(ptitle);
url += '&p[summary]=' + encodeURIComponent(text);
url += '&p[url]=' + encodeURIComponent(purl);
url += '&p[images][0]=' + encodeURIComponent(pimg);
Share.popup(url);
},
twitter: function(purl, ptitle) {
url = 'http://twitter.com/share?';
url += 'text=' + encodeURIComponent(ptitle);
url += '&url=' + encodeURIComponent(purl);
url += '&counturl=' + encodeURIComponent(purl);
Share.popup(url);
},
popup: function(url) {
var w = 626;
var h = 436;
var left = (screen.width/2)-(w/2);
var top = (screen.height/2)-(h/2);
window.open(url,'','toolbar=0,status=0,width='+w+', height='+h+', top='+top+', left='+left);
}
};
