<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
//for maintenance
//$route['default_controller'] = "home/maintenance";

$route['404_override'] = '';

//STATIC
$route['about'] = 'about';
$route['posting-an-ad'] = 'about/post_an_ad';
$route['page-advertising'] = 'about/city_page_advertising';
$route['terms-of-use'] = 'about/terms_of_use';
$route['contact-us'] = 'about/contact_us';
$route['features'] = 'about/features';
$route['founders'] = 'about/founders';
$route['privacy'] = 'about/privacy';
$route['thank-you'] = 'thanks/index';

$route['auth/auth/login'] = 'auth/auth/login';
$route['auth/auth/logout'] = 'auth/auth/logout';

$route['register'] = 'auth/register/signup';
$route['register/submit'] = 'auth/register/submit';
$route['register/facebook'] = 'auth/register/facebook';
$route['register/twitter'] = 'auth/register/twitter';

$route['user/notifications'] = 'user/notifications';
$route['user/notifications/read'] = 'user/notifications/read';
$route['user/profile'] = 'user/profile/about';
$route['user/profile/wall'] = 'user/profile/wall';
$route['user/profile/chat'] = 'user/profile/chat';
$route['user/profile/ads'] = 'user/profile/ads';
$route['user/profile/video'] = 'user/profile/video';
$route['user/profile/report'] = 'user/profile/report';
$route['user/profile/setting'] = 'user/profile/setting';



$route['ads/posted/delete/(:num)'] = 'ads/posted/delete/$1';
$route['ads/posted/undo/(:num)'] = 'ads/posted/undo/$1';
$route['ads/posted/edit/(:num)'] = 'ads/posted/edit/$1';
$route['ads/posted/update/(:num)'] = 'ads/posted/update/$1';
$route['ads/posted/changepic/(:num)'] = 'ads/posted/changepic/$1';
$route['ads/posted/deleteAll'] = 'ads/posted/deleteAll';
$route['ads/posted'] = 'ads/posted';

$route['post/ads'] = 'post/ads';
$route['post/ads/submit'] = 'post/ads/submit';
$route['post/ads/blogsubmit'] = 'post/ads/blogsubmit';
$route['post/ads/review/(:any)'] = 'post/ads/review/$1';
$route['post/ads/publish/(:num)'] = 'post/ads/publish/$1';

$route['post/payment'] = 'post/payment';
$route['post/payment/index/(:any)'] = 'post/payment/index/$1';
$route['post/payment/submit/(:any)'] = 'post/payment/submit/$1';
$route['post/payment/process'] = 'post/payment/process';
$route['post/payment/confirm'] = 'post/payment/confirm';

$route['thanks'] = 'thanks';
$route['viewpost/(:any)/(:any)'] = 'post/ads/detail/$1/$2';
$route['viewblog/(:any)/(:any)'] = 'blog/item/detail/$1/$2';


$route['forgot-password'] = 'auth/auth/forgot_password';
$route['auth/auth/check_email/(:any)'] = 'auth/auth/check_email/$1';
$route['auth/auth/forgot'] = 'auth/auth/forgot';

$route['user/profile/xeditable'] = 'user/profile/xeditable';
$route['auth/register/email_check/(:any)'] = 'auth/register/email_check/$1';
$route['auth/register/username_check/(:any)'] = 'auth/register/username_check/$1';

$route['profile/(:any)/wall'] = 'user/profile/public_wall/$1';
$route['profile/(:any)/chat'] = 'user/profile/public_chat/$1';
$route['profile/(:any)/ads'] = 'user/profile/public_ads/$1';
$route['profile/(:any)/video'] = 'user/profile/public_video/$1';
$route['profile/(:any)/report'] = 'user/profile/public_report/$1';
$route['profile/(:any)'] = 'user/profile/view/$1';

$route['user/profile/change_pic'] = 'user/profile/change_pic';

$route['account/settings'] = 'user/settings';
$route['user/like'] = 'user/like';
$route['user/profile/delete_ad/(:num)'] = 'user/profile/delete_ad/$1';
$route['user/notifications/delete/(:num)'] = 'user/notifications/delete/$1';
$route['user/profile/like/(:num)'] = 'user/profile/like/$1';
$route['user/profile/unlike/(:num)'] = 'user/profile/unlike/$1';


$route['home/maintenance'] = 'home/maintenance';
$route['home'] = 'home/index';

$route['chat'] = 'chat/index';
$route['chat/cometchatcss'] = 'chat/cometchatcss';

$route['user/video'] = 'user/video';
$route['user/video/save'] = 'user/video/save';
$route['user/video/reset'] = 'user/video/reset';


$route['user/settings/biography'] = 'user/settings/biography';
$route['user/settings/social'] = 'user/settings/social';
$route['user/settings/checkpassword/(:any)'] = 'user/settings/checkpassword/$1';
$route['user/settings/updatepassword'] = 'user/settings/updatepassword';
$route['user/settings/notifications_adposted'] = 'user/settings/notifications_adposted';
$route['user/settings/notifications_likes'] = 'user/settings/notifications_likes';
$route['user/settings/notifications_inbox'] = 'user/settings/notifications_inbox';
$route['user/settings/notifications_chat'] = 'user/settings/notifications_chat';

$route['user/settings/drop_account'] = 'user/settings/drop_account';
$route['user/(:any)/likes'] = 'user/like/view/$1';

//blog routes
$route['blog/item/review/(:any)'] = 'blog/item/review/$1';
$route['blog/item/publish/(:num)'] = 'blog/item/publish/$1';

$route['blog/posted'] = 'blog/posted/index';
$route['blog/posted/edit/(:num)'] = 'blog/posted/edit/$1';

$route['search'] = 'home/search';
$route['signup'] = 'auth/register/signup';
$route['login'] = 'auth/register/login';

$route['(:any)'] = "router/slug/$1";
/* End of file routes.php */
/* Location: ./application/config/routes.php */

