<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Amy'slist.us - Maintenance</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Loading Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Loading Flat UI -->
        <link href="<?php echo base_url(); ?>assets/css/flat-ui.css" rel="stylesheet">
				<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
				
				<link href="<?php echo base_url(); ?>assets/css/main-responsive.css" rel="stylesheet">
				
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.ico">
				
    </head>
    
    <body style="background:#ECEEF0">
        <header class="clearfix navbar navbar-inner" role="navigation">
					
        </header>
				<section>
					<div class="container text-center">
						<img src="<?php echo base_url(); ?>assets/img/City-Page-Advertising-Final_02.png" >
						<h4>Under Maintenance</h4>
					</div>
				</section>
				
		</body>	
</html>
					
			