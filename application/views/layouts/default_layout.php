<!DOCTYPE html>
<html lang="en">
    <head>
        
        <title>Amy's List: provides local classifieds for jobs, housing, for sale, services, personals, gigs, side jobs, and events.<?php echo $title; ?></title>
				<meta charset="UTF-8">
        
				<?php echo $meta; ?>
				<?php echo view_port(); ?>
				
				<?php if($this->uri->segment(1) == 'viewpost') : ?>
				<meta property="og:url" content="<?php echo $open_graph['url']; ?>" />
				<meta property="og:type" content="<?php echo $open_graph['type']; ?>" />
				<meta property="og:title" content="<?php echo $title; ?>" />
				<meta property="og:image" content="<?php echo $open_graph['image']; ?>" />
				<meta property="og:description" content="<?php echo $open_graph['description']; ?>" />
				<meta property="og:updated_time" content="<?php echo $open_graph['updated_time']; ?>" />
				<?php endif; ?>
				
				<?php echo add_css(array('bootstrap.min', 'flat-ui', 'main-responsive', 'gridpak', 'bootstrap-switch.min')); ?>
				<link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

        <?php echo favicons(); ?>
				
				<?php echo shiv(); ?>
				<?php echo jquery('1.9.1'); ?>
				<?php echo add_js(array('vendor/plugins', 'vendor/main','bootstrap-3.min', 'bootstrap-select', 'bootstrap-switch.min', 'flatui-checkbox', 'flatui-radio', 'jquery.tagsinput', 
					'jquery.placeholder', 'jquery.stacktable', 'hoverIntent.js','modernizr.custom.js', 'jquery.dlmenu.js', 'flatui-fileinput.js', 'holder.js', 'jquery.form', 'jquery.noty.packaged.min', 'jquery.ddslick.min', 'js-sharer.js')); ?>
				
				<?php if(is_logged_in()) : ?>	
			<link type="text/css" href="/cometchat/cometchatcss.php" rel="stylesheet" charset="utf-8" />
            <script type="text/javascript" src="/cometchat/cometchatjs.php" charset="utf-8"></script>     
            <?php endif; ?>
				
				<script>
					window.fbAsyncInit = function() {
						FB.init({
							appId      : <?php echo (ENVIRONMENT == 'development') ? '313733295455219' : '354933521337236'; ?>,
							cookie     : true,
							xfbml      : true, 
							version    : 'v2.1'
						});

					};

					(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s); js.id = id;
						js.src = "//connect.facebook.net/en_US/sdk.js";
						fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
				</script>
				
    </head>
    
		<body>
				<base id="base" alt="<?php echo base_url(); ?>" />
        <header class="clearfix navbar navbar-inner" role="navigation">
					<?php echo $nav; ?>
        </header>
				
					<?php echo $content; ?>

        <footer>
            <?php echo $footer; ?>
        </footer>
				
				<script>
					$("select").selectpicker({style: 'btn', menuStyle: 'dropdown-inverse'});
					
				</script>
				
				<?php if(!is_logged_in()) : ?>
				<script src="<?php echo asset_url(); ?>js/facebook-login.js"></script>
				<?php endif; ?>
				
		</body>
</html>