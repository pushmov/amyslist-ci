<!DOCTYPE html>
<html lang="en">
    <head>
        
        <title>Amyslist - <?php echo $title; ?></title>
				<meta charset="UTF-8">
        
				<?php echo $meta; ?>
				<?php echo view_port(); ?>
				
				<?php echo add_css(array('bootstrap.min', 'flat-ui', 'main-responsive', 'gridpak', 'bootstrap-switch.min')); ?>
				<link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

        <?php echo favicons(); ?>
				
				<?php echo shiv(); ?>
				<?php echo jquery('1.9.1'); ?>
				<?php echo add_js(array('vendor/plugins', 'vendor/main','bootstrap-3.min', 'bootstrap-select', 'bootstrap-switch.min', 'flatui-checkbox', 'flatui-radio', 'jquery.tagsinput', 
					'jquery.placeholder', 'jquery.stacktable', 'hoverIntent.js','modernizr.custom.js', 'jquery.dlmenu.js', 'flatui-fileinput.js', 'holder.js', 'jquery.form', 'jquery.noty.packaged.min', 'jquery.ddslick.min', 'js-sharer.js')); ?>
				
				<?php if(is_logged_in()) : ?>	
			<link type="text/css" href="/cometchat/cometchatcss.php" rel="stylesheet" charset="utf-8" />
            <script type="text/javascript" src="/cometchat/cometchatjs.php" charset="utf-8"></script>     
            <?php endif; ?>
						
				<script src="http://www.google.com/jsapi" type="text/javascript"></script>
				<script>
					window.fbAsyncInit = function() {
						FB.init({
							appId      : <?php echo (ENVIRONMENT == 'development') ? '313733295455219' : '354933521337236'; ?>,
							cookie     : true,
							xfbml      : true, 
							version    : 'v2.1'
						});

					};

					(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s); js.id = id;
						js.src = "//connect.facebook.net/en_US/sdk.js";
						fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
					
				</script>
				
				<script type="text/javascript">
					google.load('search', '1', {language : 'en', style : google.loader.themes.V2_DEFAULT});
					google.setOnLoadCallback(function() {
						var customSearchOptions = {};  var customSearchControl = new google.search.CustomSearchControl(
							'009586108374223591604:zqslevpwsso', customSearchOptions);
						customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
						 customSearchControl.setLinkTarget(google.search.Search.LINK_TARGET_SELF);
						customSearchControl.draw('cse');

						<?php $qu = (isset($_GET["q"]))?$_GET["q"]:''; ?> // php $_GET to capture the search keyword
						customSearchControl.execute('<?php echo $qu; ?>');

						function parseParamsFromUrl() {
							var params = {};
							var parts = window.location.search.substr(1).split('\x26');
							for (var i = 0; i < parts.length; i++) {
								var keyValuePair = parts[i].split('=');
								var key = decodeURIComponent(keyValuePair[0]);
								params[key] = keyValuePair[1] ?
										decodeURIComponent(keyValuePair[1].replace(/\+/g, ' ')) :
										keyValuePair[1];
							}
							return params;
						}

						var urlParams = parseParamsFromUrl();
						var queryParamName = "q";
						if (urlParams[queryParamName]) {
							customSearchControl.execute(urlParams[queryParamName]);
						}
					}, true);
				</script>
				<style type="text/css">
					.gsc-result-info {}
					.gsc-input-box{height:30px;}
					.gsc-input{margin:4px !important}
					.gsst_a{padding-top:5px !important;}
					.gs-snippet{padding-left:8px;padding-right:8px}
					.gs-result .gs-title, .gs-result .gs-title *{}
					.gs-result a.gs-visibleUrl, .gs-result .gs-visibleUrl {}
					.gs-result .gs-snippet {}
					.gsc-results .gsc-cursor-box .gsc-cursor-page {}
					.gsib_a{padding:0 !important}
					.gsc-input-box-hover, .gsc-input-box-focus{border:1px solid #d9d9d9;}
					.gsc-search-box .gsc-input > input:hover, .gsc-input-box-hover{box-shadow:none !important}
					.gsc-search-box .gsc-input > input:focus, .gsc-input-box-focus{border:1px solid #4d90fe !important;box-shadow:none !important}
					.cse .gsc-search-button input.gsc-search-button-v2, input.gsc-search-button-v2,
					.cse .gsc-search-button input.gsc-search-button-v2:hover, input.gsc-search-button-v2:hover{
						height:29px !important;
						background-image:url(http://www.google.com/uds/css/v2/search_box_icon.png);
						background-position:center;
						background-repeat:no-repeat;
						
					}
					.gs-image-box.gs-web-image-box.gs-web-image-box-portrait{padding-left:8px;padding-right:8px;}
					
				</style>
    </head>
    
		<body>
				<base id="base" alt="<?php echo base_url(); ?>" />
        <header class="clearfix navbar navbar-inner" role="navigation">
					<?php echo $nav; ?>
        </header>
				
					<?php echo $content; ?>

        <footer>
            <?php echo $footer; ?>
        </footer>
				
				<script>
					$("select").selectpicker({style: 'btn', menuStyle: 'dropdown-inverse'});
					
				</script>
				
				<?php if(!is_logged_in()) : ?>
				<script src="<?php echo asset_url(); ?>js/facebook-login.js"></script>
				<?php endif; ?>
				
		</body>
</html>
