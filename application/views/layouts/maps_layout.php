<!DOCTYPE html>
<html lang="en">
    <head>
        
        <title>
            Amy's List: <?php echo $city_name; ?> jobs, apartments, personals, for sale, services, neighborhood, gigs, events and more.
        </title>
                    <meta charset="UTF-8">
        
                <?php echo $meta; ?>
                <?php echo view_port(); ?>
                
                <?php echo add_css(array('bootstrap.min', 'flat-ui', 'main-responsive', 'gridpak', 'bootstrap-switch.min')); ?>
                <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

        <?php echo favicons(); ?>
                
                <?php echo shiv(); ?>
                <?php echo jquery('1.9.1'); ?>
                <?php echo add_js(array('vendor/plugins', 'vendor/main','bootstrap-3.min', 'bootstrap-select', 'bootstrap-switch.min', 'flatui-checkbox', 'flatui-radio', 'jquery.tagsinput', 
                    'jquery.placeholder', 'jquery.stacktable', 'hoverIntent.js','modernizr.custom.js', 'jquery.dlmenu.js', 'flatui-fileinput.js', 'holder.js', 'jquery.form', 'jquery.noty.packaged.min', 'jquery.ddslick.min')); ?>
                
                <script>
                    window.fbAsyncInit = function() {
                        FB.init({
                            appId      : <?php echo (ENVIRONMENT == 'development') ? '313733295455219' : '354933521337236'; ?>,
                            cookie     : true,
                            xfbml      : true, 
                            version    : 'v2.1'
                        });

                    };

                    (function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = "//connect.facebook.net/en_US/sdk.js";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                </script>
                
                
                
                    <link href="<?php echo asset_url(); ?>css/owl.carousel.css" rel="stylesheet">
                    <link href="<?php echo asset_url(); ?>css/owl.transitions.css" rel="stylesheet">
                    <link href="<?php echo asset_url(); ?>css/owl.theme.css" rel="stylesheet">
                    
                    <script src="<?php echo asset_url(); ?>js/owl.carousel.js"></script>
                    <script>
                        $(document).ready(function() {
                            $("#owl-demo").owlCarousel({
                                navigation : true,
                                slideSpeed : 300,
                                paginationSpeed : 400,
                                singleItem : true
                            });
                        });
                    </script>
                    
                    <script type="text/javascript">
                        var a2a_config = a2a_config || {};
                        a2a_config.color_main = "D7E5ED";
                        a2a_config.color_border = "AECADB";
                        a2a_config.color_link_text = "333333";
                        a2a_config.color_link_text_hover = "333333";
                    </script>
                    <script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
                
                
                
                <script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyBM08iuN7kJgE2ALNRDypDJrq0ur52lByM"></script>
                <script src="<?php echo asset_url(); ?>markerclustererplus-master/src/markerclusterer.js"></script> 
                <script type="text/javascript">
                var locations = <?php echo json_encode($positions); ?>;
                var map,infowindow_c,infowindow_override;
                var all_markers_latlng=[];
                var duplicate_markers=[];
                function initialize() 
                {
                    all_markers_latlng=[];
                    duplicate_markers=[];
                   var myLatlng = new google.maps.LatLng('<?php echo $center['latitude']; ?>','<?php echo $center['longitude']?>');
                   var mapOptions = {
                   zoom: 10,
                   center: myLatlng,
                   panControl: false,
                   panControl: false,
                   //zoomControl: false,
                   scaleControl: true,
                   mapResized :true,
                   mapTypeId: google.maps.MapTypeId.ROADMAP
                   }
                    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);   
                   
                   //console.log(locations);
                   image=$('base').attr('alt') + 'assets/img/icons/custom-poi-plus.png';
                   
                   //console.log(locations.length);
                   var bounds = new google.maps.LatLngBounds();
                   var markers = [];
                   for (i = 0; i < locations.length; i++) 
                   {  
                       var myString = locations[i][4];    
                       myString=String(myString);
                       var arrayimg = myString.split(',');
                       
                       var marker = new google.maps.Marker({
                       position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                       map: map,
                       title: locations[i][5],
                       icon: image,
                       customInfo: i
                     });
                     
                     bounds.extend(new google.maps.LatLng(locations[i][1], locations[i][2]));
                     markers.push(marker);
                     
                     
                     var imagesvar= '';
                     for(var a in arrayimg){
                           imagesvar=  imagesvar + '<li><img src="'+$('base').attr('alt')+'assets/'+arrayimg[a]+'" width="300" height="260" /></li>';
                     }

                     var hover_infowindow = new google.maps.InfoWindow({
                        content: '<div id="popover_plus_content" style="width:200px;"><div class="popover-custom-content clearfix">' + 
                                    '<h5>'+locations[i][5]+'</h5>' +
                                    '</div></div>'
                     });
                     
                     var infowindow = new google.maps.InfoWindow({
                         content: '<div id="popover_plus_content" ><div class="popover-custom-content clearfix"><div>' + 
                                    '<h5>'+locations[i][5]+'</h5>' + 
                                    
                                     '<div class="maining"><div class="top-block" id="top-detail">'+
                                      '<ul>'+
                                           '<li><a href="#"><span><i class="fa fa-envelope"></i></span>'+locations[i][7]+'</a> </li>'+
                                           '<li><a href="'+$('base').attr('alt')+'profile/'+locations[i][8]+'"><span><i class="fa fa-user"></i></span>'+locations[i][8]+'</a> </li>'+
                                           '<li><a href="#"><span><i class="fa fa-inbox"></i></span>Inbox</a> </li>'+
                                           '<li><a href="#"><span><i class="fa fa-comment"></i></span>Chat</a> </li>'+
                                           '<li><a href="#"><span><i class="fa fa-youtube-play"></i></span>Video</a> </li>'+
                                     '</ul></div></div>'+
                                    
                                    
                                    
                                    '<div><div class="mask">'+
																		'<i onclick="prev_testimonial()" class="fa fa-chevron-left fa-5x"></i>'+
																		'<ul class="images" id="testimonial-list">' +imagesvar+'</ul>'+
																		'<i onclick="next_testimonial()" class="fa fa-chevron-right fa-5x"></i>'+
																		'</div></div>'+
																		'<div class="map-description">' +
																		locations[i][9] +
																		'</div>' +
                                    '<div class="navbutton" style="display:none">' + 
                                        '<img class="prev" id="prv-testimonial" onclick="prev_testimonial()" src="'+$('base').attr('alt')+'assets/img/icons/carousel-prev.png">' + 
                                        '<img class="next" id="nxt-testimonial" onclick="next_testimonial()" src="'+$('base').attr('alt')+'assets/img/icons/carousel-next.png">' + 
                                    '</div>' + 
                                    
                                    
                                    
                                    
                                    '<div class="footer-carousel clearfix">' + 
                                        '<div class="pull-left">' + 
                                            '<div class="foot-menu view-pic"><a href="javascript:void(0)" id="btn-show-info" onclick="display_description()">Post Info</a><a href="javascript:void(0)" id="btn-show-images" style="display:none" onclick="display_carousel()">View Image</a></div>' + 
                                        '</div>' + 
                                        '<div class="pull-right">' + 
                                            '<div class="foot-menu contact-info"><a target="_blank" href="'+$('base').attr('alt')+'viewpost/'+locations[i][6]+'/'+locations[i][5]+'">View Post</a></div>' + 
                                        '</div>' + 
                                    '</div>' + 
                                    
                                '</div>' + 
                        '</div>' + 
                    '</div>',
                    }); 
                     makeInfoWindowEvent(map, infowindow, marker);
                     makeInfoWindowHover(map, hover_infowindow, marker);
                     all_markers_latlng.push([locations[i][1], locations[i][2]]);
                  }
                  
                  //console.log(all_markers_latlng);
                  //console.log(find_duplicates(all_markers_latlng));
                  //Check Markers array for duplicate position and offset a little
                    if(markers.length != 0) {
                       //duplicate_markers=[];
                       var duplicate_latlng = find_duplicates(all_markers_latlng);
                        for (i=0; i < markers.length; i++) {
                            var existingMarker = markers[i];
                            var pos = existingMarker.getPosition().lat()+','+existingMarker.getPosition().lng();
                            for(var j=0;j<duplicate_latlng.length;j++){
                                if (duplicate_latlng[j] == pos) {
                                    //console.log(markers[i]);
                                    duplicate_markers.push(markers[i]);    
                                }    
                            }
                        }
                    }
                    //console.log('duplicate_markers');
                 //console.log(duplicate_markers);  
                  
                  map.fitBounds(bounds);
                  //console.log(markers);
                  
                    var markerCluster = new MarkerClusterer(map, duplicate_markers);
                  // onClick OVERRIDE
                    markerCluster.onClick = function(clickedClusterIcon) { 
                      return multiChoice(clickedClusterIcon.cluster_,map); 
                    }
                  
                  
                }

                
                
                function makeInfoWindowEvent(map, infowindow, marker) 
                {
                    google.maps.event.addListener(marker, 'click', function() {  
                        google.maps.event.trigger(map, 'resize');  
                        infowindow.open(map, marker);
                        //console.log(marker);     
                          
                    });
                }
                
                function makeInfoWindowHover(map, infowindow, marker) 
                {
                    google.maps.event.addListener(marker, 'mouseover', function() {  
                        google.maps.event.trigger(map, 'resize');  
                        infowindow.open(map, marker);    
                          
                    });
                    
                    // assuming you also want to hide the infowindow when user mouses-out
                    google.maps.event.addListener(marker, 'mouseout', function() {
                        infowindow.close();
                    });
                }
                
                function multiChoice(clickedCluster,map) {
                    try{
                        infowindow_c.close();
                    }
                    catch(e){
                        console.log(e);
                    }
                    try{
                        infowindow_override.close();
                    }
                    catch(e){
                        console.log(e);
                    }
                    console.log('inside function');
                  if (clickedCluster.getMarkers().length > 1){
                      var markers_c = clickedCluster.getMarkers();
                      //console.log(clickedCluster.getMarkers());
                      var content_c = '<div id="popover_plus_content" style="overflow:auto!important;width:200px;"><div style="overflow:auto!important;max-height:200px;">';
                                        for (var i=0;i<markers_c.length;i++){
                                            content_c+='<a onclick="clusterMarkerInfoWindow('+markers_c[i].customInfo+')"><h5>'+markers_c[i].title+'</h5></a></br>';
                                                                                        
                                        }
                      content_c+= '</div></div>';
                      infowindow_c = new google.maps.InfoWindow({
                          content: content_c,
                          position: markers_c[0].position
                      });
                      //console.log(markers_c[0].position);
                      infowindow_c.open(map);                      
                    return false;
                  }
                  return true;
                };
                
                function clusterMarkerInfoWindow(i){
                    console.log(new google.maps.LatLng(locations[i][1], locations[i][2]));
                    var myString = locations[i][4];    
                       myString=String(myString);
                       var arrayimg = myString.split(',');
                    var imagesvar= '';
                     for(var a in arrayimg){
                           imagesvar=  imagesvar + '<li><img src="'+$('base').attr('alt')+'assets/'+arrayimg[a]+'" width="300" height="260" /></li>';
                     }
                     
                     infowindow_override = new google.maps.InfoWindow({
                         content: '<div id="popover_plus_content" ><div class="popover-custom-content clearfix"><div><div style="padding: 6px 0px 2px;"><button onclick="showInfowindowC()" style="float:left;margin:3px 0 0">back</button>' + 
                                    '<h5 style="display:inline;">'+locations[i][5]+'</h5></div>' + 
                                    
                                     '<div class="maining"><div class="top-block" id="top-detail">'+
                                      '<ul>'+
                                           '<li><a href="#"><span><i class="fa fa-envelope"></i></span>'+locations[i][7]+'</a> </li>'+
                                           '<li><a href="'+$('base').attr('alt')+'profile/'+locations[i][8]+'"><span><i class="fa fa-user"></i></span>'+locations[i][8]+'</a> </li>'+
                                           '<li><a href="#"><span><i class="fa fa-inbox"></i></span>Inbox</a> </li>'+
                                           '<li><a href="#"><span><i class="fa fa-comment"></i></span>Chat</a> </li>'+
                                           '<li><a href="#"><span><i class="fa fa-youtube-play"></i></span>Video</a> </li>'+
                                     '</ul></div></div>'+
                                    
                                    
                                    
                                    '<div><div class="mask">'+
																		'<i onclick="prev_testimonial()" class="fa fa-chevron-left fa-5x"></i>'+
																		'<ul class="images" id="testimonial-list">' +imagesvar+'</ul>'+
																		'<i onclick="next_testimonial()" class="fa fa-chevron-right fa-5x"></i>'+
																		'</div></div>'+
																		'<div class="map-description">' +
																		locations[i][9] +
																		'</div>' +
                                    '<div class="navbutton" style="display:none">' + 
                                        '<img class="prev" id="prv-testimonial" onclick="prev_testimonial()" src="'+$('base').attr('alt')+'assets/img/icons/carousel-prev.png">' + 
                                        '<img class="next" id="nxt-testimonial" onclick="next_testimonial()" src="'+$('base').attr('alt')+'assets/img/icons/carousel-next.png">' + 
                                    '</div>' + 
                                    
                                    
                                    
                                    
                                    '<div class="footer-carousel clearfix">' + 
                                        '<div class="pull-left">' + 
                                            '<div class="foot-menu view-pic"><a href="javascript:void(0)" id="btn-show-info" onclick="display_description()">Post Info</a><a href="javascript:void(0)" id="btn-show-images" style="display:none" onclick="display_carousel()">View Image</a></div>' + 
                                        '</div>' + 
                                        '<div class="pull-right">' + 
                                            '<div class="foot-menu contact-info"><a target="_blank" href="'+$('base').attr('alt')+'viewpost/'+locations[i][6]+'/'+locations[i][5]+'">View Post</a></div>' + 
                                        '</div>' + 
                                    '</div>' + 
                                    
                                '</div>' + 
                        '</div>' + 
                    '</div>',
                    position: new google.maps.LatLng(locations[i][1], locations[i][2])
                    });
                    infowindow_c.close();
                    infowindow_override.open(map);    
                  
                  
                  /*setTimeout(function(){
                    infowindow_override.setPosition(new google.maps.LatLng(locations[i][1], locations[i][2]));
                    infowindow_override.open(map);
                    console.log('trigger');  
                  },2000);*/
                                    
                }
                
                function showInfowindowC(){
                    infowindow_override.close();
                    infowindow_c.open(map);    
                }
                
                function find_duplicates(arr) {
                  var len=arr.length,
                      out=[],
                      counts={};
                
                  for (var i=0;i<len;i++) {
                    var item = arr[i];
                    counts[item] = counts[item] >= 1 ? counts[item] + 1 : 1;
                  }
                
                  for (var item in counts) {
                    if(counts[item] > 1)
                      out.push(item);
                  }
                
                  return out;
                }  
                
              
                //google.maps.event.addDomListener(window, 'load', initialize);   
                
                </script>


                <!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>-->
                <!--  <script type="text/javascript">
                      $(document).ready(function(){
                            var t = setInterval(function(){
                                $("#carousel ul").animate({marginLeft:-480},1000,function(){
                                    $(this).find("li:last").after($(this).find("li:first"));
                                    $(this).css({marginLeft:0});
                                })
                            },3000);
                    });
                  </script>    -->
                <script type="">
                  function prev_testimonial()
                  {
                    var $last = $('#testimonial-list li:last');
                    $last.remove().css({ 'margin-left': '-400px' });
                    $('#testimonial-list li:first').before($last);
                    $last.animate({ 'margin-left': '0px' }, 1000); 
                  }

                  function next_testimonial()
                  {
                    var $first = $('#testimonial-list li:first');
                    $first.animate({ 'margin-left': '-400px' }, 1000, function() {
                        $first.remove().css({ 'margin-left': '0px' });
                        $('#testimonial-list li:last').after($first);
                    });
                  }
									
									function display_description()
									{
										$('.map-description').show();
										$('.mask').hide();
										$('#btn-show-info').hide();
										$('#btn-show-images').show();
									}
									
									function display_carousel()
									{
										$('.map-description').hide();
										$('.mask').show();
										$('#btn-show-info').show();
										$('#btn-show-images').hide();
									}
                  </script>
                
                <script>
                  $(document).ready(function(){
                      $(document).on('click','.map_canvas', function(){
                        initialize();
                      });
                    }); 
                    
                  /* $(document).reday(function(){
                      $(document).on('click','.case', function(){
                            window.location('');
                      }); 
                   })*/ 
                    
                    
                </script> 
                
                
                
                
                
           
             
<style type="">
   

    #testimonial-list {
        overflow: hidden;
        padding: 10px;
        width: 1600px;
    }
    
    #testimonial-list>li {
        overflow: hidden;
        padding: 0;
        display: inline-block;
    }
    
     #content 
     {
        padding: 0 !important; 
        position : absolute !important; 
        top : 0 !important; 
        right : 0 !important; 
        bottom : 40px !important;  
        left : 0 !important;     
    }  
    

.top-block {float: left;margin: 0;padding: 0;width: 100%;}
#top-detail > ul {float: left;margin: 0 0 0 7px;padding: 0;width: 100%;}
#top-detail li {float: left;list-style: outside none none;}
#top-detail a {color: #2a4d8e;font-size: 8px;font-weight: bold;padding: 2px;}      
.map-description{display:none;min-height:260px;padding-top:15px;}
.mask{position:relative}
.mask i.fa{color:#c9ced2;z-index:100;cursor:pointer}
.mask i.fa-chevron-right{position:absolute;right:0;top:90px;bottom:0}
.mask i.fa-chevron-left{position:absolute;left:0;top:90px;bottom:0}



</style>   
           
                 
    </head>
    
        <body>
               
                <base id="base" alt="<?php echo base_url(); ?>" />
        <header class="clearfix navbar navbar-inner" role="navigation">
                    <?php echo $nav; ?>
        </header>
                
                    <?php echo $content; ?>

        <footer>
            <?php echo $footer; ?>
        </footer>
                
                <script>
                    $("select").selectpicker({style: 'btn', menuStyle: 'dropdown-inverse'});
                    
                </script>
                
                <?php if(!is_logged_in()) : ?>
                <script src="<?php echo asset_url(); ?>js/facebook-login.js"></script>
                <?php endif; ?>
                
        </body>
</html>