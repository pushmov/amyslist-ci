<!DOCTYPE html>
<html>
	<head>
		<title>Amy's List: jobs, apartments, personals, for sale, services, neighborhood, gigs, events, and more.</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<?php echo view_port(); ?>
		<?php echo $meta; ?>
				
		<?php echo add_css(array('bootstrap.min', 'flat-ui', 'font-awesome.min', 'info', 'gridpak')); ?>

    <?php echo favicons(); ?>

		<?php echo favicons(); ?>
		
		<link href='http://fonts.googleapis.com/css?family=Lato:400,600,700,300' rel='stylesheet' type='text/css'>

    <?php echo shiv(); ?>
		
		<?php echo jquery('1.9.1'); ?>
		<?php echo add_js(array('bootstrap-3.min')); ?>
	</head>
	<body>
		<div id="main">
		
			<header class="navbar" role="navigation">
				<?php echo $nav; ?>
			</header>
			
			<?php echo $content; ?>
			
			<footer>
				<?php echo $footer; ?>
      </footer>
		</div>
    
	</body>
</html>