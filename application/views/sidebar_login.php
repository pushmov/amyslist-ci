							<div class="col-lg-2">
								<div class="logged-out clearfix">
									<div class="admin-pn">
										<div class="admin-logo">
											<a href="">
												<img src="<?php echo $this->config->item('assets_images'); ?>amyface.png" width="110" height="110">
											</a>
										</div>
									<h5>Welcome to Amy's List.</h5>
									</div>
									<div class="navbar-collapse left-menu collapse">
										<div class="for-login clearfix">
											<div class="row clearfix">
												<div class="login-panel">
													<a href="<?php echo site_url('signup'); ?>" class="btn btn-pink sign-up">Sign Up</a>
												</div>
												<div class="login-panel">
													<a class="btn sign-in" href="<?php echo site_url('login'); ?>">Login</a>
												</div>
											</div>
											
											<div class="social-connect clearfix">
												<div class="soc-log-fb">
													<a href="javascript:void(0)" onclick="checkLoginState();" >
														<i class="fa fa-facebook-square"></i>
														Connect with Facebook
													</a>
												</div>
												<div class="soc-log-tw">
													<a href="#" class="soc-log-tw">
														<i class="fa fa-twitter"></i>
														Connect with Twitter
													</a>
												</div>
											</div>
										</div>
									</div>
									<br>
									<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Footer Ad Amy's List -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3122936194507304"
     data-ad-slot="7189162304"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
								</div>
							</div>