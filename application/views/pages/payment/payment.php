				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-payment">
							
							<div class="payment-block head">
								<p>We charge to <a href="#">post the following ads</a>. Please review your total below. Thank you!</p>
							</div>
							<div class="payment-block wrapper-body">
								<h2>Payment Info</h2>
								
								<ul class="list-unstyled breadcumb">
									<li><?php echo $location; ?></li>
									<li>/</li>
									<li><?php echo $category['parent']; ?></li>
									<li>/</li>
									<li><?php echo $category['child']; ?></li>
								</ul>
								<h4>Total Cost : $<?php echo round($charge->amount, 2); ?></h4>
								
								<form action="<?php echo site_url('post/payment/submit/' . $post->post_num_id); ?>" method="POST" name="promocode">
									<fieldset>
										<div class="form-group clearfix">
											<label class="col-sm-1 control-label">
												<img src="<?php echo asset_url(); ?>img/icons/Gift-Box@2x.png" width="40" height="40">
											</label>
											<div class="col-sm-4 holder">
												<input type="text" name="charge[promocode]" placeholder="Enter Promo Code :)">
											</div>
											<div class="col-sm-7 holder">
											</div>
										</div>
										<p>By clicking continue you will be sent to a secure PayPal page to make your payment.</p>
										<div class="form-group clearfix button-group">
											<div class="col-sm-2">
												<button type="submit" class="btn btn-primary btn-continue">Continue</button>
											</div>
											<div class="col-sm-2">
												<button type="button" class="btn btn-primary btn-back" onclick="window.location='<?php echo site_url('post/ads/review/' . $post->post_num_id); ?>'">Go Back</button>
											</div>
											<div class="col-sm-8"></div>
										</div>
										
									</fieldset>
								</form>
								
							</div>
						</article>
					</div>
				</section>