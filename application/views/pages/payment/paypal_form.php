

				<form method="POST" action="https://payflowlink.paypal.com" name="paypal">
					<input type="hidden" name="LOGIN" value="<?php echo $login; ?>">
					<input type="hidden" name="PARTNER" value="<?php echo $partner; ?>">
					<input type="hidden" name="AMOUNT" value="<?php echo $price; ?>">
					<input type="hidden" name="TYPE" value="<?php echo $type; ?>">
					<input type="hidden" name="CUSTID" value="<?php echo $post_num_id; ?>">
					<input type="hidden" name="DESCRIPTION" value="<?php echo $description; ?>">
					<input type="hidden" name="METHOD" value="<?php echo $method; ?>">
						
					<noscript>
							<input type="submit" value="Click Here to Proceed">
					</noscript>
				</form>
				
				<script src="<?php echo asset_url(); ?>js/jquery-1.8.3.min.js"></script>
				<script>
					$(document).ready(function(){$('form[name="paypal"]').submit()});
				</script>