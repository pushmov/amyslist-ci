			<section class="ct-content clearfix ct-homepage">
					<div class="container">
						
						<?php load_sidebar() ?>
						
						<div class="col-lg-10 ct-homepage-content">
							
							
							<div class="row clearfix">
								
								<div class="sm-toggle-ct">
								
								<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Footer Ad Amy's List -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3122936194507304"
     data-ad-slot="7189162304"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<br>
								
									<div class="sm-toggle" data-target=".us-cities">
										<a class="sm-toggle-trigger" href="#">US Cities<i class="fa fa-chevron-right"></i></a>
									</div>
									<div class="sm-toggle" data-target=".usa">
										<a class="sm-toggle-trigger" href="#">United States<i class="fa fa-chevron-right"></i></a>
									</div>
									<div class="sm-toggle" data-target=".cities">
										<a class="sm-toggle-trigger" href="#">International Cities<i class="fa fa-chevron-right"></i></a>
									</div>
									<div class="sm-toggle" data-target=".countries">
										<a class="sm-toggle-trigger" href="#">International Countries<i class="fa fa-chevron-right"></i></a>
									</div>
									<!--
									<div class="sm-toggle">
										<a class="sm-toggle-trigger" href="#">List Item menu Static<i class="fa fa-chevron-right"></i></a>
									</div>
									-->
								</div>
								
								
								<?php if(!empty($us_cities)) : ?>
								<div class="col-lg-2 list-toggle">
									<h3>Us Cities</h3>
									<div class="us-cities">
										<ul class="list-unstyled home-list">
											<?php foreach($us_cities as $usc) : ?>
											<li><a href="<?php echo site_url('/' . $usc->url_name); ?>"><?php echo $usc->city_name; ?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>
								</div>
								<?php endif; ?>
								
								<?php if(!empty($us_states)) : ?>
								<div class="col-lg-4 list-toggle">
									<h3>United States of America</h3>
									<div class="clearfix">
										<div class="sub-list col-sm-6">
											<div class="usa">
												<ul class="list-unstyled home-list">
													<?php $counter = 0; ?>
													<?php foreach($us_states as $state) : ?>
													<li><a href="<?php echo site_url('/' . $state->url_name); ?>"><?php echo $state->name; ?></a></li>
													
													<?php if($counter == 27) : ?>
													</ul>
													</div>
													</div>
													<div class="sub-list col-sm-6">
													<div class="usa">
													<ul class="list-unstyled home-list">
													<?php endif; ?>
													
													<?php $counter++; ?>
													<?php endforeach; ?>
												</ul>
											</div>
										</div>
										
									</div>
								</div>
								<?php endif; ?>
								
								
								<div class="col-lg-6 list-toggle">
									<h3>International Cities and Countries</h3>
									<?php if(!empty($intl_cities)) : ?>
									<div class="clearfix">
										<div class="sub-list col-sm-4">
											<div class="cities">
												<ul class="list-unstyled home-list">
													<?php $half = count($intl_cities) / 2;$c=0 ?>
													<?php foreach($intl_cities as $i) : ?>
													<li><a href="<?php echo site_url('/' . $i->url_name); ?>"><?php echo $i->city_name; ?></a></li>
													<?php if($c == $half) : ?>
													</ul>
													</div>
													</div>
													<div class="sub-list col-sm-4">
													<div class="cities">
													<ul class="list-unstyled home-list">
													<?php endif; ?>
													<?php $c++; ?>
													<?php endforeach; ?>
												</ul>
											</div>
										</div>
										
										<div class="sub-list col-sm-4">
											<div class="countries">
												<ul class="list-unstyled home-list">
													<li><a href="<?php echo site_url('/canada'); ?>">Canada</a></li>
													<li><a href="<?php echo site_url('/mexico'); ?>">Mexico</a></li>
													<li><a href="<?php echo site_url('/asia'); ?>">Asia</a></li>
													<li><a href="<?php echo site_url('/europe'); ?>">Europe</a></li>
													<li><a href="<?php echo site_url('/latin-america'); ?>">Latin America & Caribbean</a></li>
													<li><a href="<?php echo site_url('/australia'); ?>">Australia</a></li>
													<li><a href="<?php echo site_url('/brazil'); ?>">Brazil</a></li>
													<li><a href="<?php echo site_url('/india'); ?>">India</a></li>
													<li><a href="<?php echo site_url('/germany'); ?>">Germany</a></li>
													<li><a href="<?php echo site_url('/united-kingdom'); ?>">UK</a></li>
													<li><a href="<?php echo site_url('/japan'); ?>">Japan</a></li>
													<li><a href="<?php echo site_url('/africa'); ?>">Africa</a></li>
													<li><a href="<?php echo site_url('/middle-east'); ?>">Middle East</a></li>
																	<li><a href="<?php echo site_url('/china'); ?>">China
																	</a></li>
												</ul>
											</div>
										</div>
									</div>
									<?php endif; ?>
									<div class="recommend-country">
										<h5>Want to recommend a country, city, town or state?</h5>
										<h5>Email us at: <span>info@amyslist.us</span></h5>
									<br>
									</div>
									
								</div>
								
							</div>
							
						</div>
						
					</div>
				</section>
				
				<script>
					$('.sm-toggle').click(function(){
						var target = $(this).attr('data-target');
						if($(this).hasClass('dropdown-show'))
						{
							$(this).find('ul.home-list').remove();
							$(this).removeClass('dropdown-show');
						}
						else
						{
							var append = "";
							
							if($(target).length > 1) {
								$(target).each(function(){
									append += $(this).html();
								});
								
							}
							else
							{
								append = $(target).html();
							}
							
							$(this).addClass('dropdown-show').append(append);
							
						}
					});
				</script>
				
				<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-32380720-1', 'auto');
  ga('send', 'pageview');

</script>



<script>
	$(window).load(function(){
		var url = $('#base').attr('alt');
		
		<?php if($this->session->flashdata('trigger_social_fb') == 'Y') : ?>
		Share.facebook(url, 'asd','','');
		<?php endif; ?>
		
		<?php if($this->session->flashdata('trigger_social_tw') == 'Y') : ?>
		Share.twitter(url, 'I signed up for #AmysList which has local classifieds for jobs, for sale, personals, services and more.');
		<?php endif; ?>
		
	});
</script>