				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-thank-you">
							
							<div class="thank-you-box">
								
								<h2>Thank You! Your Payment Was Successful</h2>
								<p>We sent you a confirmation email with the details of your ad post.</p>
								
								<div class="payment">
									<p>We have good news! We donate $1 dollar of every ad post to <span class="charity">Amy's List Charity</span> :)</p>
									<p>So because of your post a dollar will go to a great cause to help others around the world.</p>
									<p><a href="<?php echo site_url('ads/posted'); ?>">Click the Ads Posted Tab</a> to review, edit, and delete your post</p>
								</div>
								
								<h1><img src="<?php echo asset_url(); ?>img/document.png">Ads Posted</h1>
								
								<button class="btn btn-primary btn-thank-you" onclick="window.location='<?php echo site_url('ads/posted'); ?>'">Continue</button>
								
							</div>
						</article>
					</div>
				</section>