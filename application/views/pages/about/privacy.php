			<section class="ct-head ct-city-page">
				<div class="container">
					<div class="clearfix">
						<div class="row">
							<div class="col-lg-3">
								<div class="head-block founder" onclick="window.location='<?php echo site_url('founders'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/founders.png" >FOUNDERS</h5>
								</div>
								<div class="head-block features" onclick="window.location='<?php echo site_url('features'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/features.png" >FEATURES</h5>
								</div>
								<div class="head-block post-an-ad" onclick="window.location='<?php echo site_url('posting-an-ad'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/post-an-ad.png" >POSTING AN AD</h5>
								</div>
								<div class="head-block city-page-ad city-page-ad-left clearfix" onclick="window.location='<?php echo site_url('page-advertising'); ?>'">
									<img src="<?php echo $this->config->item('assets_images'); ?>info/city-page-ad.png" class="pull-left">
									<h5>CITY PAGE ADVERTISING</h5>
								</div>
							</div>
							<div class="col-lg-9 amys-feature privacy">
								
								<h2>Privacy<img src="<?php echo $this->config->item('assets_images'); ?>info/privacy.png" style="margin-left:10px"></h2>
								<h3>AMY'S LIST PRIVACY POLICY</h3>
								<h3 class="title">Article 1 Modifications</h3>
								<p>Amy's List LLC ("Amyslist") may change this privacy policy (this "Privacy Policy") from time to time. Amy's List will post changes to this Privacy Policy on this page.</p>
								
								<h3 class="title">Article 2 Usage</h3>
								<p>Amy's List provides various services to you (our "Users"). The information our Users share with Amy's List is utilized for delivering services and products.</p>
								<h3 class="title">Article 3 Information Collected</h3>
								<p>Amy's List collects information our Users voluntarily give us (such as name and email address). Amyslist uses analytics tools to deliver the best possible service to our Users. These analyticstools may collect information, including but not limited to operating systems, unique device identifiers, referral URLs and ISPs.</p>
								<h3 class="title">Article 4 Cookies</h3>
								<p>Amyslist utilizes cookies for various purposes, including for our Users to login to their accounts.</p>
								<h3 class="title">Article 5 Third Party Service Providers</h3>
								<p>Amy's List interfaces with several third party service providers in order to provide our Users with access to services. Third party providers also provide advertisements.</p>
								<h3 class="title">Article 6 Sharing</h3>
								<p>Amy's List does not share identifiable personal information with third parties unless our Users provide consent or Amy's List is required to disclose information by law. Amy's List may shareaggregated, personally non-identifiable information.</p>
								<h3 class="title">Article 7 Applicability</h3>
								<p>This Privacy Policy applies to services provided by Amyslist but does not apply to services (particularly services provided by third parties) that do not incorporate this Privacy Policy.</p>
								
								<h3 class="title">Any questions regarding our Privacy Policy </h3>
								<h3 class="title"><span>Contact:</span> Info@amyslist.us</h3>
								
							</div>
						</div>
						
					</div>
				</div>
			</section>