			<section class="ct-head ct-city-page">
				<div class="container">
					<div class="clearfix">
						<div class="row">
							<div class="col-lg-3">
								<div class="head-block founder" onclick="window.location='<?php echo site_url('founders'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/founders.png" >FOUNDERS</h5>
								</div>
								<div class="head-block features" onclick="window.location='<?php echo site_url('features'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/features.png" >FEATURES</h5>
								</div>
								<div class="head-block post-an-ad" onclick="window.location='<?php echo site_url('posting-an-ad'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/post-an-ad.png" >POSTING AN AD</h5>
								</div>
								<div class="head-block city-page-ad city-page-ad-left clearfix" onclick="window.location='<?php echo site_url('page-advertising'); ?>'">
									<img src="<?php echo $this->config->item('assets_images'); ?>info/city-page-ad.png" class="pull-left">
									<h5>CITY PAGE ADVERTISING</h5>
								</div>
							</div>
							<div class="col-lg-9">
								<div class="clearfix">
									<img src="<?php echo $this->config->item('assets_images'); ?>info/window.png" class="pull-left" style="margin-right:15px;">
									<h2>City Page Advertising</h2>
								</div>
								<p>City Page Advertising is great for businesses and individuals looking to reach people that are located in a specific geographic area. </p>
								
								<img src="<?php echo $this->config->item('assets_images'); ?>info/ad-post-scr.png" >
								
								<div class="content">
									<h3>Advertise a product, service, cause, event, fundraiser, video, website, announcement, or Amy's List post!</h3>
									<p>Cost:  Rates will very depending on the city pages(s) selected.</p>
									<p class="rules">The following pages may contain a standard fee plus a daily, weekly, and monthly rate. </p>
									<p>Atlanta, Austin, Baltimore, Boston, Charlotte, Chicago, Cincinnati, Cleveland, Columbus, Dallas, Denver, Detroit, Houston, Inland Empire, Jacksonville, Kansas City, Las Vegas, Los Angeles, Minneapolis, Nashville, New York City, Orlando, Philadelphia, Phoenix, Pittsburgh, Portland, Raleigh, Sacramento, St. Louis , San Antonio, Seattle, Miami, Tampa, Washington DC.</p>
									
									<h3>Right to Refuse:</h3>
									<p>Amy's List reserves the right to refuse any company or business based on our Terms of Use and the discretion of our goals, values, morals, standards, opinions, beliefs. And pretty much anything we feel does not represent us as a company. Thanks.</p>
									
									<h3>Contact us for Fees and Rates:</h3>
									<p>adsales@amyslist.us</p>
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
			</section>