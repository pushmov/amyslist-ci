			<section class="ct-head ct-city-page">
				<div class="container">
					<div class="clearfix">
						<div class="row">
							<div class="col-lg-3">
								<div class="head-block founder" onclick="window.location='<?php echo site_url('founders'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/founders.png" >FOUNDERS</h5>
								</div>
								<div class="head-block features" onclick="window.location='<?php echo site_url('features'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/features.png" >FEATURES</h5>
								</div>
								<div class="head-block post-an-ad" onclick="window.location='<?php echo site_url('posting-an-ad'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/post-an-ad.png" >POSTING AN AD</h5>
								</div>
								<div class="head-block city-page-ad city-page-ad-left clearfix" onclick="window.location='<?php echo site_url('page-advertising'); ?>'">
									<img src="<?php echo $this->config->item('assets_images'); ?>info/city-page-ad.png" class="pull-left">
									<h5>CITY PAGE ADVERTISING</h5>
								</div>
							</div>
							<div class="col-lg-9 amys-feature amys-founder">
								<h2>Amy's List Founders</h2>
								<p>Amy's List was founded by Chris Manhattan in Isla Vista, California in 2009. Isla Vista is a college community located along the beach of California's Central Coast.</p>
								<p>The housing situation in Isla Vista is extremely difficult. Every semester thousands of students search for rooms, apartments, and houses to rent, with little options.  Chris noticed this lack of reliable online resources to find a place. Communicating with potential roommates aside from email and using the web to secure housing was difficult.</p>
								<p>Chris realized the problem of communicating via classifieds was not limited to housing, nor was it just a local problem, but a global one.  There was only one main option for classified ads and it wasn't the most reliable.  In any industry there should be fair and reasonable competition that provides an alternative to those who seek it.  </p>
								<p>And with that belief Amy's List was created by:</p>
								
							</div>
						</div>
						
					</div>
				</div>
			</section>
			
			<section class="ct-founders">
				<div class="container">
					<div class="row">
						<div class="col-lg-3">
							<img src="<?php echo $this->config->item('assets_images'); ?>info/chris.jpg" width="176">
						</div>
						<div class="col-lg-9">
							<h2>Chris Manhattan</h2>
							<h2 class="position">CEO and Co-Founder</h2>
							
							<p>Chris is taking courses at Harvard University earning a Master in Liberal Arts Government leading to a Law Degree. He holds an MBA in Media Management from the Metropolitan College of New York, and a Bachelor's Degree in Mass Communication and Media Arts (with a minor in Philosophy) from Southern Illinois University Carbondale.  </p>
							<p>Chris enlisted in the United States Marine Corps after high school.  He earned several awards including three Certificates of Commendation, two Navy and Marine Corps Achievement Medals, two Marine of the Quarters, and one Marine of the Year Award (DFAS-KC).  Chris was also awarded a Certificate of Appreciation from the President of the United States. </p>
							<p>Chris is a medically retired Marine, Executive Producer, Filmmaker, and Entrepreneur who has a passion for higher education, politics, government, news, entertainment, astrobiology, quantum mechanics, CrossFit, (NDES) Near Death Experiences, and many other topics. </p>
						</div>
					</div>
					
					<div class="row">
						<div class="col-lg-3">
							<img src="<?php echo $this->config->item('assets_images'); ?>info/michelle.jpg" width="176">
						</div>
						<div class="col-lg-9">
							<h2>Michelle Hardy</h2>
							<h2 class="position">Spokesmodel and Co-Founder</h2>
							
							<p>Michelle earned a Bachelor's of Arts Degree in Communication from California State University Channel Island. Michelle was born and raised in Hinsdale, Illinois.</p>
							<p>Upon graduation from Hinsdale Central High School, she relocated to California to attend Santa Barbara City College (SBCC).   While attending SBCC Michelle became a Co-Founder and the official Spokesmodel for Amy's List.   </p>
							<p>Michelle enjoys living an active healthy life style evolved around physical fitness. She likes to work out during the sunrise, and enjoys relaxing Santa Barbara sunsets. She is committed to making a positive impact in the lives of others through the Amy's List Charity Foundation.</p>
						</div>
					</div>
					
					<div class="row">
						<div class="col-lg-3">
							<img src="<?php echo $this->config->item('assets_images'); ?>info/paula.jpg" width="176">
						</div>
						<div class="col-lg-9">
							<h2>Paula Landry</h2>
							<h2 class="position">Co-Founder</h2>
							
							<p>Paula is a graduate professor in business, media, and film. She holds a Master of Business Administration and a Bachelor of Arts Degree from Ithaca College. </p>
							<p>Paula is a producer of film, music, and television. She is also an author who recently published a book titled, “Scheduling and Budgeting Your Film: A Panic-Free Guide”. Paula has worked on several projects across media and entertainment, winning awards, including a film debuting at the Sundance Festival.</p>
							<p>Paula also enjoys making music as a professional flutist.  She has performed in Paris for royalty and made guest appearances with the Gulbenkian Orchestra. </p>
							
						</div>
						
						<div class="row">
						<div class="col-lg-3">
							<img src="<?php echo $this->config->item('assets_images'); ?>info/michael.jpg" width="176">
						</div>
						<div class="col-lg-9">
							<h2>Michael W. Nichols</h2>
							<h2 class="position">Co-Founder</h2>
							
							<p>Michael served 12 years in the United States Marine Corps holding several leadership positions including Platoon Sergeant in Iraq and Marine Corps Drill Instructor.  While serving at the Marine Corp Recruit Depot San Diego, Michael successfully turned hundreds of young men -- including Chris Manhattan -- into Marines.</p>
							<p>Michael is the Senior Drill Instructor in the boot camp film series Black Friday: Dark Dawn.  Michael founded MDI 8 Fitness in San Diego, California. Michael is an inspiration to many people setting a positive example as a mentor and leader in life.</p>
							
						</div>
					</div>
					
				</div>
			</section>