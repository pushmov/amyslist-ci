			<section class="ct-head">
				<div class="container">
					<div class="clearfix">
						<div class="row">
							<div class="col-lg-3">
								<img src="<?php echo $this->config->item('assets_images'); ?>about-page-final.png" >
							</div>
							<div class="col-lg-9">
								<h2>Amy's List is the future of classified ads</h2>
								<p>AmysList is a classified ads website that combines social networking, text, and video chat.  Amy's List users can create a profile, send inbox messages, and upload video links regarding classified ads.  These are only some of the many great features of Amy's List.  Signing up is free -- Amy's List is focused on being a site free of spam, misconduct, and other illegal activities.  </p>
							</div>
						</div>
						
					</div>
				</div>
			</section>
			
			<section class="ct-block">
				<div class="container">
					<div class="row">
						<div class="col-lg-3">
							<div class="head-block founder" onclick="window.location='<?php echo site_url('founders'); ?>'">
								<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/founders.png" >FOUNDERS</h5>
							</div>
							<p>Amy's List was founded in 2009 in the town of Isla Vista located near Santa Barbara, California. </p>
							<a href="<?php echo site_url('founders'); ?>">Read more</a>
						</div>
						<div class="col-lg-3">
							<div class="head-block features" onclick="window.location='<?php echo site_url('features'); ?>'">
								<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/features.png" >FEATURES</h5>
							</div>
							<p>Amy's List has several features and functionality that make our site different from all of the rest. </p>
							<a href="<?php echo site_url('features'); ?>">Read more</a>
						</div>
						<div class="col-lg-3">
							<div class="head-block post-an-ad" onclick="window.location='<?php echo site_url('posting-an-ad'); ?>'">
								<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/post-an-ad.png" >POSTING AN AD</h5>
							</div>
							<p>Posting an ad on Amy's List is quick and simple. All ad posts on Amy's List are free except...</p>
							<a href="<?php echo site_url('posting-an-ad'); ?>">Read more</a>
						</div>
						<div class="col-lg-3">
							<div class="head-block city-page-ad clearfix" onclick="window.location='<?php echo site_url('page-advertising'); ?>'">
								<img src="<?php echo $this->config->item('assets_images'); ?>info/city-page-ad.png" class="pull-left">
								<h5>CITY PAGE ADVERTISING</h5>
							</div>
							<p>Do you want to advertise something to the people of a specifictown, city, or state? </p>
							<a href="<?php echo site_url('page-advertising'); ?>">Read more</a>
						</div>
					</div>
				</div>
			</section>