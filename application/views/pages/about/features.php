			<section class="ct-head ct-city-page">
				<div class="container">
					<div class="clearfix">
						<div class="row">
							<div class="col-lg-3">
								<div class="head-block founder" onclick="window.location='<?php echo site_url('founders'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/founders.png" >FOUNDERS</h5>
								</div>
								<div class="head-block features" onclick="window.location='<?php echo site_url('features'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/features.png" >FEATURES</h5>
								</div>
								<div class="head-block post-an-ad" onclick="window.location='<?php echo site_url('posting-an-ad'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/post-an-ad.png" >POSTING AN AD</h5>
								</div>
								<div class="head-block city-page-ad city-page-ad-left clearfix" onclick="window.location='<?php echo site_url('page-advertising'); ?>'">
									<img src="<?php echo $this->config->item('assets_images'); ?>info/city-page-ad.png" class="pull-left">
									<h5>CITY PAGE ADVERTISING</h5>
								</div>
							</div>
							<div class="col-lg-9 amys-feature">
								<h2>Amy's List Features</h2>
								<div class="header clearfix">
									<h2 class="pull-left">Profile Pages</h2>
									<img src="<?php echo $this->config->item('assets_images'); ?>icons/Girl@2x.png" width="95"><img src="<?php echo $this->config->item('assets_images'); ?>icons/Dude@2x.png" width="95">
								</div>
								
								<div class="profile-content">
									<p>Every user gets a profile where they can provide basic information about themselves. This allows other users to feel more comfortable and confident with whom they are communicating with via classified ads.  Profile Pages allow Amy's List to be a more safe, secure, and reliable social commerce site -- where there is less spam, misconduct, scams and other illegal activity.</p>
									<p>If you find that a user is violating our Terms of Use, posting spam, soliciting scams, and/or other misconduct, you can easily report the user from their profile page by clicking on the "Report Profile" button.  This allows Amy's List users to have oversight as a social networking community, to monitor and take action, keeping the site a safe, secure, reliable, user-friendly, classified ads experience.</p>
									
									<h2><img src="<?php echo $this->config->item('assets_images'); ?>icons/Mail@2x.png" width="95">Inbox Messages</h2>
									<p>Every user has access to their own personal inbox where they can send, receive, forward, save, and compose messages.  This makes classifieds ads much simpler and convenient. Inbox messages are usually quicker and direct than email. Although we still provide users with the option to email, we feel inbox messaging is a safer option to communicate.</p>
									
									<div class="clearfix">
										<h2 class="pull-right"><img src="<?php echo $this->config->item('assets_images'); ?>info/message.png" width="95">Ad Post Comments</h2>
									</div>
									<p>The Ad Post comment section will provide oversight and transparency � allowing users to ask questions, respond to ad posts, provide feedback, inquires, etc.  Users can respond to  the original user or other users who comment on an ad instead of emailing back and  forth.</p>
									<p>We feel the Ad Post comment sections will help users alert others of any spam, scams, fraud, fake posts, or other illegal activity.  We all know how comment sections work on the web and so we know this is an awesome feature for users to have on Amy's List. </p>
									
									<div class="clearfix">
										<h2 class="pull-right">Text Chat with Users<img src="<?php echo $this->config->item('assets_images'); ?>info/texting.png" class="texting" width="95"></h2>
									</div>
									<p>Text Chat allows users to communicate instantly with each other regarding classified ad posts. Text Chat is an option available to users when they post an ad. You can turn text chat on or off. If you turn text chat on, when users message you, a pop up will appear. If you are receiving too many chat messages you can turn chat off at any time by click the chat menu and going "Offline". </p>
									
									
									<h2>Video Chat with Users<img src="<?php echo $this->config->item('assets_images'); ?>info/videocam.png" class="texting" width="95"></h2>
									<p>Video Chat takes classified ads to a whole new level. Imagine video chatting with someone first before having to check out a room for rent.  Imagine talking to someone selling a cellphone on Amy's List first using video chat, or talking to someone selling a car. You can video chat and see the car (via webcam) first without having to be there in person.  The possibilities of video chat and classified ads are endless.  This is yet another great feature we have on Amy's List.</p>
									
									<h2 style="text-align:center"><img src="<?php echo $this->config->item('assets_images'); ?>info/window-2.png" width="95">Ad Post Video Links</h2>
									<p>Ad Post Video links are something we are providing for users who want to take their ad post to the next level. Pictures and words can only do so much. Video can help turn a classified ad postinto something much more. Almost like a classified ad commercial.  Our Ad Post Video link is simple and easy.  You record a video for your classified ad.</p>
									<p>Let's say you're selling a cellphone.  In the video you can show and describe anything you want about the phone, show on video anything you think potential buyers should know, just like if you were selling to them face to face. Let's say you are renting a room in your apartment, take a video. </p>
									<p>Introduce yourself and show potential roommates who you are and what the room looks like. You can describe everything in your video as if you were giving them a real tour. Once you are done, upload the video to YouTube, Instagram, Vimeo, Facebook, Vine, etc., then copy and paste the link to the video in your ad post. And you're done. It's that simple. Now everyone who views your ad post can also view your video. </p>
									
									<h2><img src="<?php echo $this->config->item('assets_images'); ?>info/handycam.png" width="95">Video Bio</h2>
									<p>A Video Bio is an easy and fun way to broadcast yourself to the world. Record a video saying anything you think people should know about you. No matter what kind of classified ads you post on Amy's List, a Video Bio is a great feature that helps other users know with whom they are communicating.</p>
									<p>For example, did you apply for a job on Amy's List? </p>
									<p>Record a Video Bio. Then send a link to the company who posted the job ad.  Tell them why you applied for the job, why you are interested in the position, all your qualifications relevant to the position basically all the things you would put in a cover letter, you can record in your Video Bio :) </p>
									<p>The Video Bio, Video Resume, Video Ad concept is something we believe is going to start spreading quickly across the job market in every industry. This will become a great tool for job recruitment and the overall hiring process within HR departments.  </p>
									
									<h2>Amy's List Charity<img src="<?php echo $this->config->item('assets_images'); ?>info/charity.png" class="texting" width="95"></h2>
									<p>Amy's List Charity was created to give back to those in need.  Amy's List will donate one dollar for every ad posted on our website that requires payment of a fee. </p>
									<p>Click here to see a list of our classified ads that cost a fee:  <a href="#">http:/www.amyslist.us/adpostInfo</a></p>
									<p>Amy's List is dedicated to making a positive impact in the lives of others through our gift of giving. We are fully committed to providing credible, nonprofit organizations with financial donations based on our success.  As we grow as a company and website, so too will our ad posts, resulting in increased philanthropy, and overall giving. </p>
									
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
			</section>