			<section class="ct-head ct-city-page">
				<div class="container">
					<div class="clearfix">
						<div class="row">
							<div class="col-lg-3">
								<div class="head-block founder" onclick="window.location='<?php echo site_url('founders'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/founders.png" >FOUNDERS</h5>
								</div>
								<div class="head-block features" onclick="window.location='<?php echo site_url('features'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/features.png" >FEATURES</h5>
								</div>
								<div class="head-block post-an-ad" onclick="window.location='<?php echo site_url('posting-an-ad'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/post-an-ad.png" >POSTING AN AD</h5>
								</div>
								<div class="head-block city-page-ad city-page-ad-left clearfix" onclick="window.location='<?php echo site_url('page-advertising'); ?>'">
									<img src="<?php echo $this->config->item('assets_images'); ?>info/city-page-ad.png" class="pull-left">
									<h5>CITY PAGE ADVERTISING</h5>
								</div>
							</div>
							<div class="col-lg-9 info-contact">
								<div class="clearfix">
									<h2 class="pull-left">Contact</h2>
									<img src="<?php echo $this->config->item('assets_images'); ?>info/cs.png" >
								</div>
								
								<div>
									<h3>General Info</h3>
									<p class="info">info@amyslist.us</p>
									<h3>City Page Advertising</h3>
									<p class="info">adsales@amyslist.us</p>
									<h3>Amy's List Charity </h3>
									<p class="info">charity@amyslist.us</p>
									<h3>Media Inquirers</h3>
									<p class="info">chrismanhattan@amyslist.us</p>
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
			</section>