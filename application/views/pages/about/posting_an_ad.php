			<section class="ct-head ct-city-page">
				<div class="container">
					<div class="clearfix">
						<div class="row">
							<div class="col-lg-3">
								<div class="head-block founder" onclick="window.location='<?php echo site_url('founders'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/founders.png" >FOUNDERS</h5>
								</div>
								<div class="head-block features" onclick="window.location='<?php echo site_url('features'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/features.png" >FEATURES</h5>
								</div>
								<div class="head-block post-an-ad" onclick="window.location='<?php echo site_url('posting-an-ad'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/post-an-ad.png" >POSTING AN AD</h5>
								</div>
								<div class="head-block city-page-ad city-page-ad-left clearfix" onclick="window.location='<?php echo site_url('page-advertising'); ?>'">
									<img src="<?php echo $this->config->item('assets_images'); ?>info/city-page-ad.png" class="pull-left">
									<h5>CITY PAGE ADVERTISING</h5>
								</div>
							</div>
							<div class="col-lg-9 amys-feature post-an-ad-final">
								<img src="<?php echo $this->config->item('assets_images'); ?>info/amy-full.png" class="amy-full">
								<h2><img src="<?php echo $this->config->item('assets_images'); ?>info/window-3.png" style="margin-right:15px">Posting An Ad</h2>
								<p>Posting an ad is quick and simple.  All ad posts on Amy's List are free except: </p>
								
								<div class="post-an-ad-final-content">
									<h2>Job Posting in the following cities:</h2>
									<p>Cost: $10</p>
									<p>Atlanta, Austin, Baltimore, Boston, Charlotte, Chicago, Cincinnati, Cleveland, Columbus, Dallas, Denver, Detroit, Houston, Inland Empire, Jacksonville, Kansas City, Las Vegas, Los Angeles, Minneapolis, Nashville, New York City, Orlando, Philadelphia, Phoenix, Pittsburgh, Portland, Raleigh, Sacramento, St. Louis , San Antonio, Seattle, Miami, Tampa, Washington DC.</p>
									<h2>Therapeutic Service:</h2>
									<p>Cost: $5</p>
									<p>All cities across all countries.</p>
									<h2>Contact:</h2>
									<p>billing@amyslist.us</p>
									
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
			</section>