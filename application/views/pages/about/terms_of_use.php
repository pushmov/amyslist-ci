			<section class="ct-head ct-city-page">
				<div class="container">
					<div class="clearfix">
						<div class="row">
							<div class="col-lg-3">
								<div class="head-block founder" onclick="window.location='<?php echo site_url('founders'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/founders.png" >FOUNDERS</h5>
								</div>
								<div class="head-block features" onclick="window.location='<?php echo site_url('features'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/features.png" >FEATURES</h5>
								</div>
								<div class="head-block post-an-ad" onclick="window.location='<?php echo site_url('posting-an-ad'); ?>'">
									<h5><img src="<?php echo $this->config->item('assets_images'); ?>info/post-an-ad.png" >POSTING AN AD</h5>
								</div>
								<div class="head-block city-page-ad city-page-ad-left clearfix" onclick="window.location='<?php echo site_url('page-advertising'); ?>'">
									<img src="<?php echo $this->config->item('assets_images'); ?>info/city-page-ad.png" class="pull-left">
									<h5>CITY PAGE ADVERTISING</h5>
								</div>
							</div>
							<div class="col-lg-9 amys-feature privacy">
								
								<h2>Terms of Use<img src="<?php echo $this->config->item('assets_images'); ?>info/tos.png" style="margin-left:10px"></h2>
								<h3>TERMS OF USE</h3>
								<h3 class="title">ACCEPTANCE</h3>
								<p>Your use of Amyslist and the services provided by Amyslist (the "Service") is subject to the following Terms and Conditions (the "Terms and Conditions").</p>
								<p>1. By using the Service, you agree to and accept the Terms and Conditions.</p>
								<p>2. If you do not agree to the Terms and Conditions in their entirety, you must immediately discontinue use of the Service.   No other recourse is available to you.</p>
								
								<h3 class="title">CODE OF CONDUCT</h3>
								<p>You will not:</p>
								<p>1. Make any of the following content available.</p>
								<p>a. Pornography;</p>
								<p>b. Content that violates any federal, state or local laws;</p>
								<p>c. Content that is, or can be considered false, misleading or deceptive;</p>
								<p>d. Content that is, or can be considered illegally discriminatory;</p>
								<p>e. Third party personal information;</p>
								<p>f. Content that infringes the intellectual property rights of others;</p>
								<p>g. Defamatory material;</p>
								<p>h. Content that is, or can be considered abusive, threatening or harassing;</p>
								<p>i. SPAM or content that links to third party sites or services;</p>
								<p>j. Content that violates the contractual (written and unwritten) rights of others.</p>
								<p>2. Impersonate anyone or any entity or attempt to disguise the origin of any content.</p>
								<p>3. Post commercial content in areas of the Service designed for non-commercial content.</p>
								<p>4. Advertise any items or services prohibited by any federal, state or local laws.</p>
								<p>5. Attempt to access or disrupt the Service in any way, including (without limitation) by use of any computer code, viruses, programs or denial of service attacks.</p>
								<p>6. Harass anyone.</p>
								<p>7. Collect personal data about any Service users.</p>
								<p>8. Use automated devices or programs to download data or post in bulk or to conduct any activity designed for non-automated use.</p>
								<p>9. Make multiple postings of the same content in more than one category..</p>
								<p>10. Permit third parties to post content our otherwise use the Service on your behalf.</p>
								<h3 class="title">RESPONSIBILITY</h3>
								<p>You are solely responsible for all content originated or transmitted by you.  Amyslist and its affiliates are not responsible for and does not control or pre-screen, any content.  You agree that use of the Service may expose you to objectionable, misleading or offensive content and by using the Service, you agree to indemnify and hold Amyslist and its affiliates harmless for any claims or liabilities related to such content.  In the event you access third party content as a result of your use of the Service, you hereby agree that such dealings are solely between you and the third parties and that Amyslist and its affiliates are not responsible for any claims or liabilities arising from the foregoing dealings, and you agree to indemnify and hold Amyslist and its affiliates harmless for any claims or liabilities related to such dealings.</p>
								<h3 class="title">INFORMATION DISCLOSURE</h3>
								<p>You agree that Amyslist may disclose, in its sole and absolute discretion, any and all information about (without limitation) you, your content and your activities, as needed to comply with the orders of law enforcement officials or the legal process.</p>
								<h3 class="title">ACKNOWLEDGEMENT</h3>
								<p>You acknowledge:</p>
								<p>1. Amyslist may charge (or choose not to charge) fees for any and all services provided by the Service, at any time, without prior notice.</p>
								<p>2. Amyslist may limit use of, modify or eliminate the Service (in whole or in part), at any time, without prior notice.</p>
								<p>3. All fees are non-refundable.</p>
								<p>4. Amyslist may terminate your access to the Service, at any time, without prior notice.</p>
								<p>5. The Service and all content is protected by international intellectual property laws, including (without limitation) trademarks and copyrights and any reproduction, retransmission, decompilation, or disassembly, in whole or in part of any content or the Service is strictly prohibited.</p>
								<p>6. You hereby grant Amyslist a perpetual, non-revocable, fully paid, worldwide license to use all content for any purpose, including (without limitation) reproduction, retransmission, and preparation of derivative works.</p>
								<h3 class="title">DISCLAIMERS, INDEMNIFICATION AND LIMITATIONS OF LIABILITY</h3>
								<p>THE SERVICE IS PROVIDED ON AN �AS IS� BASIS, WITHOUT ANY WARRANTIES, REPRESENTATIONS, OR COVENANTS OF ANY KIND.  ALL WARRANTIES, EXPRESS AND IMPLIED, INCLUDING (WITHOUT LIMITATION) WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT ARE EXPRESSLY DISCLAIMED.   AMYSLIST DISCLAIMS ANY WARRANTIES FOR THE PERFORMANCE OF THE SERVICE (AND ANY THIRD PARTY SITES (IF ANY)), INCLUDING (WITHOUT LIMITATION) ANY WARRANTIES FOR RELIABILITY, HARMFUL SOFTWARE AND SECURITY.</p>
								<h3 class="title">YOUR USE OF THE SERVICE IS ENTIRELY AT YOUR OWN RISK.</h3>
								<p>You agree to indemnify and hold Amyslist and its affiliates harmless from any claim or demand, including legal fees arising out of your use of the Service.  You hereby release and hold Amyslist and its affiliates harmless from any and all claims, liabilities, and damages of every kind or nature, known or unknown, arising out of or in any way related to the Service and your use thereof.</p>
								<p>AMYSLIST AND ITS AFFILIATES SHALL NOT BE LIABLE FOR DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES OF ANY KIND (EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES), RESULTING FROM YOUR USE OF THE SERVICE.</p>
								<h3>GENERAL</h3>
								<p>1. The laws of the State of California (without giving effect to its conflict of laws principles) govern all matters arising out of or relating to your relationship with Amyslist and/or your use of the Service, and the transactions contemplated, including, without limitation, interpretation, construction, performance, and enforcement of these Terms and Conditions.  Any claims or actions must be brought exclusively in a court of competent jurisdiction sitting in Los Angeles, California, and you agree to submit to the jurisdiction of such courts for the purposes of all legal actions and proceedings arising out of or relating to your relationship with Amyslist and/or your use of the Service.  You waive, to the fullest extent permitted by law, any objection that you may now or later have to (i) the venue of any legal action or proceeding brought in any state or federal court sitting in Los Angeles, California; and (ii) any claim that any action or proceeding brought in any such court has been brought in an inconvenient forum.</p>
								<p>2. No waiver or failure by Amyslist to exercise any option, right or privilege under the terms of these Terms and Conditions on any occasion or occasions shall be construed to be a waiver of the same or any other option, right or privilege on any other occasion.</p>
								<p>3. The provisions of these Terms and Conditions shall be deemed severable and the invalidity or unenforceability of any provision shall not affect the validity or enforceability of the other provisions hereof.</p>
								<p>4. If an ambiguity or question of intent or interpretation arises, these Terms and Conditions shall be construed as if drafted jointly by the parties and no presumption or burden of proof shall arise favoring or disfavoring Amyslist because of the authorship of any provision of these Terms and Conditions.</p>
								<p>5. Amyslist may, in its sole and absolute discretion, alter these Terms and Conditions at any time without notice.</p>
								<p>6. If you believe that content posted by third parties on Amyslist constitutes an infringement of your intellectual property rights, notify Amyslist at info@amyslist.us.  In your notice, clearly identify the material you believe is infringing your intellectual property rights, a sworn and signed statement that the content is not authorized along with your address, email, cell phone and telephone numbers.  All infringing content will be removed pursuant to the procedures of the DMCA.</p>
								<h3 class="title">Any questions regarding our Privacy Policy </h3>
								<h3 class="title"><span>Contact:</span> Info@amyslist.us</h3>
								
							</div>
						</div>
						
					</div>
				</div>
			</section>