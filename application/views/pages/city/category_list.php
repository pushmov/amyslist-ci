				<section class="ct-content clearfix ct-homepage">
					<div class="container">
						<?php load_sidebar() ?>
						
						<article class="col-lg-10 ct-city-page">
							
							<div>
							<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Footer Ad Amy's List -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3122936194507304"
     data-ad-slot="7189162304"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<br>
								<?php if(!empty($parent)) :?>
								<a style="color:#111;font-size:13px;" href="<?php echo site_url('/' . $parent->url_name); ?>">
									<i class="fa fa-chevron-left"></i>Back to <?php echo $parent->city_name; ?></a>
								<?php endif; ?>
								<ul class="list-unstyled list-cities">
									<li class="main"><a href="#"><?php echo strtoupper($city->city_name); ?></a></li>
									<!--
									<li class="active"><a href="#"><i class="fa fa-chevron-right"></i>All of <?php echo $city->city_name; ?></a></li>
									-->
									<?php if(!empty($city_breadcumb)) : ?>
									<?php foreach($city_breadcumb as $c) : ?>
									<li><a href="<?php echo site_url('/' . $c->url_name); ?>"><i class="fa fa-chevron-right"></i><?php echo $c->city_name; ?></a></li>
									<?php endforeach; ?>
									<?php endif; ?>
								</ul>
						
							<div class="row clearfix ct-list">
								<div class="col-sm-3">
									
									<?php if(!empty($categories['neighborhood'])) :?>
									<h3>Neighborhood
										<i class="fa fa-plus"></i>
									</h3>
									<div class="ct-list-toggle">
										<ul class="list-unstyled ct-list-content">
											<?php foreach($categories['neighborhood'] as $row) : ?>
											<li><a href="<?php echo site_url('/' . $city->url_name . '/neighborhood/' . $row->url_category_name); ?>"><?php echo $row->category_name; ?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>
									<?php endif; ?>
									
									<?php if(!empty($categories['personals'])) :?>
									<h3>Personals
										<i class="fa fa-plus"></i>
									</h3>
									<div class="ct-list-toggle">
										<ul class="list-unstyled ct-list-content">
											<?php foreach($categories['personals'] as $row) : ?>
											<li><a href="<?php echo site_url('/' . $city->url_name . '/personals/' . $row->url_category_name); ?>"><?php echo $row->category_name; ?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>
									<?php endif; ?>
									
									<?php if(!empty($categories['blogs-and-vlogs'])) : ?>
									<h3>Blogs and Vlogs
										<i class="fa fa-plus"></i>
									</h3>
									<div class="row clearfix ct-list-toggle">
										<div class="col-sm-6">
											<ul class="list-unstyled ct-list-content">
												<?php $half = count($categories['blogs-and-vlogs']) / 2;$c=0;?>
												<?php foreach($categories['blogs-and-vlogs'] as $row) : ?>
												<li><a href="<?php echo site_url('/' . $city->url_name . '/blogs-and-vlogs/' . $row->url_category_name); ?>"><?php echo $row->category_name; ?></a></li>
												<?php if($half == $c) : ?>
												</ul>
												</div>
												<div class="col-sm-6">
												<ul class="list-unstyled ct-list-content">
												<?php endif; ?>
												<?php $c++;?>
												<?php endforeach; ?>
											</ul>
										</div>
									</div>
									<?php endif; ?>
									
								</div>
								<div class="col-sm-3">
									<?php if(!empty($categories['housing'])) : ?>
									<h3>Housing
										<i class="fa fa-plus"></i>
									</h3>
									<div class="ct-list-toggle">
										<ul class="list-unstyled ct-list-content">
											<?php foreach($categories['housing'] as $row) : ?>
											<li><a href="<?php echo site_url('/' . $city->url_name . '/housing/' . $row->url_category_name); ?>"><?php echo $row->category_name; ?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>
									<?php endif; ?>
									
									<?php if(!empty($categories['services'])) : ?>
									<h3>Services
										<i class="fa fa-plus"></i>
									</h3>
									<div class="ct-list-toggle">
										<ul class="list-unstyled ct-list-content">
											<?php foreach($categories['services'] as $row) : ?>
											<li><a href="<?php echo site_url('/' . $city->url_name . '/services/' . $row->url_category_name); ?>"><?php echo $row->category_name; ?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>
									<?php endif; ?>
									
									<?php if(!empty($categories['video'])) : ?>
									<h3>Video
										<i class="fa fa-plus"></i>
									</h3>
									<div class="ct-list-toggle">
										<ul class="list-unstyled ct-list-content">
											<?php foreach($categories['video'] as $row) : ?>
											<li><a href="<?php echo site_url('/' . $city->url_name . '/video/' . $row->url_category_name); ?>"><?php echo $row->category_name; ?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>
									<?php endif; ?>
									
								</div>
								<div class="col-sm-3">
									<?php if(!empty($categories['for-sale'])) : ?>
									<h3>For Sale
										<i class="fa fa-plus"></i>
									</h3>
									<div class="ct-list-toggle">
										<ul class="list-unstyled ct-list-content">
											<?php foreach($categories['for-sale'] as $row) : ?>
											<li><a href="<?php echo site_url('/' . $city->url_name . '/for-sale/' . $row->url_category_name); ?>"><?php echo $row->category_name; ?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>
									<?php endif; ?>
								</div>
								
								<div class="col-sm-3">
									<?php if(!empty($categories['employment'])) : ?>
									<h3>Employment
										<i class="fa fa-plus"></i>
									</h3>
									<div class="ct-list-toggle">
										<ul class="list-unstyled ct-list-content">
											<?php foreach($categories['employment'] as $row) : ?>
											<li><a href="<?php echo site_url('/' . $city->url_name . '/employment/' . $row->url_category_name); ?>"><?php echo $row->category_name; ?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>
									<?php endif; ?>
									
									<?php if(!empty($categories['gigs-side-jobs'])) : ?>
									<h3>Gigs and Jobs
										<i class="fa fa-plus"></i>
									</h3>
									<div class="ct-list-toggle">
										<ul class="list-unstyled ct-list-content">
											<?php foreach($categories['gigs-side-jobs'] as $row) : ?>
											<li><a href="<?php echo site_url('/' . $city->url_name . '/gigs-side-jobs/' . $row->url_category_name); ?>"><?php echo $row->category_name; ?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>
									<?php endif; ?>
									
								</div>
								
								</div>
								
								<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-32380720-1', 'auto');
  ga('send', 'pageview');

</script>
							</div>
							
						</article>
						
					</div>
				</section>
				<script src="<?php echo asset_url(); ?>js/category-toggle.js"></script>