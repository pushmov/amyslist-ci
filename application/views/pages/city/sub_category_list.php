				<section class="ct-content clearfix ct-homepage">
					<div class="container">
						
						<?php load_sidebar() ?>
						
						<div class="col-lg-10 ct-homepage-content">
							
                            <div class="row clearfix">
                            
                               <div class="row clearfix breadcumb-wrapper">
                                    <ul class="list-unstyled breadcumb">
                                        <?php if(!empty($city_parent_row)){ ?>
                                            <li><a href="<?php echo site_url('/' . $city_parent_row->url_name); ?>"><?php echo $city_parent_row->city_name; ?></a></li>
                                            <li><a href="#">/</a></li>
                                        <?php } ?>
                                        <li><a href="<?php echo site_url('/' . $city->url_name); ?>"><?php echo $city->city_name; ?></a></li>
                                        <li><a href="#">/</a></li>
                                        <li><a href="#"><?php echo $category['row_item']->category_name; ?></a></li>
                                    </ul>
                                </div>
                            
								
								<div class="sm-toggle-ct">
									<div class="sm-toggle" data-target=".us-cities">
										<a class="sm-toggle-trigger" href="#"><?php echo $parents->category_name; ?><i class="fa fa-chevron-right"></i></a>
									</div>
								</div>
								
								<?php if(!empty($childs)) : ?>
								<div class="col-lg-4 list-toggle">
									<h3><?php echo $title; ?></h3>
									<div class="us-cities">
										<ul class="list-unstyled home-list">
											<?php foreach($childs as $c) : ?>
											<li><a href="<?php echo site_url('/' .$city->url_name .'/' .$category['row_item']->url_category_name .'/' . $c->url_category_name); ?>"><?php echo $c->category_name; ?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>
								</div>
								<?php endif; ?>
								
							</div>
							
						</div>
						<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-32380720-1', 'auto');
  ga('send', 'pageview');

</script>
						</div>
						
						
					</div>
				</section>
				
				<script>
					$('.sm-toggle').click(function(){
						var target = $(this).attr('data-target');
						if($(this).hasClass('dropdown-show'))
						{
							$(this).find('ul.home-list').remove();
							$(this).removeClass('dropdown-show');
						}
						else
						{
							var append = "";
							
							if($(target).length > 1) {
								$(target).each(function(){
									append += $(this).html();
								});
								
							}
							else
							{
								append = $(target).html();
							}
							
							$(this).addClass('dropdown-show').append(append);
							
						}
					});
				</script>