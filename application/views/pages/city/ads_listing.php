   			<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						<?php load_sidebar() ?>
						<style type="text/css" >
						.cc_dd_size .dd-options{
							height: 174px !important;
						}
						</style>
                        

						<article class="col-lg-10 right-content ct-adpost-listing cc_dd_size">
							
							<div class="row clearfix">
								
								<?php if(!empty($city_breadcumb)) : ?>
                             
								<select name="cities-list">
									<option value="<?php echo $city->url_name; ?>"><?php echo $city->city_name; ?></option>
									<?php foreach($city_breadcumb as $c) : ?>
									<option <?php echo ($c->id == $city->city_id) ? 'selected' : ''; ?>value="<?php echo $c->url; ?>"><?php echo $c->name; ?></option>
									<?php endforeach; ?>
								</select>
								
								<script>
									$('select[name="cities-list"]').ddslick({
										onSelected: function(data){
											if(data.selectedData.value != '<?php echo $city->url_name; ?>')
											{
												window.location=$('base').attr('alt') + data.selectedData.value + '/' + '<?php echo $category['parent_item']->url_category_name; ?>' + '/' + '<?php echo $category['row_item']->url_category_name; ?>';
											}
											
										}
									});
									
								</script>
								<?php endif; ?>
                                
                                
								
								<div class="row clearfix breadcumb-wrapper">
									<ul class="list-unstyled breadcumb">
                                        <?php if(!empty($city_parent_row)){ ?>
                                            <li><a href="<?php echo site_url('/' . $city_parent_row->url_name); ?>"><?php echo $city_parent_row->city_name; ?></a></li>
                                            <li><a href="#">/</a></li>
                                        <?php } ?>
                                        <li><a href="<?php echo site_url('/' . $city->url_name); ?>"><?php echo $city->city_name; ?></a></li>
										<li><a href="#">/</a></li>
										<li><a href="<?php echo site_url('/' . $city->url_name."/".$category['parent_item']->url_category_name); ?>"><?php echo $category['parent_item']->category_name; ?></a></li>
										<li><a href="#">/</a></li>
										<li><a href="#"><?php echo $category['row_item']->category_name; ?></a></li>
									</ul>
								</div>
								
								<fieldset>
									<form>
										<div class="clearfix">
											<div class="form-list">
											<?php if(!empty($sibling_category)) : ?>
												<select class="select-block clearfix" style="width:100%;width:220px;" value="<?php echo $category['row_item']->category_name; ?>" name="herolist">
													<?php foreach($sibling_category as $sibling) : ?>
													<option value="<?php echo $sibling->id; ?>"><?php echo $sibling->category_name; ?></option>
													<?php endforeach; ?>
												</select>
											<?php endif; ?>
											</div>
											<div class="form-list">
												<input type="text" name="search[text]" class="search" placeholder="Search">
												<button class="btn btn-search" type="submit"><span class="fui-search"></span></button>
											</div>
											
											<?php if((int) $category['parent_item']->id == 3) : ?>
											<div class="form-list">
												<h5>Rent:</h5>
											</div>
											<div class="form-list">
												<input type="text" name="search[min]" class="min" placeholder="Min">
											</div>
											<div class="form-list">
												<input type="text" name="search[max]" class="max" placeholder="Max">
											</div>
											<div class="form-list">
												<input type="text" name="search[num]" class="numeric" value="2">
											</div>
											<?php endif; ?>
											<div class="form-list">
												<label class="checkbox">
													<input type="checkbox" name="search[pics]" value="" data-toggle="checkbox">
													<span>Pics</span>
												</label>
											</div>
											<div class="form-list">
												<label class="checkbox">
													<input type="checkbox" name="search[title]" value="" data-toggle="checkbox">
													<span>Title</span>
												</label>
											</div>
										</div>
									</form>
								</fieldset>
								
                                  
                               
								<div class="tab-wrapper" id="tabs">
									
									<ul class="nav nav-tabs resp-tabs-list adpostlist-tabs">
										<li class="active">
											<a href="#list" data-toggle="tab">List</a>
										</li>
										<li>
											<a href="#picture" data-toggle="tab">Picture</a>
										</li>
										<li>
											<a href="#square" data-toggle="tab">Square</a>
										</li>
										<li>
                                           <a href="#map_canvas" class="map_canvas" data-toggle="tab">Map</a>
										</li>
										
									</ul>
									
									<div id="tab-content-dk" class="tab-content resp-tabs-container">
										<div class="tab-pane active list" id="list">
											<?php if(!empty($postlist)) : ?>
											<?php foreach($postlist as $t => $item) : ?>
                                                <div style="text-align:left"><?php //echo sizeof($item) . ' record found.'; ?></div>
												<h3><?php echo date('F d, Y', strtotime($t)); ?></h3>
												<ul class="list-unstyled">
													<?php foreach($item as $ad) : ?>
													<li>
														<a href="<?php echo site_url('/viewpost/' . $ad->post_num_id . '/' . $ad->post_url_name); ?>">
															<?php echo $ad->title; ?>
															
															<span class="location"><?php echo ($ad->price > 0) ? '$'.$ad->price : '' ?> (<?php echo $ad->address; ?>)</span>
															
															<?php if((int) $ad->latitude != 0 && (int) $ad->longitude != 0) : ?>
															<span class="maps">map</span>
															<?php endif; ?>
															
															<?php if(count($ad->images) > 0) : ?>
															<span class="pic">pic</span>
															<?php endif; ?>
															
														</a>
													</li>
													<?php endforeach; ?>
												</ul>
											<?php endforeach; ?>
											<?php else : ?>
											Sorry there are no postings. Be the first by clicking <a href="<?php echo site_url('post/ads'); ?>">Post an Ad</a> above. 
											<?php endif; ?>
											
										</div>
										
										<div class="tab-pane picture" id="picture">
											<?php if(!empty($postlist)) : ?>
											<?php foreach($postlist as $t => $item) : ?>
                                               <div style="text-align:left"><?php //echo sizeof($item) . ' record found.'; ?></div>
												<h3><?php echo date('F d, Y', strtotime($t)); ?></h3>
												<?php foreach($item as $ad) : ?>
												<div class="list-picture">
													<div class="item clearfix">
														<div class="pull-left">
															<img src="<?php echo asset_url() . $ad->images[0]->image_path; ?>" height="50" width="50">
															<div style="background: url(<?php echo asset_url() . 'tn/image.php?width=280&image=' . asset_url() . $ad->images[0]->image_path; ?>) no-repeat center;" class="big-img"></div>
															
														</div>
														<div>
															<a href="<?php echo site_url('/viewpost/' . $ad->post_num_id . '/' . $ad->post_url_name); ?>"><?php echo $ad->title; ?></a>
															<h6><?php echo ($ad->price > 0) ? '$'.$ad->price : '' ?> (<?php echo $ad->address; ?>)<span class="maps">map</span><span class="pic">pic</span></h6>
														</div>
													</div>
												</div>
												<?php endforeach; ?>
											<?php endforeach; ?>
											<?php endif; ?>
										</div>
										
										<script>
											$('.list-picture img').hover(function(){
												$(this).parent().find('.big-img').show();
											}, function(){
												$(this).parent().find('.big-img').hide();
											});
										</script>
										
										<div class="tab-pane picture square" id="square">
										  <?php if(!empty($postlist)) : ?>
											<?php foreach($postlist as $t => $item) : ?>
												
											<div class="row clearfix"> 
												 <h3><?php echo date('F d, Y', strtotime($t)); ?></h3>
													
													<?php foreach($item as $ad) : ?>
													  
														<div class="col-sm-4 item">
															
															<?php if(count($ad->images) > 1) : ?>
															<div id="owl-demo" class="owl-carousel">
																<?php foreach($ad->images as $pic) : ?>
																<div class="item">
																	<img src="<?php echo asset_url() .'tn/image.php?width=280&image='. asset_url() . $pic->image_path; ?>" />
																</div>
																<?php endforeach; ?>
															</div>
															<?php else : ?>
																<img src="<?php echo 'http://amyslist.us/assets/tn/image.php?width=280&image='.asset_url() . $ad->images[0]->image_path; ?>">
															<?php endif; ?>
															
															<a href="<?php echo site_url('/viewpost/' . $ad->post_num_id . '/' . $ad->post_url_name); ?>"><?php echo $ad->title; ?></a>
														    <h6><?php echo ($ad->price > 0) ? '$'.$ad->price : '' ?> (<?php echo $ad->address; ?>) <span class="maps">map</span> <span class="pic">pic</span></h6>
																	<div class="maining">
																		<div class="top-block" id="top-detail">
																			<ul>
																				<li><a href="#"><span><i class="fa fa-envelope"></i></span><?php echo $ad->email ?></a> </li>
																				<li><a href="#"> <span><i class="fa fa-user"></i></span> Hidden </a> </li>
																				<li><a href="#"> <span><i class="fa fa-inbox"></i></span> Inbox </a> </li>
																				<li> <a href="#"> <span><i class="fa fa-comment"></i></span>Text Video Chat </a> </li>
																				<li> <a href="#"> <span><i class="fa fa-youtube-play"></i></span>Video</a> </li>
																			</ul>
																		</div>
																	</div>
														</div>
													  
														<!-- old item 
													    <div class="item">
															<?php foreach($ad->images as $img) : ?>
																<img src="<?php //echo asset_url() . $img->image_path; ?>" class="img-responsive">
															<?php endforeach; ?>
													    </div>
															
																<a href="<?php echo site_url('/viewpost/' . $ad->post_num_id . '/' . $ad->post_url_name); ?>"><?php echo $ad->title; ?></a>
														    <h6><?php echo ($ad->price > 0) ? '$'.$ad->price : '' ?> (<?php echo $ad->address; ?>) <span class="maps">map</span> <span class="pic">pic</span></h6>
																	<div class="maining">
																		<div class="top-block" id="top-detail">
																			<ul>
																				<li><a href="#"><span><img src="<?php echo asset_url() ?>img/icons/mail.png" alt="mail"> </span><?php echo $ad->email ?></a> </li>
																				<li><a href="#"> <span> <img src="<?php echo asset_url() ?>img/icons/man.png" alt="man"> </span> Hidden </a> </li>
																				<li><a href="#"> <span>  <img src="<?php echo asset_url() ?>img/icons/inbox.png" alt="inbox"> </span> Inbox </a> </li>
																				<li> <a href="#"> <span>  <img src="<?php echo asset_url() ?>img/icons/video.png" alt="video"></span>Text Video Chat </a> </li>
																				<li> <a href="#"> <span> <img src="<?php echo asset_url() ?>img/icons/video-1.png" alt="video"> </span>Video</a> </li>
																			</ul>
																		</div>
																	</div>
																	
																	<div class="btn-ctrl">
																		<button class="btn btn-primary btn-view-pic">Post Info</button>
																		<button class="btn btn-primary btn-open">View Post</button>
																		<button class="btn btn-primary btn-view-map">View Map</button>
                                    <div class="a2a_kit a2a_kit_size_12 a2a_default_style">
																			<a class="a2a_dd" href="https://www.addtoany.com/share_save"></a>
																			<a class="a2a_button_facebook"></a>
																			<a class="a2a_button_twitter"></a>
																		</div>
																	</div>
													    -->
												  
													<?php endforeach; ?>
											</div>   
											<?php endforeach; ?>
											</div>
											<?php endif; ?>
										
											

<style>
    .scrolloff {
        pointer-events: none;
    }
</style>

<script>
    $(document).ready(function () {

        // you want to enable the pointer events only on click;

        $('#map_canvas').addClass('scrolloff'); // set the pointer events to none on doc ready
        $('#tab-content-dk').on('click', function () {
            $('#map_canvas').removeClass('scrolloff'); // set the pointer events true on click
        });

        // you want to disable pointer events when the mouse leave the canvas area;

        $("#map_canvas").mouseleave(function () {
            $('#map_canvas').addClass('scrolloff'); // set the pointer events to none when mouse leaves the map area
        });
    });
</script>

<div class="tab-pane" id="map_canvas"></div>




									</div>  
								</div>
									
								</div>

							</div>   
							<script>
                              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                              ga('create', 'UA-32380720-1', 'auto');
                              ga('send', 'pageview');

                           </script>  
							</div>
									</div>
							
						</article>
					</div>
				</section>