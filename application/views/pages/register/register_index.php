				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<article class="col-lg-12 ct-signup">
							<img src="<?php echo asset_url(); ?>img/amy-login.png" class="amy-bg">
							<div class="row clearfix signup">
								
								<h2>Create An Account</h2>
								<h3>Classified ads made easy</h3>
								
								<fieldset>
									<?php echo form_open('register/submit', array('name' => 'signup-form', 'class' => 'signup-form', 'enctype' => 'multipart/form-data', 'id' => 'sign-up-form')); ?>
									
										<span id="notifier" class="notifier" style="color:#ff0000"></span>
										<div class="form-group">
										
											<div class="col-sm-12 inputgroup">
												<span class="fui fui-user"></span>
												<input type="text" placeholder="Username" id="inputEmail3" class="form-control" name="register[username]" >
												<!--<div class="sign-notification correct"><img src="img/icons/blue-cross.png">Username Available</div>-->
											</div>
											
											<div class="col-sm-12 inputgroup">
												<span class="fui fui-location"></span>
												<input type="text" placeholder="City, State or Location" id="inputEmail3" class="form-control" name="register[address]">
												<!--<div class="sign-notification correct"><img src="img/icons/blue-cross.png">Username Available</div>-->
											</div>
											
											<div class="col-sm-12 inputgroup">
												<span class="fui fui-mail"></span>
												<input type="text" placeholder="Email" id="inputEmail3" class="form-control" name="register[email]">
												<!--<div class="sign-notification correct"><img src="img/icons/blue-cross.png">Username Available</div>-->
											</div>
											
											<div class="col-sm-12 inputgroup">
												<span class="fui fui-check"></span>
												<input type="text" placeholder="Confirm Email" id="inputEmail3" class="form-control" name="register[confirm]">
												<!--<div class="sign-notification correct"><img src="img/icons/blue-cross.png">Username Available</div>-->
											</div>
											
											<div class="col-sm-12 inputgroup">
												<span class="fui fui-lock"></span>
												<input type="password" placeholder="Password" id="inputEmail3" class="form-control" name="register[password]">
												<!--<div class="sign-notification correct"><img src="img/icons/blue-cross.png">Username Available</div>-->
											</div>
											
											<div class="col-sm-12 inputgroup">
												<span class="fui fui-user"></span>
												<input type="text" placeholder="Name" id="inputEmail3" class="form-control" name="register[fullname]">
												<!--<div class="sign-notification correct"><img src="img/icons/blue-cross.png">Username Available</div>-->
											</div>
											
											<div class="col-sm-12 inputgroup clearfix social-signup">
												<div class="col-sm-6">
													<label class="checkbox">
														<input type="checkbox" value="1" name="register[share_fb]" data-toggle="checkbox" checked="checked">
														<div class="banner fb-banner"><i class="fa fa-facebook"></i> Share</div>
													</label>
													<label class="checkbox">
														<input type="checkbox" value="1" name="register[share_tw]" data-toggle="checkbox" checked="checked">
														<div class="banner tw-banner"><i class="fa fa-twitter"></i> Tweet</div>
													</label>
												</div>
												<div class="col-sm-6">
													<div class="profpic">
														<span class="fui-user"></span><a href="javascript:void(0)" id="file-chooser">Select a Profile Picture</a>
														<input type="file" style="display:none" name="profpic" id="userProfPic" >
													</div>
													<div class="facebook-sign-in">
														<a class="fb-connect" onclick="checkLoginState();" href="javascript:void(0)"><i class="fa fa-facebook-square fa-lg"></i>Sign In With Facebook</a>
													</div>
												</div>
												
											</div>
											
											<div class="col-sm-12 clearfix signup-progress"></div>
											<div class="col-sm-12">
												<p>By clicking sign up, you have read and</p>
												<p>agree to our <a href="<?php echo site_url('terms-of-use'); ?>">Amy's List Terms of Use</a></p>
											</div>
											<div class="col-sm-12 form-group inputgroup">
												<input type="submit" class="btn btn-submit" value="Sign Up" name="submit">
											</div>
											
										</div>
									<?php echo form_close(); ?>
								</fieldset>
								
							</div>
						</article>
					</div>
				</section>
				<script>
					var bar = $('.signup-progress');
					
					$('input.btn-submit').on('click', function(){
						
						
						$('form[name="signup-form"]').ajaxSubmit({
								dataType:  'json', 
								url: $('base').attr('alt') + 'register/submit/',
								beforeSend: function() {
									var percentVal = '0%';
									bar.width(percentVal)
								},
								
								uploadProgress: function(event, position, total, percentComplete) {
									var percentVal = percentComplete + '%';
									bar.width(percentVal)
								},
								
								success: processJson
						});
						
						return false;
					});
					
					
					function processJson(data)
					{
						console.log(data);
						if(data.status == 'OK')
						{
							window.location=data.redirect;
						}
					}
					
					$('#file-chooser').click(function(){
						$('input[type="file"]').click();
						return false;
					});
				</script>