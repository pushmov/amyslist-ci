				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<article class="col-lg-12 ct-signup">
							
							<div class="row clearfix signup">
								
								<h2>Login</h2>
								<p>Don't have an account? <a href="<?php echo site_url('signup'); ?>">Sign Up</a></p>
								
								
									<?php echo form_open('auth/auth/login', array('name' => 'signup-form', 'class' => 'signup-form', 'enctype' => 'multipart/form-data', 'id' => 'sign-up-form')); ?>
									
										<span id="notifier" class="notifier" style="color:#ff0000"></span>
										<div class="form-group">
										
											
											<div class="col-sm-12 inputgroup">
												<span class="fui fui-mail"></span>
												<input type="text" placeholder="Email" id="inputEmail3" class="form-control" name="email">
												<!--<div class="sign-notification correct"><img src="img/icons/blue-cross.png">Username Available</div>-->
											</div>
											
											<div class="col-sm-12 inputgroup">
												<span class="fui fui-lock"></span>
												<input type="password" placeholder="Password" id="inputEmail3" class="form-control" name="password">
												<!--<div class="sign-notification correct"><img src="img/icons/blue-cross.png">Username Available</div>-->
											</div>
											
											<div class="col-sm-12 inputgroup clearfix">
												<p style="text-align:left;"><a href="<?php echo site_url('forgot-password'); ?>">Forgot Password ?</a></p>
											</div>
											
											<div class="col-sm-12 inputgroup clearfix">
													<div class="checkbxs">
														<label class="checkbox">
															<input type="checkbox" name="remember" data-toggle="checkbox" >
															Remember me next time
														</label>
													</div>
											</div>
											
											<div class="col-sm-12 form-group inputgroup">
												
												<button type="submit" class="btn btn-submit">Login</button>
											</div>
											
										</div>
										
									<?php echo form_close(); ?>
								
								
							</div>
						</article>
					</div>
				</section>
				