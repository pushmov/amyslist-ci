				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-profile-wrapper">
							
							<div class="row clearfix profile">
								
								<div class="heading clearfix">
									<div class="col-md-6">
										
										<div class="clearfix">
											<div class="photo-wrapper" style="display:block;position:relative;cursor:pointer;">
												
													<img class="img-circle" width="110" height="110" src="<?php echo asset_url() .'tn/tn.php?w=110&h=110&src='. asset_url() . $user->profpic; ?>">
												
											</div>
											
											<div class="info-wrapper">
												<div>
													<h3><?php echo profile_name(); ?><span>
													<?php if((int) $user->like_count > 0) : ?>
														<a style="color:#90C0D8" href="<?php echo site_url('user/like'); ?>">
															<i class="fa fa-check-circle fa-lg"></i><?php echo $user->like_count; ?> likes</a>
														</span>
													<?php endif; ?>
													</h3>
												</div>
												<h5><?php echo $user->current_city; ?>, <?php echo $user->current_state; ?></h5>
												
											</div>
										</div>
										
									</div>
									<div class="col-md-6 share-wrapper">
										<ul class="list-unstyled share-btn">
											<?php if(!empty($social)) : ?>
											
											<?php foreach($social as $cols => $row_s) : ?>
											<?php if($cols == 'user_id') continue; ?>
											<?php if($row_s != '') : ?>
												<li>
													<a target="_blank" href="<?php echo $row_s; ?>">
														<img src="<?php echo asset_url(); ?>img/social/32-<?php echo $cols; ?>.png" >
													</a>
												</li>
											<?php endif; ?>	
											<?php endforeach; ?>
											<?php endif; ?>
											<?php if(count($social) < 10) : ?>
											<li><a title="Add Social Networks to your profile." href="<?php echo site_url('account/settings'); ?>">
												<img src="<?php echo asset_url(); ?>img/social/32-plus.png" >
											</a>
											</li>
											<?php endif; ?>
										</ul>
									</div>
								</div>
								
								<!-- tabs section -->
								<div id="tabs">
								
									<?php $this->load->view('pages/user/profile_nav'); ?>
									
									<div class="tab-content resp-tabs-container">
										
										<div class="tab-pane active about" id="about">
											<div class="tab-pane-heading">
												<h3>About</h3>
											</div>
											<div class="tab-pane-heading edit">
												<p><i class="fa fa-edit fa-lg"></i><a href="#" data-type="textarea" data-pk="biography" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Biography" class="xeditable<?php echo ($user->biography == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->biography == '') ? 'Tell us a little about yourself here...' : $user->biography; ?></a></p>
											</div>
											<div class="briefcase">
												<h4><i class="fa fa-briefcase fa-lg"></i><a href="#" data-type="text" data-pk="occupation" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Occupation" class="xeditable<?php echo ($user->occupation == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->occupation == '') ? 'Enter your occupation here' : $user->occupation; ?></a> at <a href="#" data-type="text" data-pk="workplace" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Workplace" class="xeditable<?php echo ($user->workplace == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->workplace == '') ? 'Enter your workplace' : '<strong>' . $user->workplace . '</strong>'; ?></a></h4>
												<h4><i class="fa fa-graduation-cap fa-lg"></i>
													Studied <a href="#" data-type="text" data-pk="major" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Major Study" class="xeditable<?php echo ($user->major == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->major == '') ? 'Enter Major Study' : $user->major ; ?></a> at <a href="#" data-type="text" data-pk="study" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter University" class="xeditable<?php echo ($user->study == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->study == '') ? 'Enter University' : '<strong>' . $user->study . '</strong>'; ?></a>
												</h4>
												<h5>Past : <a href="#" data-type="text" data-pk="past_study" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Major Study" class="xeditable<?php echo ($user->past_study == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->past_study == '') ? 'Enter Past Study' : $user->past_study; ?></a></h5>
												<h4><i class="fa fa-home fa-lg"></i>Lives in <a href="#" data-type="text" data-pk="current_city" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Current City" class="xeditable<?php echo ($user->current_city == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->current_city == '') ? 'Enter Current City' : '<strong>' . $user->current_city . '</strong>'; ?></a>, <a href="#" data-type="text" data-pk="current_state" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter State" class="xeditable<?php echo ($user->current_state == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->current_state == '') ? 'Enter State' : '<strong>' . $user->current_state . '</strong>'; ?></a> <a href="#" data-type="text" data-pk="current_country_id" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Country" class="xeditable<?php echo ($user->current_country_id == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->current_country_id == '') ? 'Enter Country' : '<strong>' . $user->current_country_id . '</strong>'; ?></a></h4>
												<h4><i class="fa fa-map-marker fa-lg"></i>From <a href="#" data-type="text" data-pk="city" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter City From" class="xeditable<?php echo ($user->city == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->city == '') ? 'Enter City From' : '<strong>' . $user->city . '</strong>'; ?></a>, <a href="#" data-type="text" data-pk="state" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter State" class="xeditable<?php echo ($user->state == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->state == '') ? 'Enter State' : '<strong>' . $user->state . '</strong>'; ?></a> <a href="#" data-type="text" data-pk="country_id" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Country" class="xeditable<?php echo ($user->country_id == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->country_id == '') ? 'Enter Country' : '<strong>'.$user->country_id . '</strong>'; ?></a></h4>
												
												<?php if((int)$user->like_count > 0) : ?>
												<h4><i class="fa fa-thumbs-o-up fa-lg"></i>Likes <a href="<?php echo site_url('user/like'); ?>"><strong><?php echo $user->like_count; ?> profile<?php echo ($user->like_count > 1) ? 's' : ''; ?></strong></a></h4>
												<?php endif; ?>
												
											</div>
										</div>
										
									</div>
								</div>
								
							</div>
						</article>
					</div>
				</section>
				
				
				<link href="<?php echo asset_url(); ?>css/bootstrap-editable.css" rel="stylesheet">
				<link href="<?php echo asset_url(); ?>css/bootstrap-editable-custom.css" rel="stylesheet">
				<script src="<?php echo asset_url(); ?>js/bootstrap-editable.js"></script>
				
				<script src="<?php echo asset_url(); ?>js/profile.js"></script>
				
				
				<form enctype="multipart/form-data" method="post" id="profpic" name="profpic" style="background:#fff">
					<input type="file" style="visibility:hidden;" name="profpic_file">
				</form>
				
				
				<div class="modal fade modal-login" id="processing-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<div class="clearfix">
									<button aria-hidden="true" style="display:none" data-dismiss="modal" class="close" type="button">�</button>
									Updating
								</div>
							</div>
							<div class="modal-body">
								<div id="notifier" style="display:none">Updating ...</div>
								<div id="progress-1" class="progress">
									<div class="bar"><span class="percent"></span></div >
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<a class="btn process" data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#processing-modal" style="display:none">process</a>