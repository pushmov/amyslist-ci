				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						<?php load_sidebar() ?>
						
						<article class="col-lg-10 right-content ct-notification-wrapper">
							
							<div class="row clearfix notification">
	
								<?php if(empty($notifications)) : ?>
								<div class="clearfix notification-list">
									No record found
								</div>
								<?php else : ?>
								<?php foreach($notifications as $n) :?>
								<div class="clearfix notification-list">
									<div class="pull-left">
										<img src="<?php echo asset_url() . $n->profpic; ?>" height="44" width="43">
									</div>
									<div class="wrap">
										<p>
											<a href="<?php echo site_url('profile/' . $n->username); ?>" class="profile-link">
												<?php echo $n->message; ?>
											</a>
											<i onclick="return deleteNotification('<?php echo $n->notification_id; ?>');" class="fa fa-times"></i>
										</p>
										<span><?php echo date('H:ia M d, Y', strtotime($n->date_added)); ?></span>
									</div>
								</div>
								<?php endforeach; ?>
								<?php endif; ?>
								
							</div>
						</article>
					</div>
				</section>
				
				<script>
					function processSuccess(data){
						
						if(data.status == 'OK')
						{
							$('span.num-notif').fadeOut(2000).remove();
							$('span.notif-circle').fadeOut(2000).remove();
						}
						
					}
					
					$(window).load(function(){
						
						setTimeout(function(){
							$.ajax({
								type: "POST",
								url: $('base').attr('alt') + "user/notifications/read/",
								data: {},
								success: processSuccess,
								dataType: 'json'
							});
						}, 2000)
						
						
					});
					
					function deleteNotification(id)
					{
						if(confirm('Are you sure want to delete this notification?'))
						{
							window.location=$('base').attr('alt') + 'user/notifications/delete/' + id;
						}
						else
						{
							return false;
						}
					}
				</script>