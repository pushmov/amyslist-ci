				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-profile-wrapper">
							
							<div class="row clearfix profile">
								
								<div class="heading clearfix">
									<div class="col-md-6">
										
										<div class="clearfix">
											<div class="photo-wrapper">
												<a href="#">
													<img class="img-circle" width="110" height="110" src="<?php echo asset_url() .'tn/tn.php?w=110&h=110&src='. asset_url() . $user->profpic; ?>">
												</a>
											</div>
											
											<div class="info-wrapper">
												<div>
													<h3><?php echo $user->first_name . ' ' . $user->last_name; ?><span>
													<?php if((int) $user->like_count > 0) : ?>
													<a style="color:#90C0D8" href="<?php echo site_url('user/' . $user->username .'/likes'); ?>">
														<i class="fa fa-check-circle fa-lg"></i><?php echo $user->like_count; ?> likes
													</a></span>
													<?php endif; ?>
													</h3>
												</div>
												<h5><?php echo $user->address; ?> <?php echo $user->current_state; ?></h5>
												
												<?php if($session['user_id'] != $user->id && is_logged_in()) : ?>
												<div class="chat-wrapper">
													<button class="btn btn-primary btn-chat btn-chat-message">Message</button>
													<button class="btn btn-primary btn-chat">Chat</button>
												</div>
												<?php endif; ?>
												
												<?php if($session['user_id'] != $user->id && is_logged_in()) : ?>
												<div class="btn-action">
													<?php if((bool) $already_liked) : ?>
													<button onclick="window.location='<?php echo site_url('user/profile/unlike/' . $user->id); ?>'" type="button" class="btn btn-primary btn-liked"><i class="fa fa-check"></i>Liked</button>
					<?php else : ?>

													<button style="background:#5398BA" type="button" onclick="window.location='<?php echo site_url('user/profile/like/' . $user->id); ?>'" class="btn btn-primary"><i class="fa fa-thumbs-o-up fa-lg"></i>Like</button>
													<?php endif; ?>
													
													<!--
													<button class="btn btn-primary"><i class="fa fa-envelope"></i>Send Message</button>
													-->
												</div>
												<?php endif; ?>
												
											</div>
										</div>
										
									</div>
									<div class="col-md-6 share-wrapper">
										<ul class="list-unstyled share-btn">
											<?php if(!empty($social)) : ?>
											<?php foreach($social as $cols => $row_s) : ?>
											<?php if($cols == 'user_id') continue; ?>
											<?php if($row_s != '') : ?>
												<li>
													<a target="_blank" href="<?php echo $row_s; ?>">
														<img src="<?php echo asset_url(); ?>img/social/32-<?php echo $cols; ?>.png" >
													</a>
												</li>
											<?php endif; ?>	
											<?php endforeach; ?>
											<?php endif; ?>
											
										</ul>
									</div>
								</div>
								
					<!-- tabs section -->
								<div id="tabs">
									<ul class="nav nav-tabs resp-tabs-list profile-tabs">
										<li class="active">
											<a href="#about" data-toggle="tab">ABOUT</a>
										</li>
										<li>
											<a href="#wall" data-toggle="tab">WALL</a>
										</li>
											<li>
					<a href="#ads-posted" data-toggle="tab">ADS POSTED</a>
										</li>
										<li>
											<a href="#video" data-toggle="tab">VIDEO</a>
										</li>
										<li>
					<a href="<?php echo site_url('user/' . $user->username .'/likes'); ?>">LIKES</a>
										</li>
										<?php if($session['user_id'] != $user->id) : ?>
										<li class="dropdown">
											<a href="#report" data-toggle="tab" data-popover="popover">
												REPORT PROFILE
											</a>
										</li>
										<?php endif; ?>
									</ul>
									
									<div class="tab-content resp-tabs-container">
										
										<div class="tab-pane active about" id="about">
											<div class="tab-pane-heading">
												<h3>About</h3>
											</div>
											<div class="tab-pane-heading edit">
												<p><i class="fa fa-edit fa-lg"></i>
													<?php echo $user->biography; ?>
												</p>
											</div>
											<div class="briefcase">
												<h4><i class="fa fa-briefcase fa-lg"></i>
													Work: <?php echo $user->occupation; ?>
													<?php echo ($user->occupation != '' && $user->workplace != '') ? ' at ' : ''; ?>
													<?php echo $user->workplace; ?>
												</h4>
												<h4><i class="fa fa-graduation-cap fa-lg"></i>
													School: <?php echo ($user->major != '') ? 'Studied' : '' ; ?>
													<?php echo $user->major; ?>
													<?php echo ($user->major != '' && $user->study != '' ) ? ' at ' : ''; ?>
													<?php echo $user->study; ?>
												</h4>
												<?php if($user->past_study != '') : ?>
												<h5>
													Past : <?php echo $user->past_study; ?>
												</h5>
												<?php endif; ?>
												
												<h4><i class="fa fa-home fa-lg"></i>
													Lives in <?php echo $user->current_city; ?><?php echo ($user->current_city != '' && $user->current_state != '') ? ', ' : ''; ?><?php echo $user->current_state; ?> <?php echo $user->current_country_id; ?>
												</h4>
												<h4><i class="fa fa-map-marker fa-lg"></i>
													From <?php echo $user->city; ?><?php echo ($user->city != '' && $user->state != '') ? ', ' : ''?><?php echo $user->state; ?> <?php echo $user->country_id; ?>
												</h4>
												
												<?php if((int)$user->like_count > 0) : ?>
												<h4><i class="fa fa-thumbs-o-up fa-lg"></i>Likes <a href="<?php echo site_url('user/'.$user->username.'/likes'); ?>"><strong><?php echo $user->like_count; ?> profile<?php echo ($user->like_count > 1) ? 's' : ''; ?></strong></a></h4>
												<?php endif; ?>
												
											</div>
										</div>
										<div class="tab-pane wall" id="wall">
											<div class="tab-pane-heading">
									<h3>Wall</h3>
											</div>
											
											<div class="comments-wrapper">
													<!-- Code added -->
                                                <div id="disqus_thread"></div>
    
	<script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = 'amyslistus'; // required: replace example with your forum shortname

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
    <!--end code added -->							
												<div class="fb-comments" data-href="<?php echo base_url() . 'profile/'.$user->username ; ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
									
											</div>
										
										</div>
									
										<div class="tab-pane" id="ads-posted">
											<div class="tab-pane-heading">
												<h3>Ads Posted</h3></h3>
											</div>
											
											<div class="tab-advt-post">
												
												<?php if(empty($ads)) :?>
												No ads posted yet
												<?php else : ?>
												<?php foreach($ads as $ad) : ?>
												<div class="advt-item">
													<h4><?php echo $ad->title; ?></h4>
													<div class="row">
														<div class="col-sm-3">
															<button type="button" class="btn btn-primary btn-edit" onclick="window.location='<?php echo site_url('ads/posted/edit/' . $ad->id); ?>'">Edit</button>
															<button type="button" class="btn btn-primary btn-delete">Delete</button>
														</div>
														<div class="col-sm-5">
															<h5>Link : <a href="<?php echo site_url('viewpost/' . $ad->post_num_id . '/' . seo_friendly($ad->title)); ?>"><?php echo site_url('viewpost/' . $ad->post_num_id . '/' . seo_friendly($ad->title)); ?></a></h5>
														</div>
														<div class="col-sm-4 inf">
														
														<?php if(time() > strtotime($ad->expired_at)) : ?>
															<span class="inactive">Expired</span>
														<?php else : ?>
															<?php if($ad->post_status == 1) : ?>
																<span>Active</span>
															<?php else : ?>
																<span class="inactive">Inactive</span>
															<?php endif; ?>
														<?php endif; ?>
															<span>Date Posted : <?php echo date('d/m/Y', strtotime($ad->created_at)); ?></span>
														</div>
													</div>
												</div>
												<?php endforeach; ?>
												<?php endif; ?>
												
											</div>
										</div>
										
										<div class="tab-pane" id="video">
											<div class="tab-pane-heading">
												<h3>Video</h3>
											</div>
											
											<div class="video-pane-wrapper">
												<div class="row clearfix">
													<div class="col-sm-6">
														<h3><i class="fa fa-user fa-lg"></i>Amy Admin</h3>
													</div>
													<?php if(!empty($video)) : ?>
													<div class="col-sm-6">
														<h3 class="clock"><i class="fa fa-clock-o fa-lg"></i>Date Posted : <?php echo date('F d, Y. H:i a', strtotime($video->created_at));?></h3>
														<?php if(is_logged_in()) : ?>
														<button type="button" onclick="window.location='<?php echo site_url('user/video/reset'); ?>'" class="btn btn-primary btn-edit-video">Edit</button>
														<?php endif; ?>
													</div>
													<?php endif; ?>
												</div>
												
												<?php if(!empty($video)) : ?>
												<div class="video-yt-wrapper">
													<?php echo $video->video_source; ?>													
												</div>
												<div class="fb-comments" data-href="<?php echo base_url() . 'profile/'.$user->username.'/video'; ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
												<?php else : ?>
												<div class="video-ct-wrapper">
													
													
												</div>
												<?php endif; ?>
											</div>
										</div>
										
										<?php if($session['user_id'] != $user->id) : ?>
										<div class="tab-pane" id="report">
											<div class="tab-pane-heading">
												<h3>Report Profile</h3>
												
											</div>
											<div>
												<ul class="list-unstyled setting-list">
													<li><a href="#">This is a fake profile</a></li>
													<li><a href="#">This user is posting spam ads</a></li>
													<li><a href="#">This user keep inboxing me span</a></li>
													<li><a href="#">This user is doing something shady</a></li>
													<li><a href="#">Block this user</a></li>
												</ul>
											</div>
										</div>
										<?php endif; ?>
										
									</div>
								</div>
								
							</div>
						</article>
					</div>
				</section>
				
				<link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>css/easy-responsive-tabs.css" />
				<script src="<?php echo asset_url(); ?>js/easyResponsiveTabs.js" type="text/javascript"></script>
				
				<link href="<?php echo asset_url(); ?>css/bootstrap-editable.css" rel="stylesheet">
				<link href="<?php echo asset_url(); ?>css/bootstrap-editable-custom.css" rel="stylesheet">
				<script src="<?php echo asset_url(); ?>js/bootstrap-editable.js"></script>
				
				<script>
					$('[data-popover="popover"]').popover({
						trigger: 'click',
						html : true,
						'placement': 'bottom',
						'content' : function(){
							return $('#popover-content').html()
						}
					});
					
					$(document).ready(function(){
						$('#tabs').easyResponsiveTabs({
								type: 'default', //Types: default, vertical, accordion           
								width: 'auto', //auto or any width like 600px
								fit: true,   // 100% fit in a container
								closed: 'accordion', // Start closed if in accordion view
								activate: function(event) { // Callback function if tab is switched
										var $tab = $(this);
										var $info = $('#tabInfo');
										var $name = $('span', $info);

										$name.text($tab.text());

										$info.show();
								}
						});
						$('#setting').hide();
						// $.fn.editable.defaults.mode = 'inline';
						$('.xeditable').editable({
							mode : 'inline'
						});
						
					});
					
					$(window).load(function(){
							
					});
					
					$('.btn-liked').hover(function(){
						$(this).html('<i class="fa fa-thumbs-o-down"></i>Unlike');
					}, function(){
						$(this).html('<i class="fa fa-check"></i>Liked');
					});
				</script>
				