				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-profile-wrapper">
							
							<div class="row clearfix profile">
								
								<div class="heading clearfix">
									<div class="col-md-6">
										
										<div class="clearfix">
											<div class="photo-wrapper" style="display:block;position:relative;cursor:pointer;">
												
													<img class="img-circle" width="110" height="110" src="<?php echo asset_url() .'tn/tn.php?w=110&h=110&src='. asset_url() . $user->profpic; ?>">
												
											</div>
											
											<div class="info-wrapper">
												<div>
													<h3><?php echo profile_name(); ?><span>
													<?php if((int) $user->like_count > 0) : ?>
														<a style="color:#90C0D8" href="<?php echo site_url('user/like'); ?>">
															<i class="fa fa-check-circle fa-lg"></i><?php echo $user->like_count; ?> likes</a>
														</span>
													<?php endif; ?>
													</h3>
												</div>
												<h5><?php echo $user->current_city; ?>, <?php echo $user->current_state; ?></h5>
												
											</div>
										</div>
										
									</div>
									<div class="col-md-6 share-wrapper">
										<ul class="list-unstyled share-btn">
											<?php if(!empty($social)) : ?>
											
											<?php foreach($social as $cols => $row_s) : ?>
											<?php if($cols == 'user_id') continue; ?>
											<?php if($row_s != '') : ?>
												<li>
													<a target="_blank" href="<?php echo $row_s; ?>">
														<img src="<?php echo asset_url(); ?>img/social/32-<?php echo $cols; ?>.png" >
													</a>
												</li>
											<?php endif; ?>	
											<?php endforeach; ?>
											<?php endif; ?>
											<?php if(count($social) < 10) : ?>
											<li><a title="Add Social Networks to your profile." href="<?php echo site_url('account/settings'); ?>">
												<img src="<?php echo asset_url(); ?>img/social/32-plus.png" >
											</a>
											</li>
											<?php endif; ?>
										</ul>
									</div>
								</div>
								
								<!-- tabs section -->
								<div id="tabs">
								
									<?php $this->load->view('pages/user/profile_nav'); ?>
									
									<div class="tab-content resp-tabs-container">
										
										
										<div class="tab-pane" id="ads-posted">
											<div class="tab-pane-heading">
												<h3>Ads Posted</h3>
											</div>
											
											<div class="tab-advt-post">
												
												<?php if(empty($ads)) :?>
												No ads posted yet
												<?php else : ?>
												<?php foreach($ads as $ad) : ?>
												<div class="advt-item">
													<h4><?php echo $ad->title; ?></h4>
													<div class="row">
														<div class="col-sm-3">
															<button type="button" class="btn btn-primary btn-edit" onclick="window.location='<?php echo site_url('ads/posted/edit/' . $ad->id); ?>'">Edit</button>
															<button type="button" class="btn btn-primary btn-delete" onclick="window.location='<?php echo site_url('user/profile/delete_ad/' . $ad->id); ?>'">Delete</button>
														</div>
														<div class="col-sm-5">
															<?php if((bool) $ad->post_status == 1) : ?>
															<h5>Link : <a href="<?php echo site_url('viewpost/' . $ad->post_num_id . '/' . seo_friendly($ad->title)); ?>"><?php echo site_url('viewpost/' . $ad->post_num_id . '/' . seo_friendly($ad->title)); ?></a></h5>
															<?php endif; ?>
														</div>
														<div class="col-sm-4 inf">
														
														<?php if($ad->expired_at == '' || TRUE) : ?>
															<?php if($ad->post_status == 1) : ?>
																<span>Active</span>
															<?php else : ?>
																<span class="inactive">Inactive</span>
															<?php endif; ?>
														<?php else : ?>
															<?php if(time() > strtotime($ad->expired_at)) : ?>
																<span class="inactive">Expired</span>
															<?php else : ?>
																<?php if($ad->post_status == 1) : ?>
																	<span>Active</span>
																<?php else : ?>
																	<span class="inactive">Inactive</span>
																<?php endif; ?>
															<?php endif; ?>
														<?php endif; ?>
															
															<span>Date Posted : <?php echo date('d/m/Y', strtotime($ad->created_at)); ?></span>
														</div>
													</div>
												</div>
												<?php endforeach; ?>
												<?php endif; ?>
												
											</div>
										</div>
										
										
									</div>
								</div>
								
							</div>
						</article>
					</div>
				</section>
				
				<link href="<?php echo asset_url(); ?>css/bootstrap-editable.css" rel="stylesheet">
				<link href="<?php echo asset_url(); ?>css/bootstrap-editable-custom.css" rel="stylesheet">
				<script src="<?php echo asset_url(); ?>js/bootstrap-editable.js"></script>
				
				<script src="<?php echo asset_url(); ?>js/profile.js"></script>
				
				<form enctype="multipart/form-data" method="post" id="profpic" name="profpic" style="background:#fff">
					<input type="file" style="visibility:hidden;" name="profpic_file">
				</form>
				
				
				<div class="modal fade modal-login" id="processing-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<div class="clearfix">
									<button aria-hidden="true" style="display:none" data-dismiss="modal" class="close" type="button">�</button>
									Updating
								</div>
							</div>
							<div class="modal-body">
								<div id="notifier" style="display:none">Updating ...</div>
								<div id="progress-1" class="progress">
									<div class="bar"><span class="percent"></span></div >
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<a class="btn process" data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#processing-modal" style="display:none">process</a>