				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-profile-wrapper">
							
							<div class="row clearfix profile">
								
								<div class="heading clearfix">
									<div class="col-md-6">
										
										<div class="clearfix">
											<div class="photo-wrapper" style="display:block;position:relative;cursor:pointer;">
												
													<img class="img-circle" width="110" height="110" src="<?php echo asset_url() .'tn/tn.php?w=110&h=110&src='. asset_url() . $user->profpic; ?>">
												
											</div>
											
											<div class="info-wrapper">
												<div>
													<h3><?php echo profile_name(); ?><span>
													<?php if((int) $user->like_count > 0) : ?>
														<a style="color:#90C0D8" href="<?php echo site_url('user/like'); ?>">
															<i class="fa fa-check-circle fa-lg"></i><?php echo $user->like_count; ?> likes</a>
														</span>
													<?php endif; ?>
													</h3>
												</div>
												<h5><?php echo $user->current_city; ?>, <?php echo $user->current_state; ?></h5>
												
											</div>
										</div>
										
									</div>
									<div class="col-md-6 share-wrapper">
										<ul class="list-unstyled share-btn">
											<?php if(!empty($social)) : ?>
											
											<?php foreach($social as $cols => $row_s) : ?>
											<?php if($cols == 'user_id') continue; ?>
											<?php if($row_s != '') : ?>
												<li>
													<a target="_blank" href="<?php echo $row_s; ?>">
														<img src="<?php echo asset_url(); ?>img/social/32-<?php echo $cols; ?>.png" >
													</a>
												</li>
											<?php endif; ?>	
											<?php endforeach; ?>
											<?php endif; ?>
											<?php if(count($social) < 10) : ?>
											<li><a title="Add Social Networks to your profile." href="<?php echo site_url('account/settings'); ?>">
												<img src="<?php echo asset_url(); ?>img/social/32-plus.png" >
											</a>
											</li>
											<?php endif; ?>
										</ul>
									</div>
								</div>
								
								<!-- tabs section -->
								<div id="tabs">
								
									<?php $this->load->view('pages/user/profile_nav'); ?>
									
									<div class="tab-content resp-tabs-container">
										
										<div class="tab-pane" id="video">
											<div class="tab-pane-heading">
												<h3>Video</h3>
											</div>
											
											<div class="video-pane-wrapper">
												<div class="row clearfix">
													<div class="col-sm-6">
														<h3><i class="fa fa-user fa-lg"></i><?php echo $user->username; ?></h3>
													</div>
													<?php if(!empty($video)) : ?>
													<div class="col-sm-6">
														<h3 class="clock"><i class="fa fa-clock-o fa-lg"></i>Date Posted : <?php echo date('F d, Y. H:i a', strtotime($video->created_at));?></h3>
														<button type="button" onclick="window.location='<?php echo site_url('user/video/reset'); ?>'" class="btn btn-primary btn-edit-video">Edit</button>
													</div>
													<?php endif; ?>	
												</div>
												
												<?php if(!empty($video)) : ?>
												<div class="video-yt-wrapper">
													<?php echo $video->video_source; ?>
												</div>
												<?php else : ?>
												<div class="video-ct-wrapper">
													
													<div class="vid-player">
														
														<form method="post" name="video" action="<?php echo site_url('user/video/save'); ?>">
														<div class="video-play-desc">
															<h3>Record a Video Bio or Video Resume</h3>
															<p>Broadcast yourself with more than just pictures. Create a Video Bio / Resume on YouTube at: https://www.youtube.com/my_webcam</p>
															<p>Then copy &amp; paste: <input type="text" class="input" name="source" placeholder="Your YouTube Video Bio / Resume Link Here"></p>
															<p>It's your window to the world. Tell us a little about who your are. People can watch your Video Bio and be confident with the ads you post on Amy's List. Or send potential employers a link to your Video Resume for a job you apply for on Amy's List.</p>
															<div class="share">
																<a href="#"><i class="fa fa-share-alt fa-lg"></i>Share</a>
																<button class="btn btn-primary btn-fb"><i class="fa fa-facebook fa-lg"></i>Like</button>
																<button class="btn btn-primary btn-tw"><i class="fa fa-twitter fa-lg"></i>Tweet</button>
															</div>
														</div>
														</form>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</div>
										
									</div>
								</div>
								
							</div>
						</article>
					</div>
				</section>
				
				
				<link href="<?php echo asset_url(); ?>css/bootstrap-editable.css" rel="stylesheet">
				<link href="<?php echo asset_url(); ?>css/bootstrap-editable-custom.css" rel="stylesheet">
				<script src="<?php echo asset_url(); ?>js/bootstrap-editable.js"></script>
				
				<script src="<?php echo asset_url(); ?>js/profile.js"></script>
				
				<form enctype="multipart/form-data" method="post" id="profpic" name="profpic" style="background:#fff">
					<input type="file" style="visibility:hidden;" name="profpic_file">
				</form>
				
				
				<div class="modal fade modal-login" id="processing-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<div class="clearfix">
									<button aria-hidden="true" style="display:none" data-dismiss="modal" class="close" type="button">�</button>
									Updating
								</div>
							</div>
							<div class="modal-body">
								<div id="notifier" style="display:none">Updating ...</div>
								<div id="progress-1" class="progress">
									<div class="bar"><span class="percent"></span></div >
								</div>
							</div>
						</div>
						
					</div>
				</div>
				
				<a class="btn process" data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#processing-modal" style="display:none">process</a>