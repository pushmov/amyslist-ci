				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ">
							
							<div class="wrapper-body account-setting">
								<h2>Account Settings</h2>
								
								<h4>Profile Pic</h4>
									
									<div class="form-group">
												
										<div class="fileinput fileinput-new clearfix" data-provides="fileinput">
											<div class="col-sm-12">
												<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="float:left;margin-right:15px;background:transparent;border:none;max-height:100px;max-width:100px;">
													<img src="<?php echo asset_url() . $user->profpic ?>" width="100" height="100" class="img-circle" style="border:2px solid #ECEEF0">
												</div>
													<div style="padding-top:30px">
														<form enctype="multipart/form-data" method="post" id="profpic" name="profpic">
															<span class="btn btn-primary btn-embossed btn-file btn-file-wrapper">
															<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
															<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
															<input type="file" name="profpic_file" id="register[profpic]" >
															
															</span>
														</form>
													</div>
													<a href="#" class="btn btn-primary btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
											</div>
										</div>
									</div>
									
									<h4>About Info</h4>
									<div class="control-group span6">
										<textarea class="span12 about-text" placeholder="Tell us a little about yourself here..."><?php echo $user->biography; ?></textarea>
											<span class="username"></span>
													
										<!--<i class="input-icon fui-check-inverted"></i>-->
									</div>
									<div class="control-group">
										<button class="btn btn-primary btn-setting" id="btn-about">Save Info</button>
										<span class="notifier" style="margin-left:10px;"></span>
									
									</div>
									
									<h4>Social Media Icons</h4>
									<div class="clearfix">
										<ul class="list-unstyled social-info">
											<li><a alt="facebook" href="javascript:void(0)"><img src="<?php echo asset_url(); ?>img/social/64-facebook.png"></a></li>
											<li><a alt="twitter" href="javascript:void(0)"><img src="<?php echo asset_url(); ?>img/social/64-twitter.png"></a></li>
											<li><a alt="linkedin" href="javascript:void(0)"><img src="<?php echo asset_url(); ?>img/social/64-linkedin.png"></a></li>
											<li><a alt="pinterest" href="javascript:void(0)"><img src="<?php echo asset_url(); ?>img/social/64-pinterest.png"></a></li>
											<li><a alt="instagram" href="javascript:void(0)"><img src="<?php echo asset_url(); ?>img/social/64-instagram.png"></a></li>
											<li><a alt="gplus" href="javascript:void(0)"><img src="<?php echo asset_url(); ?>img/social/64-googleplus.png"></a></li>
											<li><a alt="tumblr" href="javascript:void(0)"><img src="<?php echo asset_url(); ?>img/social/64-tumblr.png"></a></li>
											<li><a alt="vimeo" href="javascript:void(0)"><img src="<?php echo asset_url(); ?>img/social/64-vimeo.png"></a></li>
											<li><a alt="youtube" href="javascript:void(0)"><img src="<?php echo asset_url(); ?>img/social/64-youtube.png"></a></li>
											<li><a alt="skype" href="javascript:void(0)"><img src="<?php echo asset_url(); ?>img/social/64-skype.png"></a></li>
								</ul>
										
									</div>
									<div>
										<div class="clearfix">
											<span class="social-error" style="color:#ff0000"></span>
											<div class="control-group social-input clearfix">
												<input type="text" class="col-sm-6" value="<?php echo (isset($social->facebook)) ? $social->facebook : ''; ?>" placeholder="Enter your facebook link here" name="social[facebook]">
											</div>
											<div class="control-group social-input clearfix">
												<input type="text" class="col-sm-6" value="<?php echo (isset($social->twitter)) ? $social->twitter : ''; ?>" placeholder="Enter your twitter link here" name="social[twitter]">
											</div>
											<div class="control-group social-input clearfix">
												<input type="text" class="col-sm-6" value="<?php echo (isset($social->linkedin)) ? $social->linkedin : ''; ?>" placeholder="Enter your linkedin link here" name="social[linkedin]">
											</div>
											<div class="control-group social-input clearfix">
												<input type="text" class="col-sm-6" value="<?php echo (isset($social->pinterest)) ? $social->pinterest : ''; ?>" placeholder="Enter your pinterest link here" name="social[pinterest]">
											</div>
											<div class="control-group social-input clearfix">
												<input type="text" class="col-sm-6" value="<?php echo (isset($social->instagram)) ? $social->instagram : ''; ?>" placeholder="Enter your instagram link here" name="social[instagram]">
											</div>
											<div class="control-group social-input clearfix">
												<input type="text" class="col-sm-6" value="<?php echo (isset($social->gplus)) ? $social->gplus : ''; ?>" placeholder="Enter your gplus link here" name="social[gplus]">
											</div>
											<div class="control-group social-input clearfix">
												<input type="text" class="col-sm-6" value="<?php echo (isset($social->tumblr)) ? $social->tumblr : ''; ?>" placeholder="Enter your tumblr link here" name="social[tumblr]">
											</div>
											<div class="control-group social-input clearfix">
												<input type="text" class="col-sm-6" value="<?php echo (isset($social->vimeo)) ? $social->vimeo : ''; ?>" placeholder="Enter your vimeo link here" name="social[vimeo]">
											</div>
											<div class="control-group social-input clearfix">
												<input type="text" class="col-sm-6" value="<?php echo (isset($social->youtube)) ? $social->youtube : ''; ?>" placeholder="Enter your youtube link here" name="social[youtube]">
											</div>
											<div class="control-group social-input clearfix">
												<input type="text" class="col-sm-6" value="<?php echo (isset($social->skype)) ? $social->skype : ''; ?>" placeholder="Enter your skype link here" name="social[skype]">
											</div>
											
											<div class="control-group social-input button clearfix">
												<button class="btn btn-primary btn-setting btn-links">Save Links</button>
												<span class="social_notifier" style="margin-left:10px;"></span>
											</div>
										</div>
									</div>
									
									<h4>Turn Chat On and Off</h4>
									
									<div class="clearfix chat-settings">
										<div class="control-group">
											<input type="checkbox" data-toggle="switch" <?php echo ($user->notify_ads_posted == '1') ? 'checked' : ''; ?> name="notification[ads-posted]"/>
											<span></span>
										</div>
									
									<h4>Change Password</h4>
									<div class="clearfix">
										<div class="control-group">
											<input type="password" placeholder="Old Password" name="password[old]">
												<span class="old_notifier" style="margin-left:10px;"></span>
										</div>
										
										<div class="control-group">
											<input type="password" placeholder="Enter New Password" name="password[new]">
											<span class="new_notifier" style="margin-left:10px;"></span>
										</div>
										
										<div class="control-group">
											<input type="password" placeholder="Enter New Password Again" name="password[repeat]">
											<span class="repeat_notifier" style="margin-left:10px;"></span>
										</div>
										
										<div class="control-group">
											<button class="btn btn-primary btn-setting btn-password">Save Password</button>
											<span class="update_pass_notif" style="margin-left:10px;"></span>
										</div>
									</div>
									
									<h4>Select a City Page</h4>
									<div class="clearfix">
										<div class="control-group">
										</div>
									
									<h4>Email Notifications</h4>
									
									<div class="clearfix account-notifications">
										<div class="control-group">
											<input type="checkbox" data-toggle="switch" <?php echo ($user->notify_ads_posted == '1') ? 'checked' : ''; ?> name="notification[ads-posted]"/>
											<span>Ads Posted Notifications</span>
										</div>
										<div class="control-group">
											<input type="checkbox" data-toggle="switch" <?php echo ($user->notify_like == '1') ? 'checked' : ''; ?>  name="notification[likes]" />
											<span>New "Likes"</span>
										</div>
										<div class="control-group">
											<input type="checkbox" data-toggle="switch" <?php echo ($user->notify_inbox_message == '1') ? 'checked' : ''; ?>  name="notification[inbox]" />
											<span>Inbox Messages</span>
										</div>
										
									</div>
									
									<h4>Report a Problem</h4>
									<h3>Email : <span>problem@amyslist.us</span></h3>
									
									<div style="padding:20px 0"><a style="color:#ff0000;font-size:15px;" class="delete-account-t" href="javascript:void(0)">Delete Account</a></div>
									
									<div class="delete-account" style="display:none">
										<i class="fa fa-times fa-lg"></i>
										<h2>Delete My Account</h2>
										<p>I would like to <span>delete my account</span>. I understand by deleting my account all my info will be lost.</p>
										<p>And If I want to post ads on Amy's List in the future I will have to create a new account</p>
										<div>
											<button onclick="if(confirm('Are you sure want to delete account?')) window.location='<?php echo site_url('user/settings/drop_account/'); ?>' " class="btn btn-danger">Yes! Delete My Account</button>
											<button class="btn btn-primary btn-remove">No Way! I love Amy's List</button>
										</div>
									</div>
									
									
									<button class="btn btn-primary btn-all-save">SAVE ALL SETTINGS</button>
								
								
							</div>
							
						</article>
					</div>
				</section>
				
				<script>
					$("input[type='checkbox']").bootstrapSwitch();
					
					$('ul.social-info li a').click(function(){
						
						$('.social-input').hide();
						$('.button').hide();
						
						$('ul.social-info li a').find('i').remove();
						$(this).append('<i class="fa fa-check-circle checked"></i>');
						
						var target = $(this).attr('alt');
						
						$('input[name="social['+target+']"]').parent().fadeIn();
						$('.button').fadeIn();
					});
					
					$('.delete-account-t').click(function(){
						$('.delete-account').fadeIn('fast');
					});
					
					$('.delete-account i.fa-times').click(function(){
						$('.delete-account').fadeOut('fast');
					});
					
					$('.btn-remove').click(function(){
						$('.delete-account').fadeOut('fast');
					});
					
					$('input[name="profpic"]').change(function(){
						//do upload
						
						
					});
					
				</script>
				
				<div class="modal fade modal-login" id="processing-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<div class="clearfix">
									<button aria-hidden="true" style="display:none" data-dismiss="modal" class="close" type="button">�</button>
									Updating
								</div>
							</div>
							<div class="modal-body">
								<div id="notifier" style="display:none">Updating ...</div>
								<div id="progress-1" class="progress">
									<div class="bar"><span class="percent"></span></div >
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<a class="btn process" data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#processing-modal" style="display:none">process</a>
				
				
				<script>
					$(document).ready(function(){
					
						$('input[name="profpic_file"]').change(function(){
							// alert('ahs');
							var bar = $('.bar');
							if($(this).val() != '')
							{
								$('a.process').click();
								$('form[name="profpic"]').ajaxSubmit({
									dataType:  'json', 
									url: $('base').attr('alt') + 'user/profile/change_pic/',
									beforeSend: function() {
										var percentVal = '0%';
										bar.width(percentVal)
									},
									
									uploadProgress: function(event, position, total, percentComplete) {
										var percentVal = percentComplete + '%';
										bar.width(percentVal)
									},
									
									success: processJson
								});
							
								return false;
							}
						});
					});
					
					
					function processJson(data)
					{
						
						$('.img-circle').attr('src', data.src);
						$('.profpic-sidebar').attr('src', data.src);
						$('button.close').click();
					}
					
					$('#btn-about').click(function(){
						var val = $('textarea.about-text').val();
						
						$('.notifier').html('Updating...').css({color: '#264E90'});
						
						if(val != '')
						{
							setTimeout(function(){
								$.post( $('base').attr('alt') + 'user/settings/biography', 
									{
										biography : val,
									}, function(data){
										
										var j = $.parseJSON(data);
										
										if(j.status == 'OK')
										{
											$('.notifier').html(j.message);
											
											setTimeout(function(){
												$('.notifier').html('');
											}, 2000);
										}
										
									} )
							}, 3000);
							
						}
						
					});
					
					function isValidURL(url){
						if(url == '') return true;
						var reg = /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
						return reg.test(url)
					}
					
					$('.btn-links').click(function(){
					
						var facebook = $('input[name="social[facebook]"]').val();
						var twitter = $('input[name="social[twitter]"]').val();
						var linkedin = $('input[name="social[linkedin]"]').val();
						var pinterest = $('input[name="social[pinterest]"]').val();
						var instagram = $('input[name="social[instagram]"]').val();
						var gplus = $('input[name="social[gplus]"]').val();
						var tumblr = $('input[name="social[tumblr]"]').val();
						var vimeo = $('input[name="social[vimeo]"]').val();
						var youtube = $('input[name="social[youtube]"]').val();
						var skype = $('input[name="social[skype]"]').val();
						
						
						$('.social-error').hide();
						if(!isValidURL(facebook) || !isValidURL(twitter) || !isValidURL(linkedin) || !isValidURL(pinterest) || !isValidURL(instagram) 
						|| !isValidURL(gplus) || !isValidURL(tumblr) || !isValidURL(vimeo) || !isValidURL(youtube))
						{
							$('.social-error').html('Invalid URL.. Please complete the URL with valid protocol ex. http://example.com/').show();
							return false;
						}
						
						$('.social_notifier').html('Updating...').css({color: '#264E90'});
						
						setTimeout(function(){
							$.post($('base').attr('alt') + 'user/settings/social', 
								{
									facebook : facebook,
									twitter : twitter,
									linkedin : linkedin,
									pinterest : pinterest,
									instagram : instagram,
									gplus : gplus,
									tumblr : tumblr,
									vimeo : vimeo,
									youtube : youtube,
									skype : skype
								}).done(function(data){
									
									var j = $.parseJSON(data);
										
									if(j.status == 'OK')
									{
										$('.social_notifier').html(j.message);
											
										setTimeout(function(){
											$('.social_notifier').html('').css({color: '#264E90'});
										}, 2000);
									}
									
								});
						}, 3000);
					});
					
					$('input[name="password[old]"]').bind('keyup', function(){
						$.get($('base').attr('alt') + 'user/settings/checkpassword/' + $(this).val())
							.done(function(data){
								
								var j = $.parseJSON(data);
								if(j.result == '1')
								{
									$('.old_notifier').html(j.message).css({color: '#264E90'});
								}
								else
								{
									$('.old_notifier').html('');
								}
								
							})
					});
					
					$('input[name="password[new]"]').bind('keyup', function(){
						if($(this).val().length > 4)
						{
							$('.new_notifier').html('<i class="fa fa-check-circle fa-lg">').css({color: '#264E90'});
						}
						else
						{
							$('.new_notifier').html('');
						}
					});
					
					
					$('input[name="password[repeat]"]').bind('keyup', function(){
						if($(this).val().length > 4 && $(this).val() == $('input[name="password[new]"]').val())
						{
							$('.repeat_notifier').html('<i class="fa fa-check-circle fa-lg">').css({color: '#264E90'});
						}
						else
						{
							$('.repeat_notifier').html('');
						}
					});
					
					$('.btn-password').click(function(){
						
						var old = $('input[name="password[old]"]').val();
						var newpass = $('input[name="password[new]"]').val();
						var repeat = $('input[name="password[repeat]"]').val();
						
						$.get($('base').attr('alt') + 'user/settings/checkpassword/' + old)
							.done(function(data){
								
								var j = $.parseJSON(data);
								if(j.result == '1')
								{
									$('.update_pass_notif').html('Updating...').css({color: '#264E90'});
									if(newpass == repeat)
									{
										setTimeout(function(){
											$.post($('base').attr('alt') + 'user/settings/updatepassword/', {
												old : old,
												newpass : newpass,
												repeat : repeat
											}).done(function(data){
												
												var r = $.parseJSON(data);
												if(r.status == '1')
												{
													$('.update_pass_notif').html(r.message).css({color: '#264E90'});
													
													setTimeout(function(){
														$('.update_pass_notif').html('');
													}, 2000);
												}
												else
												{
													$('.update_pass_notif').html('');
												}
												
											});
										}, 3000)
										
									}
									
								}
								else
								{
									return false;
								}
								
							});
						
					});
					
					$('input[name="notification[ads-posted]"]').on('switchChange.bootstrapSwitch', function(event, state){
						
						$.post($('base').attr('alt') + 'user/settings/notifications_adposted',{
							notify_ads_posted : state
						}).done(function(data){
							console.log(data);
						});
					});
					
					$('input[name="notification[likes]"]').on('switchChange.bootstrapSwitch', function(event, state){
						
						$.post($('base').attr('alt') + 'user/settings/notifications_likes',{
							notify_like : state
						}).done(function(data){
							console.log(data);
						});
					});
					
					$('input[name="notification[inbox]"]').on('switchChange.bootstrapSwitch', function(event, state){
						
						$.post($('base').attr('alt') + 'user/settings/notifications_inbox',{
							notify_inbox_message : state
						}).done(function(data){
							console.log(data);
						});
					});
					
					$('input[name="notification[chatsetting]"]').on('switchChange.bootstrapSwitch', function(event, state){
						
						$.post($('base').attr('alt') + 'user/settings/notifications_chat',{
							notify_chat : state
						}).done(function(data){
							console.log(data);
						});
					});
					
					
				</script>