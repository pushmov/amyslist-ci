								<div class="heading clearfix">
									<div class="col-md-6">
										
										<div class="clearfix">
											<div class="photo-wrapper">
												
													<img class="img-circle" width="110" height="110" src="<?php echo asset_url() .'tn/tn.php?w=110&h=110&src='. asset_url() . $user->profpic; ?>">
												
											</div>
											
											<div class="info-wrapper">
												<div>
													<h3><?php echo $user->first_name . ' ' . $user->last_name; ?><span>
													<?php if((int) $user->like_count > 0) : ?>
														<a style="color:#90C0D8" href="<?php echo site_url('user/' . $user->username .'/likes'); ?>">
															<i class="fa fa-check-circle fa-lg"></i><?php echo $user->like_count; ?> likes</a>
														</span>
													<?php endif; ?>
													</h3>
												</div>
												<h5><?php echo $user->current_city; ?>, <?php echo $user->current_state; ?></h5>
												
												<?php if($session['user_id'] != $user->id && is_logged_in()) : ?>
												<div class="chat-wrapper">
													<button class="btn btn-primary btn-chat btn-chat-message">Message</button>
													<button class="btn btn-primary btn-chat">Chat</button>
													
													<?php if($session['user_id'] != $user->id && is_logged_in()) : ?>
													<div class="btn-action" style="display:inline">
														<?php if((bool) $already_liked) : ?>
														<button onclick="window.location='<?php echo site_url('user/profile/unlike/' . $user->id); ?>'" type="button" class="btn btn-primary btn-liked"><i class="fa fa-check"></i>Liked</button>
														<?php else : ?>
														<button style="background:#5398BA" type="button" onclick="window.location='<?php echo site_url('user/profile/like/' . $user->id); ?>'" class="btn btn-primary"><i class="fa fa-thumbs-o-up fa-lg"></i>Like</button>
														<?php endif; ?>
													</div>
													<?php endif; ?>
													
												</div>
												<?php endif; ?>
												
												
											</div>
										</div>
										
									</div>
									<div class="col-md-6 share-wrapper">
										<ul class="list-unstyled share-btn">
											<?php if(!empty($social)) : ?>
											
											<?php foreach($social as $cols => $row_s) : ?>
											<?php if($cols == 'user_id') continue; ?>
											<?php if($row_s != '') : ?>
												<li>
													<a target="_blank" href="<?php echo $row_s; ?>">
														<img src="<?php echo asset_url(); ?>img/social/32-<?php echo $cols; ?>.png" >
													</a>
												</li>
											<?php endif; ?>	
											<?php endforeach; ?>
											<?php endif; ?>
											
										</ul>
									</div>
								</div>