				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-profile-wrapper">
							
							<div class="row clearfix profile">
								
								<?php $this->load->view('pages/user/public/profile_heading'); ?>
								
								<!-- tabs section -->
								<div id="tabs">
								
									<?php $this->load->view('pages/user/public/profile_nav'); ?>
									
									<div class="tab-content resp-tabs-container">
										
										<div class="tab-pane" id="report">
											<div class="tab-pane-heading">
												<h3>Report Profile</h3>
												
											</div>
											<div>
												<ul class="list-unstyled setting-list">
													<li><a href="#">This is a fake profile</a></li>
													<li><a href="#">This user is posting spam ads</a></li>
													<li><a href="#">This user keep inboxing me span</a></li>
													<li><a href="#">This user is doing something shady</a></li>
													<li><a href="#">Block this user</a></li>
												</ul>
											</div>
										</div>
										
									</div>
								</div>
								
							</div>
						</article>
					</div>
				</section>
				
				