				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-profile-wrapper">
							
							<div class="row clearfix profile">
								
								<?php $this->load->view('pages/user/public/profile_heading'); ?>
								
								<!-- tabs section -->
								<div id="tabs">
								
									<?php $this->load->view('pages/user/public/profile_nav'); ?>
									
									<div class="tab-content resp-tabs-container">
										
										<div class="tab-pane" id="video">
											<div class="tab-pane-heading">
												<h3>Video</h3>
											</div>
											
											<div class="video-pane-wrapper">
												<div class="row clearfix">
													<div class="col-sm-6">
														<h3><i class="fa fa-user fa-lg"></i><?php echo $user->username; ?></h3>
													</div>
													<?php if(!empty($video)) : ?>
													<div class="col-sm-6">
														<h3 class="clock"><i class="fa fa-clock-o fa-lg"></i>Date Posted : <?php echo date('F d, Y. H:i a', strtotime($video->created_at));?></h3>
														
													</div>
													<?php endif; ?>
												</div>
												
												<?php if(!empty($video)) : ?>
												<div class="video-yt-wrapper">
													<?php echo $video->video_source; ?>													
												</div>
												<div class="fb-comments" data-href="<?php echo base_url() . 'profile/'.$user->username.'/video'; ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
												<?php else : ?>
												<div class="video-ct-wrapper">
													
													
												</div>
												<?php endif; ?>
											</div>
										</div>
										
									</div>
								</div>
								
							</div>
						</article>
					</div>
				</section>
				
				