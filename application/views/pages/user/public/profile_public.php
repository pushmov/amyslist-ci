				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-profile-wrapper">
							
							<div class="row clearfix profile">
								
								<?php $this->load->view('pages/user/public/profile_heading'); ?>
								
								<!-- tabs section -->
								<div id="tabs">
								
									<?php $this->load->view('pages/user/public/profile_nav'); ?>
									
									<div class="tab-content resp-tabs-container">
										
										<div class="tab-pane active about" id="about">
											<div class="tab-pane-heading">
												<h3>About</h3>
											</div>
											<div class="tab-pane-heading edit">
												<p><i class="fa fa-edit fa-lg"></i>
													<?php echo $user->biography; ?>
												</p>
											</div>
											<div class="briefcase">
												<h4><i class="fa fa-briefcase fa-lg"></i>
													Work: <?php echo $user->occupation; ?>
													<?php echo ($user->occupation != '' && $user->workplace != '') ? ' at ' : ''; ?>
													<?php echo $user->workplace; ?>
												</h4>
												<h4><i class="fa fa-graduation-cap fa-lg"></i>
													School: <?php echo ($user->major != '') ? 'Studied' : '' ; ?>
													<?php echo $user->major; ?>
													<?php echo ($user->major != '' && $user->study != '' ) ? ' at ' : ''; ?>
													<?php echo $user->study; ?>
												</h4>
												<?php if($user->past_study != '') : ?>
												<h5>
													Past : <?php echo $user->past_study; ?>
												</h5>
												<?php endif; ?>
												
												<h4><i class="fa fa-home fa-lg"></i>
													Lives in <?php echo $user->current_city; ?><?php echo ($user->current_city != '' && $user->current_state != '') ? ', ' : ''; ?><?php echo $user->current_state; ?> <?php echo $user->current_country_id; ?>
												</h4>
												<h4><i class="fa fa-map-marker fa-lg"></i>
													From <?php echo $user->city; ?><?php echo ($user->city != '' && $user->state != '') ? ', ' : ''?><?php echo $user->state; ?> <?php echo $user->country_id; ?>
												</h4>
												
												<?php if((int)$user->like_count > 0) : ?>
												<h4><i class="fa fa-thumbs-o-up fa-lg"></i>Likes <a href="<?php echo site_url('user/'.$user->username.'/likes'); ?>"><strong><?php echo $user->like_count; ?> profile<?php echo ($user->like_count > 1) ? 's' : ''; ?></strong></a></h4>
												<?php endif; ?>
												
											</div>
										</div>
										
									</div>
								</div>
								
							</div>
						</article>
					</div>
				</section>
				
				