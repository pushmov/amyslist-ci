				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-profile-wrapper">
							
							<div class="row clearfix profile">
								
								<?php $this->load->view('pages/user/public/profile_heading'); ?>
								
								<!-- tabs section -->
								<div id="tabs">
								
									<?php $this->load->view('pages/user/public/profile_nav'); ?>
									
									<div class="tab-content resp-tabs-container">
										
										
										<div class="tab-pane" id="ads-posted">
											<div class="tab-pane-heading">
												<h3>Ads Posted</h3>
											</div>
											
											<div class="tab-advt-post">
												
												<?php if(empty($ads)) :?>
												No ads posted yet
												<?php else : ?>
												<?php foreach($ads as $ad) : ?>
												<div class="advt-item">
													<h4><?php echo $ad->title; ?></h4>
													<div class="row">
														<div class="col-sm-3">
															<button type="button" class="btn btn-primary btn-edit" onclick="window.location='<?php echo site_url('ads/posted/edit/' . $ad->id); ?>'">Edit</button>
															<button type="button" class="btn btn-primary btn-delete">Delete</button>
														</div>
														<div class="col-sm-5">
															<h5>Link : <a href="<?php echo site_url('viewpost/' . $ad->post_num_id . '/' . seo_friendly($ad->title)); ?>"><?php echo site_url('viewpost/' . $ad->post_num_id . '/' . seo_friendly($ad->title)); ?></a></h5>
														</div>
														<div class="col-sm-4 inf">
														
														<?php if($ad->expired_at == '' || TRUE) : ?>
															<?php if($ad->post_status == 1) : ?>
																<span>Active</span>
															<?php else : ?>
																<span class="inactive">Inactive</span>
															<?php endif; ?>
														<?php else : ?>
															<?php if(time() > strtotime($ad->expired_at)) : ?>
																<span class="inactive">Expired</span>
															<?php else : ?>
																<?php if($ad->post_status == 1) : ?>
																	<span>Active</span>
																<?php else : ?>
																	<span class="inactive">Inactive</span>
																<?php endif; ?>
															<?php endif; ?>
														<?php endif; ?>
														
															<span>Date Posted : <?php echo date('d/m/Y', strtotime($ad->created_at)); ?></span>
														</div>
													</div>
												</div>
												<?php endforeach; ?>
												<?php endif; ?>
												
											</div>
										</div>
										
										
									</div>
								</div>
								
							</div>
						</article>
					</div>
				</section>
				