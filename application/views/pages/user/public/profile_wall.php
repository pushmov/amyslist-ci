				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-profile-wrapper">
							
							<div class="row clearfix profile">
								
								<?php $this->load->view('pages/user/public/profile_heading'); ?>
								
								<!-- tabs section -->
								<div id="tabs">
								
									<?php $this->load->view('pages/user/public/profile_nav'); ?>
									
									<div class="tab-content resp-tabs-container">
										
										<div class="tab-pane wall" id="wall">
											<div class="tab-pane-heading">
												<h3>Wall</h3>
											</div>
											
											<div class="comments-wrapper">
													<!-- Code added -->
                                                					
												<div class="fb-comments" data-href="<?php echo base_url() . 'profile/'.$user->username .'/wall'; ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
									
											</div>
										
										</div>
										
										
									</div>
								</div>
								
							</div>
						</article>
					</div>
				</section>
				