				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-like-page">
							
							<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Footer Ad Amy's List -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3122936194507304"
     data-ad-slot="7189162304"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<br>				
							<div class="clearfix like-page-wrapper">
								
								<div class="clearfix breadcumb-wrapper">
									<ul class="list-unstyled breadcumb">
										<li><a href="#"><?php echo $user->first_name . ' ' . $user->last_name; ?></a></li>
										<li><a href="#">/</a></li>
										<li><a href="#">Likes</a></li>
									</ul>
								</div>
									
								<div class="col-sm-6 col-1">
									
									<div class="panel-wrapper">
										<div class="panel">
											<h3><span><strong><?php echo $all_likes; ?> Users</strong> <small>liked</small> </span><i class="fa fa-thumbs-o-up fa-lg"></i><?php echo $user->first_name . ' ' . $user->last_name; ?></span></h3>
										</div>
									</div>
									
									<div class="clearfix profiles">
										<div class="profiles-col">
											<?php foreach($liked as $row_like) : ?>
											<div class="person-like clearfix">
												<div class="pull-left"><img src="<?php echo asset_url() . $row_like->profpic; ?>" width="45" height="45" style="border:1px solid #d2d2d2"></div>
												<div class="name">
													<h3><a style="color:#024883" href="<?php echo site_url('profile/' . $row_like->username); ?>"><?php echo $row_like->first_name . ' ' . $row_like->last_name; ?></a> <small style="font-size:15px;color:#111">liked</small> <a style="color:#024883" href="<?php echo site_url('user/profile'); ?>"><?php echo $user->first_name . ' ' . $user->last_name; ?></a></h3>
													<h4><?php echo $row_like->address; ?> <?php echo ($row_like->country_id != '') ? ', ' . $row_like->country_id : ''; ?></h4>
												</div>
											</div>
											<?php endforeach; ?>
											
										</div>
										
										<!--
										<div class="more">
											<a href="#">Show More</a>
										</div>
										-->
									</div>
								</div>
								
								
								<div class="col-sm-6 col-2">
									<div class="panel-wrapper">
										<div class="panel" style="min-height:46px">
											<?php if(sizeof($all_liked) >= 1) : ?>
											<h3><span><i class="fa fa-thumbs-o-up fa-lg"></i><?php echo $user->first_name . ' ' . $user->last_name; ?></span> <small>likes</small> <strong><?php echo $all_liked; ?> Users</strong></h3>
											<?php endif; ?>
										</div>
									</div>
									
									<div class="clearfix profiles">
										<div class="profiles-col">
											<?php foreach($likes as $row_likes): ?>
											<div class="person-like clearfix">
												<div class="pull-left"><img src="<?php echo asset_url() . $row_likes->profpic; ?>" width="45" height="45" style="border:1px solid #d2d2d2"></div>
												<div class="name">
													<h3><a style="color:#024883" href="<?php echo site_url('user/profile'); ?>"><?php echo $user->first_name . ' ' . $user->last_name; ?></a> <small style="font-size:15px;color:#111">likes</small>  <a style="color:#024883" href="<?php echo site_url('profile/' . $row_likes->username); ?>"><?php echo $row_likes->first_name . ' ' . $row_likes->last_name; ?></a></h3>
													<h4><?php echo $row_likes->address; ?> <?php echo ($row_likes->country_id != '') ? ', ' . $row_likes->country_id : ''; ?></h4>
												</div>

											</div>
											<?php endforeach; ?>
										</div>
										<!--
										<div class="more">
											<a href="#">Show More</a>
										</div>
										-->
										
									</div>
								</div>
								
							</div>
						
							
						</article>
					</div>
				</section>