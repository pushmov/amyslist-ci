				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-payment">
							
							<div class="payment-block wrapper-body">
								<h2>Forgot Password</h2>
								
								<?php echo form_open('auth/auth/forgot', array('name' => 'forgot_password', 'style' => 'min-height:500px;')); ?>
									<fieldset>
										<div class="form-group clearfix">
											<div class="col-sm-4 holder email-forgot-password">
												<input type="text" name="forgot[email]" placeholder="Enter Your Email Address">
												<i class="fa fa-check-circle fa-lg fa-ok" style="margin-left:10px;color:#004A8B;display:none"></i>
												<i class="fa fa-times-circle fa-lg fa-nok" style="margin-left:10px;color:#ff0000;display:none"></i>
												
											</div>
											<div class="col-sm-7 holder">
											</div>
										</div>
										<div class="form-group clearfix button-group">
											<div class="col-sm-2">
												<button type="submit" class="btn btn-primary btn-continue">Continue</button>
											</div>
											<div class="col-sm-8"></div>
										</div>
										
									</fieldset>
								<?php echo form_close(); ?>
								
							</div>
						</article>
					</div>
				</section>
				
				<script>
					$('input[name="forgot[email]"]').bind('keyup', function(){
						$.get( $('base').attr('alt') + 'auth/auth/check_email/' + encodeURIComponent($(this).val()))
							.done(function(data){
								var j = $.parseJSON(data);
								
								if(j.status == 'OK')
								{
									$('i.fa-ok').show();
									$('i.fa-nok').hide();
								}
								else
								{
									$('i.fa-ok').hide();
									$('i.fa-nok').show();
								}
							});
					});
				</script>