				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-profile-wrapper">
							
							<div class="row clearfix profile">
								
								
								
								<!-- tabs section -->
								<div>
									
									
									<div>
										
										
										<div class="tab-pane" id="video">
											<div class="tab-pane-heading">
												<h3>Video</h3>
											</div>
											
											<div class="video-pane-wrapper">
												<div class="row clearfix">
													<div class="col-sm-6">
														<h3><i class="fa fa-user fa-lg"></i><?php echo $session['first_name'] . ' ' . $session['last_name']; ?></h3>
													</div>
													
													<?php if(!empty($video)) : ?>
													<div class="col-sm-6">
														<h3 class="clock"><i class="fa fa-clock-o fa-lg"></i>Date Posted : <?php echo date('F d, Y. H:i a', strtotime($video->created_at));?>
															<button type="button" onclick="window.location='<?php echo site_url('user/video/reset'); ?>'" class="btn btn-primary btn-edit-video">Edit</button>
														</h3>				
													</div>
													<?php endif; ?>
												</div>
												
												<?php if(!empty($video)) : ?>
												<div class="video-yt-wrapper">
													<?php echo $video->video_source; ?>
												</div>
												<?php else : ?>
												<div class="video-ct-wrapper">
													
													<div class="vid-player">
														
														<form method="post" name="video" action="<?php echo site_url('user/video/save'); ?>">
														<div class="video-play-desc">
															<h3>Record a Video Bio or Video Resume</h3>
															<p>Broadcast yourself with more than just pictures. Create a Video Bio / Resume on YouTube at: https://www.youtube.com/my_webcam</p>
															<p>Copy and paste your YouTube link here then press enter: <input type="text" class="input" name="source" placeholder="Your YouTube Video Bio / Resume Link Here" /> <button type="submit" class="btn btn-primary" disabled="disabled" id="enter_btn">Enter</button> </button></p>
															<p>It's your window to the world. Tell us a little about who your are. People can watch your Video Bio and be confident with the ads you post on Amy's List. Or send potential employers a link to your Video Resume for a job you apply for on Amy's List.</p>
															<div class="share">
																<a href="#"><i class="fa fa-share-alt fa-lg"></i>Share</a>
																<button class="btn btn-primary btn-fb"><i class="fa fa-facebook fa-lg"></i>Like</button>
																<button class="btn btn-primary btn-tw"><i class="fa fa-twitter fa-lg"></i>Tweet</button>
															</div>
														</div>
														</form>
													</div>
												</div>
                                                
                                                <script type="text/javascript">
                                                    $(document).ready(function(){
                                                        $('input[name=source]').blur(function(){
                                                            if($(this).val() != "")
                                                            {
                                                                $("#enter_btn").removeAttr("disabled");
                                                            }else
                                                            {
                                                                $("#enter_btn").attr("disabled","disabled");
                                                            }
                                                        });
                                                        
                                                        $('input[name=source]').keyup(function(){
                                                            if($(this).val() != "")
                                                            {
                                                                $("#enter_btn").removeAttr("disabled");
                                                            }else
                                                            {
                                                                $("#enter_btn").attr("disabled","disabled");
                                                            }
                                                        });
                                                    });
                                                </script>
                                                
												<?php endif; ?>
											</div>
										</div>
										
									</div>
								</div>
								
							</div>
						</article>
					</div>
				</section>