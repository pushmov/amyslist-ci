				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-profile-wrapper">
							
							<div class="row clearfix profile">
								
								<div class="heading clearfix">
									<div class="col-md-6">
										
										<div class="clearfix">
											<div class="photo-wrapper" style="display:block;position:relative;cursor:pointer;">
												
													<img class="img-circle" width="110" height="110" src="<?php echo asset_url() .'tn/tn.php?w=110&h=110&src='. asset_url() . $user->profpic; ?>">
												
											</div>
											
											<div class="info-wrapper">
												<div>
													<h3><?php echo profile_name(); ?><span>
													<?php if((int) $user->like_count > 0) : ?>
														<a style="color:#90C0D8" href="<?php echo site_url('user/like'); ?>">
															<i class="fa fa-check-circle fa-lg"></i><?php echo $user->like_count; ?> likes</a>
														</span>
													<?php endif; ?>
													</h3>
												</div>
												<h5><?php echo $user->current_city; ?> <?php echo $user->current_state; ?></h5>
												
											</div>
										</div>
										
									</div>
									<div class="col-md-6 share-wrapper">
										<ul class="list-unstyled share-btn">
											<?php if(!empty($social)) : ?>
											
											<?php foreach($social as $cols => $row_s) : ?>
											<?php if($cols == 'user_id') continue; ?>
											<?php if($row_s != '') : ?>
												<li>
													<a target="_blank" href="<?php echo $row_s; ?>">
														<img src="<?php echo asset_url(); ?>img/social/32-<?php echo $cols; ?>.png" >
													</a>
												</li>
											<?php endif; ?>	
											<?php endforeach; ?>
											<?php endif; ?>
											<?php if(count($social) < 10) : ?>
											<li><a title="Add Social Networks to your profile." href="<?php echo site_url('account/settings'); ?>">
												<img src="<?php echo asset_url(); ?>img/social/32-plus.png" >
											</a>
											</li>
											<?php endif; ?>
										</ul>
									</div>
								</div>
								
								<!-- tabs section -->
								<div id="tabs">
									<ul class="nav nav-tabs resp-tabs-list profile-tabs">
										<li>
											<a href="#setting" data-toggle="tab">
												<i class="fa fa-bars"></i>
											</a>
										</li>
										<li class="active">
											<a href="#about" data-toggle="tab">ABOUT</a>
										</li>
										<li>
											<a href="#wall" data-toggle="tab">WALL</a>
										</li>
										<li>
													<a href="#ads-posted" data-toggle="tab">ADS POSTED</a>
										</li>
										<li>
									<a href="#video" data-toggle="tab">VIDEO</a>
										</li>
										<li>
											<a href="<?php echo site_url('user/like'); ?>">LIKES</a>
										</li>
										
										<!--
										<li class="dropdown">
											<a href="#report" data-toggle="tab" data-popover="popover">
												REPORT PROFILE
											</a>
										</li>
										-->
									</ul>
									
									<div class="tab-content resp-tabs-container">
										<div class="tab-pane" id="setting">
											<div class="tab-pane-heading">
												<h3>Account Setting</h3>
											</div>
											
											<div>
												<ul class="list-unstyled setting-list">
													<li><a href="<?php echo site_url('account/settings'); ?>">Profile Pic</a></li>
													<li><a href="<?php echo site_url('account/settings'); ?>">About Info</a></li>
													<li><a href="<?php echo site_url('account/settings'); ?>">Social Media Icons</a></li>
													<li><a href="<?php echo site_url('account/settings'); ?>">Password Info</a></li>
													<li><a href="<?php echo site_url('account/settings'); ?>">Email Notifications</a></li>
													<li><a href="<?php echo site_url('account/settings'); ?>">Report A Problem</a></li>
													<li><a href="<?php echo site_url('account/settings'); ?>">Delete Account</a></li>
												</ul>
											</div>
										</div>
										<div class="tab-pane active about" id="about">
											<div class="tab-pane-heading">
												<h3>About</h3>
											</div>
											<div class="tab-pane-heading edit">
												<p><i class="fa fa-edit fa-lg"></i><a href="#" data-type="textarea" data-pk="biography" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Biography" class="xeditable<?php echo ($user->biography == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->biography == '') ? 'Tell us a little about yourself here...' : $user->biography; ?></a></p>
											</div>
											<div class="briefcase">
												<h4><i class="fa fa-briefcase fa-lg"></i><a href="#" data-type="text" data-pk="occupation" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Occupation" class="xeditable<?php echo ($user->occupation == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->occupation == '') ? 'Enter your occupation here' : $user->occupation; ?></a> at <a href="#" data-type="text" data-pk="workplace" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Workplace" class="xeditable<?php echo ($user->workplace == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->workplace == '') ? 'Enter your workplace' : '<strong>' . $user->workplace . '</strong>'; ?></a></h4>
												<h4><i class="fa fa-graduation-cap fa-lg"></i>
													Studied <a href="#" data-type="text" data-pk="major" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Major Study" class="xeditable<?php echo ($user->major == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->major == '') ? 'Enter Major Study' : $user->major ; ?></a> at <a href="#" data-type="text" data-pk="study" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter University" class="xeditable<?php echo ($user->study == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->study == '') ? 'Enter University' : '<strong>' . $user->study . '</strong>'; ?></a>
												</h4>
												<h5>Past : <a href="#" data-type="text" data-pk="past_study" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Major Study" class="xeditable<?php echo ($user->past_study == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->past_study == '') ? 'Enter Past Study' : $user->past_study; ?></a></h5>
												<h4><i class="fa fa-home fa-lg"></i>Lives in <a href="#" data-type="text" data-pk="current_city" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Current City" class="xeditable<?php echo ($user->current_city == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->current_city == '') ? 'Enter Current City' : '<strong>' . $user->current_city . '</strong>'; ?></a>, <a href="#" data-type="text" data-pk="current_state" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter State" class="xeditable<?php echo ($user->current_state == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->current_state == '') ? 'Enter State' : '<strong>' . $user->current_state . '</strong>'; ?></a> <a href="#" data-type="text" data-pk="current_country_id" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Country" class="xeditable<?php echo ($user->current_country_id == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->current_country_id == '') ? 'Enter Country' : '<strong>' . $user->current_country_id . '</strong>'; ?></a></h4>
												<h4><i class="fa fa-map-marker fa-lg"></i>From <a href="#" data-type="text" data-pk="city" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter City From" class="xeditable<?php echo ($user->city == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->city == '') ? 'Enter City From' : '<strong>' . $user->city . '</strong>'; ?></a>, <a href="#" data-type="text" data-pk="state" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter State" class="xeditable<?php echo ($user->state == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->state == '') ? 'Enter State' : '<strong>' . $user->state . '</strong>'; ?></a> <a href="#" data-type="text" data-pk="country_id" data-url="<?php echo site_url('user/profile/xeditable/'); ?>" data-title="Enter Country" class="xeditable<?php echo ($user->country_id == '') ? '' : ' xeditable-done' ?>"><?php echo ($user->country_id == '') ? 'Enter Country' : '<strong>'.$user->country_id . '</strong>'; ?></a></h4>
												
												<?php if((int)$user->like_count > 0) : ?>
												<h4><i class="fa fa-thumbs-o-up fa-lg"></i>Likes <a href="<?php echo site_url('user/like'); ?>"><strong><?php echo $user->like_count; ?> profile<?php echo ($user->like_count > 1) ? 's' : ''; ?></strong></a></h4>
												<?php endif; ?>
												
											</div>
										</div>
										<div class="tab-pane wall" id="wall">
											<div class="tab-pane-heading">
									<h3>Wall</h3>
											</div>
											
											<div class="comments-wrapper">
													<!-- Code added -->
                                                <div id="disqus_thread"></div>
    
	<script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = 'amyslistus'; // required: replace example with your forum shortname

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
    <!--end code added -->							
												<div class="fb-comments" data-href="<?php echo base_url() . 'profile/'.$user->username ; ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
									
											</div>
										
										</div>
									
										<div class="tab-pane" id="ads-posted">
											<div class="tab-pane-heading">
												<h3>Ads Posted</h3>
											</div>
											
											<div class="tab-advt-post">
												
												<?php if(empty($ads)) :?>
												No ads posted yet
												<?php else : ?>
												<?php foreach($ads as $ad) : ?>
												<div class="advt-item">
													<h4><?php echo $ad->title; ?></h4>
													<div class="row">
														<div class="col-sm-3">
															<button type="button" class="btn btn-primary btn-edit" onclick="window.location='<?php echo site_url('ads/posted/edit/' . $ad->id); ?>'">Edit</button>
															<button type="button" class="btn btn-primary btn-delete" onclick="window.location='<?php echo site_url('user/profile/delete_ad/' . $ad->id); ?>'">Delete</button>
														</div>
														<div class="col-sm-5">
															<?php if((bool) $ad->post_status == 1) : ?>
															<h5>Link : <a href="<?php echo site_url('viewpost/' . $ad->post_num_id . '/' . seo_friendly($ad->title)); ?>"><?php echo site_url('viewpost/' . $ad->post_num_id . '/' . seo_friendly($ad->title)); ?></a></h5>
															<?php endif; ?>
														</div>
														<div class="col-sm-4 inf">
														
														<?php if(time() > strtotime($ad->expired_at)) : ?>
															<span class="inactive">Expired</span>
														<?php else : ?>
															<?php if($ad->post_status == 1) : ?>
																<span>Active</span>
															<?php else : ?>
																<span class="inactive">Inactive</span>
															<?php endif; ?>
														<?php endif; ?>
															<span>Date Posted : <?php echo date('d/m/Y', strtotime($ad->created_at)); ?></span>
														</div>
													</div>
												</div>
												<?php endforeach; ?>
												<?php endif; ?>
												
											</div>
										</div>
										
										<div class="tab-pane" id="video">
											<div class="tab-pane-heading">
												<h3>Video</h3>
											</div>
											
											<div class="video-pane-wrapper">
												<div class="row clearfix">
													<div class="col-sm-6">
														<h3><i class="fa fa-user fa-lg"></i>Amy Admin</h3>
													</div>
													<?php if(!empty($video)) : ?>
													<div class="col-sm-6">
														<h3 class="clock"><i class="fa fa-clock-o fa-lg"></i>Date Posted : <?php echo date('F d, Y. H:i a', strtotime($video->created_at));?></h3>
														<button type="button" onclick="window.location='<?php echo site_url('user/video/reset'); ?>'" class="btn btn-primary btn-edit-video">Edit</button>
													</div>
													<?php endif; ?>	
												</div>
												
												<?php if(!empty($video)) : ?>
												<div class="video-yt-wrapper">
													<?php echo $video->video_source; ?>
												</div>
												<?php else : ?>
												<div class="video-ct-wrapper">
													
													<div class="vid-player">
														
														<form method="post" name="video" action="<?php echo site_url('user/video/save'); ?>">
														<div class="video-play-desc">
															<h3>Record a Video Bio or Video Resume</h3>
															<p>Broadcast yourself with more than just pictures. Create a Video Bio / Resume on YouTube at: https://www.youtube.com/my_webcam</p>
															<p>Then copy &amp; paste: <input type="text" class="input" name="source" placeholder="Your YouTube Video Bio / Resume Link Here"></p>
															<p>It's your window to the world. Tell us a little about who your are. People can watch your Video Bio and be confident with the ads you post on Amy's List. Or send potential employers a link to your Video Resume for a job you apply for on Amy's List.</p>
															<div class="share">
																<a href="#"><i class="fa fa-share-alt fa-lg"></i>Share</a>
																<button class="btn btn-primary btn-fb"><i class="fa fa-facebook fa-lg"></i>Like</button>
																<button class="btn btn-primary btn-tw"><i class="fa fa-twitter fa-lg"></i>Tweet</button>
															</div>
														</div>
														</form>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</div>
										
										<!--
										<div class="tab-pane" id="report">
											<div class="tab-pane-heading">
												<h3>Report Profile</h3>
												
											</div>
											<div>
												<ul class="list-unstyled setting-list">
													<li><a href="#">This is a fake profile</a></li>
													<li><a href="#">This user is posting spam ads</a></li>
													<li><a href="#">This user keep inboxing me span</a></li>
													<li><a href="#">This user is doing something shady</a></li>
													<li><a href="#">Block this user</a></li>
												</ul>
											</div>
										</div>
										-->
										
									</div>
								</div>
								
							</div>
						</article>
					</div>
				</section>
				
				<link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>css/easy-responsive-tabs.css" />
				<script src="<?php echo asset_url(); ?>js/easyResponsiveTabs.js" type="text/javascript"></script>
				
				<link href="<?php echo asset_url(); ?>css/bootstrap-editable.css" rel="stylesheet">
				<link href="<?php echo asset_url(); ?>css/bootstrap-editable-custom.css" rel="stylesheet">
				<script src="<?php echo asset_url(); ?>js/bootstrap-editable.js"></script>
				
				<script>
					$('[data-popover="popover"]').popover({
						trigger: 'click',
						html : true,
						'placement': 'bottom',
						'content' : function(){
							return $('#popover-content').html()
						}
					});
					
					$(document).ready(function(){
						$('#tabs').easyResponsiveTabs({
								type: 'default', //Types: default, vertical, accordion           
								width: 'auto', //auto or any width like 600px
								fit: true,   // 100% fit in a container
								closed: 'accordion', // Start closed if in accordion view
								activate: function(event) { // Callback function if tab is switched
										var $tab = $(this);
										var $info = $('#tabInfo');
										var $name = $('span', $info);

										$name.text($tab.text());

										$info.show();
								}
						});
						$('#setting').hide();
						// $.fn.editable.defaults.mode = 'inline';
						$('.xeditable').editable({
							mode : 'inline'
						});
						
					});
					
					$(window).load(function(){
							
					});
					
					$('.photo-wrapper').click(function(){
						$('input[name="profpic_file"]').click();
					});
					
					
					$(document).ready(function(){
					
						$('input[name="profpic_file"]').change(function(){
							// alert('ahs');
							var bar = $('.bar');
							if($(this).val() != '')
							{
								$('a.process').click();
								$('form[name="profpic"]').ajaxSubmit({
									dataType:  'json', 
									url: $('base').attr('alt') + 'user/profile/change_pic/',
									beforeSend: function() {
										var percentVal = '0%';
										bar.width(percentVal)
									},
									
									uploadProgress: function(event, position, total, percentComplete) {
										var percentVal = percentComplete + '%';
										bar.width(percentVal)
									},
									
									success: processJson
								});
							
								return false;
							}
						});
					});
					
					
					function processJson(data)
					{
						
						$('.img-circle').attr('src', data.src);
						$('.profpic-sidebar').attr('src', data.src);
						$('button.close').click();
					}
				</script>
				
				<form enctype="multipart/form-data" method="post" id="profpic" name="profpic" style="background:#fff">
					<input type="file" style="visibility:hidden;" name="profpic_file">
				</form>
				
				
				<div class="modal fade modal-login" id="processing-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<div class="clearfix">
									<button aria-hidden="true" style="display:none" data-dismiss="modal" class="close" type="button">�</button>
									Updating
								</div>
							</div>
							<div class="modal-body">
								<div id="notifier" style="display:none">Updating ...</div>
								<div id="progress-1" class="progress">
									<div class="bar"><span class="percent"></span></div >
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<a class="btn process" data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#processing-modal" style="display:none">process</a>