				<section class="ct-content clearfix ct-post-ad-final">

					<div class="container">

					

						<?php load_sidebar(); ?>

						

						<article class="col-lg-10 right-content ct-review">

							

							<div class="clearfix ct-review-wrapper">

								

								<div class="row clearfix breadcumb-wrapper">

									<div class="review-breadcumb">

										<ul class="list-unstyled breadcumb">

											<li><a href="#"><?php echo $location; ?></a></li>

											<li><a href="#">/</a></li>

											<li><a href="#"><?php echo $category['parent']; ?></a></li>

											<li><a href="#">/</a></li>

											<li><a href="#"><?php echo $category['child']; ?></a></li>

										</ul>

									</div>

								</div>

								

								<div class="review-to-publish">

									<h3>Check your post below. If everything is correct, read and agree to the <a href="<?php echo site_url('terms-of-use'); ?>">Amy's List Terms of Use</a> to publish your post.</h3>

									

									<div class="clearfix review-btn-control">

										<div class="col-sm-5 cols-no-padding">
                                            <button type="button" class="btn edit-btn">Edit Post</button>

											<button type="button" class="btn publish-btn">Publish</button>

										</div>

										<div class="col-sm-7 cols-no-padding">

											<label class="checkbox">

												<span class="icons"><span class="first-icon fui-checkbox-unchecked"></span><span class="second-icon fui-checkbox-checked"></span></span>

												<input type="checkbox" data-toggle="checkbox" value="" name="agreement">

												I have read and agree to the <a href="<?php echo site_url('terms-of-use'); ?>">Amy's List Terms of Use</a> please publish my ad.

                      </label>

										</div>

									</div>

								</div>

								

								<h2><?php echo $post->title; ?></h2>

								<ul class="list-unstyled nav-head">

									<li><a href="#"><i class="fa fa-envelope-o fa-lg"></i><?php echo ((bool) $post->is_anonymous_email) ? $post->anonymous_email : $post->email; ?></a></li>

									<li><a href="#"><i class="fa fa-user fa-lg"></i><?php echo ((bool) $post->is_show_username) ? $post->username : '<i>-hidden-</i>'; ?></a></li>

									<li><a href="#"><i class="fa fa-inbox fa-lg"></i>Inbox</a></li>

									<li><a href="#"><i class="fa fa-comment-o fa-lg"></i><?php echo ((bool) $post->is_allow_text_chat) ? 'Text Video Chat' : '<i>-disabled-</i>'; ?></a></li>

								</ul>

								

								<div class="clearfix">

									<div class="col-md-8">

										<?php if(!empty($images)) : ?>

										<div id="owl-demo" class="owl-carousel">
										
											<?php foreach($images as $img) : ?>
												
											<div class="item">

                                                <!--<img src="<?php// echo asset_url() .'tn/tn.php?w=600&h=450&src='.asset_url().$img->image_path; ?>" class="img-responsive" />-->
												<img src="<?php echo asset_url() .'tn/image.php?width=600&height=450&image='.asset_url().$img->image_path; ?>" class="img-responsive" alt="Ad image" />
                                                

												<?php if($img->image_description != '') : ?>

												<div class="caption">

													<?php echo $img->image_description; ?>

												</div>

												<?php endif; ?>

											</div>

											<?php endforeach; ?>

										</div>
										
										<?php else : ?>
										<div class="description"><?php echo $post->description; ?></div>

										<?php endif; ?>

									</div>
										
										

									<div class="col-md-4">

										<div style="min-height:300px;">

										<?php if($post->map_address != '') : ?>

                        <div id="map-canvas" class="map-canvas" style="height:315px;width:100%;"></div>

                        <address>

                            <?php echo $post->map_address; ?>

                        </address>

										<?php endif; ?>

                    </div>

									</div>
									
									<?php if(!empty($images)) : ?>
									
										<p class="description"><?php echo $post->description; ?></p>
										
									<?php endif; ?>
									
								</div>
							
						</article>
					</div>
				</section>
				<span id="map_address" alt="<?php echo $post->map_address; ?>"></span>
				
				<link href="<?php echo asset_url(); ?>css/owl.carousel.css" rel="stylesheet">
				<link href="<?php echo asset_url(); ?>css/owl.transitions.css" rel="stylesheet">
				<link href="<?php echo asset_url(); ?>css/owl.theme.css" rel="stylesheet">
				
				<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
				<script src="<?php echo asset_url(); ?>js/owl.carousel.js"></script>
				<script>
					$(document).ready(function() {
						$("#owl-demo").owlCarousel({

							navigation : true,
							slideSpeed : 300,
							paginationSpeed : 400,
							singleItem : true

						});
						
					});
					
					$('button.publish-btn').click(function(){
					
						if(!$('input[name="agreement"]').is(':checked'))
						{
							var n = noty({
								text: 'You must agree to the Amy\'s list terms of use ',
								type: 'error'
							});
							return false;
							
						}
						
						window.location = $('base').attr('alt') + 'blog/item/publish/' + <?php echo $post->id; ?>;
						
					});
				
					$('button.edit-btn').click(function(){
						window.location=$('base').attr('alt') + 'blog/posted/edit/' + <?php echo $post->id; ?> + '/?review=1';
					});
				</script>