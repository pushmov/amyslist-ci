				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content">
							
							<div class="dl-menu-wrapper row clearfix">
								<div class="col-sm-5 clearfix dl-menu-ct">
									<div id="dl-menu" class="dl-menuwrapper">
										
										<div class="dl-trigger custom-select" id="custom-select-location"><?php echo $location_select; ?><b class="caret"></b></div>
										<ul class="dl-menu" id="dl-menu-location" style="z-index:999999 !important">
										
										<?php if(!empty($us_cities)) : ?>
											<li class="first-child">
												<a href="#">US Cities<b class="right-caret"></b></a>
												<ul class="dl-submenu">
													<?php foreach($us_cities as $us) : ?>
													<li>
														<a data-id="<?php echo $us->city_id; ?>" href="#"><?php echo $us->city_name; ?>														
															<?php if(!empty($us->child)) : ?>
																<b class="right-caret"></b>
															<?php endif; ?>
														</a>
														<?php if(!empty($us->child)) : ?>
														<ul class="dl-submenu">
															<?php foreach($us->child as $child) : ?>
															<li><a data-id="<?php echo $child->city_id; ?>" href="#"><?php echo $child->city_name; ?></a></li>
															<?php endforeach; ?>
														</ul>
														<?php endif; ?>
													</li>
													<?php endforeach; ?>
												</ul>
											</li>
										<?php endif; ?>
										
										<?php if(!empty($us_state)) : ?>
											<li>
												<a href="#">United States<b class="right-caret"></b></a>
												<ul class="dl-submenu">
													<?php foreach($us_state as $row_state) : ?>
													<li>
														<a data-id="<?php echo $row_state->id; ?>" href="#"><?php echo $row_state->name; ?>
															<?php if(!empty($row_state->child)) :?>
															<b class="right-caret"></b>
															<?php endif; ?>
														</a>
														<?php if(!empty($row_state->child)) :?>
														<ul class="dl-submenu">
															<?php foreach($row_state->child as $child) : ?>
															<li><a data-id="<?php echo $child->city_id; ?>" href="#"><?php echo $child->city_name; ?></a></li>
															<?php endforeach; ?>
														</ul>
														<?php endif; ?>
													</li>
													<?php endforeach; ?>
													
												</ul>
												
												
											</li>
										<?php endif; ?>
										
										<?php if(!empty($intl_cities)) : ?>
											<li>
												<a href="#">International Cities
													<b class="right-caret"></b>
												</a>
												<ul class="dl-submenu">
													<?php foreach($intl_cities as $intl_c) : ?>
													<li><a data-id="<?php echo $intl_c->city_id; ?>" href="#"><?php echo $intl_c->city_name; ?></a></li>
													<?php endforeach; ?>
												</ul>
											</li>
										<?php endif; ?>
										
										<?php if(!empty($international)) : ?>
											<li>
												<a href="#">International Countries<b class="right-caret"></b></a>
												<ul class="dl-submenu">
													<?php foreach($international as $int) : ?>
													<li>
														<a data-id="<?php echo $int->city_id; ?>" href="#"><?php echo ucwords($int->city_name); ?>
															<?php if(!empty($int->sub_cities)) : ?>
															<b class="right-caret"></b>
															<?php endif; ?>
														</a>
														<?php if(!empty($int->sub_cities)) : ?>
														<ul class="dl-submenu">
															<?php foreach($int->sub_cities as $sub) : ?>
															<li>
																<a data-id="<?php echo $sub->city_id; ?>" href="#"><?php echo $sub->city_name; ?>
																	<?php if(!empty($sub->child)) : ?>
																		<b class="right-caret"></b>
																	<?php endif; ?>
																</a>
																<?php if(!empty($sub->child)) : ?>
																	<ul class="dl-submenu">
																		<?php foreach($sub->child as $child_cities) : ?>
																		<li><a data-id="<?php echo $child_cities->city_id; ?>" href="#"><?php echo $child_cities->city_name; ?></a></li>
																		<?php endforeach; ?>
																	</ul>
																<?php endif; ?>
															</li>
															<?php endforeach; ?>
														</ul>
														<?php endif; ?>
													</li>
													<?php endforeach; ?>
													
												</ul>
											</li>
										<?php endif; ?>
										</ul>
									</div>
									
								</div>
								<div class="col-sm-5 dl-menu-ct clearfix" id="dl-menu-2-container">
									<div id="dl-menu-2" class="dl-menuwrapper">
										
										<?php if(!empty($categories)) : ?>
										<div class="dl-trigger custom-select" id="custom-select-category"><?php echo $category_select; ?><b class="caret"></b></div>
										<ul class="dl-menu" id="dl-menu-category" style="z-index:999999 !important">
											<?php foreach($categories as $cat) : ?>
											<li class="first-child">
												<a data-id="<?php echo $cat->id ?>" href="#"><?php echo $cat->category_name; ?>
													<?php if(!empty($cat->child)) : ?>
													<b class="right-caret"></b>
													<?php endif; ?>
												</a>
													
													<?php if(!empty($cat->child)) : ?>
													<ul class="dl-submenu">
														<?php foreach($cat->child as $cat_c) : ?>
															<li><a data-id="<?php echo $cat_c->id ?>" href="#"><?php echo $cat_c->category_name; ?></a></li>
														<?php endforeach; ?>
													</ul>
													<?php endif; ?>
											</li>
											<?php endforeach; ?>
										</ul>
										<?php endif; ?>
									</div>
									
								</div>
								<div class="col-sm-2"></div>
							</div>
							
							<div class="form-wrapper wrapper">
								
								<fieldset>
									<div class="notifier alert-message hidden">Helo</div>
                                    <?php ($post->post_status==0)?$review=0:$review=1;  ?>
									<form name="adpost" role="form" action="<?php echo site_url('ads/posted/update/' . $post->id . '/?review='.$review); ?>" method="POST" enctype="multipart/form-data">
										
										<input type="hidden" name="adpost[location_id]" id="location-id" value="<?php echo $post->location_id; ?>">
										<input type="hidden" name="adpost[category_id]" id="category-id" value="<?php echo $post->category_id; ?>">
										<input type="hidden" name="adpost[type]" id="postType" value="">
										
										<div class="category-form clearfix">
											<div class="input col-sm-5">
												<input type="text" name="adpost[title]" class="input" placeholder="Ad Post Title" value="<?php echo $post->title; ?>"/>
											</div>
											<div class="input col-sm-4">
												<input type="text" name="adpost[address]" class="input" placeholder="Location" value="<?php echo $post->address; ?>"/>
											</div>
											<div class="input col-sm-2 housing-additional-form" style="display:none">
												<input type="text" name="adpost[rent]" class="input" placeholder="Rent" value="<?php echo $post->rent; ?>"/>
											</div>
											<div class="input col-sm-1 housing-additional-form" style="display:none">
												<input type="text" name="adpost[rooms]" class="input numeric" placeholder="0" value="<?php echo $post->rooms; ?>"/>
											</div>
										</div>
										
										<div class="blog-form clearfix" style="display:none">
											<div class="input col-sm-5">
												<input type="text" name="blogpost[title]" class="input" placeholder="Blog Post Title"/>
											</div>
											<div class="input col-sm-4">
												<input type="text" name="blogpost[address]" class="input" placeholder="Location"/>
											</div>
											<div class="input col-sm-2 housing-additional-form" style="display:none">
												<input type="text" name="blogpost[rent]" class="input" placeholder="Rent"/>
											</div>
											<div class="input col-sm-1 housing-additional-form" style="display:none">
												<input type="text" name="blogpost[rooms]" class="input numeric" placeholder="0"/>
											</div>
										</div>
										
										
										
										<div class="clearfix">
											<div class="input col-sm-12">
												<textarea class="jqte-test" name="adpost[description]" ><?php echo $post->description; ?></textarea>
											</div>
										</div>
										<div class="category-form clearfix m_top_10">
											<div class="input info col-sm-5">
												<div>
													<input autocomplete="off" id="email-address" name="adpost[email]" type="text" placeholder="Email Address" value="<?php echo $post->email; ?>"/>
													<i id="email-address-valid" class="fa fa-times-circle fa-lg"></i>
												</div>
												<?php /*?><div>
													<input autocomplete="off" id="email-address-2" name="adpost[email]" type="hidden" placeholder="Email Address" value="<?php echo $post->email; ?>"/>
													<i id="email-address-valid-2" class="fa fa-times-circle fa-lg hid_it"></i>
												</div><?php */?>
												<div class="checkbxs email-checkbxs">
													<label class="checkbox">
														<input type="checkbox" value="" name="adpost[is_anonymous_email]" data-toggle="checkbox" <?php echo ((bool)$post->is_anonymous_email) ? 'checked="checked"' : ''; ?>>
															Anyonmize email
															Example:<span class="email">938291@amyslist.us</span>
                          </label>
												</div>
												<div class="checkbxs">
													<label class="checkbox">
														<input type="checkbox" name="adpost[is_show_username]" value="" data-toggle="checkbox" <?php echo ((bool)$post->is_show_username) ? 'checked="checked"' : ''; ?>>
															Show Username
                          </label>
												</div>
												<div class="checkbxs">
													<label class="checkbox">
														<input type="checkbox" name="adpost[is_allow_text_chat]" value="" data-toggle="checkbox" <?php echo ((bool)$post->is_allow_text_chat) ? 'checked="checked"' : ''; ?>>
															Allow text chat (when online)
                          </label>
												</div>
												<div class="checkbxs">
													<label class="checkbox phone_number_w">
														<input type="checkbox" class="phone_number"  value="" data-toggle="checkbox" <?php echo ((bool)$post->phone_number) ? 'checked="checked"' : ''; ?>>
															Enter Phone Number
                          </label>
												</div>
												
												<div class="checkbxs">
													<input type="text" name="adpost[phone_number]"  value="<?php echo $post->phone_number; ?>">
												</div>
												
											</div>
											<div class="map-input col-sm-7">
												<div id="map-canvas" class="map-canvas"></div>
												<div>
													<input id="address" type="text" name="adpost[map_address]" placeholder="Address (Cross Street) or (Neigborhood) (City) (State)" value="<?php echo $post->map_address; ?>"/>
													<button type="button" class="btn btn-large btn-find-place"><i class="fui-search"></i></button>
												</div>
											</div>
										</div>
										<div class="blog-form clearfix" style="display:none">
												<div class="input info col-sm-5">
													<div class="facebook">
														<input id="facebook-link" name="blogpost[facebook]" type="text" placeholder="Facebook Profile Page Link"/>
														<i id="fa-facebook" class="fa fa-check-circle fa-lg"></i>
													</div>
													<div class="twitter">
														<input id="twitter-link" name="blogpost[twitter]" type="text" placeholder="Twitter Link @"/>
														<i id="fa-twitter" class="fa fa-check-circle fa-lg"></i>
													</div>
													<div class="gplus">
														<input id="google-link" name="blogpost[google]" type="text" placeholder="Google Plus Profile"/>
														<i id="fa-google" class="fa fa-check-circle fa-lg"></i>
													</div>
													<div class="tumblr">
														<input id="tumblr-link" name="blogpost[tumblr]" type="text" placeholder="Tumblr"/>
														<i id="fa-tumblr" class="fa fa-check-circle fa-lg"></i>
													</div>
													<div class="pinterest">
														<input id="pinterest-link" name="blogpost[pinterest]" type="text" placeholder="Pinterest Profile"/>
														<i id="fa-pinterest" class="fa fa-check-circle fa-lg"></i>
													</div>
													<div class="other">
														<input type="text" name="blogpost[other]" placeholder="Other Link"/>
														<i id="fa-other" class="fa fa-check-circle fa-lg"></i>
													</div>
													
												</div>
												
												<div class="col-sm-7">
													<div>
														<input type="text" name="adpost[video_link]" placeholder="Copy and Paste a Video Link Here - YouTube, Instagram, Facebook, Vine" value="<?php echo $post->video_link; ?>"/>
													</div>
													<div>
														<img class="img-responsive" src="<?php echo $this->config->item('assets_uploads'); ?>sample-blog-video.jpg">
													</div>
												</div>
										</div>
										
										<div class="category-form clearfix">
											<div class="input video-link col-sm-5">
												<input type="hidden" name="adpost[video_link]" placeholder="Video Link (Copy and paste here) (Optional)" value="<?php echo $post->video_link; ?>"/>
											</div>
											<div class="col-sm-7">
												<?php /*?><div class="video-description">
													<h3>Video Link Description</h3>
													<p>Record a video using Vine, Instagram, YouTube, Facebook etc. Then paste the link to the video in the box to the left. The video will appear in your ad for users to see. If you don't have a video for your ad it's okay. This is only optional. If you decide to make a video describing your ad, you can edit your post and add the video link later.</p>
												</div><?php */?>
											</div>
										</div>
										<div class="blog-form clearfix" style="display:none">
												<div class="col-sm-5"></div>
													<div class="col-sm-7">
														<div class="video-description">
															<h3>Video Description</h3>
															<p>Please type a brief of video description and caption here. So people who read your blog will know a little bit about what the video is and how it relates to your blog</p>
														</div>
													</div>
										</div>
										
										<div class="upload-wrapper">
											<div class="clearfix">
												<div class="col-sm-4">
													<div class="control">
														<div class="btn-control">
															<h3>
																<span>Upload Photos</span>
															</h3>
														</div>
														<h4>Edit your photos<br>
															<span>(Select up to 8 photos)</span>
														</h4>
													</div>
												</div>
												<div class="col-sm-4">
													
													<?php if(isset($images[0])) : ?>
													<div class="fileinput fileinput-exists" data-provides="fileinput">
														<div class="img-upload-wrapper fileinput-preview thumbnail" data-trigger="fileinput" style="line-height: 175px;">
															<img src="<?php echo asset_url() . $images[0]->image_path; ?>" >
														</div>
														
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"><?php echo $images[0]->image_description; ?></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]">
															</span>
															<a data-dismiss="fileinput" class="btn btn-danger btn-embossed fileinput-exists file-remove" data-id="<?php echo $images[0]->id; ?>" href="#"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php else : ?>
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php endif; ?>
													
												</div>
												
												<div class="col-sm-4">
													<?php if(isset($images[1])) : ?>
													<div class="fileinput fileinput-exists" data-provides="fileinput">
														<div class="img-upload-wrapper fileinput-preview thumbnail" data-trigger="fileinput" style="line-height: 175px;">
															<img src="<?php echo asset_url() . $images[1]->image_path; ?>" >
														</div>
														
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"><?php echo $images[1]->image_description; ?></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]">
															</span>
															<a data-dismiss="fileinput" class="btn btn-danger btn-embossed fileinput-exists file-remove" data-id="<?php echo $images[1]->id; ?>" href="#"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php else : ?>
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php endif; ?>
												</div>
											</div>
											
											<div class="clearfix">
												<div class="col-sm-4">
													<?php if(isset($images[2])) : ?>
													<div class="fileinput fileinput-exists" data-provides="fileinput">
														<div class="img-upload-wrapper fileinput-preview thumbnail" data-trigger="fileinput" style="line-height: 175px;">
															<img src="<?php echo asset_url() . $images[2]->image_path; ?>" >
														</div>
														
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"><?php echo $images[2]->image_description; ?></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]">
															</span>
															<a data-dismiss="fileinput" class="btn btn-danger btn-embossed fileinput-exists file-remove" data-id="<?php echo $images[2]->id; ?>" href="#"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php else : ?>
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php endif; ?>
												</div>
												<div class="col-sm-4">
													<?php if(isset($images[3])) : ?>
													<div class="fileinput fileinput-exists" data-provides="fileinput">
														<div class="img-upload-wrapper fileinput-preview thumbnail" data-trigger="fileinput" style="line-height: 175px;">
															<img src="<?php echo asset_url() . $images[3]->image_path; ?>" >
														</div>
														
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"><?php echo $images[3]->image_description; ?></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]">
															</span>
															<a data-dismiss="fileinput" class="btn btn-danger btn-embossed fileinput-exists file-remove" data-id="<?php echo $images[3]->id; ?>" href="#"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php else : ?>
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php endif; ?>
												</div>
												<div class="col-sm-4">
													<?php if(isset($images[4])) : ?>
													<div class="fileinput fileinput-exists" data-provides="fileinput">
														<div class="img-upload-wrapper fileinput-preview thumbnail" data-trigger="fileinput" style="line-height: 175px;">
															<img src="<?php echo asset_url() . $images[4]->image_path; ?>" >
														</div>
														
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"><?php echo $images[4]->image_description; ?></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]">
															</span>
															<a data-dismiss="fileinput" class="btn btn-danger btn-embossed fileinput-exists file-remove" data-id="<?php echo $images[4]->id; ?>" href="#"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php else : ?>
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php endif; ?>
												</div>
											</div>
											
											<div class="clearfix">
												<div class="col-sm-4">
													<?php if(isset($images[5])) : ?>
													<div class="fileinput fileinput-exists" data-provides="fileinput">
														<div class="img-upload-wrapper fileinput-preview thumbnail" data-trigger="fileinput" style="line-height: 175px;">
															<img src="<?php echo asset_url() . $images[5]->image_path; ?>" >
														</div>
														
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"><?php echo $images[5]->image_description; ?></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]">
															</span>
															<a data-dismiss="fileinput" class="btn btn-danger btn-embossed fileinput-exists file-remove" data-id="<?php echo $images[5]->id; ?>" href="#"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php else : ?>
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php endif; ?>
												</div>
												<div class="col-sm-4">
													<?php if(isset($images[6])) : ?>
													<div class="fileinput fileinput-exists" data-provides="fileinput">
														<div class="img-upload-wrapper fileinput-preview thumbnail" data-trigger="fileinput" style="line-height: 175px;">
															<img src="<?php echo asset_url() . $images[6]->image_path; ?>" >
														</div>
														
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"><?php echo $images[6]->image_description; ?></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]">
															</span>
															<a data-dismiss="fileinput" class="btn btn-danger btn-embossed fileinput-exists file-remove" data-id="<?php echo $images[6]->id; ?>" href="#"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php else : ?>
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php endif; ?>
												</div>
												<div class="col-sm-4">
													<?php if(isset($images[7])) : ?>
													<div class="fileinput fileinput-exists" data-provides="fileinput">
														<div class="img-upload-wrapper fileinput-preview thumbnail" data-trigger="fileinput" style="line-height: 175px;">
															<img src="<?php echo asset_url() . $images[7]->image_path; ?>" >
														</div>
														
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"><?php echo $images[7]->image_description; ?></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]">
															</span>
															<a data-dismiss="fileinput" class="btn btn-danger btn-embossed fileinput-exists file-remove" data-id="<?php echo $images[7]->id; ?>" href="#"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php else : ?>
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
													<?php endif; ?>
												</div>
											</div>
										</div>
										<div class="submit">
											<input type="submit" class="btn btn-primary btn-continue" value="Continue">
										</div>
										
									</form>
								</fieldset>
							</div>
							
						</article>
					</div>
				</section>
				
				<div class="modal fade modal-login" id="processing-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<div class="clearfix">
									Processing
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								</div>
							</div>
							<div class="modal-body">
								<div id="notifier" style="display:none">Uploading ...</div>
								<div id="progress-1" class="progress">
									<div class="bar"><span class="percent"></span></div >
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<a class="btn process" data-toggle="modal" href="#processing-modal" style="display:none">process</a>
		
		<script type="text/javascript" src="<?php echo asset_url(); ?>jqte/jquery-te-1.4.0.min.js" charset="utf-8"></script>
		<link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>jqte/jquery-te-1.4.0.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('assets_css'); ?>component.css" />
        
        <link href="<?php echo asset_url(); ?>forala_editor/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo asset_url(); ?>forala_editor/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo asset_url(); ?>forala_editor/css/froala_style.min.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo asset_url(); ?>forala_editor/js/froala_editor.min.js"></script>
        <!--[if lt IE 9]>
            <script src="<?php echo asset_url(); ?>forala_editor/js/froala_editor_ie8.min.js"></script>
        <![endif]-->
         <script src="<?php echo asset_url(); ?>forala_editor/js/plugins/tables.min.js"></script>
          <script src="<?php echo asset_url(); ?>forala_editor/js/plugins/lists.min.js"></script>
          <script src="<?php echo asset_url(); ?>forala_editor/js/plugins/colors.min.js"></script>
          <script src="<?php echo asset_url(); ?>forala_editor/js/plugins/media_manager.min.js"></script>
          <script src="<?php echo asset_url(); ?>forala_editor/js/plugins/font_family.min.js"></script>
          <script src="<?php echo asset_url(); ?>forala_editor/js/plugins/font_size.min.js"></script>
          <script src="<?php echo asset_url(); ?>forala_editor/js/plugins/block_styles.min.js"></script>
          <script src="<?php echo asset_url(); ?>forala_editor/js/plugins/video.min.js"></script>  
		<script>
			$(document).ready(function(){
				
				if($('#email-address').val() != '')
				{
					validateEmail($('#email-address').val());
				}
				
				if($('#email-address-2').val() != '')
				{
					validateEmail($('#email-address-2').val());
				}
				
				$('input[name="adpost[phone_number]"]').prop('disabled', true);
				
				//$('.jqte-test').jqte();
				$('.jqte-test').editable({inlineMode: false, height: 200, buttons: ['bold', 'italic', 'underline', 'strikeThrough', 'fontSize', 'color', 'sep', 'formatBlock', 'blockStyle', 'align', 'insertOrderedList', 'insertUnorderedList', 'outdent', 'indent', 'sep', 'createLink', 'insertImage', 'insertVideo', 'undo', 'redo', 'inserthorizontalrule']});
				
				$('.numeric').stepper();
				$('.numeric').css({'padding-right' : 25});
				$('.stepper-wrap').css({'margin-right':0});
				$('.stepper-btn-wrap').css({'height' : 35,'margin' : 0});
				
				$('#custom-select-location').click(function(){
					$('#dl-menu-2-container').addClass('addlayer');
				});
				
				
			});
		</script>
		<script src="<?php echo $this->config->item('assets_js'); ?>jquery.stepper.min.js"></script>
		<script src="<?php echo $this->config->item('assets_js'); ?>hoverIntent.js"></script>
		<script src="<?php echo $this->config->item('assets_js'); ?>modernizr.custom.js"></script>
		<script src="<?php echo $this->config->item('assets_js'); ?>jquery.dlmenu.js"></script>
		<script src="<?php echo $this->config->item('assets_js'); ?>flatui-fileinput.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
		<script>
				function renderMap(){
				
					var address = $('#address').val();
					if(address == '') {
						google.maps.event.addDomListener(window, 'load', initialize);
					} 
					else
					{
					
						var places = [];
						var map = $('#map-canvas');
						// Adding a LatLng object for each city
						//places.push(new google.maps.LatLng(40.756, -73.986));
						//places.push(new google.maps.LatLng(37.775, -122.419));
						//places.push(new google.maps.LatLng(47.620, -122.347));
						//places.push(new google.maps.LatLng(-22.933, -43.184));
						var geocoder = new google.maps.Geocoder();
						geocoder.geocode( { 'address': address}, function(results, status) {
							console.log(status);
							if (status == google.maps.GeocoderStatus.OK) {
								
								var myLatlng = new google.maps.LatLng(results[0].geometry.location.lat(),results[0].geometry.location.lng());
								var mapOptions = {
									zoom: 12,
									center: myLatlng
								}
								var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

								var marker = new google.maps.Marker({
										position: myLatlng,
										map: map,
										title: 'Hello World!'
								});
								
							} else {
								alert("Geocode was not successful for the following reason: " + status);
							}
						});
						
					}
					
					return false;
					
				}
					function initialize() {
						var myLatlng = new google.maps.LatLng(40.7590615,-73.969231);
						var mapOptions = {
							zoom: 12,
							center: myLatlng
						}
						var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

						var marker = new google.maps.Marker({
								position: myLatlng,
								map: map,
								title: 'Hello World!'
						});
					}
					
					if($('#address').val() == '')
					{
						google.maps.event.addDomListener(window, 'load', initialize);
					}
					else
					{
						renderMap();
					}

		</script>
		<script>
			
			$('input#address, form#postanAd').keypress(function(e){
				
				if(e.keyCode == 10 || e.keyCode == 13)
				{
					renderMap();
					e.preventDefault();
				}
				
			});
			
			function validateEmail(email) { 
				/*var re = '/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';*/
				var re = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
				return re.test(email);
			}
			
			$('#email-address').keyup(function(){
				
				var is_valid = validateEmail($(this).val());
				
				if(is_valid)
				{
					$('#email-address-valid').removeClass('fa-times-circle').addClass('fa-check-circle').show();
					if($(this).val() == ''){
						$('#email-address-valid').removeClass('fa-times-circle').addClass('fa-check-circle').hide();
					}
				}
				else
				{
					$('#email-address-valid').removeClass('fa-check-circle').addClass('fa-times-circle').show();
					if($(this).val() == ''){
						$('#email-address-valid').removeClass('fa-times-circle').addClass('fa-check-circle').hide();
					}
				}
				
				
				
			});
			
			$('#email-address-2').keyup(function(){
				
				
				var is_valid = validateEmail($(this).val());
				
				if(is_valid)
				{
					$('#email-address-valid-2').removeClass('fa-times-circle').addClass('fa-check-circle').show();
					if($(this).val() == ''){
						$('#email-address-valid-2').removeClass('fa-times-circle').addClass('fa-check-circle').hide();
					}
				}
				else
				{
					$('#email-address-valid-2').removeClass('fa-check-circle').addClass('fa-times-circle').show();
					if($(this).val() == ''){
						$('#email-address-valid-2').removeClass('fa-times-circle').addClass('fa-check-circle').hide();
					}
				}
			});
			
			$("select").selectpicker({style: 'btn', menuStyle: 'dropdown-inverse'});
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
				
				$( '#dl-menu-2' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
				
				$('#dl-menu-location a').click(function(){
					if(!$(this).find('b').length){
						var val = $(this).text();
						var num = $(this).attr('data-id');
						
						$('#location-id').val(num);
						$('#custom-select-location').html(val + '<b class="caret"></b>');
						$('body').click();
					}
				});
				
				$('#dl-menu-category a').click(function(){
					var is_housing = false;
					
					if($(this).attr('alt') == 'housing'){
						$('.housing-additional-form').show();
					} else {
						$('.housing-additional-form').hide();
					}
					
					if(!$(this).find('b').length){
						var val = $(this).text();
						var num = $(this).attr('data-id');
						
						$('#category-id').val(num);
						$('#custom-select-category').html(val + '<b class="caret"></b>');
						$('body').click();
					}
					
					if($(this).attr('alt') == 'blog'){
						$('.category-form').hide();
						$('.blog-form').show();
					} else {
						$('.category-form').show();
						$('.blog-form').hide();
					}
				});
				
				
				
				
				$(document).ready(function(){
					$('li.dl-back a').each(function(){
						if($(this).text() == ''){
							$(this).remove();
						}
					});
					
					$('ul.dl-submenu a').each(function(){
						if($.trim($(this).text()) == ''){
							$(this).remove();
						}
					});
					
					
					$('ul.dl-submenu li').not(':first').each(function(){
					});
					
					$('.fa-times-circle').hide();
					
					
					if(validateEmail($('#email-address').val())){
						$('#email-address-valid').removeClass('fa-times-circle').addClass('fa-check-circle').show();
					}
					
					if(validateEmail($('#email-address-2').val())){
						$('#email-address-valid-2').removeClass('fa-times-circle').addClass('fa-check-circle').show();
					}
					
					renderMap();
				});
				
				
				
				$('#map-search').click(function(){
					renderMap();
				});
				
				$('.file-remove').click(function(){
					var id = $(this).attr('data-id');
					
					$.get( $('base').attr('alt') + 'ads/posted/changepic/' + id )
						.done(function(data){
							console.log(data);
						});
				});
				
				$('.btn-continue').click(function(){
					console.log('helo');
					
					var type = 'error';
					var timeout = 2000;
				
				
					var categoryId = $('#category-id').val();
					var locationId = $('#location-id').val();
					
					$('.alert-message').html('').hide();
					
					if(categoryId == '' || locationId == ''){
						
						var n = noty({
							text: 'Please select a Location and Ad Post Category',
							type: type,
							timeout:timeout
						});
						return false;
						
					}
					
					var title = $('input[name="adpost[title]"]');
					var location = $('input[name="adpost[address]"]');
					var description = $('input[name="adpost[description]"]');
					var email = $('input[name="adpost[email]"]');
					var confirmEmail = $('input[name="adpost[email]"]');
					// var address = $('input[name="adpost[map_address]"]');
					// var picture = $('input[name="uploadFile"]');;
					
					
					
					if(title.val() == ''){
						title.focus();
						// $('.alert-message').html('Please enter title').show();
						var n = noty({
							text: 'Please enter title',
							type: type,
							timeout:timeout
						});
						return false;
					}
					if(location.val() == ''){
						location.focus();
						// $('.alert-message').html('Please enter location field').show();
						var n = noty({
							text: 'Please enter location field',
							type: type,
							timeout:timeout
						});
						return false;
					}
					if(description.val() == ''){
						description.focus();
						// $('.alert-message').html('Please enter description').show();
						var n = noty({
							text: 'Please enter location field',
							type: type,
							timeout:timeout
						});
						return false;
					}
					if(email.val() == ''){
						email.focus();
						// $('.alert-message').html('Please enter your email address').show();
						var n = noty({
							text: 'Please enter your email address',
							type: type,
							timeout:timeout
						});
						return false;
					}
					if(confirmEmail.val() == ''){
						confirmEmail.focus();
						// $('.alert-message').html('Please enter your email address').show();
						var n = noty({
							text: 'Please enter your email address',
							type: type,
							timeout:timeout
						});
						return false;
					}
					if(confirmEmail.val() != email.val()){
						confirmEmail.focus();
						// $('.alert-message').html('Email address does not match').show();
						var n = noty({
							text: 'Email address does not match',
							type: type,
							timeout:timeout
						});
						return false;
					}
					
					// if(address.val() == ''){
						// address.focus();
						// $('.alert-message').html('Please enter address / location').show();
						// return false;
					// }
					
					$('.process').click();
					
					var bar = $('.bar');
					var percent = $('.percent');
					// var status = $('#status');
					
					$('form[name="adpost"]').ajaxSubmit({
						
							dataType:  'json', 
							url: $('form[name="adpost"]').attr('action'),
							beforeSend: function() {
								console.log('a');
								// status.empty();
								var percentVal = '0%';
								bar.width(percentVal)
								percent.html(percentVal);
							},
							
							uploadProgress: function(event, position, total, percentComplete) {
								console.log('b');
								var percentVal = percentComplete + '%';
								bar.width(percentVal)
								percent.html(percentVal);
							},
							
							success: processJson
					});
					
					return false;
				});
				
				function processJson(data){
				
					var bar = $('.bar');
					var percent = $('.percent');
					// var status = $('#status');
					
					var percentVal = '100%';
					bar.width(percentVal)
					percent.html(percentVal);
					if(data.success == 'success'){
						window.location = data.redirect
						// alert(data.redirect);
					} else {
						return false;
					}
					
				}
				
				$('.btn-find-place').click(function(e){
					renderMap();
					e.preventDefault();
					return false;
				});
				
				$('.phone_number').change(function(){
					
					if($('.phone_number_w').hasClass('checked'))
					{
						$('input[name="adpost[phone_number]"]').prop('disabled', false);
					}
					else
					{
						$('input[name="adpost[phone_number]"]').prop('disabled', true);
					}
					
				});
			//$('.jqte-test').jqte();
		</script>