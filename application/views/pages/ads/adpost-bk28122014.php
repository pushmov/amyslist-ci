				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content">
							
							<div class="dl-menu-wrapper row clearfix">
								<div class="col-sm-5 clearfix dl-menu-ct">
									<div id="dl-menu" class="dl-menuwrapper">
										
										<div class="dl-trigger custom-select" id="custom-select-location">Location <b class="caret"></b></div>
										<ul class="dl-menu" id="dl-menu-location" style="z-index:999999 !important">
										
										<?php if(!empty($us_cities)) : ?>
											<li class="first-child">
												<a href="#">US Cities<b class="right-caret"></b></a>
												<ul class="dl-submenu">
													<?php foreach($us_cities as $us) : ?>
													<li>
														<a data-id="<?php echo $us->city_id; ?>" href="#"><?php echo $us->city_name; ?>														
															<?php if(!empty($us->child)) : ?>
																<b class="right-caret"></b>
															<?php endif; ?>
														</a>
														<?php if(!empty($us->child)) : ?>
														<ul class="dl-submenu">
															<?php foreach($us->child as $child) : ?>
															<li><a data-id="<?php echo $child->city_id; ?>" href="#"><?php echo $child->city_name; ?></a></li>
															<?php endforeach; ?>
														</ul>
														<?php endif; ?>
													</li>
													<?php endforeach; ?>
												</ul>
											</li>
										<?php endif; ?>
										
										<?php if(!empty($us_state)) : ?>
											<li>
												<a href="#">United States<b class="right-caret"></b></a>
												<ul class="dl-submenu">
													<?php foreach($us_state as $row_state) : ?>
													<li>
														<a data-id="<?php echo $row_state->id; ?>" href="#"><?php echo $row_state->name; ?>
															<?php if(!empty($row_state->child)) :?>
															<b class="right-caret"></b>
															<?php endif; ?>
														</a>
														<?php if(!empty($row_state->child)) :?>
														<ul class="dl-submenu">
															<?php foreach($row_state->child as $child) : ?>
															<li><a data-id="<?php echo $child->city_id; ?>" href="#"><?php echo $child->city_name; ?></a></li>
															<?php endforeach; ?>
														</ul>
														<?php endif; ?>
													</li>
													<?php endforeach; ?>
													
												</ul>
												
												
											</li>
										<?php endif; ?>
										
										<?php if(!empty($intl_cities)) : ?>
											<li>
												<a href="#">International Cities
													<b class="right-caret"></b>
												</a>
												<ul class="dl-submenu">
													<?php foreach($intl_cities as $intl_c) : ?>
													<li><a data-id="<?php echo $intl_c->city_id; ?>" href="#"><?php echo $intl_c->city_name; ?></a></li>
													<?php endforeach; ?>
												</ul>
											</li>
										<?php endif; ?>
										
										<?php if(!empty($international)) : ?>
											<li>
												<a href="#">International Countries<b class="right-caret"></b></a>
												<ul class="dl-submenu">
													<?php foreach($international as $int) : ?>
													<li>
														<a data-id="<?php echo $int->city_id; ?>" href="#"><?php echo ucwords($int->city_name); ?>
															<?php if(!empty($int->sub_cities)) : ?>
															<b class="right-caret"></b>
															<?php endif; ?>
														</a>
														<?php if(!empty($int->sub_cities)) : ?>
														<ul class="dl-submenu">
															<?php foreach($int->sub_cities as $sub) : ?>
															<li>
																<a data-id="<?php echo $sub->city_id; ?>" href="#"><?php echo $sub->city_name; ?>
																	<?php if(!empty($sub->child)) : ?>
																		<b class="right-caret"></b>
																	<?php endif; ?>
																</a>
																<?php if(!empty($sub->child)) : ?>
																	<ul class="dl-submenu">
																		<?php foreach($sub->child as $child_cities) : ?>
																		<li><a data-id="<?php echo $child_cities->city_id; ?>" href="#"><?php echo $child_cities->city_name; ?></a></li>
																		<?php endforeach; ?>
																	</ul>
																<?php endif; ?>
															</li>
															<?php endforeach; ?>
														</ul>
														<?php endif; ?>
													</li>
													<?php endforeach; ?>
													
												</ul>
											</li>
										<?php endif; ?>
										</ul>
									</div>
									
								</div>
								<div class="col-sm-5 dl-menu-ct clearfix" id="dl-menu-2-container">
									<div id="dl-menu-2" class="dl-menuwrapper">
										
										<?php if(!empty($categories)) : ?>
										<div class="dl-trigger custom-select" id="custom-select-category">Ad Post Category <b class="caret"></b></div>
										<ul class="dl-menu" id="dl-menu-category" style="z-index:999999 !important">
											<?php foreach($categories as $cat) : ?>
											<li class="first-child">
												<a alt="<?php echo ($cat->id == '8' || $cat->parent_id == '8') ? 'blog' : ''; ?>" data-id="<?php echo $cat->id ?>" href="#"><?php echo $cat->category_name; ?>
													<?php if(!empty($cat->child)) : ?>
													<b class="right-caret"></b>
													<?php endif; ?>
												</a>
													
													<?php if(!empty($cat->child)) : ?>
													<ul class="dl-submenu">
														<?php foreach($cat->child as $cat_c) : ?>
															<li>
																<a alt="<?php echo ($cat->id == '8' || $cat->parent_id == '8') ? 'blog' : ''; ?>" data-id="<?php echo $cat_c->id ?>" href="#"><?php echo $cat_c->category_name; ?>
																<?php if(!empty($cat_c->child)) : ?>
																<b class="right-caret"></b>
																<?php endif; ?>
																</a>
																
																<?php if(!empty($cat_c->child)) : ?>
																<ul class="dl-submenu">
																	<?php foreach($cat_c->child as $cat_d) : ?>
																		<li>
																			<a alt="<?php echo ($cat->id == '8' || $cat->parent_id == '8') ? 'blog' : ''; ?>" data-id="<?php echo $cat_c->id ?>" href="#"><?php echo $cat_d->category_name; ?></a>
																		</li>
																	<?php endforeach; ?>
																</ul>
																<?php endif; ?>
																
															</li>
														<?php endforeach; ?>
													</ul>
													<?php endif; ?>
											</li>
											<?php endforeach; ?>
										</ul>
										<?php endif; ?>
									</div>
									
								</div>
								<div class="col-sm-2"></div>
							</div>
							
							<div class="form-wrapper wrapper">
								
								<fieldset>
									<div class="notifier alert-message" style="display:none">Helo</div>
									<form name="adpost" role="form" action="<?php echo site_url('post/ads/submit'); ?>" method="POST" enctype="multipart/form-data">
										
										<input type="hidden" name="adpost[location_id]" id="location-id" value="">
										<input type="hidden" name="adpost[category_id]" id="category-id" value="">
										<input type="hidden" name="adpost[type]" id="postType" value="">
										
										<div class="category-form clearfix">
											<div class="input col-sm-5">
												<input type="text" name="adpost[title]" class="input" placeholder="Ad Post Title"/>
											</div>
											<div class="input col-sm-4">
												<input type="text" name="adpost[address]" class="input" placeholder="Location"/>
											</div>
											<div class="input col-sm-2 housing-additional-form" style="display:none">
												<input type="text" name="adpost[rent]" class="input" placeholder="Rent"/>
											</div>
											<div class="input col-sm-1 housing-additional-form" style="display:none">
												<input type="text" name="adpost[rooms]" class="input numeric" placeholder="0"/>
											</div>
										</div>
										
										<div class="blog-form clearfix" style="display:none">
											<div class="input col-sm-5">
												<input type="text" name="blogpost[title]" class="input" placeholder="Blog Post Title"/>
											</div>
											<div class="input col-sm-4">
												<input type="text" name="blogpost[address]" class="input" placeholder="Location"/>
											</div>
											<div class="input col-sm-2 housing-additional-form" style="display:none">
												<input type="text" name="blogpost[rent]" class="input" placeholder="Rent"/>
											</div>
											<div class="input col-sm-1 housing-additional-form" style="display:none">
												<input type="text" name="blogpost[rooms]" class="input numeric" placeholder="0"/>
											</div>
										</div>
										
										
										
										<div class="clearfix">
											<div class="input col-sm-12">
												<textarea class="jqte-test" name="adpost[description]"></textarea>
											</div>
										</div>
										
										<div class="category-form clearfix">
											<div class="input info col-sm-5">
												<div>
													<input autocomplete="off" id="email-address" name="adpost[email]" type="text" placeholder="Email Address"/>
													<i id="email-address-valid" class="fa fa-times-circle fa-lg"></i>
												</div>
												<div>
													<input autocomplete="off" id="email-address-2" name="adpost[email]" type="text" placeholder="Email Address"/>
													<i id="email-address-valid-2" class="fa fa-times-circle fa-lg"></i>
												</div>
												<div class="checkbxs email-checkbxs">
													<label class="checkbox">
														<input type="checkbox" value="" name="adpost[is_anonymous_email]" data-toggle="checkbox">
															Anyonmize email
															Example:<span class="email">938291@amyslist.us</span>
                          </label>
												</div>
												<div class="checkbxs">
													<label class="checkbox">
														<input type="checkbox" name="adpost[is_show_username]" value="" data-toggle="checkbox">
															Show Username
                          </label>
												</div>
												<div class="checkbxs">
													<label class="checkbox">
														<input type="checkbox" name="adpost[is_allow_text_chat]" value="" data-toggle="checkbox">
															Allow text chat (when online)
                          </label>
												</div>
												
												<div class="checkbxs">
													<label class="checkbox phone_number_w">
														<input type="checkbox" class="phone_number" value="" data-toggle="checkbox">
															Enter Phone Number
                          </label>
												</div>
												
												<div class="checkbxs">
													<input type="text" name="adpost[phone_number]" value="">
												</div>
												
											</div>
											<div class="map-input col-sm-7">
												<div id="map-canvas" class="map-canvas"></div>
												<div>
													<input id="address" type="text" name="adpost[map_address]" placeholder="Address (Cross Street) or (Neigborhood) (City) (State)"/>
													<button type="button" class="btn btn-large btn-find-place"><i class="fui-search"></i></button>
												</div>
											</div>
										</div>
										<div class="blog-form clearfix" style="display:none">
												<div class="input info col-sm-5">
													<div class="facebook">
														<input id="facebook-link" name="blogpost[facebook]" type="text" placeholder="Facebook Profile Page Link"/>
														<i id="fa-facebook" class="fa fa-check-circle fa-lg"></i>
													</div>
													<div class="twitter">
														<input id="twitter-link" name="blogpost[twitter]" type="text" placeholder="Twitter Link @"/>
														<i id="fa-twitter" class="fa fa-check-circle fa-lg"></i>
													</div>
													<div class="gplus">
														<input id="google-link" name="blogpost[google]" type="text" placeholder="Google Plus Profile"/>
														<i id="fa-google" class="fa fa-check-circle fa-lg"></i>
													</div>
													<div class="tumblr">
														<input id="tumblr-link" name="blogpost[tumblr]" type="text" placeholder="Tumblr"/>
														<i id="fa-tumblr" class="fa fa-check-circle fa-lg"></i>
													</div>
													<div class="pinterest">
														<input id="pinterest-link" name="blogpost[pinterest]" type="text" placeholder="Pinterest Profile"/>
														<i id="fa-pinterest" class="fa fa-check-circle fa-lg"></i>
													</div>
													<div class="other">
														<input type="text" name="blogpost[other]" placeholder="Other Link"/>
														<i id="fa-other" class="fa fa-check-circle fa-lg"></i>
													</div>
													
												</div>
												
												<div class="col-sm-7">
													<div>
														<input type="text" name="blogpost[video_link]" placeholder="Copy and Paste a Video Link Here - YouTube, Instagram, Facebook, Vine"/>
													</div>
													<div>
														<img class="img-responsive" src="<?php echo $this->config->item('assets_uploads'); ?>sample-blog-video.jpg">
													</div>
												</div>
										</div>
										
										<div class="category-form clearfix">
											<div class="input video-link col-sm-5">
												<input type="text" name="adpost[video_link]" placeholder="Video Link (Copy and paste here) (Optional)"/>
											</div>
											<div class="col-sm-7">
												<div class="video-description">
													<h3>Video Link Description</h3>
													<p>Want to add a video with your post? Record a video, upload it to YouTube, then copy and paste the  link here. Your video will display with your post under the text and icon titled "video".</p>
												</div>
											</div>
										</div>
										<div class="blog-form clearfix" style="display:none">
												<div class="col-sm-5"></div>
													<div class="col-sm-7">
														<div class="video-description">
															<h3>Video Description</h3>
															<p>Please type a brief of video description and caption here. So people who read your blog will know a little bit about what the video is and how it relates to your blog</p>
														</div>
													</div>
										</div>
										
										<div class="upload-wrapper">
											<div class="clearfix">
												<div class="col-sm-4">
													<div class="control">
														<div class="btn-control">
															<h3>
																<span>Upload Photos</span>
															</h3>
														</div>
														<h4>Edit your photos<br>
															<span>(Select up to 8 photos)</span>
														</h4>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
												</div>
												
												<div class="col-sm-4">
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
												</div>
											</div>
											
											<div class="clearfix">
												<div class="col-sm-4">
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
												</div>
											</div>
											
											<div class="clearfix">
												<div class="col-sm-4">
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div data-trigger="fileinput" class="img-upload-wrapper fileinput-preview thumbnail"></div>
														<textarea name="adpost[photo_description][]" placeholder="Photo description here (optional)"></textarea>
														<div class="btn-select-image">
															<span class="btn btn-primary btn-embossed btn-file">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="photo_file[]" >
															</span>
															<a href="#" class="btn btn-danger btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="submit">
											<input type="submit" class="btn btn-primary btn-continue" value="Continue">
										</div>
										
									</form>
								</fieldset>
							</div>
							
						</article>
					</div>
				</section>
				
				<div class="modal fade modal-login" id="processing-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<div class="clearfix">
									Processing
								</div>
							</div>
							<div class="modal-body">
								<div id="notifier" style="display:none">Uploading ...</div>
								<div id="progress-1" class="progress">
									<div class="bar"><span class="percent"></span></div >
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<a class="btn process" data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#processing-modal" style="display:none">process</a>
		<script type="text/javascript" src="<?php echo asset_url(); ?>jqte/jquery-te-1.4.0.min.js" charset="utf-8"></script>
		<link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>jqte/jquery-te-1.4.0.css">
		
		<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('assets_css'); ?>component.css" />
		<script>
			$(document).ready(function(){
				
				
				if($('#email-address').val() != '')
				{
					validateEmail($('#email-address').val());
				}
				
				if($('#email-address-2').val() != '')
				{
					validateEmail($('#email-address-2').val());
				}
				
				$('input[name="adpost[phone_number]"]').prop('disabled', true);
				$('.jqte-test').jqte({
					fsize:false,
				});
				$('.numeric').stepper();
				$('.numeric').css({'padding-right' : 25});
				$('.stepper-wrap').css({'margin-right':0});
				$('.stepper-btn-wrap').css({'height' : 35,'margin' : 0});
				
				$('#custom-select-location').click(function(){
					$('#dl-menu-2-container').addClass('addlayer');
				});
				
				
			});
		</script>
		<script src="<?php echo $this->config->item('assets_js'); ?>jquery.stepper.min.js"></script>
		<script src="<?php echo $this->config->item('assets_js'); ?>hoverIntent.js"></script>
		<script src="<?php echo $this->config->item('assets_js'); ?>modernizr.custom.js"></script>
		<script src="<?php echo $this->config->item('assets_js'); ?>jquery.dlmenu.js"></script>
		<script src="<?php echo $this->config->item('assets_js'); ?>flatui-fileinput.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
		<script>
					function initialize() {
						var myLatlng = new google.maps.LatLng(40.7590615,-73.969231);
						var mapOptions = {
							zoom: 12,
							center: myLatlng
						}
						var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

						var marker = new google.maps.Marker({
								position: myLatlng,
								map: map,
								title: 'Hello World!'
						});
					}

					google.maps.event.addDomListener(window, 'load', initialize);

		</script>
		<script>
			
			$('input#address, form#postanAd').keypress(function(e){
				
				if(e.keyCode == 10 || e.keyCode == 13)
				{
					renderMap();
					e.preventDefault();
				}
				
			});
			
			$('.btn-find-place').click(function(e){
				renderMap();
				e.preventDefault();
				return false;
			});
			
			function validateEmail(email) { 
				var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test(email);
			}
			
			$('#email-address').keyup(function(){
				
				var is_valid = validateEmail($(this).val());
				
				if(is_valid)
				{
					$('#email-address-valid').removeClass('fa-times-circle').addClass('fa-check-circle').show();
					if($(this).val() == ''){
						$('#email-address-valid').removeClass('fa-times-circle').addClass('fa-check-circle').hide();
					}
				}
				else
				{
					$('#email-address-valid').removeClass('fa-check-circle').addClass('fa-times-circle').show();
					if($(this).val() == ''){
						$('#email-address-valid').removeClass('fa-times-circle').addClass('fa-check-circle').hide();
					}
				}
				
				
				
			});
			
			$('#email-address-2').keyup(function(){
				
				
				var is_valid = validateEmail($(this).val());
				
				if(is_valid)
				{
					$('#email-address-valid-2').removeClass('fa-times-circle').addClass('fa-check-circle').show();
					if($(this).val() == ''){
						$('#email-address-valid-2').removeClass('fa-times-circle').addClass('fa-check-circle').hide();
					}
				}
				else
				{
					$('#email-address-valid-2').removeClass('fa-check-circle').addClass('fa-times-circle').show();
					if($(this).val() == ''){
						$('#email-address-valid-2').removeClass('fa-times-circle').addClass('fa-check-circle').hide();
					}
				}
			});
			
			$("select").selectpicker({style: 'btn', menuStyle: 'dropdown-inverse'});
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
				
				$( '#dl-menu-2' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
				
				$('#dl-menu-location a').click(function(){
					if(!$(this).find('b').length){
						var val = $(this).text();
						var num = $(this).attr('data-id');
						
						$('#location-id').val(num);
						$('#custom-select-location').html(val + '<b class="caret"></b>');
						$('body').click();
					}
				});
				
				$('#dl-menu-category a').click(function(){
					var is_housing = false;
					
					if($(this).attr('alt') == 'housing'){
						$('.housing-additional-form').show();
					} else {
						$('.housing-additional-form').hide();
					}
					
					if(!$(this).find('b').length){
						var val = $(this).text();
						var num = $(this).attr('data-id');
						
						$('#category-id').val(num);
						$('#custom-select-category').html(val + '<b class="caret"></b>');
						$('body').click();
					}
					
					if($(this).attr('alt') == 'blog'){
						$('.category-form').hide();
						$('.blog-form').show();
					} else {
						$('.category-form').show();
						$('.blog-form').hide();
					}
				});
				
				
				
				
				$(document).ready(function(){
					$('li.dl-back a').each(function(){
						if($(this).text() == ''){
							$(this).remove();
						}
					});
					
					$('ul.dl-submenu a').each(function(){
						if($.trim($(this).text()) == ''){
							$(this).remove();
						}
					});
					
					
					$('ul.dl-submenu li').not(':first').each(function(){
					});
					
					$('.fa-times-circle').hide();
					
					
					if(validateEmail($('#email-address').val())){
						$('#email-address-valid').removeClass('fa-times-circle').addClass('fa-check-circle').show();
					}
					
					if(validateEmail($('#email-address-2').val())){
						$('#email-address-valid-2').removeClass('fa-times-circle').addClass('fa-check-circle').show();
					}
				});
				
				function renderMap(){
				
					var address = $('#address').val();
					if(address == '') {
						google.maps.event.addDomListener(window, 'load', initialize);
					} 
					else
					{
					
						var places = [];
						var map = $('#map-canvas');
						// Adding a LatLng object for each city
						//places.push(new google.maps.LatLng(40.756, -73.986));
						//places.push(new google.maps.LatLng(37.775, -122.419));
						//places.push(new google.maps.LatLng(47.620, -122.347));
						//places.push(new google.maps.LatLng(-22.933, -43.184));
						var geocoder = new google.maps.Geocoder();
						geocoder.geocode( { 'address': address}, function(results, status) {
							console.log(status);
							if (status == google.maps.GeocoderStatus.OK) {
								
								var myLatlng = new google.maps.LatLng(results[0].geometry.location.lat(),results[0].geometry.location.lng());
								var mapOptions = {
									zoom: 12,
									center: myLatlng
								}
								var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

								var marker = new google.maps.Marker({
										position: myLatlng,
										map: map,
										title: 'Hello World!'
								});
								
							} else {
								alert("Geocode was not successful for the following reason: " + status);
							}
						});
						
					}
					
					return false;
					
				}
				
				$('#map-search').click(function(){
					renderMap();
				});
				
				$('.btn-continue').click(function(){
					
					var type = 'error';
					var timeout = 2000;
					
					var categoryId = $('#category-id').val();
					var locationId = $('#location-id').val();
					
					$('.alert-message').html('').hide();
					
					if(categoryId == '' || locationId == ''){
						
						var n = noty({
							text: 'Please select a Location and Ad Post Category',
							type: type,
							timeout: timeout
						});
						return false;
						
					}
					
					var title = $('input[name="adpost[title]"]');
					var location = $('input[name="adpost[address]"]');
					var description = $('input[name="adpost[description]"]');
					var email = $('input[name="adpost[email]"]');
					var confirmEmail = $('input[name="adpost[email]"]');
					// var address = $('input[name="adpost[map_address]"]');
					// var picture = $('input[name="uploadFile"]');;
					
					if(title.val() == ''){
						title.focus();
						// $('.alert-message').html('Please enter title').show();
						var n = noty({
							text: 'Please enter title',
							type: type,
							timeout: timeout
						});
						return false;
					}
					if(location.val() == ''){
						location.focus();
						// $('.alert-message').html('Please enter location field').show();
						var n = noty({
							text: 'Please enter location field',
							type: type,
							timeout: timeout
						});
						return false;
					}
					if(description.val() == ''){
						description.focus();
						// $('.alert-message').html('Please enter description').show();
						var n = noty({
							text: 'Please enter description',
							type: type,
							timeout: timeout
						});
						return false;
					}
					if(email.val() == ''){
						email.focus();
						// $('.alert-message').html('Please enter your email address').show();
						var n = noty({
							text: 'Please enter your email address',
							type: type,
							timeout: timeout
						});
						return false;
					}
					if(confirmEmail.val() == ''){
						confirmEmail.focus();
						// $('.alert-message').html('Please enter your email address').show();
						var n = noty({
							text: 'Please enter your email address',
							type: type,
							timeout: timeout
						});
						return false;
					}
					if(confirmEmail.val() != email.val()){
						confirmEmail.focus();
						// $('.alert-message').html('Email address does not match').show();
						var n = noty({
							text: 'Email address does not match',
							type: type,
							timeout: timeout
						});
						return false;
					}
					
					 // if(address.val() == ''){
						 // address.focus();
						 // $('.alert-message').html('Please enter address / location').show();
						 // return false;
					 // }
					
					// var select_files = false;
					// $('input[name="photo_file[]"]').each(function(){
						// if($(this).val() != '')
						// {
							// select_files = true;
						// }
					// });
					
					// if(select_files === false)
					// {
						// $('.alert-message').html('Please select at least 1 photo').show();
						// return false;
					// }
					
					$('.process').click();
					
					var bar = $('.bar');
					var percent = $('.percent');
					var status = $('#status');
					
					
					
					$('form[name="adpost"]').ajaxSubmit({
							dataType:  'json', 
							url: $('base').attr('alt') + 'post/ads/submit/',
							beforeSend: function() {
								status.empty();
								var percentVal = '0%';
								bar.width(percentVal)
								percent.html(percentVal);
							},
							
							uploadProgress: function(event, position, total, percentComplete) {
								var percentVal = percentComplete + '%';
								bar.width(percentVal)
								percent.html(percentVal);
							},
							
							success: processJson
					});
					
					return false;
				});
				
				function processJson(data){
				
					var bar = $('.bar');
					var percent = $('.percent');
					var status = $('#status');
					
					var percentVal = '100%';
					bar.width(percentVal)
					percent.html(percentVal);
					if(data.success == 'success'){
						window.location = data.redirect
						// alert(data.redirect);
					} else {
						return false;
					}
					
				}
				
				$('.phone_number').change(function(){
					
					if($('.phone_number_w').hasClass('checked'))
					{
						$('input[name="adpost[phone_number]"]').prop('disabled', false);
					}
					else
					{
						$('input[name="adpost[phone_number]"]').prop('disabled', true);
					}
					
				});
			
		</script>
		
		
		