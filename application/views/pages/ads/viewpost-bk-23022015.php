				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-review">
							
							<div class="clearfix ct-review-wrapper">
							
								<?php
								/*echo "<pre>";
								print_r($category);
								echo "</pre>";
								function get_url_name($real_name){
									$url_name = str_replace(' ', '-', strtolower($real_name));
									$url_name = str_replace('/', '-', $url_name);
									$url_name = str_replace('.', '', $url_name);
									$url_name = str_replace('\'', '', $url_name);
								return $url_name;
								}*/
								?>
								<div class="row clearfix breadcumb-wrapper">
									<div class="review-breadcumb">
										<ul class="list-unstyled breadcumb">
                                            <!--<li><a href="<?php //echo site_url($location_parent . '/' . $location_child);?>"><?php echo $location; ?></a></li>-->
											<li><a href="<?php echo site_url($location_child);?>"><?php echo $location; ?></a></li>
											<li><a href="#">/</a></li>
											<li><a href="<?php  echo site_url($location_child."/".$category['parent-url-name']); ?>"><?php echo $category['parent']; ?></a></li>
											<li><a href="#">/</a></li>
                                            <!--<li><a href="<?php // echo site_url($location_parent . '/' . $location_child); ?>"><?php echo $category['child']; ?></a></li>-->
											<li><a href="<?php  echo site_url($location_child."/".$category['parent-url-name']."/".$category['child-url-name']); ?>"><?php echo $category['child']; ?></a></li>
										</ul>
									</div>
									
								</div>
                                
                                <style type="text/css">
                                    #rad_ul .dropdown-menu
                                    {
                                        background: #fff;
                                        overflow: hidden;
                                        width: 115%;
                                        border: 1px solid #ccc;
                                        padding: 2px 7px;
                                    }
                                    
                                    #rad_ul .dropdown-menu li
                                    {
                                        padding-top: 10px;
                                        min-height: 60px;  
                                        padding-left: 40px;
                                    }
                                    
                                    #rad_ul .dropdown-menu li:hover
                                    {
                                        background-color: #fff;
                                    }
                                    
                                    #rad_ul .dropdown-menu li a
                                    {
                                        color: #000;
                                        font-weight: bold;
                                    }
                                    
                                    #rad_ul .dropdown-menu li a:hover
                                    {
                                        background: none !important;
                                    }
                                    
                                    #rad_ul .copy_icon
                                    {
                                        background: url('<?php echo asset_url();?>img/icons/copy.png') no-repeat left #fff;
                                    }
                                    
                                    #rad_ul .copy_icon #copy_email_id
                                    {
                                        display: none;
                                    }
                                    
                                    #rad_ul .gmail_icon
                                    {
                                        background: url('<?php echo asset_url();?>img/icons/gmail.png') no-repeat left #fff;
                                    }
                                    
                                    #rad_ul .yahoo_icon
                                    {
                                        background: url('<?php echo asset_url();?>img/icons/yahoo.png') no-repeat left #fff;
                                    }
                                    
                                    #rad_ul .hotmail_icon
                                    {
                                        background: url('<?php echo asset_url();?>img/icons/hotmail.png') no-repeat left #fff;
                                    }
                                    
                                    #rad_ul .aol_icon
                                    {
                                        background: url('<?php echo asset_url();?>img/icons/aol.png') no-repeat left #fff;
                                    }
                                    
                                </style>
                                
								<h2><?php echo $detail->title; ?></h2>
								<ul id="rad_ul" class="list-unstyled nav-head">
									
									<li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-envelope-o fa-lg"></i><?php echo ((bool) $detail->is_anonymous_email) ? $detail->anonymous_email : $detail->email; ?></a>
                                        <ul class="dropdown-menu" style="margin-top: -1px;">
                                            <li class="copy_icon">
                                                <a href="#">Copy and Paste Email</a>
                                                <span id="copy_email_id"><?php echo ((bool) $detail->is_anonymous_email) ? $detail->anonymous_email : $detail->email; ?></span>
                                            </li>
                                            <li class="gmail_icon"><a href="https://mail.google.com" title="Gmail" target="_blank">Gmail</a></li>
                                            <li class="yahoo_icon"><a href="https://mail.yahoo.com" title="Yahoo Mail" target="_blank">Yahoo Mail</a></li>
                                            <li class="hotmail_icon"><a href="https://mail.aol.com/" title="Hotmail / Outlook" target="_blank">Hotmail / Outlook</a></li>
                                            <li class="aol_icon"><a href="https://mail.live.com" title="Aol Mail" target="_blank">Aol Mail</a></li>
                                        </ul>
                                    </li>
									
									<?php if($detail->phone_number != '') :?>
									<li id="phone_num_rad"><a href="#"><i class="fa fa-mobile-phone fa-lg"></i><?php echo $detail->phone_number; ?></a><span style="display: none;" id="rad_phone_txt"><?php echo $detail->phone_number; ?></span></li>
									<?php endif; ?>
									
									<li>
										<a href="<?php echo ((bool) $detail->is_show_username) ? site_url('/profile/' . $user->username) : 'javascript:void(0)'; ?>">
											<i class="fa fa-user fa-lg"></i><?php echo ((bool) $detail->is_show_username) ? $user->username : '<em>-hidden-</em>';?>
										</a>
									</li>
									<li><a href="#"><i class="fa fa-inbox fa-lg"></i>Inbox</a></li>
									
                                    <!-- chat buton -->
                                    <?php if($session['user_id'] != $user->id && is_logged_in()) : ?>
                                    <?php if((bool) $detail->is_allow_text_chat) :?>
									   <li><a onclick='javascript:jqcc.cometchat.chatWith("<?=$user->id;?>");' href="javascript:void(0);"><i class="fa fa-comment-o fa-lg"></i>Text Video Chat</a></li>
									<?php endif; ?>
                                    <?php endif; ?>
									<!-- chat button -->
                                    
									<?php if($detail->video_link != '') : ?>
									<li><a data-toggle="modal" href="#video-in-modal"><i class="fa fa-youtube-play fa-lg"></i>Video</a></li>
									<?php endif; ?>
								</ul>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $('ul#rad_ul li.dropdown').hover(function() {
                                          $(this).find('.dropdown-menu').stop(true, true).delay(200).show();
                                          $(this).addClass('open');
                                        }, function() {
                                            var temp = false;
                                            $(this).find('.dropdown-menu').hover(function(){}, function(){ temp = true;});
                                            
                                            if(!temp){
                                                $(this).find('.dropdown-menu').stop(true, true).delay(300).hide();
                                                $(this).removeClass('open');
                                            }
                                              
                                        });
                                        
                                        $("#phone_num_rad").click(function(){
                                            //alert($("#rad_phone_txt").html()); 
                                            window.prompt ("Copy phone number:", $("#rad_phone_txt").html());                                       
                                        });
                                         
                                        $("#rad_ul li.dropdown ul li.copy_icon a").click(function(){
                                              var holdtext = $("li.copy_icon #copy_email_id").html();                                                                                            
                                              window.prompt ("Copy and paste into your email:", holdtext);                                                                                            
                                        });
                                    });                                    
                                    
                                </script>
								
								<div class="clearfix">
									<div class="col-md-8">
										<?php if(!empty($detail->pictures)) : ?>
										<div id="owl-demo" class="owl-carousel">
											<?php foreach($detail->pictures as $pic) : ?>
											<div class="item">
												<img src="<?php echo asset_url() .'tn/image.php?width=600&image='. asset_url() . $pic->image_path; ?>" class="img-responsive" />
												<?php if($pic->image_description != '') : ?>
												<div class="caption"><?php echo $pic->image_description; ?></div>
												<?php endif; ?>
											</div>
											<?php endforeach; ?>
										</div>
										<?php else : ?>
										<div class="description"><?php echo $detail->description; ?></div>
										<?php endif; ?>
									</div>
									
                                    
                                    <div class="col-md-4">
										<div style="min-height:350px;">
										    <?php if($detail->map_address != '') : ?>
                                            <div id="map-canvas" class="map-canvas" style="height:315px;width:100%;"></div>
                                            <address>
                                                <?php echo $detail->map_address; ?>
                                            </address>
										                    <?php endif; ?>
                                        </div>
									</div>
								
                                
                                </div>
								
								<?php if(!empty($detail->pictures)) : ?>
								<br/>
								<div class="description"><?php echo $detail->description; ?></div>
								<?php endif; ?>
								
								<div>
								<br />
								<br /> 
								
<!-- AddToAny BEGIN -->
<div class="a2a_kit a2a_kit_size_30 a2a_default_style">
<a class="a2a_dd" href="https://www.addtoany.com/share_save"></a>
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_pinterest"></a>
<a class="a2a_button_google_gmail"></a>
<a class="a2a_button_yahoo_mail"></a>
<a class="a2a_button_linkedin"></a>
<a class="a2a_button_tumblr"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_button_whatsapp"></a>

</div>
<script type="text/javascript">
var a2a_config = a2a_config || {};
a2a_config.color_main = "D7E5ED";
a2a_config.color_border = "AECADB";
a2a_config.color_link_text = "333333";
a2a_config.color_link_text_hover = "333333";
</script>
<script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->

</div>

							</div>
							<br> 
							
							<div class="fb-comments" data-href="<?php echo base_url() . 'viewpost/'.$detail->post_num_id .'/'. $detail->url_name; ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
							
						</article>
					</div>
				</section>
				
				<link href="<?php echo asset_url(); ?>css/owl.carousel.css" rel="stylesheet">
				<link href="<?php echo asset_url(); ?>css/owl.transitions.css" rel="stylesheet">
				<link href="<?php echo asset_url(); ?>css/owl.theme.css" rel="stylesheet">
				
				<script src="<?php echo asset_url(); ?>js/owl.carousel.js"></script>
				<script>
					$(document).ready(function() {
						$("#owl-demo").owlCarousel({
							navigation : true,
							slideSpeed : 300,
							paginationSpeed : 400,
							singleItem : true
						});
					});
				</script>
			
				
				<script src="<?php echo asset_url(); ?>js/facebook-share.js"></script>
				
					
						<div class="modal fade modal-login" id="video-in-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<?php if($detail->video_link != '') : ?>
									<div class="modal-body">
										<div class="video-box clearfix">
											<div class="video-yt-wrapper">
												<?php echo preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>", $detail->video_link);?>
											</div>
											
											<div class="clearfix">
												<ul class="list-unstyled nav-head">
													<li><a href="#"><i class="fa fa-envelope-o fa-lg"></i><?php echo ((bool) $detail->is_anonymous_email) ? $detail->anonymous_email : $detail->email; ?></a></li>
													<li><a href="<?php echo site_url('/profile/' . $user->username); ?>"><i class="fa fa-user fa-lg"></i><?php echo ((bool) $detail->is_show_username) ? $user->username : '<em>-hidden-</em>';?></a></li>
													<li><a href="#"><i class="fa fa-inbox fa-lg"></i>Inbox</a></li>
													<?php if((bool) $detail->is_allow_text_chat) :?>
													<li><a href="#"><i class="fa fa-comment-o fa-lg"></i>Text Video Chat </a></li>
													<?php endif; ?>
												</ul>
											</div>
											 
											<div class="clearfix">
												<div class="fb-comments" data-href="<?php echo base_url() . 'viewpost/'.$detail->post_num_id .'/'. $detail->url_name; ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
											</div>
			
										</div>
				                     </div>
				                   <?php endif; ?>
				</script>
				
				<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
                <span id="map_address" alt="<?php echo $detail->map_address; ?>"></span>
				<script>
					$(document).ready(function() {
						
						var address = $('#map_address').attr('alt');
						if(address == '') return false;
						
						var places = [];
						var map = $('#map-canvas');
						var geocoder = new google.maps.Geocoder();
						geocoder.geocode( { 'address': address}, function(results, status) 
                        {
							console.log(status);
							if (status == google.maps.GeocoderStatus.OK) {
								
								var myLatlng = new google.maps.LatLng(results[0].geometry.location.lat(),results[0].geometry.location.lng());
								var mapOptions = {
									zoom: 12,
									center: myLatlng
								}
								var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

								var marker = new google.maps.Marker({
										position: myLatlng,
										map: map,
										title: 'Hello World!'
								});
								
							} else {
								alert("Geocode was not successful for the following reason: " + status);
							}
						});
						
					});
				</script>
      
									
								</div>
							</div>
							
						</div>
					