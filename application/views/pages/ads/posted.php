				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
					
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-advt-posted">
							<div class="row clearfix breadcumb-wrapper">
								<ul class="list-unstyled breadcumb">
									<li><a href="<?php echo site_url('user/profile/' . $user_id); ?>"><?php echo $name; ?></a></li>
									<li><a href="#">/</a></li>
									<li><a href="#">Ads Posted</a></li>
								</ul>
							</div>
							
							<?php if(empty($my_ads)) : ?>
							<div style="padding:30px 15px">
								<h3 style="font-size:14px;">No record found.</h3>
								<button class="btn btn-primary" onclick="window.location='<?php echo site_url('post/ads'); ?>'">Create new post</button>
							</div>
							<?php else : ?>
							<div class="advt-posted-wrapper">
								<form action="<?php echo site_url('ads/posted/deleteAll/'); ?>" method="POST" name="delete_form">
								    <?php foreach($my_ads as $row) : ?>
									<div class="clearfix advt-posted">
										<div class="pull-left check">
											<label class="checkbox">
												<input type="checkbox" class="checkBoxClass" name="ads[]" value="<?php echo $row->id; ?>" data-toggle="checkbox">                
											</label>
										</div>
										<div class="info">
											<h3><a href="<?php echo site_url('viewpost/' . $row->post_num_id . '/' . $row->url_name); ?>"><?php echo $row->title; ?></a></h3>
											<div class="status">
												<?php if($row->expired_at == '' || TRUE) : ?>
													<?php if((bool)$row->post_status == 1) : ?>
													<span class="active">Active</span>
													<?php else : ?>
													<span class="expired">Inactive</span>
													<?php endif; ?>
												<?php else : ?>
													<?php if(time() > strtotime($row->expired_at)) : ?>
													<span class="expired">Expired</span>
													<?php elseif((bool)$row->post_status == 1) : ?>
													<span class="active">Active</span>
													<?php else : ?>
													<span class="expired">Inactive</span>
													<?php endif; ?>
												<?php endif; ?>
												<span>Date Posted: <?php echo date('m/d/Y', strtotime($row->created_at)); ?></span>
												
												<?php if((bool) $row->post_status == 1) : ?>
												<span>Link : <a style="color:#024883" href="<?php echo site_url('viewpost/' . $row->post_num_id . '/' . $row->url_name); ?>"><?php echo site_url('viewpost/' . $row->post_num_id . '/' . $row->url_name); ?></a></span>
												<?php endif; ?>
											</div>
											<div class="action">
                                                
                                                <?php if((bool)$row->post_status == 0) { ?>
                                                   <button type="button" class="btn unpublish-btn" onclick="window.location='<?php echo site_url('post/ads/review/' . $row->post_num_id); ?>'">Publish</button>
                                                <?php } else if(time() > strtotime($row->expired_at)) { ?>
												   <button class="btn" onclick="window.location='<?php echo site_url('ads/posted/repost/' . $row->id); ?>'">RePost</button>
                                                <?php } ?>
												
												<button type="button" class="btn" onclick="window.location='<?php echo site_url('ads/posted/edit/' . $row->id); ?>'">Edit</button>
												<button type="button" class="btn btn-primary" onclick="confirmDelete('<?php echo $row->id; ?>')">Delete</button>
											</div>
										</div>
									</div>   
									<?php endforeach; ?>  
									
									<div class="action-row">
										<button class="btn btn-primary" onclick="confirm('Are you sure want to delete selected posts?')">Delete Selected</button>
									</div>
								</form>
                                <button class="btn" id="selecctall" onclick="selecctall('selecctall')">Select All</button>         
								
							</div>
							<?php endif; ?>
						</article>
					</div>
				</section>
				
				<script>
					function confirmDelete(id)
					{
						if(confirm('Are you sure want to delete this post?'))
						{
							window.location=$('base').attr('alt') + 'ads/posted/delete/' + id;
						}
						else
							return false;
					}
                    
                 function selecctall(id)
                 {
                    $('.checkBoxClass').each(function() {
                         $('.checkbox').addClass('checked');
                         this.checked = true;          
                    });    
                 }
                    
				</script>