				<section class="ct-content clearfix ct-post-ad-final">
					<div class="container">
						<?php load_sidebar(); ?>
						
						<article class="col-lg-10 right-content ct-thank-you">
							
							<div class="thank-you-box">
								
								<h2>Thank You For Posting on Amy's List</h2>
								<p>We sent you a confirmation email with the details of your ad post.</p>
								<p>You can also <a href="<?php echo site_url('ads/posted'); ?>">Click the Ads Posted Tab</a> to review, edit, and or delete your post</p>
								
								<h1><img src="<?php echo asset_url(); ?>img/document.png">Ads Posted</h1>
								
								<button onclick="window.location='<?php echo site_url('ads/posted'); ?>'" class="btn btn-primary btn-thank-you">Continue</button>
								
							</div>
						</article>
					</div>
				</section>