						<div class="col-lg-2">
							<div class="logged-in clearfix">
								<div class="admin-logo">
									<a href="">
										<img src="<?php echo $this->config->item('assets_images'); ?>amyface.png" width="110" height="110">
									</a>
								</div>
								<div class="navbar-collapse left-menu collapse">
									<ul class="list-unstyled">
                    <li class="user-name clearfix">
											<p><a style="color:#333" href="<?php echo site_url('user/profile'); ?>"><?php echo profile_name(); ?></a></p>
											<img class="img-circle" width="45" height="45" src="<?php echo asset_url() .'tn/tn.php?w=45&h=45&src='. asset_url() . profile_picture(); ?>">
                    </li>
                    <li rel="0" class="">
											<span class="u-icon profile"></span>
											<a tabindex="-1" href="<?php echo site_url('user/profile'); ?>" class="opt">
												Profile
                      </a>
                    </li>
                    <li rel="1">
											<a tabindex="-1" href="#" class="opt ">
												<span class="u-icon inbox"></span> Inbox
                      </a>
                    </li>
                  							<li rel="3">
											<a tabindex="-1" href="<?php echo site_url('user/notifications'); ?>" class="opt ">
												<span class="u-icon notifications"></span> Notifications
												<?php if($notif > 0) : ?>
												<span class="num-notif"><?php echo $notif; ?></span>
												<?php endif; ?>
											</a>
										</li>
										<li rel="4">
											<a tabindex="-1" href="<?php echo site_url('ads/posted'); ?>" class="opt ">
												<span class="u-icon ads-posted"></span> Ads Posted
											</a>
										</li>
										<li rel="5">
											<a tabindex="-1" href="<?php echo site_url('user/video'); ?>" class="opt ">
												<span class="u-icon video-bio"></span> Video
											</a>
										</li>
										<li rel="8">
											<a tabindex="-1" href="<?php echo site_url('account/settings'); ?>" class="opt ">
												<span class="u-icon account-settings"></span> Account Settings
											</a>
										</li>
										<li rel="9">
											<a tabindex="-1" href="<?php echo site_url('auth/auth/logout'); ?>" class="opt ">
												<span class="u-icon log-out"></span> Log Out
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>