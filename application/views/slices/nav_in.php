					<div class="container">
						<div class="navbar-header">
					
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							
							<a class="navbar-brand" href="<?php echo site_url('/'); ?>"><img width="40" class="face" height="40" src="<?php echo $this->config->item('assets_images'); ?>amyface.png"><img src="<?php echo $this->config->item('assets_images'); ?>theme/logo.png" width="" height=""/></a>
							
							<ul class="nav navbar-nav">
								<li><a href="<?php echo site_url('/'); ?>"><span class="header-icon logo"></span><span class="text">City Page</span></a></li>
								<li><a href="<?php echo site_url('post/ads'); ?>"><span class="header-icon post-advt"></span><span class="text">Post An ad</span></a></li>
								<li>
									<a href="<?php echo site_url('user/notifications'); ?>">
										<span class="header-icon notifications"></span>
										<span class="text">Notifications
										<?php if(count_notification() > 0) : ?>
											<span class="notif-circle"></span>
										<?php endif; ?>
										</span>
									</a>
								</li>
								<li class="search-icon"><a href="<?php echo site_url('search'); ?>"><span class="fui-search"></span><span class="text">Search</span></a></li>
							</ul>
							
						</div>
						
						<?php $this->load->view('slices/navbar_right'); ?>
						
					</div>