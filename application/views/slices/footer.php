						<div class="container">       
                <div class="bottom-menu">
                
									<div class="clearfix">
										<div class="col-sm-3">
																				
										<a href="<?php echo site_url('/'); ?>" class="footer-brand">
											<img src="<?php echo $this->config->item('assets_images'); ?>theme/footer-logo.png" >
										</a>
										<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.BOTTOM_LEFT, gaTrack: true, gaId: 'UA-32380720-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
										</div>
										
										<div class="col-sm-9">
                                        <ul class="bottom-links clearfix">
                                        <li><a href="<?php echo site_url('about'); ?>">About Us</a></li>
												<li><a href="<?php echo site_url('features'); ?>">Features</a></li>
												<li><a href="<?php echo site_url('founders'); ?>">Founders</a></li>
												<li><a href="<?php echo site_url('posting-an-ad'); ?>">Posting An Ad</a></li>
												<li><a href="<?php echo site_url('page-advertising'); ?>">Page Advertising</a></li>
												<li><a href="<?php echo site_url('terms-of-use'); ?>">Terms</a></li>
												<li><a href="<?php echo site_url('privacy'); ?>">Privacy</a></li>
												<li><a href="http://www.facebook.com/amyslist.us"><i class="fa fa-facebook-square"></i>Facebook</a></li>
												<li><a href="http://www.twitter.com/amyslist_us"><i class="fa fa-twitter"></i>Twitter</a></li>
												<li><a href="<?php echo site_url('contact-us'); ?>">Contact</a></li>
                                        
					</div>
                                        
										
										<div class="col-sm-9">
                                        <ul class="bottom-links clearfix">
                                      			
				        <li>Amy's List is designed by <a href="https://plus.google.com/+ChrisManhattan/posts">Chris Manhattan</a></li>
		                        <li>Coded and developed by <a href="https://www.odesk.com/users/~01008da20e623e6f65">Rizki Aprilian</a></li>
                                        <li>Text and Video Chat provided by <a href="https://www.cometchat.com">CometChat</a></li>
                                        </ul>
        
										</div>
									</div>
	            </div>
            </div>
				
						
						<?php load_noty(); ?>
						<?php if(!is_logged_in()) :?>
						<div class="modal fade modal-login" id="sign-in-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<div class="clearfix">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										</div>
										<div class="clearfix">
											<div class="pull-left amy-face"><img width="70" height="70" src="<?php echo $this->config->item('assets_images'); ?>sign/amyface.png"></div>
											<h4 class="modal-title">Sign In</h4>
										</div>
										
										
									</div>
									<div class="modal-body">
										<span style="color:#ff0000;font-size:13px;" id="a-result"></span>
										<form action="<?php echo site_url('auth/auth/login'); ?>" id="sign-in-form" method="POST" class="sign-in-form">
											<div class="control-group">
												<input type="text" name="auth[email]" placeholder="Username" >
													<span class="username"></span>
													<!-- <p>Username <br/>available</p> -->
													<!-- <i class="input-icon fui-check-inverted"></i> -->
											</div>
											<div class="control-group">
												<input type="password" placeholder="Password" name="auth[password]">
												<span class="password"></span>
												<!-- <p>Passwords <br/>don�t match</p> -->
												<!-- <i class="input-icon fui-check-inverted"></i> -->
											</div>
											<button class="btn btn-pink btn-block btn-in" type="button">Sign In</button>
											
											<div class="row clearfix">
												<div class="col-sm-6">
													<div class="checkbxs email-checkbxs">
														<label class="checkbox">
															<input type="checkbox" value="" data-toggle="checkbox" name="auth[remember]">
															Remember Me
														</label>
													</div>
												</div>
												<div class="col-sm-6">
													<a href="<?php echo site_url('forgot-password'); ?>" class="forgot">Forgot password?</a>
												</div>
											</div>
											
										</form>
										<a onclick="checkLoginState();" href="javascript:void(0)" class="fb-connect"><img src="<?php echo $this->config->item('assets_images'); ?>sign/fb-connect.png" class="img-responsive"></a>
									</div>
								</div>
							</div>
						</div>
						
						<script>
							
							function processLogin(e,p)
							{
								
								$('#a-result').html('');
								
								var e = $('input[name="auth[email]"]').val();
								var p = $('input[name="auth[password]"]').val()
								
								if(e == '')
								{
									$('#a-result').html('Incorrect Email');
									return false;
								}
								
								if(e.length < 3)
								{
									$('#a-result').html('Incorrect Email');
									return false;
								}
								
								if(p == '')
								{
									$('#a-result').html('Incorrect Password');
									return false;
								}
								
								if(p.length < 3)
								{
									$('#a-result').html('Incorrect Password');
									return false;
								}
								
								$.ajax({
									type : "POST",
									url  : $('#sign-in-form').attr('action'),
									data : { email : e, password : p }
								}).done(function(r){
									
									var j = $.parseJSON(r);
									
									if(j.status == '1')
									{
										window.location = j.redirect;
									}
									else
									{
										$('#a-result').html(j.info);
									}
									
								});
								
								return false;
								
							}
							
							$('input[name="auth[email]"], input[name="auth[password]"]').keypress(function(e){
								if(e.keyCode == 10 || e.keyCode == 13)
								{
									processLogin($('input[name="auth[email]"]').val(), $('input[name="auth[password]"]').val());
								}
							});
							
							$('.btn-in').click(function(){
								processLogin($('input[name="auth[email]"]').val(), $('input[name="auth[password]"]').val());
							});
						</script>
						
						
						<div class="modal fade modal-login modal-signup" id="sign-up-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<div class="clearfix">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										</div>
										<div class="clearfix">
											<div class="pull-left amy-face"><img width="70" height="70" src="<?php echo asset_url(); ?>img/sign/amyface.png"></div>
											<h4 class="modal-title">Sign Up</h4>
										</div>
									</div>
									<div class="modal-body">
										<?php echo form_open('register/submit', array('class' => 'sign-up-form', 'id' => 'sign-up-form', 'enctype' => 'multipart/form-data')); ?>
											<div class="notifier" id="notifier" style="color:#ff0000"></div>
										
											<div class="control-group">
												<input type="text" placeholder="Username" name="register[username]">
													<span class="username"></span>
													
													<!--<i class="input-icon fui-check-inverted"></i>-->
											</div>
											
											<div class="control-group">
												<input type="email" placeholder="Email" name="register[email]">
													<span class="email"></span>
													<!-- <p>Username <br/>available</p> -->
													<!-- <i class="input-icon fui-check-inverted"></i> -->
											</div>
											
											<div class="control-group">
												<input type="password" placeholder="Password" name="register[password]">
												<span class="password"></span>
												<!-- <p>Passwords <br/>don?t match</p> -->
												<!-- <i class="input-icon fui-check-inverted"></i> -->
											</div>
											
											<div class="control-group">
												<input type="text" placeholder="Location (city, state)" name="register[address]">
													<span class="location"></span>
													<!-- <p>Username <br/>available</p> -->
													<!-- <i class="input-icon fui-check-inverted"></i> -->
											</div>
											
											<div class="control-group">
												<input type="text" placeholder="Name" name="register[fullname]">
                                                <!--<input type="text" placeholder="Name (optional)" name="register[fullname]">-->
													<span class="full_name"></span>
													<!-- <p>Username <br/>available</p> -->
													<!-- <i class="input-icon fui-check-inverted"></i> -->
											</div>
											
											<div class="form-group">
												
												<div class="fileinput fileinput-new clearfix" data-provides="fileinput">
													<div class="col-sm-6">
														<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100px; height: 100px;"></div>
													</div>
													
													<div class="col-sm-6">
														<div style="padding:25px 0">
															<span class="btn btn-primary btn-embossed btn-file btn-file-wrapper">
																<span class="fileinput-new"><span class="fui-image"></span>&nbsp;&nbsp;Select image</span>
																<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change</span>
																<input type="file" name="profpic" id="register[profpic]" >
															</span>
															<a href="#" class="btn btn-primary btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>&nbsp;&nbsp;Remove</a>
														</div>
													</div>
												</div>
											</div>
											
											<div class="bar sign-up-bar"></div>
											
											<button class="btn btn-pink btn-block" id="btn-signup" type="submit">Sign Up</button>
											
											<div class="clearfix row-footer">
												<p>By clicking sign up, you have read and agree to our <a href="<?php echo site_url('terms-of-use'); ?>">Amy's List Terms of Use</a></p>
											</div>
											
											<div class="row clearfix col-social-1">
												<div class="col-sm-6 col-social">
													<div class="checkbxs email-checkbxs social-media-footer fb-checkbox">
														<label class="checkbox">
															<input type="checkbox" name="register[share_fb]" data-toggle="checkbox" checked="checked">
															<a class="fb-sharer" href="javascript:void(0)" onclick="Share.facebook('<?php echo base_url(); ?>','<?php echo 'Amyslist.us'; ?>','<?php echo asset_url() .'img/theme/logo.png'; ?>','<?php echo 'Amys List provides local classifieds and forums for jobs, housing, for sale, personals, services, gigs, side jobs, and events.'; ?>')"><i class="fa fa-facebook fa-lg"></i>Facebook</a>
														</label>
													</div>
												</div>
												<div class="col-sm-6 col-social">
													<div class="checkbxs email-checkbxs social-media-footer tw-checkbox">
														<label class="checkbox">
															<input type="checkbox" name="register[share_tw]" data-toggle="checkbox" checked="checked">
															<a class="tw-sharer" href="javascript:void(0)" onclick="Share.twitter('<?php echo base_url(); ?>','<?php echo 'Amys List provides local classifieds and forums for jobs, housing, for sale, personals, services, gigs, side jobs, and events.'; ?>')"><i class="fa fa-twitter fa-lg"></i>Twitter</a>
														</label>
													</div>
												</div>
											</div>
											<div class="clearfix row-footer">
												<p>Already have an Account? <a onclick="$('button.close').click();" href="#sign-in-modal" data-toggle="modal">Login here</a></p>
											</div>
										<?php echo form_close(); ?>
										<a href="javascript:void(0)" onclick="checkLoginState();" class="fb-connect fb-loader"><img src="<?php echo asset_url(); ?>img/sign/fb-connect.png" class="img-responsive"></a>
									</div>
								</div>
							</div>
						</div>
						
						<script>
							var bar = $('.sign-up-bar');
							var username = $('input[name="register[username]"]');
							username.bind('keyup', function(){
									var alphanum = /^[a-z0-9]+$/i;
									
									if(!alphanum.test($(this).val()))
									{
										console.log(alphanum.test($(this).val()));
										$('.notifier').html('Alpha numeric only');
									}
									else
									{
										$('.notifier').html('');
									}
							});
							username.bind('blur', function(){
									if(username.val().length < 3)
									{
										$('.notifier').html('Username min. 4 characters length');
									}
							});
							username.bind('blur', function(){
									var uname = $(this).val();
									$.get($('base').attr('alt') + 'auth/register/username_check/' + encodeURIComponent(uname))
										.done(function(data){
											var r = $.parseJSON(data);
											if(r.status != 'OK')
											{
												username.val('');
												$('.notifier').html('Username \''+uname+'\' already taken');
											}
										});
							});
							
							var regemail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
							var email = $('input[name="register[email]"]');
							email.bind('blur', function(){
								if(!regemail.test(email.val()))
								{
									$('.notifier').html('Email \''+email.val()+'\' is invalid email');
								}
								else
								{
									$('.notifier').html('');
								}
							}); 
							email.bind('blur', function(){
									var email = $(this).val();
									$.get($('base').attr('alt') + 'auth/register/email_check/' + encodeURIComponent(email))
										.done(function(data){
											var r = $.parseJSON(data);
											if(r.status != 'OK')
											{
												$('input[name="register[email]"]').val('');
												$('.notifier').html('Email \''+email+'\' already taken');
											}
										});
							});
							var password = $('input[name="register[password]"]');
							password.bind('blur', function(){
								if(password.val().length < 3)
								{
									$('.notifier').html('Password min. 4 characters length');
								}
								else
								{
									$('.notifier').html('');
								}
							});
					
							$('#btn-signup').on('click', function(){
								
								$('.notifier').html('');
								
								var address = $('input[name="register[address]"]');
								var fullname = $('input[name="register[fullname]"]');
								
								if(username.val() == '')
								{
									username.focus();
									$('.notifier').html('Please enter username');
									return false;
								}
								
								if(email.val() == '')
								{
									email.focus();
									$('.notifier').html('Please enter email address');
									return false;
								}
								
								if(password.val() == '')
								{
									password.focus();
									$('.notifier').html('Please enter password');
									return false;
								}
								
								if(address.val() == '')
								{
									address.focus();
									$('.notifier').html('Please enter address');
									return false;
								}
								
								 if(fullname.val() == '')
								 {
									fullname.focus();
									$('.notifier').html('Please enter fullname');
									return false;
								 }
								
								/*if($('input[name="profpic"]').val() == '')
								{
									$('.notifier').html('Please select your profile photo');
									return false;
								}*/
								
								//sharer 
								if($('input[name="register[share_fb]"]').is(':checked'))
								{
									$('a.fb-sharer').click();
								}
								
								if($('input[name="register[share_tw]"]').is(':checked'))
								{
									$('a.tw-sharer').click();
								}
								
								$('form.sign-up-form').ajaxSubmit({
										dataType:  'json', 
										url: $('base').attr('alt') + 'register/submit/',
										beforeSend: function() {
											var percentVal = '0%';
											bar.width(percentVal)
										},
										
										uploadProgress: function(event, position, total, percentComplete) {
											var percentVal = percentComplete + '%';
											bar.width(percentVal)
										},
										
										success: processJson
								});
								
								return false;
							});
							
							
							function processJson(data)
							{
								console.log(data);
								if(data.status == 'OK')
								{
									window.location=data.redirect;
								}
							}
						</script>
						
						<?php endif; ?>