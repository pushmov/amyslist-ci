<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function load_noty()
{
	
	$CI =& get_instance();
	if($CI->session->flashdata('noty'))
	{
		
		$data['data'] = $CI->session->flashdata('noty');
		$CI->load->view('noty', $data);
		
	}
	
}
