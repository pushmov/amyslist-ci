<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function is_logged_in()
{
	$CI =& get_instance();
	$user_session = $CI->session->userdata('user');
	
	return ($user_session) ? TRUE : FALSE;
}

function load_sidebar()
{
	$CI =& get_instance();
	
	//notification
	$data['notif'] = count_notification();
	
	return (is_logged_in()) ? $CI->load->view('sidebar_user', $data) : $CI->load->view('sidebar_login');
}

function count_notification()
{
	$CI =& get_instance();
	$user_session = $CI->session->userdata('user');
	
	if(!is_logged_in()) return 0;
	
	$count = $CI->db->where(array('user_id' => $user_session['user_id'], 'unread_status' => 1, 'delete_status' => 0))
		->from('user_notifications')
		->count_all_results();
	
	return $count;
}

function profile_picture()
{
	$CI =& get_instance();
	$user_session = $CI->session->userdata('user');
	
	$user = $CI->db->select('profpic')
		->where('id', $user_session['user_id'])
		->get('user')
		->row();
	
	return (!is_logged_in()) ? 'uploads/noimages.jpg' : $user->profpic;
}

function profile_name()
{
	$CI =& get_instance();
	$user_session = $CI->session->userdata('user');
	
	return (!is_logged_in()) ? '' : $user_session['first_name'] . ' ' . $user_session['last_name'];
}

function seo_friendly($name)
{
	return str_replace(' ', '-', strtolower($name));
}

function country_name($country_id)
{
	$CI =& get_instance();
	$row = $CI->db->select('printable_name')
		->where('id', $country_id)
		->get('location_countries')
		->row();
		
	return (empty($row)) ? $country_id : $row->printable_name;
}
