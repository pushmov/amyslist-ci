<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends MY_Controller {
	
	public function __construct()
	{
	
		parent::__construct();
		$this->stencil->layout('about_layout');
		$this->stencil->slice(array('nav' => 'about/nav', 'footer' => 'footer'));
		
  }
	
	public function index()
	{
		$this->stencil->title('About');
		$this->stencil->paint('about/about');
	}
	
	public function features()
	{
		$this->stencil->title('Features');
		$this->stencil->paint('about/features');
	}
	
	public function founders()
	{
		$this->stencil->title('Founders');
		$this->stencil->paint('about/founders');
	}
	
	public function post_an_ad()
	{
		$this->stencil->title('Posting an Ad');
		$this->stencil->paint('about/posting_an_ad');
	}
	
	public function city_page_advertising()
	{
		$this->stencil->title('City Page Advertising');
		$this->stencil->paint('about/city_page_advertising');
	}
	
	public function terms_of_use()
	{
		$this->stencil->title('Terms of Use');
		$this->stencil->paint('about/terms_of_use');
	}
	
	public function privacy()
	{
		$this->stencil->title('Privacy');
		$this->stencil->paint('about/privacy');
	}
	
	public function contact_us()
	{
		$this->stencil->title('Contact Us');
		$this->stencil->paint('about/contact');
	}

}