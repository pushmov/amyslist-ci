<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Posted extends MY_Controller {
	
	const MAX_ALLOWED_PICTURE = 51200;
	
	public function __construct()
	{
        
		parent::__construct();
		$this->load->model(array('ads/adsposted', 'ads/ads_model', 'blog/blog_model'));
		
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/register'));
		}
  }
	
	public function index()
	{
		
		$user_session = $this->session->userdata('user');
		$data['user_id'] = $user_session['user_id'];
		$data['name'] = $user_session['first_name'] . ' ' . $user_session['last_name'];
		
		$data['my_ads'] = $this->blog_model->my_ads($user_session['user_id']);
		
		print_r($data['my_ads']);
		exit();
		
		$this->stencil->title('My Blog Posted');
		$this->stencil->paint('blog/posted', $data);
	}
	
	public function edit($id)
	{
		echo 'blog posted edit page';
		exit();
	}
	
	public function delete()
	{
		echo 'delete single blog';
		exit();
	}
	
}