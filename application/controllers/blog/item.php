<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item extends MY_Controller {
	
	const MAX_ALLOWED_PICTURE = 51200;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('blog/blog_model', 'ads/ads_model'));
  }
	
	public function review($post_ref_id=NULL)
	{
		
		if(!isset($post_ref_id))
		{
			redirect(site_url('post/ads'));
		}
		$session = $this->session->userdata('user');
		
		$row_temp_post = $this->blog_model->get_blog_item($post_ref_id);
		
		if(empty($row_temp_post))
		{
			redirect(site_url('post/ads'));
		}
		
		if((int)$row_temp_post->status == 1)
		{
			$this->session->set_flashdata('noty', array('message' => 'Blog currently not available...', 'type' => 'error'));
			redirect(site_url('post/ads'));
		}
		
		$data['post'] = $row_temp_post;
		$images = $this->blog_model->get_images($row_temp_post->id);
		$data['images'] = $images;
		
		$data['location'] = $this->ads_model->get_city_name($row_temp_post->location_id);
		$data['category'] = $this->ads_model->get_parent_child_cat($row_temp_post->category_id);
		
		$this->stencil->title('Review to Publish');
		$this->stencil->paint('blogs/review', $data);
		
	}
	
	public function publish($id_blog=NULL){
		
		if(!isset($id_blog))
		{
			redirect(site_url('post/ads'));
		}
		
		$user_session = $this->session->userdata('user');
		$row_user = $this->ads_model->get_user($user_session['user_id']);
		
		$post = $this->blog_model->get_post($id_blog);
		
		if(empty($post))
		{
			redirect(site_url('post/ads'));
		}
		
		//activate blog
		$this->blog_model->activate_blog($id_blog);
		
		//notification
		$from = 1;
		$amyadmin = $this->ads_model->get_admin();
		$notification_data = array(
			'user_id' => $user_session['user_id'],
			'from_id' => $from,
			'message' => '<p><a class="profile-link" href="'
				.site_url('/profile/' . $amyadmin->username) . '">Amy Admin</a> 
				Thanks for posting! Check your <a href="'.site_url('blogs/posted').'">
				blog posted page</a> on to edit, review, or delete your 
				<a href="' . site_url( 'viewblog/' . $post->post_num_id . '/' . $post->url_name ) . '">post</a></p>'
		);
		
		$this->ads_model->create_notification($notification_data);
		
		//TODO : mailer
		
		$this->session->set_flashdata('noty', array('message' => 'Thank you for posting with amyslist.us!', 'type' => 'success'));
		redirect(site_url('thank-you'));
		
	}
	
	public function detail($num_id=NULL, $url=NULL)
	{
		echo 'detail of blog view';
		exit();
	}
	
}