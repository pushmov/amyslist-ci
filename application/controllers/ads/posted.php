<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Posted extends MY_Controller {
	
	const MAX_ALLOWED_PICTURE = 51200;
	
	public function __construct()
	{
        
		parent::__construct();
		$this->load->model(array('ads/adsposted', 'ads/ads_model'));
		
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/register'));
		}
  }
	
	public function index()
	{
        
		$user_session = $this->session->userdata('user');
		$data['user_id'] = $user_session['user_id'];
		$data['name'] = $user_session['first_name'] . ' ' . $user_session['last_name'];
		
		$data['my_ads'] = $this->adsposted->my_ads($user_session['user_id']);
		$this->stencil->title('My Ads Posted');
		$this->stencil->paint('ads/posted', $data);
		$this->session->unset_userdata('addIds'); 
	}
	
	public function delete($id_post=NULL)
	{
		if(!isset($id_post))
		{
			redirect(site_url('ads/posted'));
		}
		$this->adsposted->set_delete($id_post);
		$this->session->set_flashdata('noty', array('message' => 'Ad post deleted. <a href="'.site_url('ads/posted/undo/' . $id_post).'">Undo</a>', 'type' => 'success'));
		redirect(site_url('ads/posted'));
    }  
    
    
     
    public function deleteAll()
    {
			
        $this->form_validation->set_rules('ads[]', 'Ads', 'required');
				
        if ($this->form_validation->run()==true) 
        {
					
            $checked_ads=$this->input->post('ads');
						
            if($this->adsposted->set_delete_all($checked_ads)){
               $this->session->set_userdata('addIds', $checked_ads);
               $this->session->set_flashdata('noty', array('message' => 'Ad post deleted.<a href="'.site_url('ads/posted/undoAll/').'">Undo</a>', 'type' => 'success', 'timeout' => false));
               redirect(site_url('ads/posted')); 
            }
            else{
               $this->session->set_flashdata('noty', array('message' => 'Error occured.Please try again.', 'type' => 'error'));
               redirect(site_url('ads/posted')); 
            }
            
        }
        else
        {
              $this->session->set_flashdata('noty', array('message' => 'Please select the Ads to delete.', 'type' => 'error'));
              redirect(site_url('ads/posted')); 
        } 
    }
    
    
    
    
     
	
	public function undo($id_post=NULL)
	{
		if(!isset($id_post))
		{
			$this->session->set_flashdata('noty', array('message' => 'An error occurred', 'type' => 'error'));
			redirect(site_url('ads/posted'));
		}
		
		$this->adsposted->set_undo($id_post);
		redirect(site_url('ads/posted'));
	}
    
    public function undoAll()
    {
        $user_session = $this->session->userdata('addIds');
        if($this->adsposted->set_undo_all($user_session)){
           redirect(site_url('ads/posted'));
       }
       else{
          $this->session->set_flashdata('noty', array('message' => 'An error occurred', 'type' => 'error'));
          redirect(site_url('ads/posted'));
       }
       
    }
    
	
	public function edit($id_post=NULL)
	{
		if(!isset($id_post))
		{
			redirect(site_url('ads/posted'));
		}
		
		$post = $this->ads_model->get_post($id_post);
		
		if(empty($post))
		{
			redirect(site_url('ads/posted'));
		}
		
		$data['us_cities'] = $this->ads_model->get_us_cities();
		$data['us_state'] = $this->ads_model->get_us_states();
		
		$data['intl_cities'] = $this->ads_model->get_international_cities();
		$data['international'] = $this->ads_model->world_countries();
		
		$data['categories'] = $this->ads_model->categories();
		
		$data['post'] = $post;
		
		$data['location_select'] = $this->adsposted->get_location_name($post->location_id);
		$data['category_select'] = $this->adsposted->get_category_name($post->category_id);
		
		$data['images'] = $this->adsposted->get_images($post->id);
		
		$this->stencil->title('Edit post');
		$this->stencil->paint('ads/mypost_edit', $data);
	}
	
	function _get_latlon($addr){
	
		$json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=". urlencode($addr) ."&sensor=false");
    $json = json_decode($json);

    $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
    $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
		
		return array('lat' => $lat, 'lon' => $long);
	}
	
	public function update($id_post=NULL)
	{
		if(!isset($id_post))
		{
			redirect(site_url('ads/posted'));
		}
		
		$this->load->library('upload');
		
		$request = $this->input->post('adpost');
		
		$upload_path = './assets/uploads/post/'; 
		
		if(!is_dir($upload_path))
		{
			mkdir($upload_path);
		}
		
		$session_user = $this->session->userdata('user');
		
		$position = $this->_get_latlon($request['map_address']);
		
		$data_post = array(
			'user_id' => $session_user['user_id'],
			'category_id' => $request['category_id'],
			'location_id' => $request['location_id'],
			'title' => $request['title'],
			'description' => $request['description'],
			'rent' => $request['rent'],
			'rooms' => $request['rooms'],
			'email' => $request['email'],
			'is_anonymous_email' => (isset($request['is_anonymous_email'])) ? 1 : 0,
			'is_show_username' => (isset($request['is_show_username'])) ? 1 : 0,
			'is_allow_text_chat' => (isset($request['is_allow_text_chat'])) ? 1 : 0,
			'is_allow_text_message' => (isset($request['is_allow_text_message'])) ? 1 : 0,
			'phone_number' => $request['phone_number'],
			'address' => $request['address'],
			'map_address' => $request['map_address'],
			'video_link' => $request['video_link'],
			'latitude' => $position['lat'],
			'longitude' => $position['lon'],
			'updated_at' => date('Y-m-d H:i:s')
			// 'url_name' => $this->_url_name($this->_clean($request['title']))
		);
		
		$this->adsposted->save($data_post, $id_post);
		
		if((!empty($_FILES['photo_file']['name'])))
		{
			
			$config['allowed_types'] = 'png|jpg|jpeg|PNG|JPG|JPEG';
			$config['max_size'] = self::MAX_ALLOWED_PICTURE;
			$config['upload_path'] = $upload_path;

			$files = $_FILES;
			$post_images = array();
			$cpt = count($_FILES['photo_file']['name']);
			for($i=0; $i<$cpt; $i++)
			{
				
				$photo_name = $files['photo_file']['name'][$i];
				
				$config['file_name'] = md5($photo_name);
				
				$_FILES['photo_file']['name']= $files['photo_file']['name'][$i];
				$_FILES['photo_file']['type']= $files['photo_file']['type'][$i];
				$_FILES['photo_file']['tmp_name']= $files['photo_file']['tmp_name'][$i];
				$_FILES['photo_file']['error']= $files['photo_file']['error'][$i];
				$_FILES['photo_file']['size']= $files['photo_file']['size'][$i];
					
				$this->upload->initialize($config);
				if(!$this->upload->do_upload('photo_file'))
				{
					echo json_encode(array('message' => $this->upload->display_errors(), 'status' => 'failed', 'redirect' =>  site_url( '/submit_your_design/challenge/' )));
					exit();
				}
				else
				{
					$data_photo = $this->upload->data();
					$post_images['image_path'][] = $data_photo['file_name'];
					$post_images['description'][] = $request['photo_description'][$i];
				}
				
			}
			
		}
		
		if(!empty($post_images['image_path']))
		{
			foreach($post_images['image_path'] as $key => $post)
			{
				
				$image_data = array(
					'post_ad_id' => $id_post,
					'image_path' => 'uploads/post/' . $post,
					'image_description' => $post_images['description'][$key]
				);
				
				$this->ads_model->save_images($image_data);
			}
		}
		
		$ref_id = $this->ads_model->get_ref_id($id_post);
		
		$this->session->set_flashdata('noty', array('message' => 'Post updated successfully...', 'type' => 'success'));
		
        //$redirect = $this->input->get('review') ? site_url('ads/posted') : site_url('post/ads/review/' . $ref_id);
		$redirect = $_REQUEST['review']==1 ? site_url('ads/posted') : site_url('post/ads/review/' . $ref_id);
		
		echo json_encode(array('redirect' => $redirect, 'success' => 'success'));
		
	}
	
	public function changepic($picid){
		
		$image = $this->adsposted->get_images_row($picid);
		
		if(empty($image))
		{
			exit();
		}
		
		if(file_exists('./assets/' . $image->image_path))
		{
			unlink('./assets/' . $image->image_path);
		}
		
		$this->adsposted->remove_picture($picid);
		
		exit();
		
	}
	
	
	
}