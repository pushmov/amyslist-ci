<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('auth/auth_model');
  }
	
	
	public function login()
	{
		
		if(!$this->input->post('email') || !$this->input->post('password'))
		{
			redirect(site_url('login'));
		}
		
		// $post = $this->input->post('auth');
		$post['email'] = $this->input->post('email');
		$post['password'] = $this->input->post('password');
		
		$result = $this->auth_model->login($post['email'], $post['password']);
		
		if(empty($result))
		{
			
			$this->session->set_flashdata('noty', array('message' => 'Invalid username or password', 'type' => 'success'));
			redirect(site_url('/login'));
		}
		
		$session = array(
			'user_id' => $result->id,
			'username' => $result->username,
			'first_name' => $result->first_name,
			'last_name' => $result->last_name,
			'profpic' => $result->profpic,
		);
		$this->session->set_userdata('user', $session);
		
		session_start();
		$_SESSION['userid'] = $result->id;
		
		$this->session->set_flashdata('noty', array('message' => 'Welcome back, ' . $result->first_name . '!', 'type' => 'success'));
		redirect(site_url('/'));
		
	}
	
	public function logout()
	{
		
		$this->session->sess_destroy();
		session_start();
		$_SESSION['userid'] = 0;
		$this->session->set_flashdata('noty', array('message' => 'Logged out successfully', 'type' => 'success'));
		redirect(site_url('/'));
		
	}
	
	public function forgot_password()
	{
		
		if(is_logged_in())
		{
			redirect(site_url('/'));
		}
		
		$this->stencil->title('Forgot Password');
		$this->stencil->paint('user/forgot_password');
	}
	
	public function check_email($email)
	{
		if(!$this->input->is_ajax_request())
		{
			exit();
		}
		
		$email = urldecode($email);
		
		if(!$this->auth_model->is_email_exist($email))
		{
			echo json_encode(array('status' => 'OK'));
		}
		else
		{
			echo json_encode(array('status' => 'NOK'));
		}
		
	}
	
	public function forgot()
	{
		
		$this->form_validation->set_rules('forgot[email]', 'Email', 'required');
		if($this->form_validation->run() === TRUE)
		{
			$post = $this->input->post('forgot');
			if(!$this->auth_model->is_email_exist($post['email']))
			{
				//mail hee
				
				$row_user = $this->auth_model->get_user($post['email']);
				
				$rand = substr(sha1(time()), 0, 8);
				$this->auth_model->table = 'user';
				$this->auth_model->update(
					array('plain' => $rand, 'password' => sha1($rand)),
					$conditions = array('id' => $row_user->id)
				);
				
				$subject = "New Amy's List password";
				$body = '<div style="width:940px; height:632px; float:left; padding-left:40px;">';
				$body .= '    <div style="float:left; width:160px; height:632px; padding-top:30px;">';
				$body .= '        <img src="http://amyslist.us/images/lady.png"  width="168" height="508"/>';
				$body .= '    </div>';
				$body .= '    <div style="width:700px; float:left; height:632px;" class="topleft">';
				$body .= '        <h1 style="padding-left:150px;width:600px;">';
				$body .= '			<a class="logo" href="http://amyslist.us/"> <img src="http://amyslist.us/images/logo.gif" /></a>';
				$body .= '        </h1>';
				$body .= '        <div style="font-family:Calibri; font-size:16px; padding-left:20px;">';
				$body .= 'Dear <b>'.$row_user->first_name . ' ' . $row_user->last_name .'</b>,<br><br>Your new password is listed below.<br><br>
					 Email Address : <span style="font-weight: bold; ">' . $row_user->email . '</span><br>
					 Password : <span style="font-weight: bold; ">' . $rand . '</span><br><br>
					 This is a system generated password. Please change it after you login, for security purposes.</span><br><br><br>
					';
				$body .= "<p style='font-family:Calibri; font-size:18px;' >Also do us a favor, tell all your friends & family about Amy's List, by liking our Facebook and Twitter Pages at the links below:<br /><br /></p>";
				$body .= '<table><tr><td><a href="http://www.twitter.com/Amyslist_us"><img src="http://amyslist.us/images/tweet-button.jpeg" width="98" height="65" /></a></td><td><span style="font-family:Calibri; color:##1155CC;font-size:18px;"><a href="http://www.twitter.com/Amyslist_us" style="color:##1155CC; text-decoration:none;">http://www.twitter.com/Amyslist_us</a></span></td></tr></table>';
				$body .= '<table><tr><td><a href="http://www.facebook.com/Amyslist.us"><img src="http://amyslist.us/images/facebook.jpg" width="95" height="36" /></a></td><td><span style="font-family:Calibri;color:##1155CC;font-size:18px;a:font-size:18px;"><a href="http://www.facebook.com/Amyslist.us" style="color:##1155CC; text-decoration:none;">http://www.facebook.com/Amyslist.us</a></span></td></tr></table>';
				$body .= '            <p style="font-family:Calibri; font-size:18px;">';
				$body .= " Amy's List is the future of online classified ads. And we're glad you're part of it.</p> ";
				$body .= '			<p style="font-family:Calibri; font-size:18px;">Sincerely, <br />';
				$body .= '            <a href="http://amyslist.us/AmyAdmin 
				" style="color:#f52d8f;font-size:18px;text-decoration:none;">Amy Admin</span> </a>';
				$body .= '        </div> ';          
				$body .= '    </div>';
				$body .= '</div>';
				
				$from_email = "amyslistceo@gmail.com";
				$from_name = "Amy's List";
				
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

				// Additional headers
				$to_name = $row_user->first_name . ' ' . $row_user->last_name;
				$headers .= 'To: '.$to_name.' <'.$row_user->email.'>' . "\r\n";
				$headers .= 'From: Amyslist <amyslistceo@gmail.com>' . "\r\n";

				// Mail it
				mail($row_user->email, $subject, $body, $headers);
		
				$this->session->set_flashdata('noty', array('message' => 'Your new password will be sent to your email address.', 'type' => 'success'));
			}
			else
			{
				$this->session->set_flashdata('noty', array('message' => 'Email not exist', 'type' => 'error'));
			}
		}
		
		redirect(site_url('forgot-password'));
		
	}
	
}