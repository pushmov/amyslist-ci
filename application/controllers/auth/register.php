<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MY_Controller {
	
	const MAX_ALLOWED_SHIPMENT_PICTURE = 5120;
	const FILE_ALLOWED_TYPES = 'jpg|jpeg|bmp|gif|png|JPG|JPEG|BMP|GIF|PNG';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('auth/register_model', 'user/notification_model'));		
  }
	
	public function index()
	{
		
		if(is_logged_in())
		{
			redirect('/');
		}
		
		$this->stencil->title('Register');
		$this->stencil->paint('register/register_index');
	}
	
	public function submit()
	{
		
		$this->form_validation->set_rules('register[username]', 'Username', 'required|is_unique[user.username]');
		$this->form_validation->set_rules('register[address]', 'Location', 'required');
		$this->form_validation->set_rules('register[email]', 'Email', 'required|is_unique[user.email]');
		$this->form_validation->set_rules('register[password]', 'Password', 'required');
		// $this->form_validation->set_rules('register[fullname]', 'Full Name', 'required');
		
		if($this->form_validation->run() === TRUE)
		{
			
			$photo = (!empty($_FILES['profpic']['tmp_name'])) ? md5(time()) : '';
			
			if($photo != '')
			{
				$config['allowed_types'] = self::FILE_ALLOWED_TYPES;
				$config['max_size'] = self::MAX_ALLOWED_SHIPMENT_PICTURE;
				$config['upload_path'] = './assets/uploads/profpic/';
				$config['file_name'] = $photo;
				
				$this->load->library('upload');
				$this->upload->initialize($config);
				if(!$this->upload->do_upload('profpic'))
				{
				
					$response['success'] = 'fail';
					$response['newimage'] = '';
					$response['errorcode'] = 1;
					$response['message'] = 'Allowed picture : png, jpg or bmp only | max size : '.self::FILE_ALLOWED_TYPES.' KB';
					echo json_encode($response);
					exit();
				}
				else
				{
					$data_photo = $this->upload->data();
					$photo = $data_photo['file_name'];
				}
			}
			
			$post = $this->input->post('register');
			unset($post['confirm']);
			$fullname = explode(' ', $post['fullname']);
			
			$post['profpic'] = 'uploads/profpic/' . $photo;
			$post['plain'] = $post['password'];
			$post['password'] = sha1($post['password']);
			$post['first_name'] = isset($fullname[0]) ? $fullname[0] : '';
			$post['last_name'] = isset($fullname[1]) ? $fullname[1] : '';
			
			$tw = isset($post['share_tw']) ? 'Y' : '';
			$fb = isset($post['share_fb']) ? 'Y' : '';
			
			unset($post['share_tw']);
			unset($post['share_fb']);
			unset($post['fullname']);
			
			$this->register_model->table = 'user';
			$userid = $this->register_model->save($post);
			
			$user = $this->register_model->get_user_id($userid);
			
			$session = array(
				'user_id' => $user->id,
				'username' => $user->username,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'profpic' => $user->profpic
			);
			$this->session->set_userdata('user', $session);
			$this->session->set_flashdata('noty', array('message' => 'Thank you for registering with amyslist.us', 'type' => 'success'));
				
			//notification
			$this->notification_model->table = 'user_notifications';
			$this->notification_model->auto_like($userid);
				
			$this->register_model->table = 'user_social';
			$this->register_model->save(
				array(
					'user_id' => $userid
				)
			);
			
			$subject = "Your Amy's List Registration Info http://amyslist.us/";
			
			$body = '<div style="width:940px; height:632px; float:left; padding-left:40px;">';
			$body .= '    <div style="float:left; width:160px; height:632px; padding-top:30px;">';
			$body .= '        <img src="'.asset_url().'img/lady.png"  width="168" height="508"/>';
			$body .= '    </div>';
			$body .= '    <div style="width:700px; float:left; height:632px;" class="topleft">';
			$body .= '        <h1 style="padding-left:150px;width:600px;">';
			$body .= '			<a class="logo" href="http://amyslist.us/"> <img src="' . asset_url() .'img/theme/logo.png" /></a>';
			$body .= '        </h1>';
			$body .= '        <div style="font-family:Calibri; font-size:16px; padding-left:20px;">';
			
			$body .= 'Dear <b>Administrator</b>,<br><br>Someone signed up Amy\'s List today. Below is the user info :<br><br>
		 	Username : <span style="font-weight: bold; ">'.$user->first_name." ".$user->last_name.'</span><br>
		 	User Email : <span style="font-weight: bold; ">'.$user->email.'</span><br><br>
		 	<br><br>
		 	From,<br>Amy\'s List<br><br><br><span style="color: #0000FF;">';
			
			$body .= '            <p style="font-family:Calibri; font-size:18px;">';
			$body .= " Amy's List is the future of classified ads. We're glad you're now part of it.</p> ";
			$body .= '			<p style="font-family:Calibri; font-size:18px;">Sincerely, <br />';
			$body .= '            <a href="http://amyslist.us/amyadmin 
			" style="color:#f52d8f;font-size:18px;text-decoration:none;">Amy Admin</span> </a>';
			$body .= '        </div> ';          
			$body .= '    </div>';
			$body .= '</div>';
			
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

			// Additional headers
			$headers .= 'To: Amy Admin <amyslistceo@gmail.com>' . "\r\n";
			$headers .= 'From: Notification <notification@amyslist.com>' . "\r\n";

			$to_email = 'amyslistceo@gmail.com';
			// Mail it
			mail($to_email, $subject, $body, $headers);
		
			//=================  MAIL TO USER FROM ADMIN   =============================
		
			$username  = $user->first_name." ".$user->last_name;
			
			$subject = "Amy's List: Your Registration Info";
			$body = '<div style="width:940px; height:632px; float:left; padding-left:40px;">';
			$body .= '    <div style="float:left; width:160px; height:632px; padding-top:30px;">';
			$body .= '        <img src="'.asset_url().'img/lady.png"  width="168" height="508"/>';
			$body .= '    </div>';
			$body .= '    <div style="width:700px; float:left; height:632px;" class="topleft">';
			$body .= '        <h1 style="padding-left:150px;width:600px;">';
			$body .= '			<a class="logo" href="http://amyslist.us/"> <img src="'.asset_url().'img/theme/logo.png" /></a>';
			$body .= '        </h1>';
			$body .= '        <div style="font-family:Calibri; font-size:16px; padding-left:20px;">';
			$body .= 'Hi <b>'.$username."</b>,<br><br>Welcome to Amy's List and thank you for creating an account. <br><br>
			Your user email is: <span style='font-weight: bold;'>".$user->email."</span><br><br>
			Your password is: <span style='font-weight: bold;'>".$user->plain."</span><br><br>";
			
			$body .= "<p style='font-family:Calibri; font-size:18px;' >Do us a favor and tell all your friends & family to use Amy's List. You can check our Facebook and Twitter Pages for more postings and info at the links below:<br /><br /></p>";
			$body .= '<table><tr><td><a href="http://www.twitter.com/Amyslist_us"><img src="'.asset_url().'img/tweet-button.jpeg" width="98" height="65" /></a></td><td><span style="font-family:Calibri; color:##1155CC;font-size:18px;"><a href="http://www.twitter.com/Amyslist_us" style="color:##1155CC; text-decoration:none;">http://www.twitter.com/Amyslist_us</a></span></td></tr></table>';
			$body .= '<table><tr><td><a href="http://www.facebook.com/Amyslist.us"><img src="'.asset_url().'img/facebook.jpg" width="95" height="36" /></a></td><td><span style="font-family:Calibri;color:##1155CC;font-size:18px;a:font-size:18px;"><a href="http://www.facebook.com/Amyslist.us" style="color:##1155CC; text-decoration:none;">http://www.facebook.com/Amyslist.us</a></span></td></tr></table>';
			$body .= '            <p style="font-family:Calibri; font-size:18px;">';
			$body .= " Amy's List is the future of classified ads. And we're glad you're part of it.</p> ";
			$body .= '			<p style="font-family:Calibri; font-size:18px;">Sincerely, <br />';
			$body .= '            <a href="http://amyslist.us/amyadmin 
			" style="color:#f52d8f;font-size:18px;text-decoration:none;">Amy Admin</span> </a>';
			$body .= '        </div> ';          
			$body .= '    </div>';
			$body .= '</div>';
						
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

			// Additional headers
			$headers .= 'To: Amy Admin <amyslistceo@gmail.com>' . "\r\n";
			$headers .= 'From: Notification <notification@amyslist.com>' . "\r\n";

			$to_email = 'amyslistceo@gmail.com';
			// Mail it
			mail($user->email, $subject, $body, $headers);
			
			$this->session->set_flashdata('trigger_social_fb', $fb);
			$this->session->set_flashdata('trigger_social_tw', $tw);
			
			redirect(site_url('/'));
		}
		else
		{
			redirect(site_url('/register'));
		}
		
	}
	
	public function facebook()
	{
		
		if(!$this->input->is_ajax_request())
		{
			exit();
		}
		
		$picture = $this->input->post('picture');
		
		$arr_extension = explode('.', preg_replace('/\?.*/', '', $picture));
		$ext = $arr_extension[count($arr_extension) - 1];
				
		$uploads_dir = './assets/uploads/profpic/';
				
		// if( !is_dir($uploads_dir) )
		// {
			// mkdir('./assets/uploads/profpic');
		// }
					
		// if( !is_writable($uploads_dir) )
		// {
			// chmod($uploads_dir, 0777);
		// }
				
		$img = 'uploads/profpic/' . md5(time()) . '.' . $ext;
		file_put_contents('assets/' . $img , file_get_contents($picture));
		
		$post = array(
			'email' => $this->input->post('email'),
			'username' => preg_replace('/\@.*/', '', $this->input->post('email')),
			'first_name' => $this->input->post('firstname'),
			'last_name' => $this->input->post('lastname'),
			'profpic' => $img
		);
		
		
		//check registered
		if($this->register_model->is_email_exist($post['email']) >= 1)
		{
			
			$user = $this->register_model->get_user($post['email']);
			
			$session = array(
				'user_id' => $user->id,
				'username' => $user->username,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'profpic' => $user->profpic
			);
			$this->session->set_userdata('user', $session);
			$this->session->set_flashdata('noty', array('message' => 'Welcome back, ' . $user->first_name . '!', 'type' => 'success'));
			
		}
		else
		{
			
			$this->register_model->table = 'user';
			$userid = $this->register_model->save($post);
			if($userid)
			{
				$user = $this->register_model->get_user($post['email']);
			
				$session = array(
					'user_id' => $user->id,
					'username' => $user->username,
					'first_name' => $user->first_name,
					'last_name' => $user->last_name,
					'profpic' => $user->profpic
				);
				$this->session->set_userdata('user', $session);
				$this->session->set_flashdata('noty', array('message' => 'Thank you for registering with amyslist.us', 'type' => 'success'));
				
				//notification
				$this->notification_model->table = 'user_notifications';
				$this->notification_model->auto_like($userid);
				
				$this->register_model->table = 'user_social';
				$this->register_model->save(
					array(
						'user_id' => $userid
					)
				);
				
				$subject = "New User Registration Info http://amyslist.us/";
			
				$body = '<div style="width:940px; height:632px; float:left; padding-left:40px;">';
				$body .= '    <div style="float:left; width:160px; height:632px; padding-top:30px;">';
				$body .= '        <img src="'.asset_url().'img/lady.png"  width="168" height="508"/>';
				$body .= '    </div>';
				$body .= '    <div style="width:700px; float:left; height:632px;" class="topleft">';
				$body .= '        <h1 style="padding-left:150px;width:600px;">';
				$body .= '			<a class="logo" href="http://amyslist.us/"> <img src="'.asset_url().'img/theme/logo.png" /></a>';
				$body .= '        </h1>';
				$body .= '        <div style="font-family:Calibri; font-size:16px; padding-left:20px;">';
				
				$body .= 'Dear <b>Administrator</b>,<br><br>A new user has signed up for Amy\'s List today. Below is the  user details :<br><br>
				Username : <span style="font-weight: bold; ">'.$user->first_name." ".$user->last_name.'</span><br>
				User Email : <span style="font-weight: bold; ">'.$user->email.'</span><br><br>
				<br><br>
				From,<br>Amyslist<br><br><br><span style="color: #0000FF;">';
				
				$body .= '            <p style="font-family:Calibri; font-size:18px;">';
				$body .= " Amy's List is the future of classified ads. And we're glad you're part of it.</p> ";
				$body .= '			<p style="font-family:Calibri; font-size:18px;">Sincerely, <br />';
				$body .= '            <a href="http://amyslist.us/amyadmin 
				" style="color:#f52d8f;font-size:18px;text-decoration:none;">Amy Admin</span> </a>';
				$body .= '        </div> ';          
				$body .= '    </div>';
				$body .= '</div>';
				
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

				// Additional headers
				$headers .= 'To: Amy Admin <amyslistceo@gmail.com>' . "\r\n";
				$headers .= 'From: Notification <notification@amyslist.com>' . "\r\n";

				$to_email = 'amyslistceo@gmail.com';
				// Mail it
				mail($to_email, $subject, $body, $headers);
			
				//=================  MAIL TO USER FROM ADMIN   =============================
			
				$username  = $user->first_name." ".$user->last_name;
				
				$subject = "Amy's List: Your Registration Info";
				$body = '<div style="width:940px; height:632px; float:left; padding-left:40px;">';
				$body .= '    <div style="float:left; width:160px; height:632px; padding-top:30px;">';
				$body .= '        <img src="'.asset_url().'img/lady.png"  width="168" height="508"/>';
				$body .= '    </div>';
				$body .= '    <div style="width:700px; float:left; height:632px;" class="topleft">';
				$body .= '        <h1 style="padding-left:150px;width:600px;">';
				$body .= '			<a class="logo" href="http://amyslist.us/"> <img src="'.asset_url().'img/theme/logo.png" /></a>';
				$body .= '        </h1>';
				$body .= '        <div style="font-family:Calibri; font-size:16px; padding-left:20px;">';
				$body .= 'Hi <b>'.$username."</b>,<br><br>Welcome to Amy's List and thank you for creating an account. You can login anytime by clicking sign in with facebook <br><br>";
				
				$body .= "<p style='font-family:Calibri; font-size:18px;' >Also do us a favor, tell all your friends & family about Amy's List, by liking our Facebook and Twitter Pages at the links below:<br /><br /></p>";
				$body .= '<table><tr><td><a href="http://www.twitter.com/Amyslist_us"><img src="'.asset_url().'img/tweet-button.jpeg" width="98" height="65" /></a></td><td><span style="font-family:Calibri; color:##1155CC;font-size:18px;"><a href="http://www.twitter.com/Amyslist_us" style="color:##1155CC; text-decoration:none;">http://www.twitter.com/Amyslist_us</a></span></td></tr></table>';
				$body .= '<table><tr><td><a href="http://www.facebook.com/Amyslist.us"><img src="'.asset_url().'img/facebook.jpg" width="95" height="36" /></a></td><td><span style="font-family:Calibri;color:##1155CC;font-size:18px;a:font-size:18px;"><a href="http://www.facebook.com/Amyslist.us" style="color:##1155CC; text-decoration:none;">http://www.facebook.com/Amyslist.us</a></span></td></tr></table>';
				$body .= '            <p style="font-family:Calibri; font-size:18px;">';
				$body .= " Amy's List is the future of classified ads. And we're glad you're part of it.</p> ";
				$body .= '			<p style="font-family:Calibri; font-size:18px;">Sincerely, <br />';
				$body .= '            <a href="http://amyslist.us/amyadmin 
				" style="color:#f52d8f;font-size:18px;text-decoration:none;">Amy Admin</span> </a>';
				$body .= '        </div> ';          
				$body .= '    </div>';
				$body .= '</div>';
							
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

				// Additional headers
				$headers .= 'To: Amy Admin <amyslistceo@gmail.com>' . "\r\n";
				$headers .= 'From: Notification <notification@amyslist.com>' . "\r\n";

				$to_email = 'amyslistceo@gmail.com';
				// Mail it
				mail($user->email, $subject, $body, $headers);
				
			}
			
			
			
		}
		
		echo json_encode(array('redirect' => site_url('/')));
		
	}
	
	public function twitter(){}
	
	public function username_check($username)
	{
		if(!$this->input->is_ajax_request())
		{
			exit();
		}
		
		if($this->register_model->is_username_taken(urldecode($username)))
		{
			echo json_encode(array('status' => ''));
		}
		else
		{
			echo json_encode(array('status' => 'OK'));
		}
	}
	public function email_check($email)
	{
		if(!$this->input->is_ajax_request())
		{
			exit();
		}
		
		if($this->register_model->is_email_taken(urldecode($email)))
		{
			echo json_encode(array('status' => ''));
		}
		else
		{
			echo json_encode(array('status' => 'OK'));
		}
	}
	
	public function signup(){
			
		if(is_logged_in())
		{
			redirect('/');
		}
		
		
		$this->stencil->title('Sign Up');
		$this->stencil->paint('register/signup');
		
	}
	
	public function login(){
		
		if(is_logged_in())
		{
			redirect('/');
		}
		
		$this->stencil->title('Sign Up');
		$this->stencil->paint('register/login');
		
		
	}
	
}