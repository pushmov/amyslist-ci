<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class City extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('auth/auth_model', 'city/city_model'));
		$this->stencil->meta(array(
			'description' => "Amy's List provides local classifieds and forums for jobs, housing, for sale, personals, services, gigs, side jobs, and events.",
			'keywords' => "craigslist, local classifieds, jobs, housing, for sale, personals, local community, events",
			'author' => "Amy's List"
		));
		
  }
	
	public function index($segments)
	{
		
		$cities = explode('/',$segments);
		
		$entity_name = $this->city_model->entity_name($cities[0]);
		
		if($this->city_model->is_continent($cities[0]))
		{
			
			
			$row_continent = $this->city_model->continent_row($cities[0]);
			
			$data['childs'] = $row_continent->countries;
			
			$this->stencil->title( $row_continent->continent_display_name );
			$this->stencil->paint('city/city_list', $data);
			
		}
		elseif($this->city_model->is_a_country($cities[0]) && ($this->city_model->find_cub_cities($cities[0]) <= 0))
		{
			
			$detail_country = $this->city_model->detail_country($cities[0]);
			
			$data['continent'] = $this->city_model->detail_continent($detail_country->continent_id);
			$data['country'] = $detail_country;
			
			$data['categories'] = $this->city_model->populate_categories();
			$this->stencil->title( $detail_country->printable_name );
			$this->stencil->paint('city/category_country_list', $data);
			
		}
		elseif(!$entity_name['is_big_cities'] && count($cities) <= 1)
		{
			
			$row_item = $this->city_model->get_row_childs($entity_name);
			
			$data['title'] = $entity_name['name'];
			$data['url'] = $entity_name['url'];
			$data['childs'] = $row_item;
			
			$this->stencil->title( $data['title'] );
			$this->stencil->paint('city/city_list', $data);
		}
		else
		{
			
			if(count($cities) <= 1)
			{
				$city_id = $entity_name['id'];
				$row_city = $this->city_model->get_city($city_id);
			}
			else
			{
				$city_url = $cities[1];
				// echo $city_url;exit();
				
				$detail_city = $this->city_model->sub_city_id($city_url, $entity_name['id']);
				
				$city_id = $detail_city['id'];
				$row_city = $detail_city['data'];
				
			}
			
			
			$data['city'] = $row_city;
			$data['parent'] = $this->city_model->get_city($row_city->parent_city_id);
			$data['title'] = $entity_name['name'];
			
			$entity_name['is_big_city'] = $row_city->is_big_cities;
			$data['city_breadcumb'] = $this->city_model->get_row_childs($entity_name);
			$data['categories'] = $this->city_model->populate_categories();
			
			$this->stencil->title( $data['title'] );
			$this->stencil->paint('city/category_list', $data);
		}
	}
	
	public function listing($city_url=NULL, $cat_entity)
	{
		
		if($city_url == '' || !isset($city_url))
		{
			show_404();
		}
		
		$cat_item_entity = $this->city_model->get_cat_entity($cat_entity);
		$data['category'] = $cat_item_entity;
		$data['sibling_category'] = $this->city_model->get_sibling_cat($cat_item_entity['parent_item']->id);
		
		$category = $cat_item_entity['row_item']->id;
		if(!$this->city_model->is_a_country($city_url))
		{
			
			$city_row = $this->city_model->city_row($city_url);
			$data['city'] = $city_row;
            $data['city_name']= $data['city']->city_name;           
            $data['country']= $data['city']->country;           
           
            $city_parent= $data['city']->parent_city_id;
            if(!empty($city_parent)){
                $data['city_parent_row'] = $this->city_model->city_parent_row($city_parent);
            }
            
			
			$location_id = $city_row->city_id;
		
		}
		else
		{
			
			$country_id = $this->city_model->detail_country($city_url);
			
			$country_obj = new StdClass();
			$country_obj->city_id 				= $country_id->id;
			$country_obj->city_name 			= $country_id->printable_name;
			$country_obj->country 				= $country_id->id;
			$country_obj->state 					= '';
			$country_obj->parent_city_id 	= '';
			$country_obj->is_big_cities 	= '';
			$country_obj->url_name 				= $country_id->url_name;
			
			$data['city'] = $country_obj;
			// print_r($data['city']);exit();
			$location_id = $country_id->id;
			
        }
		
		$city_dropdown = $this->city_model->fetch_city_dropdown($city_url);
		$data['city_breadcumb'] = $city_dropdown;
		
		
		$postlist = $this->city_model->get_post($location_id, $category);
		$data['postlist'] = $postlist;
		
		$pos = array();
		$x = 0; $y = 0; $z = 0;
		
		$nx = 0;
		$ny = 0;
		$nz = 0;
		
		$n = count($postlist);
		
		if(!empty($postlist))
		{
			foreach($postlist as $row)
			{
				foreach($row as $r)
				{
                   
                    
					//converted radians position
					$x = cos($r->latitude) * cos($r->longitude);
					$y = cos($r->latitude) * sin($r->longitude);
					$z = sin($r->latitude);
					
					$nx += $x;
					$ny += $y;
					$nz += $z;
                    
                    //$map_images=array($postlistImages);
          if((int) $r->latitude != 0 && (int) $r->longitude != 0)
					{
						
						$postlistImages[] = $this->city_model->get_post_images($r->id);
						$postlistImagesDescription[] = $this->city_model->get_post_images_description($r->id);
						$r_username = ($r->is_show_username == '1') ? $r->username : 'hidden';
					
						$pos[] = array(
						$r->city_name,
						$r->latitude,
						$r->longitude,
                        $r->map_address,
                        $postlistImages,
                        $r->post_url_name,
                        $r->post_num_id,
                        $r->email,
												$r->username,
												$r->description
                    );
					}
				}
			}
			
			$tx = $nx / $n;
			$ty = $ny / $n;
			$tz = $nz / $n;
			
		}
		
		$m = count($pos);
		
		if($m > 1)
		{
			$center['longitude'] = atan2($ty, $tx);
			$center['hyp'] = sqrt(($tx * $tx) + ($ty * $ty));
			$center['latitude'] = atan2($tz, $center['hyp']);		
		}
		elseif($m == 1)
		{
			
			$center['longitude'] = $pos[0][2];
			$center['latitude'] = $pos[0][1];
			
		}
		else
		{
			$center['longitude'] = 0.000000;
			$center['latitude'] = 0.000000;
		}
		
		//get center of set location
		
        $data['positions'] = $pos;
		//$data['map_image'] = $postlistImages);
		$data['center'] = $center;
		$data['posttime'] = $this->city_model->populate_date();
		
		
		$is_parent = $this->city_model->is_parent_cat($cat_entity);
		if(!empty($is_parent) && $this->city_model->is_have_sub($cat_entity))
		{
			
			$data['childs'] = $this->city_model->find_sub_cat($is_parent->id);
			$data['parents'] = $is_parent;
			
			$this->stencil->layout('default_layout',$data);
			$this->stencil->title('Ads post listing');
			$this->stencil->paint('city/sub_category_list', $data);
			
		}
		else
		{
			
            //$this->load->library('googlemaps'); 
            /*$config['apikey'] = 'AIzaSyAPAmRqKw1HtZe0pf8ANRc0b5zi6k2nZz8';
            $this->googlemaps->initialize($config); 
            $data['map'] = $this->googlemaps->create_map(); */
           
            /*$this->load->library('googlemaps');
            $config['apikey'] = 'AIzaSyAPAmRqKw1HtZe0pf8ANRc0b5zi6k2nZz8';
            $config['center'] = $center['longitude'].",".$center['latitude'];
            $config['zoom'] = 'auto';
            $this->googlemaps->initialize($config);

            $marker = array();
            $marker['position'] = $center['longitude'].",".$center['latitude'];
            $this->googlemaps->add_marker($marker);
            $data['map'] = $this->googlemaps->create_map(); */
            
            /*$this->load->library('googlemaps');
            $config['center'] = '37.4419, -122.1419';
            $config['zoom'] = 'auto';
            $this->googlemaps->initialize($config);

            $marker = array();
            $marker['position'] = '37.429, -122.1419';
            $this->googlemaps->add_marker($marker);

            $data['map'] = $this->googlemaps->create_map();*/
            
            // print_r($postlist);
						// exit();
            $this->stencil->layout('maps_layout',$data);
			$this->stencil->title('Ads post listing');
			$this->stencil->paint('city/ads_listing', $data);
			
		}
		
		
		
	}
	
	

}