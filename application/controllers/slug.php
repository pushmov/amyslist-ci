<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Slug extends FrontEnd_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('slug_model','',TRUE);
	}
	
	function router($slug, $entity, $route, $param2 = NULL)
	{
		$res = $this->slug_model->check($slug, $entity);
		if($res === FALSE)
		{
			// display custom 404 page
			$this->template
			->set_layout('frontend')
			->build('page/frontend/404_new');
		}
		else
		{
			switch($route)
			{
				case 'submit_your_design' : Modules::run('challenge/submit_your_design_detail', $res->entity_id);
						break;
				case 'view_challenge' : Modules::run('view/qualification_detail', $res->entity_id);
						break;
				case 'gallery_detail' : Modules::run('gallery/detail', $res->entity_id);
						break;
				case 'profile' : Modules::run('user/menu/design', $res->entity_id);
						break;
				case 'forum_category' : Modules::run('forum/category', $res->entity_id);
						break;
				default : // display custom 404 page
						$this->template
						->set_layout('frontend')
						->build('page/frontend/404_new');
						break;
			}
		}
	}
}
