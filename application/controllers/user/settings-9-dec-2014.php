<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends MY_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('user/settings_model', 'user/profile_model'));
  }
	
	public function index()
	{
		
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/'));
		}
		
		$session = $this->session->userdata('user');
		
		$data['user'] = $this->profile_model->get_user($session['user_id']);
		
		$data['social'] = $this->profile_model->get_social($session['user_id']);
		
		$this->stencil->title('Account Setting');
		$this->stencil->paint('user/account_setting', $data);
	}
	
	public function save(){}
	
	public function biography()
	{
		
		$session = $this->session->userdata('user');
		
		$biography = $this->input->post('biography');
		
		$this->profile_model->table = 'user';
		if($this->profile_model->update(
			array('biography' => $biography),
			$conditions = array('id' => $session['user_id'])
		)){
			echo json_encode(array('status' => 'OK', 'message' => '<i class="fa fa-check fa-check-circle fa-lg"></i>'));
		}
		
	}
	
	public function social()
	{
		
		$session = $this->session->userdata('user');
		
		$socials = $this->input->post();
		
		$this->profile_model->table = 'user_social';
		$row = $this->profile_model->get_social($session['user_id']);
		
		if(empty($row))
		{
			$socials['user_id'] = $session['user_id'];
			$this->profile_model->save($socials);
		}
		else
		{
			$this->profile_model->update($socials,
				$conditions = array('user_id' => $session['user_id'])
			);
		}
		
		echo json_encode(array('status' => 'OK', 'message' => '<i class="fa fa-check fa-check-circle fa-lg"></i>'));
	}
	
	public function checkpassword($pass)
	{
		
		$session = $this->session->userdata('user');
		
		$res = $this->profile_model->check_password($session['user_id'], $pass);
		
		echo json_encode(array('result' => $res, 'message' => '<i class="fa fa-check-circle fa-lg"></i>'));
	}
	
	public function updatepassword()
	{
		$old = $this->input->post('old');
		$newpass = $this->input->post('newpass');
		$repeat = $this->input->post('repeat');
		
		$session = $this->session->userdata('user');
		
		$res = $this->profile_model->check_password($session['user_id'], $old);
		if($res != '1')
		{
			echo json_encode(array('status' => 0, 'error' => 1));
		}
		
		if($newpass != $repeat)
		{
			echo json_encode(array('status' => 0, 'error' => 2));
		}
		
		$this->profile_model->table = 'user';
		if($this->profile_model->update(
			array('plain' => $newpass, 'password' => sha1($newpass)),
			$conditions = array('id' => $session['user_id'])
		))
		{
			echo json_encode(array('status' => 1, 'error' => 0, 'message' => '<i class="fa fa-check-circle fa-lg"></i>'));
		}
		
	}
	
	function notifications_adposted()
	{
		$session = $this->session->userdata('user');
		
		$state = ( $this->input->post('notify_ads_posted')== 'false') ? 0 : 1;
		
		$this->profile_model->table = 'user';
		if($this->profile_model->update(
			array('notify_ads_posted' => $state),
			$conditions = array('id' => $session['user_id'])
		))
		{
			echo json_encode(array('status' => 'OK', 'message' => '<i class="fa fa-check-circle fa-lg"></i>'));
		}
	}
	
	function notifications_likes()
	{
		$session = $this->session->userdata('user');
		
		$state = ( $this->input->post('notify_like')== 'false') ? 0 : 1;
		
		$this->profile_model->table = 'user';
		if($this->profile_model->update(
			array('notify_like' => $state),
			$conditions = array('id' => $session['user_id'])
		))
		{
			echo json_encode(array('status' => 'OK', 'message' => '<i class="fa fa-check-circle fa-lg"></i>'));
		}
	}
	
	function notifications_inbox()
	{
		$session = $this->session->userdata('user');
		
		$state = ( $this->input->post('notify_inbox_message')== 'false') ? 0 : 1;
		
		$this->profile_model->table = 'user';
		if($this->profile_model->update(
			array('notify_inbox_message' => $state),
			$conditions = array('id' => $session['user_id'])
		))
		{
			echo json_encode(array('status' => 'OK', 'message' => '<i class="fa fa-check-circle fa-lg"></i>'));
		}
		
	}
	
	function notifications_chat()
	{
		$session = $this->session->userdata('user');
		
		$state = ( $this->input->post('notify_chat')== 'false') ? 0 : 1;
		
		$this->profile_model->table = 'user';
		if($this->profile_model->update(
			array('notify_chat' => $state),
			$conditions = array('id' => $session['user_id'])
		))
		{
			echo json_encode(array('status' => 'OK', 'message' => '<i class="fa fa-check-circle fa-lg"></i>'));
		}
	}
	
	function drop_account()
	{
		
		$session = $this->session->userdata('user');
		
		$this->db->where('user_id', $session['user_id'])
			->delete('user_video');
		
		$this->db->where('user_id', $session['user_id'])
			->delete('user_social');
		
		$this->db->where('user_id', $session['user_id'])
			->delete('user_photos');
		
		$this->db->where('user_id', $session['user_id'])
			->delete('user_notifications');
		
		$this->db->where('user_id', $session['user_id'])
			->delete('user_like');
		
		$this->db->where('id', $session['user_id'])
			->delete('user');
			
		redirect(site_url('auth/auth/logout'));
	}
	
}