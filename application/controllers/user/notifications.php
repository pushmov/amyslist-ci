<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user/notification_model');
		
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/register'));
		}
		
  }
	
	public function index()
	{
		
		$session = $this->session->userdata('user');
		
		$data['notifications'] = $this->notification_model->get_notifications($session['user_id']);
		
		$this->stencil->title('Notifications');
		$this->stencil->paint('user/notification', $data);
	}
	
	public function read()
	{
		
		$session = $this->session->userdata('user');
		$this->notification_model->set_read($session['user_id']);
		
		echo json_encode(array('status' => 'OK'));
		exit();
		
	}
	
	public function delete($id=NULL)
	{
		if(!isset($id))
		{
			redirect(site_url('user/notifications'));
		}
		
		$this->notification_model->table = 'user_notifications';
		$this->notification_model->update(
			array('delete_status' => 1, 'unread_status' => 0),
			array('notification_id' => $id)
		);
		
		redirect(site_url('user/notifications'));
	}

}