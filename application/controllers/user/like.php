<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Like extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user/like_model');
		
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/register'));
		}
		
  }
	
	public function index()
	{
	
		$session = $this->session->userdata('user');
		
		$user = $this->like_model->user_detail($session['user_id']);
		$total = $this->like_model->count_all_likes($session['user_id']);
		
		
		$data['user'] = $user;
		$data['all_likes'] = $total;
		$data['liked'] = $this->like_model->my_likes($session['user_id']);
		
		$data['likes'] = $this->like_model->likes($session['user_id']);
		$data['all_liked'] = count($data['likes']);
		
		$this->stencil->title('Like Page');
		$this->stencil->paint('user/likes', $data);
	}
	
	public function view($username=NULL)
	{
		if(!isset($username))
		{
			redirect(site_url('/'));
		}
		
		$row_username = $this->like_model->user_name($username);
		
		if(empty($row_username))
		{
			redirect(site_url('/'));
		}
		
		$total = $this->like_model->count_all_likes($row_username->id);
		
		
		$data['user'] = $row_username;
		$data['all_likes'] = $total;
		$data['liked'] = $this->like_model->my_likes($row_username->id);
		
		$data['likes'] = $this->like_model->likes($row_username->id);
		$data['all_liked'] = count($data['likes']);
		
		$this->stencil->title('Like Page');
		$this->stencil->paint('user/public_likes', $data);
	}
	
}