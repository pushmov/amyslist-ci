<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Video extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user/video_model');
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/register'));
		}
		
  }
	
	public function index()
	{
		$session = $this->session->userdata('user');
		
		$video_row = $this->video_model->find_video($session['user_id']);
		$data['video'] = $video_row;
		$data['session'] = $session;
		$this->stencil->title('Video Page');
		$this->stencil->paint('user/video', $data);
	}
	
	public function save()
	{
		
		$session = $this->session->userdata('user');
		
		$source = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>", $this->input->post('source'));
		$this->video_model->table = 'user_video';
		$this->video_model->save(
			array('user_id' => $session['user_id'], 'video_source' => $source)
		);
		
		redirect(site_url('user/video'));
	}
	
	public function reset()
	{
		
		$session = $this->session->userdata('user');
		
		$this->db->where('user_id', $session['user_id'])
			->delete('user_video');
		
		redirect(site_url('user/video'));
	}
	
}