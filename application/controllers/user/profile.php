<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MY_Controller {
	
	const MAX_ALLOWED_SHIPMENT_PICTURE = 5120;
	const FILE_ALLOWED_TYPES = 'jpg|jpeg|bmp|gif|png|JPG|JPEG|BMP|GIF|PNG';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('ads/adsposted', 'ads/ads_model', 'user/profile_model'));
		
  }
	
	public function index()
	{
		
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/'));
		}
		
		$session = $this->session->userdata('user');
		
		$data['user'] = $this->profile_model->get_user($session['user_id']);
		$data['ads'] = $this->profile_model->get_user_ads($session['user_id']);
		$data['video'] = $this->profile_model->find_video($session['user_id']);
		$data['social'] = $this->profile_model->get_social($session['user_id']);
		
		$this->stencil->title('My Profile');
		$this->stencil->paint('user/profile', $data);
	}
	
	public function about()
	{
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/'));
		}
		
		$session = $this->session->userdata('user');
		$data['social'] = $this->profile_model->get_social($session['user_id']);
		$data['user'] = $this->profile_model->get_user($session['user_id']);
		$data['tab'] = 'about';
		
		$this->stencil->title('My Profile');
		$this->stencil->paint('user/profile_about', $data);
	}
	
	public function delete_ad($idpost=NULL)
	{
		if(!isset($idpost))
		{
			redirect(site_url('user/profile#tabs5'));
		}
		
		$this->profile_model->table = 'post_ad';
		$this->profile_model->update(
			array('delete_status' => 1),
			array('id' => $idpost)
		);
		
		redirect(site_url('user/profile#tabs5'));
	}
	
	public function view($username=NULL)
	{
		
		if(!isset($username))
		{
			redirect('/');
		}
		
		$row_username = $this->profile_model->get_username(urldecode($username));
		
		if(empty($row_username))
		{
			redirect('/');
		}
		
		$session = $this->session->userdata('user');
		
		$data['already_liked'] = $this->profile_model->check_already_liked($row_username->id, $session['user_id']);
		$data['user'] = $this->profile_model->get_user($row_username->id);
		$data['session'] = $session;
		$data['social'] = $this->profile_model->get_social($row_username->id);
		$data['tab'] = 'about';
		
		$this->stencil->title('Profile - ' . $row_username->username);
		$this->stencil->paint('user/public/profile_public', $data);
	}
	
	public function xeditable()
	{
		
		if(!$this->input->is_ajax_request())
		{
			exit();
		}
		
		$cols = $this->input->post('pk');
		$vals = $this->input->post('value');
		
		$session = $this->session->userdata('user');
		$userid = $session['user_id'];
		
		$this->profile_model->table = 'user';
		if($this->profile_model->update(
			array( $cols => $vals ),
			$conditions = array('id' => $userid)
		))
		{
			echo json_encode(array('status' => 'OK'));
		}
	}
	
	public function change_pic()
	{
		$photo = (!empty($_FILES['profpic_file']['tmp_name'])) ? md5(time()) : '';
		
		$session = $this->session->userdata('user');
		$userid = $session['user_id'];
		
		if($photo == '')
		{
			exit();
		}
			
		$config['allowed_types'] = self::FILE_ALLOWED_TYPES;
		$config['max_size'] = self::MAX_ALLOWED_SHIPMENT_PICTURE;
		$config['upload_path'] = './assets/uploads/profpic/';
		$config['file_name'] = $photo;
			
		$this->load->library('upload');
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('profpic_file'))
		{
			
			$response['status'] = 'fail';
			$response['newimage'] = '';
			$response['message'] = 'Allowed picture : png, jpg or bmp only | max size : '.self::FILE_ALLOWED_TYPES.' KB';
			echo json_encode($response);
			exit();
		}
		else
		{
			$data_photo = $this->upload->data();
			$photo = $data_photo['file_name'];
			
			$this->profile_model->table = 'user';
			$this->profile_model->update(
				array('profpic' => 'uploads/profpic/' . $photo),
				$conditions = array('id' => $userid)
			);
			
			echo json_encode(array('status' => 'OK', 'src' => asset_url() .'tn/tn.php?w=110&h=110&src=' . asset_url() . 'uploads/profpic/' . $photo));
		}
	}
	
	public function like($toid=NULL)
	{
		
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/'));
		}
		
		if(!isset($toid))
		{
			redirect(site_url('user/profile'));
		}
		
		$session = $this->session->userdata('user');
		$liked_user = $this->profile_model->get_user($toid);
		
		//validation
		if($this->profile_model->check_already_liked($toid, $session['user_id']))
		{
			redirect(site_url('profile/' . $liked_user->username));
		}
		
		//create record user_like
		//count user like table
		
		$this->profile_model->table = 'user_like';
		$this->profile_model->save(
			array('user_id' => $toid, 'liked_by' => $session['user_id'])
		);
		
		//update count
		
		$this->profile_model->table = 'user';
		$this->profile_model->update(
			array('like_count' => $liked_user->like_count + 1),
			$conditions = array('id' => $toid)
		);
		
		
		//give notification to 'toid'
		$this->profile_model->table = 'user_notifications';
		$this->profile_model->save(
			array(
				'user_id' => $toid,
				'from_id' => $session['user_id'],
				'message' => '<p>
						<a class="profile-link" href="'.site_url('profile/' . $session['username']).'">' . $session['username'] . '</a> liked your <a href="'.site_url('user/profile').'">profile</a>
					</p>'
			)
		);
		
		redirect(site_url('profile/' . $liked_user->username));
	}
	
	public function unlike($toid=NULL)
	{
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/'));
		}
		
		if(!isset($toid))
		{
			redirect(site_url('user/profile'));
		}
		
		$session = $this->session->userdata('user');
		$liked_user = $this->profile_model->get_user($toid);
		
		if(!$this->profile_model->check_already_liked($toid, $session['user_id']))
		{
			redirect(site_url('profile/' . $liked_user->username));
		}
		
		//delete record
		$this->db->where(array('user_id' => $toid, 'liked_by' => $session['user_id']))
			->delete('user_like');
		
		//update count
		$this->profile_model->table = 'user';
		$this->profile_model->update(
			array('like_count' => $liked_user->like_count - 1),
			$conditions = array('id' => $toid)
		);
		
		//give notification to 'toid'
		$this->profile_model->table = 'user_notifications';
		$this->profile_model->save(
			array(
				'user_id' => $toid,
				'from_id' => $session['user_id'],
				'message' => '<p>
						<a class="profile-link" href="'.site_url('profile/' . $session['username']).'">' . $session['username'] . '</a> unlike your <a href="'.site_url('user/profile').'">profile</a>
					</p>'
			)
		);
		
		redirect(site_url('profile/' . $liked_user->username));
		
	}
	
	public function wall()
	{
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/'));
		}
		
		$session = $this->session->userdata('user');
		$data['social'] = $this->profile_model->get_social($session['user_id']);
		$data['user'] = $this->profile_model->get_user($session['user_id']);
		$data['tab'] = 'wall';
		
		$this->stencil->title('My Profile - Wall');
		$this->stencil->paint('user/profile_wall', $data);
	}
	
	public function public_wall($username)
	{
		if(!isset($username))
		{
			redirect('/');
		}
		
		$row_username = $this->profile_model->get_username(urldecode($username));
		
		if(empty($row_username))
		{
			redirect('/');
		}
		
		$session = $this->session->userdata('user');
		
		$data['already_liked'] = $this->profile_model->check_already_liked($row_username->id, $session['user_id']);
		$data['user'] = $this->profile_model->get_user($row_username->id);
		$data['session'] = $session;
		$data['social'] = $this->profile_model->get_social($row_username->id);
		$data['tab'] = 'wall';
		
		$this->stencil->title('Profile - ' . $row_username->username);
		$this->stencil->paint('user/public/profile_wall', $data);
	}
	
	public function chat()
	{
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/'));
		}
		
		$session = $this->session->userdata('user');
		$data['social'] = $this->profile_model->get_social($session['user_id']);
		$data['user'] = $this->profile_model->get_user($session['user_id']);
		$data['tab'] = 'chat';
		
		$this->stencil->title('My Profile - Chat');
		$this->stencil->paint('user/profile_chat', $data);
	}
	
	public function public_chat()
	{
		echo 'public chat';
	}
	
	public function ads()
	{
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/'));
		}
		
		
		
		$session = $this->session->userdata('user');
		$data['social'] = $this->profile_model->get_social($session['user_id']);
		$data['ads'] = $this->profile_model->get_user_ads($session['user_id']);
		$data['user'] = $this->profile_model->get_user($session['user_id']);
		$data['tab'] = 'ads';
		
		$this->stencil->title('My Profile - Ads');
		$this->stencil->paint('user/profile_ads', $data);
	}
	
	public function public_ads($username)
	{
		if(!isset($username))
		{
			redirect('/');
		}
		
		$row_username = $this->profile_model->get_username(urldecode($username));
		
		if(empty($row_username))
		{
			redirect('/');
		}
		
		$session = $this->session->userdata('user');
		
		$data['already_liked'] = $this->profile_model->check_already_liked($row_username->id, $session['user_id']);
		$data['user'] = $this->profile_model->get_user($row_username->id);
		$data['session'] = $session;
		$data['social'] = $this->profile_model->get_social($row_username->id);
		$data['ads'] = $this->profile_model->get_user_ads($row_username->id);
		$data['tab'] = 'ads';
		
		$this->stencil->title('Profile - ' . $row_username->username);
		$this->stencil->paint('user/public/profile_ads', $data);
	}
	
	public function video()
	{
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/'));
		}
		
		$session = $this->session->userdata('user');
		$data['social'] = $this->profile_model->get_social($session['user_id']);
		$data['video'] = $this->profile_model->find_video($session['user_id']);
		$data['user'] = $this->profile_model->get_user($session['user_id']);
		$data['tab'] = 'video';
		
		$this->stencil->title('My Profile - Video');
		$this->stencil->paint('user/profile_video', $data);
	}
	
	public function public_video($username)
	{
		if(!isset($username))
		{
			redirect('/');
		}
		
		$row_username = $this->profile_model->get_username(urldecode($username));
		
		if(empty($row_username))
		{
			redirect('/');
		}
		
		$session = $this->session->userdata('user');
		
		$data['already_liked'] = $this->profile_model->check_already_liked($row_username->id, $session['user_id']);
		$data['user'] = $this->profile_model->get_user($row_username->id);
		$data['session'] = $session;
		$data['social'] = $this->profile_model->get_social($row_username->id);
		$data['video'] = $this->profile_model->find_video($row_username->id);
		$data['tab'] = 'video';
		
		$this->stencil->title('Profile - ' . $row_username->username);
		$this->stencil->paint('user/public/profile_video', $data);
	}
	
	public function report()
	{
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/'));
		}
		
		$session = $this->session->userdata('user');
		$data['social'] = $this->profile_model->get_social($session['user_id']);
		$data['video'] = $this->profile_model->find_video($session['user_id']);
		$data['user'] = $this->profile_model->get_user($session['user_id']);
		$data['tab'] = 'report';
		
		$this->stencil->title('My Profile - Report');
		$this->stencil->paint('user/profile_report', $data);
	}
	
	public function public_report($username)
	{
		if(!isset($username))
		{
			redirect('/');
		}
		
		$row_username = $this->profile_model->get_username(urldecode($username));
		
		if(empty($row_username))
		{
			redirect('/');
		}
		
		$session = $this->session->userdata('user');
		
		$data['already_liked'] = $this->profile_model->check_already_liked($row_username->id, $session['user_id']);
		$data['user'] = $this->profile_model->get_user($row_username->id);
		$data['session'] = $session;
		$data['social'] = $this->profile_model->get_social($row_username->id);
		$data['tab'] = 'report';
		
		$this->stencil->title('Profile - ' . $row_username->username);
		$this->stencil->paint('user/public/profile_report', $data);
	}
	
	public function setting()
	{
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/'));
		}
		
		$session = $this->session->userdata('user');
		$data['social'] = $this->profile_model->get_social($session['user_id']);
		$data['video'] = $this->profile_model->find_video($session['user_id']);
		$data['user'] = $this->profile_model->get_user($session['user_id']);
		$data['tab'] = 'setting';
		
		$this->stencil->title('My Profile - Setting');
		$this->stencil->paint('user/profile_setting', $data);
	}
	

}