<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('auth/auth_model', 'home/home_model'));
		
  }
	
	public function index()
	{
		
		$data['us_cities'] = $this->home_model->get_us_cities();
		$data['us_states'] = $this->home_model->get_us_states();
		$data['intl_cities'] = $this->home_model->get_intl_cities();
		
		$this->stencil->title('Home');
		$this->stencil->meta(array(
			'description' => "Amy's List provides local classifieds for jobs, housing, for sale, services, personals, gigs, side jobs, and events.",
			'keywords' => "craigslist, local classifieds, jobs, housing, for sale, personals, services, events, gigs, events",
			'author' => "Amy's List"
		));
		
		$this->stencil->paint('home', $data);
		
	}
	
	public function maintenance()
	{
		
		$this->load->view('maintenance');
	}
	
	public function search(){
		
		$this->stencil->layout('gsce_layout');
		$this->stencil->title('Search');
		$this->stencil->meta(array(
			'description' => "Amy's List provides local classifieds for jobs, housing, for sale, services, personals, gigs, side jobs, and events.",
			'keywords' => "craigslist, local classifieds, jobs, housing, for sale, personals, services, events, gigs, events",
			'author' => "Amy's List"
		));
		
		$this->stencil->paint('search', $data);
		
	}
	
	
}