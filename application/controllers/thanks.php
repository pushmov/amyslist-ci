<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Thanks extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
  }
	
	public function index()
	{
		$this->stencil->title('Thank you');
		$this->stencil->paint('thank-you');
	}
	
}