<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Router extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('auth/auth_model', 'home/home_model', 'city/city_model'));
  }
	
	public function slug($query)
	{
		
		$is_city_entity = $this->city_model->is_cities($query);
		$seg_array = $this->uri->segment_array();
		$all_segments = join('/', $this->uri->segment_array());
		
		if($is_city_entity)
		{
			
			if(count($this->uri->segment_array()) == 1 || $this->city_model->is_cities($seg_array[2]))
			{
				
				
				//load city list page
				$this->load->library('../controllers/city');
				$this->city->index($all_segments);
			}
			else
			{
				
				$city = $this->uri->segment_array();
				
				//load city list page
				$this->load->library('../controllers/city');
				$this->city->listing($city[1], end($this->uri->segment_array()));
			}
			
			
		}
		else
		{
			
			$this->load->library('../controllers/city');
			$this->city->index($all_segments);
			
		}
		
	}
	
	public function generate_city_url()
	{
		
		$query = $this->db->get('location_cities')
			->result();
		
		foreach($query as $row)
		{
			
			$id_city = $row->city_id;
			$url_name = str_replace(' ', '-', strtolower($row->city_name));
			$url_name = str_replace('/', '', $url_name);
			$url_name = str_replace('.', '', $url_name);
			
			
			$result = $this->db->where('city_id', $id_city)
				->set('url_city_name', $url_name)
				->update('location_cities');
			
			if($result)
			{
				echo 'updated {' . $row->city_name . '} to '  . $url_name;
				echo '<br>';
			}
			else
			{
				echo 'failed {' . $row->city_name . '} to '  . $url_name;
				echo '<br>';
			}
			
		}
		
	}
	
	public function generate_category_url()
	{
		
		$query = $this->db->get('categories')
			->result();
		
		foreach($query as $row)
		{
			
			$url_name = str_replace(' ', '-', strtolower($row->category_name));
			$url_name = str_replace('/', '-', $url_name);
			$url_name = str_replace('.', '', $url_name);
			$url_name = str_replace('\'', '', $url_name);
			
		
			$result = $this->db->where('id', $row->id)
				->set('url_category_name', $url_name)
				->update('categories');
				
			if($result)
			{
				echo 'updated {' . $row->category_name . '} to '  . $url_name;
				echo '<br>';
			}
			else
			{
				echo 'failed {' . $row->category_name . '} to '  . $url_name;
				echo '<br>';
			}
		}
	}
	
	public function generate_state()
	{
		
		$query = $this->db->get('location_us_states')
			->result();
		
		foreach($query as $row)
		{
			
			$url_name = str_replace(' ', '-', strtolower($row->name));
			$url_name = str_replace('/', '-', $url_name);
			$url_name = str_replace('.', '', $url_name);
		
			$result = $this->db->where('id', $row->id)
				->set('url_state_name', $url_name)
				->update('location_us_states');
				
			if($result)
			{
				echo 'updated {' . $row->name . '} to '  . $url_name;
				echo '<br>';
			}
			else
			{
				echo 'failed {' . $row->name . '} to '  . $url_name;
				echo '<br>';
			}
		}
	}
	
	public function generate_countries()
	{
		
		$query = $this->db->get('location_countries')
			->result();
		
		foreach($query as $row)
		{
			
			$url_name = str_replace(' ', '-', strtolower($row->printable_name));
			$url_name = str_replace('/', '-', $url_name);
			$url_name = str_replace('.', '', $url_name);
			$url_name = str_replace(',', '', $url_name);
			$url_name = str_replace('(', '', $url_name);
			$url_name = str_replace(')', '', $url_name);
			$url_name = str_replace('\'', '', $url_name);
			
			
			
			
		
			$result = $this->db->where('id', $row->id)
				->set('url_country_name', $url_name)
				->update('location_countries');
				
			if($result)
			{
				echo 'updated {' . $row->printable_name . '} to '  . $url_name;
				echo '<br>';
			}
			else
			{
				echo 'failed {' . $row->printable_name . '} to '  . $url_name;
				echo '<br>';
			}
		}
	}
	
	public function generate_seo_ad()
	{
		$query = $this->db->get('post_ad')
			->result();
			
		foreach($query as $row)
		{
			
			$url_name = str_replace(' ', '-', strtolower($row->title));
			$url_name = str_replace('/', '-', $url_name);
			$url_name = str_replace('.', '', $url_name);
			$url_name = str_replace(',', '', $url_name);
			$url_name = str_replace('(', '', $url_name);
			$url_name = str_replace(')', '', $url_name);
			$url_name = str_replace('\'', '', $url_name);
			
			$result = $this->db->where('id', $row->id)
				->set('url_name', $url_name)
				->update('post_ad');
				
			if($result)
			{
				echo 'updated {' . $row->title . '} to '  . $url_name;
				echo '<br>';
			}
			else
			{
				echo 'failed {' . $row->title . '} to '  . $url_name;
				echo '<br>';
			}
		}
	}
	
}