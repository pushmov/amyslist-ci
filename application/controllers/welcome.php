<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('auth/auth_model');
  }
	
	public function index()
	{
		
		echo 'Maintenance. please go to /home/';
		exit();
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */