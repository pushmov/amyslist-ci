<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ads extends MY_Controller {
	
	const MAX_ALLOWED_PICTURE = 51200;
	
	// var $upload_path = 
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ads/ads_model');
		
  }
	
	public function index()
	{
		
		if(!is_logged_in())
		{
			$this->session->set_flashdata('noty', array('message' => 'Please login/register to continue...', 'type' => 'error'));
			redirect(site_url('/register'));
		}
		
		$data['us_cities'] = $this->ads_model->get_us_cities();
		$data['us_state'] = $this->ads_model->get_us_states();
		
		$data['intl_cities'] = $this->ads_model->get_international_cities();
		$data['international'] = $this->ads_model->world_countries();
		
		$data['categories'] = $this->ads_model->categories();
		
		$this->stencil->title('Post an Ad');
		$this->stencil->paint('ads/adpost', $data);
	}
	
	function _get_latlon($addr){
	
		$json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=". urlencode($addr) ."&sensor=false");
    $json = json_decode($json);

    $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
    $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
		
		return array('lat' => $lat, 'lon' => $long);
	}
	
	public function submit()
	{
		
		$this->load->library('upload');
        $this->load->library('image_lib');
		
		$request = $this->input->post('adpost');
		
		$upload_path = './assets/uploads/post/'; 
		
		if(!is_dir($upload_path))
		{
			mkdir($upload_path);
		}
		
		$session_user = $this->session->userdata('user');
		$rand = substr(sha1(time()), 0, 8);
		
		$position = $this->_get_latlon($request['map_address']);
		$url_name = $this->_url_name($this->_clean(trim($request['title'])));
		
		$data_post = array(
			'user_id' => $session_user['user_id'],
			'post_num_id' => strtoupper($rand),
			'category_id' => $request['category_id'],
			'location_id' => $request['location_id'],
			'title' => $request['title'],
			// 'price' => $request['price'],
			'description' => $request['description'],
			'rent' => $request['rent'],
			'rooms' => $request['rooms'],
			'email' => $request['email'],
			'is_anonymous_email' => (isset($request['is_anonymous_email'])) ? 1 : 0,
			'anonymous_email' => $rand . '@amyslist.us',
			'is_show_username' => (isset($request['is_show_username'])) ? 1 : 0,
			'is_allow_text_chat' => (isset($request['is_allow_text_chat'])) ? 1 : 0,
			'is_allow_text_message' => (isset($request['is_allow_text_message'])) ? 1 : 0,
			'phone_number' => $request['phone_number'],
			'address' => $request['address'],
			'map_address' => $request['map_address'],
			'video_link' => $request['video_link'],
			'full_url' => site_url('viewpost/' . strtoupper($rand) . '/' . $url_name),
			// 'expired_at' => date('Y-m-d H:i:s', strtotime("+30 days")),
			'latitude' => isset($position['lat']) ? $position['lat'] : '0.00000',
			'longitude' => isset($position['lon']) ? $position['lon'] : '0.00000',
			'url_name' => $url_name,
		);
		
		$insert_id = $this->ads_model->save($data_post);
		
		if((!empty($_FILES['photo_file']['name'])))
		{
			$config['allowed_types'] = 'png|jpg|jpeg|PNG|JPG|JPEG';
			$config['max_size'] = self::MAX_ALLOWED_PICTURE;
			$config['upload_path'] = $upload_path;

			$files = $_FILES;
			$post_images = array();
			$cpt = count($_FILES['photo_file']['name']);
            
			for($i=0; $i<$cpt; $i++)
			{
                $photo_name = $files['photo_file']['name'][$i];
				
               
                    $config['file_name'] = md5($photo_name.date("Y-m-d H:i:s"));
				    $_FILES['photo_file']['name']= $files['photo_file']['name'][$i];
				    $_FILES['photo_file']['type']= $files['photo_file']['type'][$i];
				    $_FILES['photo_file']['tmp_name']= $files['photo_file']['tmp_name'][$i];
				    $_FILES['photo_file']['error']= $files['photo_file']['error'][$i];
				    $_FILES['photo_file']['size']= $files['photo_file']['size'][$i];
                    
                    
                    //print_R($config);
                    $this->upload->initialize($config);  
				    if(!$this->upload->do_upload('photo_file'))
				    {
                        echo json_encode(array('message' => $this->upload->display_errors(), 'status' => 'failed', 'redirect' =>  site_url( '/submit_your_design/challenge/' )));
					    exit();
				    }
				    else
				    {
					    $data_photo = $this->upload->data();
                        /*GD LIbrary for resize $resize_conf = array(
                        'source_image'  => $data_photo['full_path'], 
                        'new_image'     => $data_photo['file_path'].$data_photo['file_name'],
                        'width'         => 600,
                        'height'        => 400
                        );
                        
                        $this->image_lib->initialize($resize_conf);
                        if (!$this->image_lib->resize()){
                           $error['resize'][] = $this->image_lib->display_errors();
                        }
                        else{
                           $success[] = $upload_data;
                        }*/
                        $post_images['image_path'][] = $data_photo['file_name'];
					    $post_images['description'][] = $request['photo_description'][$i];
                    }
				
                
                
			}
			
		}
		
        
		if(!empty($post_images['image_path']))
		{
			foreach($post_images['image_path'] as $key => $post)
			{
				
				$image_data = array(
					'post_ad_id' => $insert_id,
					'image_path' => 'uploads/post/' . $post,
					'image_description' => $post_images['description'][$key]
				);
				
				$this->ads_model->save_images($image_data);
			}
		}
		
		$ref_id = $this->ads_model->get_ref_id($insert_id);
        echo json_encode(array('redirect' => site_url('post/ads/review/' . $ref_id), 'success' => 'success'));
    
	}
    
   
   
    
    
		
	public function review($ref_id=NULL)
	{
		
		if(!isset($ref_id))
		{
			redirect(site_url('post/ads/'));
		}
		$session = $this->session->userdata('user');
		
		$row_temp_post = $this->ads_model->get_post_by_ref($ref_id);
		
		if(empty($row_temp_post))
		{
			redirect(site_url('post/ads'));
		}
		
		if((int)$row_temp_post->post_status == 1)
		{
			$this->session->set_flashdata('noty', array('message' => 'Post currently not available...', 'type' => 'error'));
			redirect(site_url('post/ads'));
		}
		
		$row_temp_post->username = $session['username'];
		
		$data['post'] = $row_temp_post;
		
		$images = $this->ads_model->get_images($row_temp_post->id);
		
		$data['images'] = $images;
		
		
		$data['location'] = $this->ads_model->get_city_name($row_temp_post->location_id);
		$data['category'] = $this->ads_model->get_parent_child_cat($row_temp_post->category_id);
		$this->stencil->title('Review to Publish');
		$this->stencil->paint('ads/review', $data);
		
	}
    
    
    
    
    
	
	public function publish($id_post=NULL)
	{
		
		//if not payment : notification apps -> email
		//else : redirect to payment
		
		$user_session = $this->session->userdata('user');
		
		$row_user = $this->ads_model->get_user($user_session['user_id']);
		
		if(!isset($id_post))
		{
			redirect(site_url('post/ads'));
		}
		
		$post = $this->ads_model->get_post($id_post);
		if(empty($post))
		{
			redirect(site_url('post/ads'));
		}
		
		$is_charged = $this->ads_model->check_charge($post->category_id, $post->location_id);
		if(!empty($is_charged))
		{
			redirect(site_url('post/payment/index/' . $post->post_num_id));
		}
		
		//update post flag
		$this->ads_model->activate_post($id_post);
		
		//notification
		$from = 1;
		$amyadmin = $this->ads_model->get_admin();
		$notification_data = array(
			'user_id' => $user_session['user_id'],
			'from_id' => $from,
			'message' => '<p><a class="profile-link" href="'.site_url('/profile/' . $amyadmin->username) . '">Amy Admin</a> Thanks for posting an ad! Check your <a href="'.site_url('ads/posted').'">Ads Posted page</a> on to edit, review, or delete your <a href="' . site_url( 'viewpost/' . $post->post_num_id . '/' . $post->url_name ) . '">ad post</a></p>'
		);
		
		$this->ads_model->create_notification($notification_data);
		
		$to_email = $post->email;
		$to_name = $user_session['username'];
		$subject = "Amyslist: Publish/Edit/Delete your post";
		
		
		$body = '<div style="width:940px; height:632px; float:left; padding-left:40px;">';
		$body .= '    <div style="float:left; width:160px; height:632px; padding-top:30px;">';
		$body .= '        <img src="'.asset_url().'img/lady.png"  width="168" height="508"/>';
		$body .= '    </div>';
		$body .= '    <div style="width:700px; float:left; height:632px;" class="topleft">';
		$body .= '        <h1 style="padding-left:150px;width:600px;">';
		$body .= '			<a class="logo" href="http://www.amyslist.us/"> <img src="'.asset_url().'img/theme/logo.png" /></a>';
		$body .= '        </h1>';
		$body .= '        <div style="font-family:Calibri; font-size:16px; padding-left:20px;">';
		$body .= "<span style='font-family:Calibri;font-size:18px; color:#084881;' ><br /><br />".$to_name.",</span><br /><br />";
		$body .= "<p style='font-family:Calibri;font-size:18px;' >Thank you for posting an ad on Amy's List. Please click the following link to publish your ad:<br /> ";
		$body .= "<span style='color:#e32b83; font-size:14px;'><a href='http://www.amyslist.us/ads/posted/edit/".$id_post."' >http://www.amyslist.us/ads/posted/edit/".$post_id."</a></span><br /><br /></p>";
		$body .= "<p style='font-family:Calibri; font-size:18px;'>You can publish, edit and delete your ad with the link above. You can also keep track with your
			ads at your profile under your <span style='color:#e32b83; font-size:16px;'><a href='http://www.amyslist.us/ads/posted/'>Ads Posted</a></span> page.<br /><br /></p> ";
		
		$body .= '            <p style="font-family:Calibri; font-size:18px;">';
		$body .= " Amy's List is the future of online classified ads. And we're glad you're part of it.</p> ";
		$body .= '			<p style="font-family:Calibri; font-size:18px;">Sincerely, <br />';
		$body .= '            <a href="http://amyslist.us/amyadmin 
		" style="color:#f52d8f;font-size:18px;text-decoration:none;">Amy Admin</span> </a>';
		$body .= '        </div> ';          
		$body .= '    </div>';
		$body .= '</div>';
		
		//===================================
		$from_email = "admin@amyslist.com";
		$from_name = "Amyslist";
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		$headers .= 'To: '.$to_name.' <'.$to_email.'>' . "\r\n";
		$headers .= 'From: Amyslist <admin@amyslist.com>' . "\r\n";

		// Mail it
		if((int) $row_user->notify_ads_posted == 1)
		{
			mail($to_email, $subject, $body, $headers);
		}
		
		$this->session->set_flashdata('noty', array('message' => 'Thank you for posting with amyslist.us!', 'type' => 'success'));
		redirect(site_url('thank-you'));
	
	}
	
	public function edit($id_post=NULL)
	{
		if(!isset($id_post))
		{
			redirect(site_url('post/ads'));
		}
	}
	
	public function detail($numid=NULL, $slug=NULL)
	{
		if(!isset($numid) || !isset($slug))
		{
			redirect(site_url('/'));
		}
		
		$post_detail = $this->ads_model->postdetail($numid, $slug);
		
		if(empty($post_detail))
		{
			redirect(site_url('/'));
		}
		
		$user = $this->ads_model->get_user($post_detail->user_id);
		
		$row_city = $this->ads_model->row_city($post_detail->location_id);
		
		$data['detail'] = $post_detail;
		$data['user'] = $user;
		$data['location_parent'] = $this->ads_model->get_city_parent($post_detail->location_id);
		$data['location_child'] = $row_city->url_name;
		
		$data['location'] = $this->ads_model->get_city_name($post_detail->location_id);
		$data['category'] = $this->ads_model->get_parent_child_cat($post_detail->category_id);
		
		$meta_config = array(
			'description' => "Amy's List provides local classifieds and forums for jobs, housing, for sale, personals, services, gigs, side jobs, and events.",
			'keywords' => "craigslist, local classifieds, jobs, housing, for sale, personals, local community, events",
			'author' => "Amy's List"
		);
		
		$this->stencil->meta($meta_config);
		
		if(!empty($post_detail->pictures))
		{
			$images = asset_url() . 'tn/image.php?width=600&amp;image=' . asset_url() . $post_detail->pictures[0]->image_path;
		}
		else
		{
			$images = asset_url() . 'img/amyface.png';
		}
		
		$data['open_graph'] = array(
			'url' => site_url('viewpost/' . $post_detail->post_num_id . '/' . $post_detail->url_name),
			'type' => 'website',
			'title' => '',
			'image' => $images,
			'description' => $meta_config['description'],
			'updated_time' => ($post_detail->updated_at != '0000-00-00 00:00:00') ? strtotime($post_detail->updated_at) : strtotime($post_detail->created_at)
		);
		$this->stencil->title($post_detail->title);
		$this->stencil->paint('ads/viewpost', $data);
		
	}
		
		
}