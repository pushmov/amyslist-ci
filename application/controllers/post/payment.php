<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('ads/payment_model', 'ads/ads_model'));
		
  }
	
	public function index($post_num=NULL)
	{
		// echo 'payment';exit();
		
		if(!isset($post_num))
		{
			redirect(site_url('post/ads'));
		}
		
		$post = $this->ads_model->get_post_by_ref($post_num);
		if(empty($post) || (int) $post->post_status == 1)
		{
			redirect(site_url('post/ads'));
		}
		
		$data['charge'] = $this->ads_model->check_charge($post->category_id, $post->location_id);
		$data['location'] = $this->ads_model->get_city_name($post->location_id);
		$data['category'] = $this->ads_model->get_parent_child_cat($post->category_id);
		$data['post'] = $post;
		
		
		$this->stencil->title('Payment');
		$this->stencil->paint('payment/payment', $data);
	}
	
	public function submit($post_num_id=NULL)
	{
		
		$post = $this->ads_model->get_post_by_ref($post_num_id);
		if(!isset($post_num_id) || empty($post))
		{
			$this->session->set_flashdata('noty', array('message' => 'Invalid token...', 'type' => 'error'));
			redirect(site_url('post/ads'));
		}
		
		if(!$this->input->post('charge'))
		{
			$this->session->set_flashdata('noty', array('message' => 'Invalid token...', 'type' => 'error'));
			redirect(site_url('post/ads'));
		}
		
		$post = $this->input->post('charge');
		
		//check promo code
		$is_valid = $this->payment_model->check_promo($post['promocode']);
		if(!$is_valid)
		{
			
			$this->load->config('paypal', TRUE);
			$paypal = $this->config->config['paypal'];
			
			$paypal['post_num_id'] = $post_num_id;
			//redirect paypal hidden form
			$this->load->view('pages/payment/paypal_form', $paypal);
			// exit();
		}
		else
		{
		
			$this->ads_model->activate_post_by_ref($post_num_id);
			$sendmail = $this->_send_email_notif($post_num_id);
			
			$this->session->set_flashdata('noty', array('message' => 'Thank you for posting with amyslist.us!', 'type' => 'success'));
			redirect(site_url('thank-you'));
			
		}
	}
	
	private function _send_email_notif($post_num_id)
	{
		
		$post = $this->ads_model->get_post_by_ref($post_num_id);
		$user_session = $this->session->userdata('user');
		//notification
		$from = 56;
		$notification_data = array(
			'user_id' => $user_session['user_id'],
			'from_id' => $from,
			'message' => '<p><a class="profile-link" href="'.site_url('user/profile/' . $from).'">Amy Admin</a> Thanks for posting an ad! Check your <a href="http://www.amyslist.us/ads/posted/">Ads Posted page</a> on to edit, review, or delete your <a href="http://www.amyslist.us/ads/posted/edit/'.$post->id.'">ad post</a></p>'
		);
		
		$this->ads_model->create_notification($notification_data);
		
		$to_email = $post->email;
		$to_name = $user_session['username'];
		$subject = "Amyslist: Publish/Edit/Delete your post";
		
		
		$body = '<div style="width:940px; height:632px; float:left; padding-left:40px;">';
		$body .= '    <div style="float:left; width:160px; height:632px; padding-top:30px;">';
		$body .= '        <img src="http://amyslist.us/images/lady.png"  width="168" height="508"/>';
		$body .= '    </div>';
		$body .= '    <div style="width:700px; float:left; height:632px;" class="topleft">';
		$body .= '        <h1 style="padding-left:150px;width:600px;">';
		$body .= '			<a class="logo" href="http://www.amyslist.us/"> <img src="http://www.amyslist.us/assets/img/logo.gif" /></a>';
		$body .= '        </h1>';
		$body .= '        <div style="font-family:Calibri; font-size:16px; padding-left:20px;">';
		$body .= "<span style='font-family:Calibri;font-size:18px; color:#084881;' ><br /><br />".$to_name.",</span><br /><br />";
		$body .= "<p style='font-family:Calibri;font-size:18px;' >Thank you for posting an ad on Amy's List. Please click the following link to publish your ad:<br /> ";
		$body .= "<span style='color:#e32b83; font-size:14px;'><a href='http://www.amyslist.us/ads/posted/edit/".$post->id."' >http://www.amyslist.us/ads/posted/edit/".$post->id."</a></span><br /><br /></p>";
		$body .= "<p style='font-family:Calibri; font-size:18px;'>You can publish, edit and delete your ad with the link above. You can also keep track with your
			ads at your profile under your <span style='color:#e32b83; font-size:16px;'><a href='http://www.amyslist.us/ads/posted/'>Ads Posted</a></span> page.<br /><br /></p> ";
		
		$body .= '            <p style="font-family:Calibri; font-size:18px;">';
		$body .= " Amy's List is the future of online classified ads. And we're glad you're part of it.</p> ";
		$body .= '			<p style="font-family:Calibri; font-size:18px;">Sincerely, <br />';
		$body .= '            <a href="http://amyslist.us/AmyAdmin 
		" style="color:#f52d8f;font-size:18px;text-decoration:none;">Amy Admin</span> </a>';
		$body .= '        </div> ';          
		$body .= '    </div>';
		$body .= '</div>';
		
		//===================================
		$from_email = "admin@amyslist.com";
		$from_name = "Amyslist";
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		$headers .= 'To: '.$to_name.' <'.$to_email.'>' . "\r\n";
		$headers .= 'From: Amyslist <admin@amyslist.com>' . "\r\n";

		// Mail it
		return mail($to_email, $subject, $body, $headers);
		
	}
	
	public function process()
	{
		
		if(!isset($_GET['CUSTID']))
		{
			$this->session->set_flashdata('noty', array('message' => 'Error paypal confirmation', 'type' => 'error'));
			redirect(site_url('post/ads'));
		}
		
		$post_num_id = $_GET['CUSTID'];
		$paypal_result = $_REQUEST['RESULT'];
		
		if($paypal_result != 0)
		{
			$this->session->set_flashdata('noty', array('message' => 'Error paypal confirmation', 'type' => 'error'));
			redirect(site_url('post/ads'));
		}
		
		$this->ads_model->activate_post_by_ref($post_num_id);
		$sendmail = $this->_send_email_notif($post_num_id);
		
		$this->session->set_flashdata('noty', array('message' => 'Thank you for posting with amyslist.us!', 'type' => 'success'));
		redirect(site_url('post/payment/confirm'));
		
	}
	
	public function confirm()
	{
		$this->stencil->title('Thank you');
		$this->stencil->paint('thank-you-payment');
	}
	
}