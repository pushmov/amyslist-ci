<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	public function __construct(){
		
		parent::__construct();
		
		$this->stencil->layout('default_layout');
		
		$nav = ($this->session->userdata('user')) ? 'nav_in' : 'nav_out';
		
		$this->stencil->slice(array('nav' => $nav, 'footer' => 'footer'));
		
	}
	
	public function _url_name($name)
	{
		$url_name = str_replace(' ', '-', strtolower($name));
		$url_name = str_replace('/', '-', $url_name);
		$url_name = str_replace('.', '', $url_name);
		$url_name = str_replace(',', '', $url_name);
		$url_name = str_replace('(', '', $url_name);
		$url_name = str_replace(')', '', $url_name);
		$url_name = str_replace('\'', '', $url_name);
		
		return $url_name;
	}
	
	public function _clean($string) {
		$string = str_replace(' ', '-', $string);
		$string = str_replace('\'', '', $string);
		

		return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
	}

}