<?php

class MY_Model extends CI_Model
{
	
	public $table = "";
	
	function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
	
	function update($data, $condition=NULL)
	{
		if(isset($condition))
		{
			$this->db->where($condition);
		}
		
		return $this->db->set($data)
			->update($this->table);
	}
	
}