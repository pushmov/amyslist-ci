<?php

Class City_model extends CI_Model
{
	
	function get_us_cities()
	{
		return $this->db->where(array('country' => 'US', 'is_big_cities' => 1))
			->get('location_cities')
			->result();
	}
	
	function is_cities($city)
	{
		
		//input : url city name from each tables : location_*
		$where = array('url_name' => $city);
		
		$cities = $this->db->where($where)
			->from('location_cities')
			->count_all_results();
		
		$continent = $this->db->where($where)
			->from('location_continent')
			->count_all_results();
		
		$countries = $this->db->where($where)
			->from('location_countries')
			->count_all_results();
		
		$us_states = $this->db->where($where)
			->from('location_us_states')
			->count_all_results();
			
		return ($cities >= 1) || ($continent >= 1) || ($countries >= 1) || ($us_states >= 1);
	}
	
	function entity_name($url)
	{
		
		$url = array( 'url_name' => $url );
		
		$cities = $this->db->where($url)
			->get('location_cities')
			->row();
		
		if(!empty($cities)) 
			return array(
				'table' => 'location_cities', 
				'id' => $cities->city_id, 
				'name' => $cities->city_name, 
				'url' => $cities->url_name,
				'is_big_cities' => TRUE
			);
		
		$continent = $this->db->where($url)
			->get('location_continent')
			->row();
		
		if(!empty($continent)) 
			return array(
				'table' => 'location_continent', 
				'id' => $continent->continent_id, 
				'name' => $continent->continent_name, 
				'url' => $continent->url_name,
				'is_big_cities' => FALSE
			);
		
		$countries = $this->db->where($url)
			->get('location_countries')
			->row();
			
		if(!empty($countries)) 
			return array(
				'table' => 'location_countries', 
				'id' => $countries->id, 
				'name' => $countries->printable_name, 
				'url' => $countries->url_name,
				'is_big_cities' => FALSE
			);
		
		$us_states = $this->db->where($url)
			->get('location_us_states')
			->row();
		
		if(!empty($us_states)) 
			return array(
				'table' => 'location_us_states', 
				'id' => $us_states->abbrev, 
				'name' => $us_states->name, 
				'url' => $us_states->url_name,
				'is_big_cities' => FALSE
			);
		
	}
	
	function get_row_childs($param)
	{
		$table = $param['table'];
		$id_parent = $param['id'];
		$is_big_cities = (isset($param['is_big_cities'])) ? $param['is_big_cities'] : FALSE;
		
		if($table == 'location_cities')
		{
			$return = $this->db->where('parent_city_id', $id_parent)
				->get('location_cities')
				->result();
			
			if(empty($return) && !$is_big_cities)
			{
				$parent = $this->db->where('city_id', $id_parent)
					->get('location_cities')
					->row();
				
				$return = $this->db->where('parent_city_id', $parent->parent_city_id)
					->get('location_cities')
					->result();
			}
			
			return $return;
		}
		elseif($table == 'location_us_states')
		{
			return $this->db->where(array('state' => $id_parent, 'country' => 'US'))
				->get('location_cities')
				->result();
		}
		elseif($table == 'location_countries')
		{
			return $this->db->where(array('country' => $id_parent, 'country !=' => 'US'))
				->get('location_cities')
				->result();
		}
		
		// return $this->db-
	}
	
	function get_city($id)
	{
		return $this->db->where('city_id', $id)
			->get('location_cities')
			->row();
	}
	
	function populate_categories()
	{
		$parent = $this->db->where('parent_id', 0)
			->get('categories')
			->result();
			
		if(!empty($parent))
		{
			foreach($parent as $row)
			{
				// $row->child = $this->_child_categories($row->id);
				
				$return[$row->url_category_name] = $this->_child_categories($row->id);
			}
		}
		
		return $return;
	}
	
	function _child_categories($id)
	{
		return $this->db->where('parent_id', $id)
			->get('categories')
			->result();
	}
	
	function get_cat_entity($url_name)
	{
		$return = array();
		$query = $this->db->where('url_category_name', $url_name)
			->get('categories')
			->row();
		
		$return['row_item'] = $query;
		$return['parent_item'] = FALSE;
		
		if((int) $query->parent_id > 0)
		{
			$return['parent_item'] = $this->db->where('id', $query->parent_id)
				->get('categories')
				->row();
		}
		
		return $return;
	}
	
	function city_row($url)
	{
		return $this->db->where('url_name', $url)
			->get('location_cities')
			->row();
	}
	
    
    function city_parent_row($parent_id)
    {  
        if(!empty($parent_id)){
           return $this->db->where('city_id' , $parent_id)
            ->get('location_cities')
            ->row();
        }
        return null;
    }
    
    
	function get_sibling_cat($id)
	{
		return $this->db->where('parent_id', $id)
			->get('categories')
			->result();
	}
	
	function populate_date()
	{
		$query = $this->db->query("SELECT created_at FROM post_ad WHERE post_status = 1 ORDER BY created_at DESC")
			->result();
		
		$return = array();
		
		foreach($query as $row)
		{
			$return[] = date('Y-m-d', strtotime($row->created_at));
		}
		return $return;
	}
	
	function get_post($city_id, $category_id)
    {  
		$return = array();
		
		$date = $this->populate_date();
		
		if(empty($date)) return $return;
        foreach($date as $row)
		{
			$result = $this->db->select('post_ad.*,post_ad.url_name as post_url_name,location_cities.*, user.username')
				->where(array('category_id' => $category_id, 'location_id' => $city_id, 'post_status' => 1))
				->like('post_ad.created_at', $row)
				->join('location_cities', "location_cities.city_id = post_ad.location_id")
				->join('user', 'user.id = post_ad.user_id')
				->get('post_ad')
				->result();
			
           
            
           // print_R($result);
            
            
			if(!empty($result))
			{   
				/*for($i=0;$i<count($result); $i++){
                   
                    $id = $result[$i]->id;
                    $query="select image_path,image_description from post_ad_images where post_ad_id=".$id." ORDER BY id ASC";
                    $query = $this->db->query($query);
                    if($query->num_rows() >=1){
                        $result[$i]->images = $query->result();
                    }
                 }
                
                $return[$row]= $result; */
                
				foreach($result as $i)
				{
					($i->images = $this->db->where('post_ad_id', $i->id)
						->order_by('id', 'ASC')
						->get('post_ad_images')
						->result());
				}
				
				$return[$row] = $result;
			}
			
		}
		
		return $return;
	}
    
    
    function get_post_images($post_id){
      $query = $this->db->query("SELECT image_path FROM post_ad_images WHERE post_ad_id = $post_id")
            ->result();
       
        $return = array();
        
        foreach($query as $row)
        {
            $return[] = $row->image_path;
        }
        return $return;
    }
    
    function get_post_images_description($post_id){
      $query = $this->db->query("SELECT image_description FROM post_ad_images WHERE post_ad_id = $post_id")
            ->result();
       
        $return = array();
        
        foreach($query as $row)
        {
            $return[] = $row->image_description;
        }
        return $return;
    }
    
    
	
	function sub_city_id($url, $abbrev)
	{
		$row = $this->db->where(array('url_name' => $url, 'state' => $abbrev))
			->get('location_cities')
			->row();
		
		return array('id' => $row->city_id, 'data' => $row);
	}
	
	function get_row_childs_by_state($state)
	{
		return $this->db->where('state', $state)
			->get('location_cities')
			->result();
	}
	
	function is_continent($c)
	{
		$query = $this->db->where('url_name', $c)
			->from('location_continent')
			->count_all_results();
		
		return ($query >= 1) ? TRUE : FALSE;
	}
	
	function continent_row($c)
	{
		$continent = $this->db->where('url_name', $c)
			->get('location_continent')
			->row();
			
		$continent->countries = $this->db->select('id as city_id, printable_name as city_name, url_name')
			->where('continent_id', $continent->continent_id)
			->get('location_countries')
			->result();
			
		return $continent;
	}
	
	function is_a_country($c)
	{
		$query = $this->db->where('url_name', $c)
			->from('location_countries')
			->count_all_results();
		
		return ($query >= 1) ? TRUE : FALSE;
	}
	
	function find_cub_cities($url)
	{
		$urldetail = $this->db->where('url_name', $url)
			->get('location_countries')
			->row();
		
		return $this->db->where('country', $urldetail->id)
			->from('location_cities')
			->count_all_results();
	}
	
	function detail_country($id)
	{
		return $this->db->where('url_name', $id)
			->get('location_countries')
			->row();
	}
	
	function detail_continent($id)
	{
		return $this->db->where('continent_id', $id)
			->get('location_continent')
			->row();
	}
	
	function is_parent_cat($cat_url)
	{
		$row = $this->db->where('url_category_name', $cat_url)
			->get('categories')
			->row();
		
		return $row;
	}
	
	function find_sub_cat($id)
	{
		return $this->db->where('parent_id', $id)
			->get('categories')
			->result();
	}
	
	function is_have_sub($url)
	{
		$row = $this->db->where('url_category_name', $url)
			->get('categories')
			->row();
		
		$test = $this->db->where('parent_id', $row->id)
			->get('categories')
			->row();
		
		return empty($test) ? FALSE : TRUE;
	}
	
	function fetch_city_dropdown($url)
	{
		
		$is_country = $this->db->where('url_name', $url)
			->get('location_countries')
			->row();
		
		if(sizeof($is_country) >= 1)
		{
			
			$number_continents = $is_country->continent_id;
			$groups = $this->db->select('id as id, printable_name as name, url_name as url')
				->where(array('continent_id' => $number_continents, 'enabled' => 1))
				->where('homepage_position IS NULL', NULL, FALSE)
				->group_by('name')
				->order_by('name', 'ASC')
				->get('location_countries')
				->result();
			
			return $groups;
		}
		else
		{
			
			//2 groups big city or no
			$is_big_city = $this->db->where('url_name', $url)
				->get('location_cities')
				->row();
			
			if($is_big_city->country == 'US')
			{
				
				$groups = $this->db->select('city_id as id, city_name as name, url_name as url')
					->where(array('country' => $is_big_city->country, 'state' => $is_big_city->state))
					->group_by('city_name')
					->order_by('city_name', 'ASC')
					->get('location_cities')
					->result();
				
				return $groups;
			}
			else
			{
				$abbr = $is_big_city->country;
				
				$groups = $this->db->select('city_id as id, city_name as name, url_name as url')
					->where('country', $is_big_city->country)
					->group_by('city_name')
					->order_by('city_name', 'ASC')
					->get('location_cities')
					->result();
				
				return $groups;
			}
			
		}
		
	}
	
}