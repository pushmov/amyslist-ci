<?php

Class Register_model extends MY_Model
{
	
	function is_email_exist($email)
	{
		return $this->db->where('email', $email)
			->from('user')
			->count_all_results();
			
	}
	
	function get_user($email)
	{
		return $this->db->where('email', $email)
			->get('user')
			->row();
	}
	
	function get_user_id($id)
	{
		return $this->db->where('id', $id)
			->get('user')
			->row();
	}
	
	function is_username_taken($username)
	{
		$query = $this->db->where('username', $username)
			->from('user')
			->count_all_results();
			
		return ($query > 0) ? TRUE : FALSE;
	}
	function is_email_taken($email)
	{
		$query = $this->db->where('email', $email)
			->from('user')
			->count_all_results();
		return ($query > 0) ? TRUE : FALSE;	
	}
	
}