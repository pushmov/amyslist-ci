<?php

Class Blog_model extends CI_Model
{
	
	function save($data)
	{
		$this->db->insert('blog', $data);
		return $this->db->insert_id();
	}
	
	function save_images($data)
	{
		$this->db->insert('blog_images', $data);
		return $this->db->insert_id();
	}
	
	function get_ref_id($id)
	{
		$query = $this->db->where('id', $id)
			->get('blog')
			->row();
		
		return $query->post_num_id;
	}
	
	function get_blog_item($ref_id)
	{
		return $this->db->where('post_num_id', $ref_id)
			->get('blog')
			->row();
	}
	
	function get_images($idpost)
	{
		return $this->db->where('post_blog_id', $idpost)
			->get('blog_images')
			->result();
	}
	
	function get_post($id)
	{
		$q = $this->db->where('id', $id)
			->get('blog')
			->row();
			
		if(!empty($q))
		{
			$q->images = $this->db->where('post_blog_id', $id)
				->get('blog_images')
				->result();
		}
		
		return $q;
	}
	
	function activate_blog($id_blog)
	{
		$this->db->where('id', $id_blog)
			->set('status', 1)
			->update('blog');
	}
	
	function my_ads($id)
	{
		return $this->db->where(array('user_id' => $id))
			->order_by('created', 'DESC')
			->get('blog')
			->result();
	}
	
}