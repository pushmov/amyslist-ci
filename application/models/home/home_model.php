<?php

Class Home_model extends CI_Model
{
	
	function get_us_cities()
	{
		return $this->db->where(array('country' => 'US', 'is_big_cities' => 1))
			->get('location_cities')
			->result();
	}
	
	function get_us_states()
	{
		return $this->db->get('location_us_states')
			->result();
	}
	
	function get_intl_cities()
	{
		return $this->db->where(array('is_big_cities' => 1, 'country != ' => 'US'))
			->get('location_cities')
			->result();
	}
	
}