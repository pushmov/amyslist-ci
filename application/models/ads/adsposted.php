<?php

Class Adsposted extends CI_Model
{
	
	function my_ads($user_id)
	{
		return $this->db->where(array('user_id' => $user_id, 'delete_status' => 0))
			->order_by('created_at', 'DESC')
			->get('post_ad')
			->result();
	}
	
	function set_delete($id_post)
	{
		$this->db->where('id', $id_post)
			->set('delete_status', 1)
			->update('post_ad');
	}
	
	function set_delete_all($ids)
	{
		$delete_data = array();
		
		if(!empty($ids))
		{
			foreach($ids as $id)
			{
				
				$this->db->where('id', $id);
				$this->db->set('delete_status', 1);
				$this->db->update('post_ad');
			}
		}
		
		return TRUE;
	}
	
	function set_undo($id_post)
	{
		$this->db->where('id', $id_post)
			->set('delete_status', 0)
			->update('post_ad');
	}
	
	function get_category_name($id)
	{
		$query = $this->db->where('id', $id)
			->get('categories')
			->row();
		
		return $query->category_name;
	}
	
	function get_location_name($id)
	{
		$query = $this->db->where('city_id', $id)
			->get('location_cities')
			->row();
			
		if(!empty($query)) return $query->city_name;
		
		$query = $this->db->where('id', $id)
			->get('location_countries')
			->row();
		
		return $query->printable_name;
	}
	
	function get_images($post_id)
	{
		return $this->db->where('post_ad_id', $post_id)
			->get('post_ad_images')
			->result();
	}
	
	function get_images_row($id)
	{
		return $this->db->where('id', $id)
			->get('post_ad_images')
			->row();
	}
	
	function remove_picture($picid)
	{
		$this->db->where('id', $picid)
			->delete('post_ad_images');
			
	}
	
	function save($data, $id)
	{
		$this->db->where('id', $id)
			->set($data)
			->update('post_ad');
	}
	
}