<?php

Class Ads_model extends CI_Model
{
	
	function get_locations(){
		
	}
	
	function get_us_cities()
	{
		$result = $this->db->where(array('is_big_cities' => 1, 'country' => 'US'))
			->get('location_cities')
			->result();
		
		if(!empty($result))
		{
			foreach($result as $row)
			{
				$row->child = $this->_append_child($row->city_id);
			}
		}
		
		return $result;
	}
	
	function _append_child($id)
	{
		return $this->db->where('parent_city_id', $id)
			->get('location_cities')
			->result();
	}
	
	function get_us_states()
	{
		$result = $this->db->order_by('name', 'ASC')
			->get('location_us_states')
			->result();
			
		if(!empty($result))
		{
			foreach($result as $row)
			{
				$row->child = $this->_append_state($row->abbrev);
			}
		}
		
		return $result;
	}
	
	function _append_state($state)
	{
		return $this->db->where('state', $state)
			->get('location_cities')
			->result();
	}
	
	function get_international_cities()
	{
		return $this->db->where("country != 'US' AND state IS NULL and is_big_cities = 1", NULL, FALSE)
			->get('location_cities')
			->result();
	}
	
	function world_countries()
	{
		
		
		$return = array();
		
		$countries = $this->db->select('id as city_id, printable_name as city_name')
			->where("enabled = 1 AND homepage_position IS NOT NULL", NULL, FALSE)
			->order_by('homepage_position', 'ASC')
			->get('location_countries')
			->result();
		
		foreach($countries as $c)
		{
			$c->sub_cities = $this->_sub_cities($c->city_id);
			$return[] = $c;
		}
		
		$continents = $this->db->select('continent_id as city_id, continent_name as city_name')
			->where("homepage_position IS NOT NULL", NULL, FALSE)
			->order_by('homepage_position', 'ASC')
			->get('location_continent')
			->result();
		
		foreach($continents as $con)
		{
			$con->sub_cities = $this->_sub_countries($con->city_id);
			$return[] = $con;
		}
		
		return $return;
	}
	
	function _sub_countries($cid)
	{
		$result = $this->db->select('id as city_id, printable_name as city_name')
			->where(array('continent_id' => $cid, 'enabled' => 1))
			->get('location_countries')
			->result();
			
		foreach($result as $row)
		{
			$row->child = $this->_sub_cities($row->city_id);
		}
		
		return $result;
	}
	
	function _sub_cities($id)
	{
		return $this->db->where('country', $id)
			->get('location_cities')
			->result();
	}
	
	function categories()
	{
		$return = $this->db->where('parent_id', 0)
			->get('categories')
			->result();
		
		foreach($return as $row)
		{
			$row->child = $this->_categories_child($row->id);
		}
		
		return $return;
	}
	
	function _categories_child_2($id)
	{
		$result = $this->db->where('parent_id', $id)
			->get('categories')
			->result();
		
		return $result;
	}
	
	function _categories_child($id)
	{
		$result = $this->db->where('parent_id', $id)
			->get('categories')
			->result();
		
		if(!empty($result))
		{
			foreach($result as $row)
			{
				$child = $this->_categories_child_2($row->id);
				if(!empty($child))
				{
					$row->child = $child;
				}
			}
		}
		
		return $result;
	}
	
	function save($data)
	{
		
		$this->db->insert('post_ad', $data);
		return $this->db->insert_id();
		
	}
	
	function save_images($data)
	{
		$this->db->insert('post_ad_images', $data);
		return $this->db->insert_id();
	}
	
	function get_ref_id($id)
	{
		$query = $this->db->where('id', $id)
			->get('post_ad')
			->row();
		
		return $query->post_num_id;
	}
	
	function get_post_by_ref($id)
	{
		return $this->db->where('post_num_id', $id)
			->get('post_ad')
			->row();
	}
	
	function get_images($id)
	{
		return $this->db->where('post_ad_id', $id)
			->get('post_ad_images')
			->result();
	}
	
	function get_city_name($id)
	{
		$query = $this->db->where('city_id', $id)
			->get('location_cities')
			->row();
			
		if(!empty($query))
		{
			return $query->city_name;
		}
		
		$query = $this->db->where('id', $id)
			->get('location_countries')
			->row();
		
		return $query->printable_name;
	}
	
	function get_parent_child_cat($id_cat)
	{
		$return = array();
		$query = $this->db->where('id', $id_cat)
			->get('categories')
			->row();
			
		$return['child'] = $query->category_name;
		$return['child-url-name'] = $query->url_category_name;
		
		$query = $this->db->where('id', $query->parent_id)
			->get('categories')
			->row();
		$return['parent'] = $query->category_name;
		$return['parent-url-name'] = $query->url_category_name;
		return $return;
	}
	
	function get_post($id)
	{
		return $this->db->where('id', $id)
			->get('post_ad')
			->row();
	}
	
	function check_charge($category_id, $location_id)
	{
		
		$row_parent = $this->db->where('city_id', $location_id)
			->get('location_cities')
			->row();
		
		$city_id = empty($row_parent) ? $location_id : $row_parent->parent_city_id;
		
		$row_cat_parent = $this->db->where('id', $category_id)
			->get('categories')
			->row();
		
		$cat_id = (empty($row_cat_parent)) ? $category_id : $row_cat_parent->parent_id;
		
		return $this->db->where(array('city_id' => $city_id, 'category_id' => $cat_id))
			->get('charge_fee')
			->row();
	}
	
	function create_notification($data)
	{
		$this->db->insert('user_notifications', $data);
		return $this->db->insert_id();
	}
	
	function activate_post($id_post)
	{
		$this->db->where('id', $id_post)
			->set('post_status', 1)
			->update('post_ad');
	}
	
	function activate_post_by_ref($id_ref)
	{
		$this->db->where('post_num_id', $id_ref)
			->set('post_status', 1)
			->update('post_ad');
	}
	
	function postdetail($numid, $slug)
	{
		$row = $this->db->where(array('post_num_id' => $numid, 'url_name' => $slug))
			->get('post_ad')
			->row();
		
		if(!empty($row))
		{
			$row->pictures = $this->db->where('post_ad_id', $row->id)
				->get('post_ad_images')
				->result();
		}
		
		return $row;
	}
	
	function get_user($uid)
	{
		return $this->db->where('id', $uid)
			->get('user')
			->row();
	}
	
	function get_city_parent($idchild)
	{
		$row_child = $this->db->where('city_id', $idchild)
			->get('location_cities')
			->row();
		
		if(empty($row_child))
		{
			return $row_child->city_name;
		}
		
		$parent = $this->db->where('city_id', $row_child->parent_city_id)
			->get('location_cities')
			->row();
		
		if(empty($parent))
		{
			$state = $this->db->where('abbrev', $row_child->state)
				->get('location_us_states')
				->row();
				
			return $state->url_name;
		}
		
		return $parent->url_name;
	}
	
	function row_city($idc)
	{
		return $this->db->where('city_id', $idc)
			->get('location_cities')
			->row();
	}
	
	function get_admin()
	{
		return $this->db->where('email', 'amyslistceo@gmail.com')
			->get('user')
			->row();
	}
	
}