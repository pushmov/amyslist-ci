<?php

Class Payment_model extends CI_Model
{
	
	function check_promo($code)
	{
		
		$return = FALSE;
		
		$query = $this->db->where('promo_code', $code)
			->get('charge_promo')
			->row();
		
		if(!empty($query))
		{
			$return = TRUE;
		}
		
		return $return;
	
	}

}