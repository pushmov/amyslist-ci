<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slug_model extends My_Model 
{
	function get($title, $entity, $entity_id)
	{
		$this->db->where('original_text', $title);
		$this->db->where('entity', $entity);
		$this->db->where('entity_id', $entity_id);
		$this->db->order_by('id','DESC');
		
		$result = $this->db->get('slug');
		if($result->result())
		{
			$result = $result->first_row();
			return $result->slug;
		}
		else
		{
			return FALSE;
		}
	}
	
	function check($slug, $entity)
	{
		$this->db->where('slug', $slug);
		$this->db->where('entity', $entity);
		
		$result = $this->db->get('slug');
		if($result->result())
		{
			return $result->first_row();
		}
		else
		{
			return FALSE;
		}
	}
	
	function set($slug, $title, $entity, $entity_id)
	{
		$this->db->set('slug', $slug);
		$this->db->set('original_text', $title);
		$this->db->set('entity', $entity);
		$this->db->set('entity_id', $entity_id);
		$this->db->set('cdate','NOW()',FALSE);
		$this->db->insert('slug');
	}
}
	
	