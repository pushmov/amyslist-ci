<?php

Class Notification_model extends MY_Model
{

	function get_notifications($userid)
	{
		return $this->db->where(array('user_id' => $userid, 'delete_status' => 0))
			->join('user', "user_notifications.from_id = user.id")
			->order_by('date_added', 'DESC')
			->get('user_notifications')
			->result();
	}
	
	function set_read($userid)
	{
		$this->db->where('user_id', $userid)
			->set('unread_status', 0)
			->update('user_notifications');
	}
	
	function auto_like($userid)
	{
		$users = $this->db->where('email', 'amyslistceo@gmail.com')
			->or_where('email', 'cmanhattan24@gmail.com')
			->get('user')
			->result();
		
		foreach($users as $u)
		{
			$message = '<p>
					<a class="profile-link" href="'.site_url('profile/' . $u->username).'">' . $u->username . '</a> liked your <a href="'.site_url('user/profile').'">profile</a>
				</p>';
			
			$data = array(
				'user_id' => $userid,
				'from_id' => $u->id,
				'message' => $message
			);
			
			$this->db->insert('user_notifications', $data);
			
			
			$like_data = array(
				'user_id' => $userid,
				'liked_by' => $u->id
			);
			$this->db->insert('user_like', $like_data);
		}
	}
	
}