<?php

Class Like_model extends CI_Model
{
	
	function user_detail($id)
	{
		return $this->db->where('id', $id)
			->get('user')
			->row();
	}
	
	function count_all_likes($userid)
	{
		return $this->db->where('user_id', $userid)
			->from('user_like')
			->count_all_results();
	}
	
	function my_likes($userid)
	{
		return $this->db->where('user_id', $userid)
			->join('user', "user_like.liked_by = user.id")
			->get('user_like')
			->result();
	}
	
	function likes($userid)
	{
		return $this->db->where('liked_by', $userid)
			->join('user', "user_like.user_id = user.id")
			->get('user_like')
			->result();
	}
	
	function user_name($username)
	{
		return $this->db->where('username', $username)
			->get('user')
			->row();
	}
	
}