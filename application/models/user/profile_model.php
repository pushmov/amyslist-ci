<?php

Class Profile_model extends MY_Model
{
	
	function get_user($id)
	{
		return $this->db->where('id', $id)
			->get('user')
			->row();
	}
	
	function get_user_ads($userid)
	{
		return $this->db->where(array('user_id' => $userid, 'delete_status' => 0))
			->order_by('created_at', 'DESC')
			->get('post_ad')
			->result();
	}
	
	function get_username($username)
	{
		return $this->db->where('username', $username)
			->get('user')
			->row();
	}
	
	function check_already_liked($public_id, $my_id)
	{
		$query = $this->db->where(array('user_id' => $public_id, 'liked_by' => $my_id))
			->from('user_like')
			->count_all_results();
			
		return (empty($query)) ? FALSE : TRUE;
	}
	
	function find_video($uid)
	{
		return $this->db->where('user_id', $uid)
			->get('user_video')
			->row();
	}
	
	function get_social($id)
	{
		$row = $this->db->where('user_id', $id)
			->get('user_social')
			->row();
		
		return $row;
	}
	
	function check_password($uid, $pass)
	{
		return $this->db->where(array('id' => $uid, 'plain' => $pass))
			->from('user')
			->count_all_results();
	}

}