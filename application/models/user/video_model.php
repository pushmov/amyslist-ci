<?php

Class Video_model extends MY_Model
{
	
	function find_video($uid)
	{
		return $this->db->where('user_id', $uid)
			->get('user_video')
			->row();
	}

}